// 画布相关变量
var bg = document.getElementById("bg");
var pw = document.getElementById("people");
var w = document.getElementById("wildanimal");
var final = document.getElementById("finallayer");
var bgcontext = bg.getContext("2d");
var pcontext = pw.getContext("2d");
var wildcontext = w.getContext("2d");
var f = final.getContext("2d");

//背景，人物，动物图片
var img = new Image();
var person = new Image();
var wild = new Image();


//背景预览图
var prImg = document.getElementById("bgscroll");
var intro_scientist = document.getElementById("scientist_intro");
var intro_animal = document.getElementById("animal_intro");
var result = new Image();

//动物的图片可以在下面push
var animalList = new Array();
animalList.push("w1.png");
animalList.push("w2.png");

//在这里面加入科学家照片
var personList = new Array();
personList.push("matting1 copy.png");
personList.push("matting2.png");
personList.push("matting3.png");
personList.push("matting4.png");
personList.push("matting5.png");
personList.push("matting6.png");
personList.push("matting7.png");
personList.push("matting8.png");
// 在这里面加入描述
var introlist = new Array();
introlist.push("description/d1.png");
introlist.push("description/d2.png");
introlist.push("description/d3.png");
introlist.push("description/d4.png");
introlist.push("description/d5.png");
introlist.push("description/d6.png");
introlist.push("description/d7.png");
introlist.push("description/d8.png");


// popup 窗口的两个按钮
var div = document.getElementById('background1');
var close = document.getElementById('close-button');


//获取滚动条距顶端的距离
function getPageScroll() {
  var yScroll;
  if (self.pageYOffset) {
    yScroll = self.pageYOffset;
    xScroll = self.pageXOffset;
  } else if (document.documentElement && document.documentElement.scrollTop) {
    yScroll = document.documentElement.scrollTop;
  } else if (document.body) {
    // all other Explorers
    yScroll = document.body.scrollTop;
  }

  return yScroll;
}


//图片合成
$("#confirm").click(function() {

  bgcontext.drawImage(img, 0, 0, 800, 480);
  pcontext.drawImage(person, 0, 0, 600, 480);
  pcontext.drawImage(wild, 550, 180, 300, 300);
  f.drawImage(img, 0, 0, 800, 480);
  f.drawImage(person, 0, 0, 600, 480);
  f.drawImage(wild, 550, 180, 300, 300);
  document.getElementById("result").src = final.toDataURL();
  div.style.display = "block";
});


//控制pop up窗口的
close.onclick = function close() {
  div.style.display = "none";
  img = new Image();
  person = new Image();
  wild = new Image();
  pcontext.clearRect(0, 0, 800, 480);
  f.clearRect(0, 0, 800, 480);
  bgcontext.clearRect(0, 0, 800, 480);
  wildcontext.clearRect(0, 0, 800, 480);
}
window.onclick = function close(e) {
  if (e.target == div) {
      div.style.display = "none";
      img = new Image();
      person = new Image();
      wild = new Image();
      intro_scientist.src=null;
      pcontext.clearRect(0, 0, 800, 480);
      f.clearRect(0, 0, 800, 480);
      bgcontext.clearRect(0, 0, 800, 480);
      wildcontext.clearRect(0, 0, 800, 480);
  }
}


//根据照片们的高度来设置它，比如要设置成600，就把其中所有的480改成600
//注意，所有html文件里面的 bgimage id下的图片，它们的id要是连续的数字
document.addEventListener("scroll", function name(params) {
  let i = getPageScroll();
  if (i < 480) {
    bg.height = bg.height;
  } else {
    let j = Math.floor((i - 240-50) / 481) + 1;
    img.src = document.getElementById(j).src;
    img.onload = function() {
      bgcontext.drawImage(img, 0, 0, 800, 480);
    };
  }
});



//清理画布的
$("#bgclear").click(function() {
  img = new Image();
  person = new Image();
  wild = new Image();
  pcontext.clearRect(0, 0, 800, 480);
  f.clearRect(0, 0, 800, 480);
  bgcontext.clearRect(0, 0, 800, 480);
  wildcontext.clearRect(0, 0, 800, 480);

});


//有n个动物，就让1.99变成n-0.01
$("#animal").click(function() {
  let index = Math.floor(1.99 * Math.random());
  w.height = w.height;
  wild.src = animalList[index];
  wild.onload = function() {
    wildcontext.drawImage(wild, 550, 180, 300, 300);
  };
});

$("#scientists").click(function() {
  let index = Math.floor(7.99 * Math.random());
  pw.height = pw.height;
  person.src = personList[index];
    person.onload = function() {
      pcontext.drawImage(person, 0, 0, 600, 480);
    }

    intro_scientist.src = introlist[index];
});



function iterateRecords(data) {

	console.log(data);

	$.each(data.result.records, function(recordKey, recordValue) {
     
    /*for (var i=6; i<=10; i++) {
		if (recordValue["_id"] == i) {
    var recordImage = recordValue["1000_pixel_jpg"];
    
        if(recordImage) {

          var processed = '<img width="1100px, height="590px" src="' + recordImage +'"/>' ;

          $(".bgimages").append(processed);

        }
    }
    }*/

    if (recordValue["_id"] == 6) {
      var recordImage = recordValue["1000_pixel_jpg"];
      
          if(recordImage) {
  
            var processed = '<img width="800px" src="' + recordImage +'" id="0"/>' ;
  
            $(".bgimages").append(processed);
  
          }
      }
    
    if (recordValue["_id"] == 7) {
      var recordImage = recordValue["1000_pixel_jpg"];
      
          if(recordImage) {
  
            var processed = '<img width="800px" src="' + recordImage +'" id="1"/>' ;
  
            $(".bgimages").append(processed);
  
          }
      }

      if (recordValue["_id"] == 8) {
        var recordImage = recordValue["1000_pixel_jpg"];
        
            if(recordImage) {
    
              var processed = '<img width="800px" src="' + recordImage +'" id="2"/>' ;
    
              $(".bgimages").append(processed);
    
            }
        }

        if (recordValue["_id"] == 48) {
          var recordImage = recordValue["1000_pixel_jpg"];
          
              if(recordImage) {
      
                var processed = '<img width="800px" src="' + recordImage +'" id="3"/>' ;
      
                $(".bgimages").append(processed);
      
              }
          }
    

	});

}







$(document).ready(function() {

	var data = {
		resource_id: "8a327cf9-cff9-4b34-a461-6f883bdc3a48",
		limit: 50,
	}

	$.ajax({
		url: "https://www.data.qld.gov.au/api/3/action/datastore_search",
		data: data,
		dataType: "jsonp", // We use "jsonp" to ensure AJAX works correctly locally (otherwise XSS).
		cache: true,
		success: function(data) {
			iterateRecords(data);
		}
	});

});


/* affect button content, but only one button which is the top button is effective
I don't know why? */
var button = document.querySelector('button')
button.onclick = function() {
  button.textContent = "OK?"
}


  
  