#!/usr/bin/env python3
"""
Assignment 2 - Sleeping Coders
CSSE1001/7030
Semester 2, 2019
"""

import random

__author__ = "Zhutianyu Yu & 45206353"

# Write your classes here

class Deck (object):
    ''' A collection of ordered cards'''

    def __init__ (self, starting_cards=None):
        ''' Constructs the attribute of card list. By default, it constructs an empty list

            Parameters:
                starting_cards (list): A list of cards. If the parameter is None, it constructs an empty list
        '''

        if starting_cards == None:
            self._card_list = []
        else:
            self._card_list = starting_cards


    def get_cards (self):
        ''' Returns the attribute of entire card list

            Returns:
                self._card_list (list): The entrie card list
        '''

        return self._card_list


    def get_card (self, slot):
        ''' Returns one card instance at the specified slot in a deck

            Parameters:
                slot (int): The index of card list

            Returns:
                self._card_list[slot] (Card): A card instance
        '''

        return self._card_list[slot]


    def top (self):
        ''' Returns the card on the top of the deck

            Returns:
                self._card_list[top_index] (Card): The card on the top of the deck
        '''

        top_index = len(self._card_list) - 1
        return self._card_list[top_index]


    def remove_card (self, slot):
        ''' Removes a card instance at the specified slot in a deck

            Parameters:
                slot (int): The index of card list
        '''

        self._card_list.pop(slot)


    def get_amount (self):
        ''' Returns the amount of cards in a deck

            Returns:
                len(self._card_list) (int): The number of cards in a deck
        '''

        return len(self._card_list)


    def shuffle (self):
        ''' Shuffles the order of the cards in the deck '''

        random.shuffle(self._card_list)


    def pick (self, amount:int=1):
        ''' Takes the amount of cards off the deck and return them

            Parameters:
                amount (int): The amount of card you want to take. By default, take one card list off the deck

            Returns:
                return_list (list): A list of cards being taken off the deck
        '''

        count = 1
        return_list = []

        while count <= amount:
            picked_card = self._card_list.pop()
            return_list.append(picked_card)
            count +=1

        return return_list


    def add_card (self, card):
        ''' Places a single card instance on top of the deck

            Parameters:
                card (Card): A single card instance
        '''

        self._card_list.append(card)


    def add_cards (self, cards:list):
        ''' Places a list of cards on top of the deck

            Parameters:
                cards (list): A list of cards
        '''

        self._card_list.extend(cards)



    def copy (self, other_deck):
        ''' Copies all card list from the other_deck parameter into current deck,
            extending the list of cards of the current deck

            Parameters:
                other_deck (Deck): Another deck instance
        '''

        self._card_list.extend(other_deck._card_list)


    def __str__ (self):
        ''' Returns the string representation of the deck, like 'Deck(cardA, cardB, cardC)' '''

        return_str = 'Deck({0})'.format(self._card_list)
        return_str = return_str.replace('[','') 
        return_str = return_str.replace(']','')  # removes the opening and closing square brackets from strings

        return return_str


    def __repr__ (self):
        ''' Same as __str__ (self) method '''

        return self.__str__()





class Player (object):
    ''' A player represents one of the player in the game '''


    def __init__ (self, player_name):
        ''' Constructs a player's name, hand deck and coder deck

            Parameters:
                player_name (str): A player's name
        '''

        self._player_name = player_name
        self._hand_deck = Deck()
        self._coders_deck = Deck()


    def get_name (self):
        ''' Returns the name of the player

            Returns:
                self._player_name (str): The name of the player
        '''

        return self._player_name


    def get_hand (self):
        ''' Returns the players deck of cards

            Returns:
                self._hand_deck (Deck): The player's deck of cards
        '''

        return self._hand_deck


    def get_coders (self):
        ''' Returns the player's deck of collected coder cards

            Returns:
                self._coders_deck (Deck): The player's deck of collected coder cards
        '''

        return self._coders_deck


    def has_won (self):
        ''' Returns True only if the player has 4 or more cards

            Returns:
                True (bool)
                or
                False (bool)
        '''

        if self._coders_deck.get_amount() >= 4:
            return True
        else:
            return False


    def __str__ (self):
        ''' Returns the string representation of the player '''

        return 'Player({0}, {1}, {2})'.format(self._player_name, self._hand_deck, self._coders_deck)


    def __repr__ (self):
        ''' Same as __str__ (self) method '''

        return self.__str__()





class Card (object):
    ''' A card class represents a card in the game, which is an abstract class '''


    def __init__ (self):
        ''' The Card class doesn't need to be initialized'''

        pass


    def play (self, player, game):
        ''' Removes the card from the player's hand
            Picks a new card list from the pickup pile
            Adds this list to player's hand
            Sets the action to 'NO_ACTION'

            Parameters:
                player (Player): A Player instance
                game (CoderGame): A game of Sleeping Coders found in the a2_support.py
        '''

        player.get_hand().get_cards().remove(self)
        picked_card_list = game.pick_card(blocked_classes=(CoderCard,))
        player.get_hand().add_cards(cards=picked_card_list)

        game.set_action(action='NO_ACTION')


    def action (self, player, game, slot:int):
        ''' Performs the action according to different types of card
            The Card class doesn't need to perform any action

            Parameters:
                player (Player): A Player instance
                game (CoderGame): A game of Sleeping Coders found in the a2_support.py
                slot (int): The index used to specify the position
        '''

        pass


    def __str__ (self):
        ''' Returns the string representation of the Card '''

        return 'Card()'


    def __repr__ (self):
        ''' Same as __str__ (self) method '''

        return self.__str__()





class NumberCard (Card):
    ''' A card whose aim is to be disposed by the player '''

    def __init__ (self, number):
        ''' Constructs the number value

            Parameters:
                number (int): The number value
        '''

        self._number = number


    def get_number (self):
        ''' Returns the number value of the card

            Returns:
                self._number (int): The number value
        '''

        return self._number


    def play (self, player, game):
        ''' Same as the play method in the Card class,
            but it should move into the next player after that

            Parameters:
                same as the play method found in the Card class
        '''

        super().play(player, game)
        game.next_player()


    def __str__ (self):
        ''' Returns the string representation of the number card '''

        return 'NumberCard({0})'.format(self._number)


    def __repr__ (self):
        ''' Same as __str__(self) method'''

        return self.__str__()





class CoderCard (Card):
    ''' A card which stores the name of a corder card'''

    def __init__ (self, name):
        ''' Constructs the name of a coder

            Parameters:
                name (str): The name of a coder
        '''

        self._name = name


    def get_name (self):
        ''' Returns the name of a coder

            Returns:
                self._name (str): The name of a coder
        '''

        return self._name


    def play (self, player, game):
        ''' Just set the action to 'NO_ACTION' '''

        game.set_action(action='NO_ACTION')


    def __str__ (self):
        ''' Returns the string representation of the coder card '''

        return 'CoderCard({0})'.format(self._name)


    def __repr__ (self):
        ''' Same as the __str__(self) method '''

        return self.__str__()





class TutorCard (Card):
    ''' A card which stores the name of a tutor card '''

    def __init__ (self, name):
        ''' Constructs the name of a tutor card

            Parameters:
                name (str): The name of a tutor card
        '''

        self._name = name


    def get_name (self):
        ''' Returns the name of a tutor card

            Returns:
                self._name (str): The name of a tutor card
        '''

        return self._name


    def play (self, player, game):
        ''' Similar to the play method in the Card class,
            but sets the action to 'PICKUP_CODER' after that

            Parameter:
                same as the play method found in the Card class
        '''
        
        player.get_hand().get_cards().remove(self)
        picked_card_list = game.pick_card(blocked_classes=(CoderCard,))
        player.get_hand().add_cards(cards=picked_card_list)

        game.set_action(action='PICKUP_CODER')


    def action (self, player, game, slot:int):
        ''' Picks the coder card from the sleeping coder list in a given slot
            Sets the None value to this position refered by the slot
            Adds the coder card to the player's coder deck
            Sets the action to 'NO_ACTION'
            Turns into the next player

            Parameters:
                same as the action method found in the Card class
        '''
        
        picked_coder_card = game.get_sleeping_coder(slot=slot)
        game.set_sleeping_coder(slot=slot, card=None)
        player.get_coders().add_card(card=picked_coder_card)

        game.set_action(action='NO_ACTION')
        game.next_player()


    def __str__ (self):
        ''' Returns the string representation of tutor card '''

        return 'TutorCard({0})'.format(self._name)


    def __repr__ (self):
        ''' Same as __str__() method'''

        return self.__str__()





class KeyboardKidnapperCard (Card):
    ''' A card which allows the player to steal a coder card from another player '''

    def play (self, player, game):
        ''' Similar to the play method in the card class,
            but sets the action to the 'STEAL_CODER' after that

            Parameters:
                same as the play method found in the Card class
        '''
        
        player.get_hand().get_cards().remove(self)
        picked_card_list = game.pick_card(blocked_classes=(CoderCard,))
        player.get_hand().add_cards(cards=picked_card_list)

        game.set_action(action='STEAL_CODER')


    def action (self, player, game, slot:int):
        ''' Picks the coder card from another player's coder card deck and stores it into variable picked_coder_card
            Removes the coder card from that coder deck
            Returns to the current player
            Adds the coder card into the current player's coder card deck using variable picked_coder_card
            Sets the action to 'NO_ACTION'
            Turns into the next player

            Parameters:
                same as the action method found in the Card class
        '''
        
        picked_coder_card = player.get_coders().get_card(slot=slot)
        player.get_coders().remove_card(slot=slot)

        player = game.current_player()
        player.get_coders().add_card(card=picked_coder_card)


        game.set_action(action='NO_ACTION')
        game.next_player()


    def __str__ (self):
        ''' Returns the string representation of the KeyboardKidnapper card'''

        return 'KeyboardKidnapperCard()'


    def __repr__ (self):
        ''' Same as the __str__(self) method '''

        return self.__str__()





class AllNighterCard (Card):
    ''' A card which allows the player to put a coder card from another player back to sleep'''

    def play(self, player, game):
        ''' Similar to the play method found in the Card class,
            but sets the action to the 'SLEEP_CODER' after that

            Parameters:
                same as the play method found in the Card class
        '''

        player.get_hand().get_cards().remove(self)
        picked_card_list = game.pick_card(blocked_classes=(CoderCard,))
        player.get_hand().add_cards(cards=picked_card_list)

        game.set_action(action='SLEEP_CODER')


    def action (self, player, game, slot:int):
        ''' Picks the coder card from another player's coder card deck, and stores it into a variable picked_coder_card
            Removes the coder card from that coder deck
            Get the sleeping coder list using the get_sleeping_coders method, and stores it into a variable sleeping_coder_list.
            Looks for the sleeping coder list, if a value is None, replace the None value with picked_coder_card.
            Break the for loop once the replacement has been done. Otherwise, it will replace all None values with picked_coder_card.
            Set the action to 'NO_ACTION'
            Turns the player into the next player

            Parameters:
                same as the action method found in the Card class
        '''

        plicked_coder_card = player.get_coders().get_card(slot=slot)
        player.get_coders().remove_card(slot=slot)

        sleeping_coder_list = game.get_sleeping_coders()

        for index, sleeping_coder in enumerate(sleeping_coder_list): # The for loop " for sleeping_coder in sleeping_coder_list" doesn't work because of the assignment
            if sleeping_coder == None:
                sleeping_coder_list[index] = plicked_coder_card # The assignment "sleeping_coder = plicked_coder_card" doesn't work
                break                                           # So, I use "sleeping_coder_list[index] = plicked_coder_card" instead

        game.set_action(action='NO_ACTION')
        game.next_player()


    def __str__ (self):
        ''' Returns the string representation of the AllNighterCard card '''

        return 'AllNighterCard()'


    def __repr__ (self):
        ''' Same as the __str__(self) method '''

        return self.__str__()


    


def main():
    print("Please run gui.py instead")


if __name__ == "__main__":
    main()
