$(document).ready(function() {
    // handle menu bar animation
    $('.nav-menu').click(function () {
        let nav_menu = $('.nav-menu');

        if (!nav_menu.hasClass('change')) {
            nav_menu.addClass('change');
            add_show_panel_class();
            black_menubar();
        }
        else {
            nav_menu.removeClass('change');
            remove_show_panel_class();
            white_menubar();
        }
    });
});

function add_show_panel_class() {
    // display block
    $('.overlay').addClass('show-panel');
    // after 400ms, increase bg width and nav
    setTimeout(add_bg_width, 300);
    setTimeout(add_nav, 700);
}

function remove_show_panel_class() {
    // remove bg wdith and nav
    remove_nav();
    setTimeout(remove_bg_width, 300);
    // after 400ms, display none
    setTimeout(remove_overlay_panel, 700);
}

function add_bg_width() {
    let bg = $('.bg');
    bg.addClass('show-panel');
}

function remove_bg_width() {
    let bg = $('.bg');
    bg.removeClass('show-panel');
}

function remove_overlay_panel() {
    $('.overlay').removeClass('show-panel');
}

function add_nav() {
    $('.overlay-content').addClass('show-panel');
}

function remove_nav() {
    $('.overlay-content').removeClass('show-panel');
}

function black_menubar() {
    $('.bar1, .bar2, .bar3').addClass('black');
    $('.nav-menu p').addClass('black');
}

function white_menubar() {
    $('.bar1, .bar2, .bar3').removeClass('black');
    $('.nav-menu p').removeClass('black');
}

