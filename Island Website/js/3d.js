import * as THREE from './THREE/three.module.js';
import { OrbitControls } from './THREE/OrbitControls.js';
import { GLTFLoader } from './THREE/GLTFLoader.js';
import { DRACOLoader } from './THREE/DRACOLoader.js';

// target the container
const container = document.getElementById( 'container' );

// renderer
const renderer = new THREE.WebGLRenderer( { antialias: true } );
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.outputEncoding = THREE.sRGBEncoding;
container.appendChild( renderer.domElement );

// scene
const scene = new THREE.Scene();
scene.background = new THREE.Color( 0x84e7f7 );
scene.fog = new THREE.FogExp2(0xffffff, 0.1);

// camera
const camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.set( 1, 2, -4 );

// controls (change the handleMouseMoveRotate function in the OrbitControl library to the method )
// and dispose the default behaviour
const controls = new OrbitControls( camera, renderer.domElement );
controls.target.set( 0, 0, 0 );
controls.enablePan = false;
controls.enableDamping = true;
controls.enableZoom = false;
controls.maxPolarAngle = 1.2;
controls.minPolarAngle = 1.2;
controls.dispose();
controls.update();
controls.rotateSpeed = 0.1;

// lights
scene.add( new THREE.HemisphereLight( 0x84e7f7, 0x000000, 0.4 ) );

const dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
dirLight.position.set( 5, 2, 8 );
scene.add( dirLight );

// envmap
const path = './images/Park2/';
const format = '.jpg';
const envMap = new THREE.CubeTextureLoader().load( [
    path + 'posx' + format, path + 'negx' + format,
    path + 'posy' + format, path + 'negy' + format,
    path + 'posz' + format, path + 'negz' + format
] );

// dracoLoader corporates with the loader to enhance the performance
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( './js/gltf/' );

// loader (load the model)
const loader = new GLTFLoader();
loader.setDRACOLoader( dracoLoader );
loader.load( './models/island.glb', function ( gltf ) {

    const model = gltf.scene;
    model.position.set( 0, 0, 0 );
    model.scale.set( 0.1, 0.1, 0.1 );
    model.traverse( function ( child ) {

        if ( child.isMesh ) child.material.envMap = envMap;

    } );

    scene.add( model );

    animate();

}, undefined, function ( e ) {

    console.error( e );

} );

// final animate
function animate() {

    requestAnimationFrame( animate );

    controls.update();

    renderer.render( scene, camera );

}

// event handler and callbacks to deal with resize window and mouse hover
document.getElementById('container').addEventListener('mousemove', onDocumentMouseMove, false);
window.addEventListener('resize', onResizeWindow);

function onDocumentMouseMove(event) {
    controls.handleMouseMoveRotate(event);
}

function onResizeWindow() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
}
