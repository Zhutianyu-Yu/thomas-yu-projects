$(document).ready(function() {
    // run rellax
    var rellax = new Rellax('.rellax');

    $(window).scroll(function() {
        parallax_gallery_bg();
    });
});

/*
 * calculate the relative position between the window bottom and a specified element
 * E.g. Set element_string = '.photo-gallery1 div', offset = 0
 *      - If the window bottom is above the upper border of the photo-gallery1 div,
 *      the function returns 0
 *      - If the window bottom is between the upper border and bottom border,
 *      the function returns a number between 0 and 100
 *      - If the window bottom is below the bottom border,
 *      the function returns 100
 * @param String element_string: the target with which the window want to compare
 *        Int    offset (px) : the offset which allows the comparison to be later or earlier. Default 0
 * @return Int : a number between 0 and 100
 */
function relative_pos_to_target(element_string, offset = 0) {
    let target = $(element_string);
    let target_height = target.height();
    let target_offset_top = target.offset().top;
    let viewport_height = window.innerHeight;
    let scroll_pos = $(window).scrollTop() + viewport_height;
    let offset_scroll_pos;

    if (scroll_pos < target_offset_top + offset) {
        offset_scroll_pos = 0;
    }
    else if (scroll_pos < target_offset_top + target_height + offset) {
        offset_scroll_pos = scroll_pos - (target_offset_top + offset);
    }
    else {
        offset_scroll_pos = target_height;
    }

    return Math.round((offset_scroll_pos / target_height) * 100);

}

function parallax_gallery_bg() {
    let div1 = $('.photo-gallery1 div');
    let div2 = $('.photo-gallery2 div');
    let div3 = $('.photo-gallery3 div');
    let div4 = $('.photo-gallery4 div');
    div1.css('background-position', 'center ' +
        relative_pos_to_target('.photo-gallery1 div', 300).toString() +
        '%');
    div2.css('background-position', '70% ' +
        relative_pos_to_target('.photo-gallery2 div', 300).toString() +
        '%');
    div3.css('background-position', 'center ' +
        relative_pos_to_target('.photo-gallery3 div', 300).toString() +
        '%');
    div4.css('background-position', 'center ' +
        relative_pos_to_target('.photo-gallery4 div', 300).toString() +
        '%');
}