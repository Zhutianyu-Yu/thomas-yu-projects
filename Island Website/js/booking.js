$(document).ready(function() {
    // the next button event
    $('.fieldset-section .button').click(change_section);

    // for formatting the card number input
    let card_number_input = $('#ccn');
    // on === addEventListener in vanilla JavaScript
    card_number_input.on('input', on_change_card_number);

    // for displaying the card number on the credit card
    card_number_input.keyup(function() {
        let value = get_input_value('#ccn');
        set_card_info('.card_number', value);
    });

    // for displaying the holder info on the card
    let card_holder_input = $('#holder');
    card_holder_input.keyup(function() {
        let value = get_input_value('#holder');
        set_card_info('#holder_name', value);
    });

    let expiration_month_selector = $('#expiration_month');
    expiration_month_selector.change(function() {
        let value = get_selection_value('#expiration_month');
        set_card_info('#expiration span:first-of-type', value);
    });

    let expiration_year_selector = $('#expiration_year');
    expiration_year_selector.change(function() {
        let value = get_selection_value('#expiration_year');
        set_card_info('#expiration span:last-of-type', value);
    });

    let cvv_input = $('#cvv');
    cvv_input.on('input', on_change_cvv);
    cvv_input.keyup(function() {
        let value = get_input_value('#cvv');
        set_card_info('.card__secret--last', value);
    });

    /******* change background images for staying and activity options *******/
    $('input:radio[name="staying option"]').change(function() {
        let staying_bg = $('.figure-img');
        if ($(this).is(':checked') && $(this).val() === 'homestay') {
            staying_bg.removeClass().addClass('figure-img homestay');
        }
        else if ($(this).is(':checked') && $(this).val() === 'hotel') {
            staying_bg.removeClass().addClass('figure-img hotel');
        }
    });

    $('input:radio[name="activity"]').change(function() {
        let staying_bg = $('.figure-img');
        if ($(this).is(':checked') && $(this).val() === 'bonfire') {
            staying_bg.removeClass().addClass('figure-img bonfire');
        }
        else if ($(this).is(':checked') && $(this).val() === 'forest wandering') {
            staying_bg.removeClass().addClass('figure-img forest-wandering');
        }
        else if ($(this).is(':checked') && $(this).val() === 'mountain climbing') {
            staying_bg.removeClass().addClass('figure-img mountain');
        }
        else if ($(this).is(':checked') && $(this).val() === 'fishing') {
            staying_bg.removeClass().addClass('figure-img fishing');
        }
    });

    /******* input animations ******/
    let input_field = $('.input-field');
    // focus animations
    input_field.on('focus', function() {
        let current_field = $(this);
        current_field.parent().addClass('input-wrapper-selected');
        current_field.attr('placeholder', '');
    });

    input_field.on('focusout', function() {
        let current_field = $(this);
        current_field.parent().removeClass('input-wrapper-selected');

        if (current_field.attr('name') === 'username' && current_field.html() === '') {
            current_field.attr('placeholder', 'Username');
        }
        else if (current_field.attr('name') === 'email' && current_field.html() === '') {
            current_field.attr('placeholder', 'Email');
        }
        else if (current_field.attr('name') === 'card number' && current_field.html() === '') {
            current_field.attr('placeholder', 'Card Number');
        }
        else if (current_field.attr('name') === 'card holder' && current_field.html() === '') {
            current_field.attr('placeholder', 'card holder');
        }
        else if (current_field.attr('name') === 'CVV' && current_field.html() === '') {
            current_field.attr('placeholder', 'CVV');
        }
    });

});

// update the form section
function change_section() {
    let button = $(this);
    let current_section = button.parents('.fieldset-section');
    let current_section_idx = current_section.index();
    let header_section = $('.steps li').eq(current_section_idx);
    current_section.removeClass('is-active').next().addClass('is-active');
    header_section.removeClass('is-active').next().addClass('is-active');

}

// get the value from an input field
function get_input_value(element_string) {
    let element = $(element_string);
    return element.val();
}

// get the value from a selection
function get_selection_value(element_string) {
    let element = $(element_string);
    return element.find(':selected').val();
}

// validate the card number input
function on_change_card_number() {
    let card_number = $('#ccn').val();
    // use a regular expression to prevent user from entering invalid letters
    let formatted_card_number = card_number.replace(/[^\d]/g, "");
    formatted_card_number = formatted_card_number.substring(0, 16);

    // Split the card number is groups of 4
    let card_number_sessions = formatted_card_number.match(/\d{1,4}/g);
    if (card_number_sessions !== null) {
        formatted_card_number = card_number_sessions.join(' ');
    }

    // update the input field
    if (card_number !== formatted_card_number) {
        $('#ccn').val(formatted_card_number);
    }
}

// validate the cvv
function on_change_cvv() {
    let cvv = $('#cvv').val();
    let formatted_cvv = cvv;
    if (cvv.length > 3) {
        formatted_cvv = cvv.slice(0, 3);
    }
    if (cvv !== formatted_cvv) {
        $('#cvv').val(formatted_cvv);
    }
}

// set information on the card
function set_card_info(element_string, value) {
    let element = $(element_string);
    element.html(value);

    // different default settings on the card
    if (element_string === '.card_number' && value.length === 0) {
        element.html('**** **** **** ****');
    }
    else if (element_string === '#holder_name' && value.length === 0) {
        element.html('Holder Name');
    }
    else if (element_string === '.card__secret--last' && value.length === 0) {
        element.html('000');
    }
}