$(document).ready(function() {
    // change the content of the thumbnail every 5000 milliseconds (5 seconds)
    setInterval(function () {
        change_thumbnails('#residency1',
            ['.customers1 p:first-of-type',
                     '.customers1 p:nth-of-type(2)',
                     '.customers1 p:last-of-type']);
    }, 5000);

    setInterval(function () {
        change_thumbnails('#residency2',
            ['.customers2 p:first-of-type',
                '.customers2 p:nth-of-type(2)',
                '.customers2 p:last-of-type']);
    }, 5000);

    $(window).scroll(function() {
        parallax_footer_bg();
        change_menubar_colors();
    });
});

function relative_pos_to_target(element_string, offset = 0) {
    let target = $(element_string);
    let target_height = target.height();
    let target_offset_top = target.offset().top;
    let viewport_height = window.innerHeight;
    let scroll_pos = $(window).scrollTop() + viewport_height;
    let offset_scroll_pos;

    if (scroll_pos < target_offset_top + offset) {
        offset_scroll_pos = 0;
    }
    else if (scroll_pos < target_offset_top + target_height + offset) {
        offset_scroll_pos = scroll_pos - (target_offset_top + offset);
    }
    else {
        offset_scroll_pos = target_height;
    }

    return Math.round((offset_scroll_pos / target_height) * 100);

}

function parallax_footer_bg() {
    let relative_position = relative_pos_to_target('footer', 100);
    $('footer').css('background-position', 'center ' + relative_position.toString() + '%');
}

/*
 * change the thumbnail bg and contents
 * @param: String bg_string (the id of thumbnail div:first-of-type)
 *         Array p_array (an array containing a series of p descriptions)
 */
function change_thumbnails(bg_string, p_array) {
    let residency = $(bg_string);
    let p1 = $(p_array[0]);
    let p2 = $(p_array[1]);
    let p3 = $(p_array[2]);

    if (residency.hasClass('bg-residency1')) {
        residency.removeClass().addClass('bg-residency2');
        p1.removeClass();
        p2.addClass('show-content');
    }
    else if (residency.hasClass('bg-residency2')) {
        residency.removeClass().addClass('bg-residency3');
        p2.removeClass();
        p3.addClass('show-content');
    }
    else if (residency.hasClass('bg-residency3')) {
        residency.removeClass().addClass('bg-residency1');
        p3.removeClass();
        p1.addClass('show-content');
    }
}

function change_menubar_colors() {
    let homestay_top_distance = document.getElementById('homestay').offsetTop;
    let section2_top_distance = document.getElementById('container2').offsetTop;
    let hotel_top_distance = document.getElementById('hotel').offsetTop;
    let footer_top_distance = document.getElementsByTagName('FOOTER')[0].offsetTop;

    let viewport_height = window.innerHeight;
    let menu_top_distance = $(window).scrollTop() + (viewport_height / 2);

    if (menu_top_distance >= footer_top_distance) {
        $('.bar1, .bar2, .bar3').removeClass('black');
        $('.nav-menu p').removeClass('black');
    }
    else if (menu_top_distance >= hotel_top_distance) {
        $('.bar1, .bar2, .bar3').addClass('black');
        $('.nav-menu p').addClass('black');
    }
    else if (menu_top_distance >= section2_top_distance) {
        $('.bar1, .bar2, .bar3').removeClass('black');
        $('.nav-menu p').removeClass('black');
    }
    else if (menu_top_distance >= homestay_top_distance) {
        $('.bar1, .bar2, .bar3').addClass('black');
        $('.nav-menu p').addClass('black');
    }
    else {
        $('.bar1, .bar2, .bar3').removeClass('black');
        $('.nav-menu p').removeClass('black');
    }
}