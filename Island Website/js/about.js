$(document).ready(function() {

    // change background image, elements and button colours over time
    setInterval(change_images_and_colours, 5000);

    //scroll animation specific to the about.html
    $(window).scroll(function() {
        parallax_footer_bg();
        if (!$('.nav-menu').hasClass('show-panel')) {
            change_menubar_colour();
        }
    });

});

/*
 * calculate the relative position between the window bottom and a specified element
 * E.g. Set element_string = '.photo-gallery1 div', offset = 0
 *      - If the window bottom is above the upper border of the photo-gallery1 div,
 *      the function returns 0
 *      - If the window bottom is between the upper border and bottom border,
 *      the function returns a number between 0 and 100
 *      - If the window bottom is below the bottom border,
 *      the function returns 100
 * @param String element_string: the target with which the window want to compare
 *        Int    offset (px) : the offset which allows the comparison to be later or earlier. Default 0
 * @return Int : a number between 0 and 100
 */
function relative_pos_to_target(element_string, offset = 0) {
    let target = $(element_string);
    let target_height = target.height();
    let target_offset_top = target.offset().top;
    let viewport_height = window.innerHeight;
    let scroll_pos = $(window).scrollTop() + viewport_height;
    let offset_scroll_pos;

    if (scroll_pos < target_offset_top + offset) {
        offset_scroll_pos = 0;
    }
    else if (scroll_pos < target_offset_top + target_height + offset) {
        offset_scroll_pos = scroll_pos - (target_offset_top + offset);
    }
    else {
        offset_scroll_pos = target_height;
    }

    return Math.round((offset_scroll_pos / target_height) * 100);

}

// callback for change images, elements and buttons
function change_images_and_colours() {
    if ($('figure').hasClass('bg-forest')) {
        $('.cloud1, .cloud2, .cloud3').addClass('disappearing');

        $('figure').removeClass().addClass('bg-lakes');
        $('.moon').removeClass('disappearing');

        $('.button').removeClass('forest').addClass('lakes');
    }
    else if ($('figure').hasClass('bg-lakes')) {
        $('.moon').addClass('disappearing');

        $('figure').removeClass().addClass('bg-cabin');

        $('.button').removeClass('lakes').addClass('cabin');
    }
    else if ($('figure').hasClass('bg-cabin')) {
        $('.cloud1, .cloud2, .cloud3').removeClass('disappearing');
        $('figure').removeClass().addClass('bg-forest');

        $('.button').removeClass('cabin').addClass('forest');
    }
}

function parallax_footer_bg() {
    let relative_position = relative_pos_to_target('footer', 150);
    $('footer').css('background-position', 'center ' + relative_position.toString() + '%' );
}

function change_menubar_colour() {
    let container_top_distance = document.getElementById('container').offsetTop;
    let section_top_distance = document.getElementsByTagName('SECTION')[0].offsetTop;
    let footer_top_distance = document.getElementsByTagName('FOOTER')[0].offsetTop;

    let viewport_height = window.innerHeight;
    let menu_top_distance = $(window).scrollTop() + (viewport_height / 2);

    if (menu_top_distance >= footer_top_distance) {
        $('.bar1, .bar2, .bar3').removeClass('black');
        $('.nav-menu p').removeClass('black');
    }
    else if (menu_top_distance >= section_top_distance) {
        $('.bar1, .bar2, .bar3').addClass('black');
        $('.nav-menu p').addClass('black');
    }
    else {
        $('.bar1, .bar2, .bar3').removeClass('black');
        $('.nav-menu p').removeClass('black');
    }
}
