"""
Simple 2d world where the player can interact with the items in the world.
"""

__author__ = "Zhutianyu Yu"
__date__ = "18th Oct 2019"
__version__ = "1.1.0"
__copyright__ = "The University of Queensland, 2019"

import math
import tkinter as tk
import time

from typing import Tuple, List
from tkinter import messagebox
from PIL import Image, ImageSequence

import pymunk
import os

from game.block import Block, MysteryBlock
from game.entity import Entity, BoundaryWall
from game.mob import Mob, CloudMob, Fireball
from game.item import DroppedItem, Coin
from game.view import GameView, ViewRenderer
from game.world import World
from game.util import get_collision_direction

from level import load_world, WorldBuilder
from player import Player

BLOCK_SIZE = 2 ** 4
MAX_WINDOW_SIZE = (1080, math.inf)

GOAL_SIZES = {
    "flag": (0.2, 9),
    "tunnel": (2, 2)
}

BLOCKS = {
    '#': 'brick',
    '%': 'brick_base',
    '?': 'mystery_empty',
    '$': 'mystery_coin',
    '^': 'cube',
    'b': 'bounce',
    'I': 'flagpole',
    '=': 'tunnel',
    'S': 'switches'
}

ITEMS = {
    'C': 'coin',
    '*': 'star'
}

MOBS = {
    '&': "cloud",
    '@': "mushroom"
}

class BounceBlock(Block):
    """ A bounce block will propel the player into the air.
        Similar to block class except the on_hit method and id.
    """
    _id = "bounce"
    _cell_size = (1, 1)

    def __init__(self):

        super().__init__(block_id=self._id)
        # set up a value to determine the state of the bounce block, which will influence the image loading
        self._bounce_state = 1
        # initialize the time tracker for pressing
        self._last_press = 0

    def on_hit(self, event, data):
        """ Callback when the player hits the bounce block
            When the player hits the bounce block from the top, he should be repelled to the air
        """

        world, player = data

        if get_collision_direction(entity=player, other=self) == "A":
            player.set_velocity(velocity=[player.get_velocity()[0], -280]) # the x-velocity should maintain
            self._last_press = time.time()
            self._bounce_state = 2

    def step(self, time_delta: float, game_data):
        """ A method which affects the image loading """

        if time.time() - self._last_press >= 0.5:
            self._bounce_state = 1
        elif time.time() - self._last_press >= 0.32:
            self._bounce_state = 2
        elif time.time() - self._last_press >= 0.16:
            self._bounce_state = 3
        elif time.time() - self._last_press >= 0:
            self._bounce_state = 2

    def get_bounce_state(self):

        return self._bounce_state




class Mushroom(Mob):
    """ The mushroom mob """

    _id = "mushroom"

    def __init__(self):

        super().__init__(mob_id=self._id, size=(16, 16), weight=100, tempo=30, max_health=5)
        # set up an attribute to affect the image loading
        self._steps = 0
        # set up a flag to affect the image loading
        self._mushroom_mob_flag = True

    def on_hit(self, event, data):
        """ Callback when the player hit the mushroom
            If the player hits the mushroom from the top, the mushroom will be killed
            If the player hits the mushroom from the left, he will be repelled to the left and lose health by one
            If the player hits the mushroom from the right, he will be repelled to the right and lose health by one
        """

        world, player = data
        self._tempo = -self._tempo # change the direction of mushroom

        if get_collision_direction(entity=player, other=self) == "A":
            self.change_health(change=-20) # change the health of mushroom to 0
            player.set_velocity(velocity=[player.get_velocity()[0], -100]) # player's x-velocity should maintain
            self._last_press_time = time.time()
            self.set_tempo(tempo=0)

        elif get_collision_direction(entity=player, other=self) == "L":
            player.set_velocity(velocity=[-50, 0])
            player.change_health(change=-1)

        elif get_collision_direction(entity=player, other=self) == "R":
            player.set_velocity(velocity=[50, 0])
            player.change_health(change=-1)

    def step(self, time_delta, game_data):
        """ A method which affects the image loaidng """
        super().step(time_delta=time_delta, game_data=game_data)

        world, player = game_data
        self._steps += 1

        # slowly switch between two images
        if self._steps >= 50:
            self._mushroom_mob_flag = False
        else:
            self._mushroom_mob_flag = True

        # When the step reaches 100, reset it
        if self._steps == 100:
            self._steps = 0

        # affect the dead image loading
        if self.is_dead() and time.time() - self._last_press_time >= 0.5:
            world.remove_mob(mob=self)

    def get_mob_flag(self):

        return self._mushroom_mob_flag


class Stick(Mob):
    """ The stick is a mob which is thrown by the player """

    _id = "stick"

    def __init__(self):

        super().__init__(mob_id=self._id, size=(16, 16), weight=100)
        self._steps = 0
        # I cannot use self._tempo by inheritance, so I create a new one to determine the velocity.
        self._tempo = 0
        # set up a stick state to affect the image loading
        self._stick_state = 1

    def step(self, time_delta, game_data):
        """ A method which affects the image loaidng """

        world, player = game_data
        self._steps += 1

        if self._steps >= 30:
            self._stick_state = 4
        elif self._steps >= 20:
            self._stick_state = 3
        elif self._steps >= 10:
            self._stick_state = 2
        else:
            self._stick_state = 1

        # if the step is 40, remove the stick.
        if self._steps == 40:
            world.remove_mob(self)
            self._steps = 0

        vx = self._tempo
        self.set_velocity((vx, self.get_velocity()[1]))

    def set_tempo(self, tempo):

        self._tempo = tempo

    def get_stick_state(self):

        return self._stick_state



class Star(DroppedItem):
    """ The star item allows the player to be invincible. """

    _id = "star"

    def collect(self, player: Player):
        """ It is originally set to callback when the player collides the star item
            However, the effect of invincibility is down by using _handle_player_collide_item,
            _handle_player_collide_mob, and redraw method in the MarioApp class.
            So, it doesn't need to do anything.
        """
        pass


class Flagpole(Block):
    """ The flagpole block allows the player to enter the next level.
        When the player lands on the top of the flagpole, increase his health to max health.
    """

    _id = "flagpole"
    _cell_size = GOAL_SIZES["flag"]

    def on_hit(self, event, data):
        """ Callback when the player collide with flagpole """

        world, player = data

        if get_collision_direction(entity=player, other=self) == "A":
            player.change_health(change=player.get_max_health()-player.get_health())


class Tunnel(Block):
    """ The tunnel block allows the player to enter into an another level """

    _id = "tunnel"
    _cell_size = GOAL_SIZES["tunnel"]

    def __init__(self):

        super().__init__(block_id=self._id)



class Switches(Block):
    """ The switches block allows the player to press,
        which will cause all bricks within the range to disappear
    """

    _id = "switches"

    def __init__(self):

        super().__init__(block_id=self._id)
        self._is_active = True
        self._last_press = 0 # initialize the time for the last press
        self._brick_position = [] # initialize a list which is ready to contain the brick position
        self._brick = [] # initialize a brick list which is ready to contain the brick instance


    def on_hit(self, event, data):
        """ Callback when the player hits the switch """

        world, player = data

        if get_collision_direction(entity=player, other=self) == "A":
            self._last_press = time.time()
            self._is_active = False
            sx, sy = self.get_position() # get the x and y position of switch

            # get all things in a list around the switches
            all_things = world.get_things_in_range(x=sx, y=sy, distance=30)
            # remove the player first. Otherwise, it will raise an exception.
            all_things.pop(0)

            for thing in all_things:
                if thing.get_id() == "brick":

                    # get the position tuple of each brick and append it to the list
                    b_position = thing.get_position()
                    self._brick_position.append(b_position)

                    # append the brick instance to the list
                    self._brick.append(thing)

                    world.remove_thing(thing=thing)

    def step(self, time_delta: float, game_data):
        """ A method which affects the image loading and the block filling """

        world, player = game_data

        if time.time()-self._last_press >= 10.0:
            self._is_active = True

            i = 0
            for thing in self._brick:
                world.add_block(block=thing, x=self._brick_position[i][0], y=self._brick_position[i][1])
                i += 1

            # clear two lists. Otherwise, these two lists will be more and more, which cause program to break down.
            self._brick_position.clear()
            self._brick.clear()



    def is_active(self):

        return self._is_active






def create_block(world: World, block_id: str, x: int, y: int, *args):
    """Create a new block instance and add it to the world based on the block_id.

    Parameters:
        world (World): The world where the block should be added to.
        block_id (str): The block identifier of the block to create.
        x (int): The x coordinate of the block.
        y (int): The y coordinate of the block.
    """
    block_id = BLOCKS[block_id]
    if block_id == "mystery_empty":
        block = MysteryBlock()
    elif block_id == "mystery_coin":
        block = MysteryBlock(drop="coin", drop_range=(3, 6))
    elif block_id == "bounce":
        block = BounceBlock()
    elif block_id == "flagpole":
        block = Flagpole()
    elif block_id == "tunnel":
        block = Tunnel()
    elif block_id == "switches":
        block = Switches()
    else:
        block = Block(block_id)

    world.add_block(block, x * BLOCK_SIZE, y * BLOCK_SIZE)


def create_item(world: World, item_id: str, x: int, y: int, *args):
    """Create a new item instance and add it to the world based on the item_id.

    Parameters:
        world (World): The world where the item should be added to.
        item_id (str): The item identifier of the item to create.
        x (int): The x coordinate of the item.
        y (int): The y coordinate of the item.
    """
    item_id = ITEMS[item_id]
    if item_id == "coin":
        item = Coin()
    elif item_id == "star":
        item = Star()
    else:
        item = DroppedItem(item_id)

    world.add_item(item, x * BLOCK_SIZE, y * BLOCK_SIZE)


def create_mob(world: World, mob_id: str, x: int, y: int, *args):
    """Create a new mob instance and add it to the world based on the mob_id.

    Parameters:
        world (World): The world where the mob should be added to.
        mob_id (str): The mob identifier of the mob to create.
        x (int): The x coordinate of the mob.
        y (int): The y coordinate of the mob.
    """
    mob_id = MOBS[mob_id]
    if mob_id == "cloud":
        mob = CloudMob()
    elif mob_id == "fireball":
        mob = Fireball()
    elif mob_id == "mushroom":
        mob = Mushroom()
    elif mob_id == "stick":
        mob = Stick()
    else:
        mob = Mob(mob_id, size=(1, 1))

    world.add_mob(mob, x * BLOCK_SIZE, y * BLOCK_SIZE)


def create_unknown(world: World, entity_id: str, x: int, y: int, *args):
    """Create an unknown entity."""
    world.add_thing(Entity(), x * BLOCK_SIZE, y * BLOCK_SIZE,
                    size=(BLOCK_SIZE, BLOCK_SIZE))


BLOCK_IMAGES = {
    "brick": "brick",
    "brick_base": "brick_base",
    "cube": "cube",
    "bounce": "bounce_block",
    "flagpole": "flag",
    "tunnel": "tunnel",
    "switches": "switch"
}

ITEM_IMAGES = {
    "coin": "coin_item",
    "star": "star"
}

MOB_IMAGES = {
    "cloud": "floaty",
    "fireball": "fireball_down",
    "mushroom": "mushroom"
}

class SpriteSheetLoader(object):

    def cut_mario(self):
        """ This method and the following ones are responsible for cutting and manipulating images
        from the SpriteSheet.
        """

        img = Image.open("spritesheets/characters.png")
        box = [80, 34, 96, 50]

        for i in range(1, 15):
            cutted_img = img.crop(box)
            cutted_img.save("spritesheets/mario{0}.png".format(i))

            box[0] += 17
            box[2] += 17

    def cut_luigi(self):

        img = Image.open("spritesheets/characters.png")
        box = [80, 99, 96, 115]

        for i in range(1, 15):
            cutted_img = img.crop(box)
            cutted_img.save("spritesheets/luigi{0}.png".format(i))

            box[0] += 17
            box[2] += 17

    def reverse_mario(self):

        for i in range(1, 15):
            img = Image.open("spritesheets/mario{0}.png".format(i))
            flipped_img = img.transpose(Image.FLIP_LEFT_RIGHT) # flip the image to the left
            flipped_img.save("spritesheets/flipped_mario{0}.png".format(i))

    def reverse_luigi(self):

        for i in range(1, 15):
            img = Image.open("spritesheets/luigi{0}.png".format(i))
            flipped_img = img.transpose(Image.FLIP_LEFT_RIGHT)
            flipped_img.save("spritesheets/flipped_luigi{0}.png".format(i))

    def cut_bounce_block(self):

        img = Image.open("spritesheets/items.png")
        box = [80, 0, 96, 32]

        for i in range(1, 4):
            cutted_img = img.crop(box)
            cutted_img.save("spritesheets/bounce_block{0}.png".format(i))

            box[0] += 16
            box[2] += 16

    def cut_mushroom_mob(self):

        img = Image.open("spritesheets/enemies.png")
        box = [0, 16, 16, 32]

        for i in range(1, 4):
            cutted_img = img.crop(box)
            cutted_img.save("spritesheets/mushroom_mob{0}.png".format(i))

            box[0] += 16
            box[2] += 16

    def cut_coin(self):

        img = Image.open("spritesheets/items.png")
        box = [290, 112, 306, 128]

        for i in range(1, 5):
            cutted_img = img.crop(box)
            cutted_img.save("spritesheets/coin{0}.png".format(i))

            box[0] += 15
            box[2] += 15

    def cut_stick(self):

        img = Image.open("spritesheets/items.png")
        box = [70, 192, 78, 208]
        cutted_img = img.crop(box)
        cutted_img.save("spritesheets/stick.png")

    def rotate_stick(self):

        img = Image.open("spritesheets/stick.png")
        i = 90
        while i <= 270:
            rotated_img = img.rotate(i)
            rotated_img.save("spritesheets/stick{0}.png".format(i))
            i += 90


class MarioViewRenderer(ViewRenderer):
    """A customised view renderer for a game of mario."""

    def __init__(self, block_images, item_images, mob_images):
        super().__init__(block_images, item_images, mob_images)

        self._sprite_loader = SpriteSheetLoader()
        self._sprite_loader.cut_mario()
        self._sprite_loader.reverse_mario()
        self._sprite_loader.cut_bounce_block()
        self._sprite_loader.cut_mushroom_mob()
        self._sprite_loader.cut_luigi()
        self._sprite_loader.reverse_luigi()
        self._sprite_loader.cut_coin()
        self._sprite_loader.cut_stick()
        self._sprite_loader.rotate_stick()

        self._image = {}

    def load_sprite(self, file: str) -> tk.PhotoImage:
        """ Load sprite sheets from the spritesheets file"""

        if file in self._image:
            return self._image[file]

        image = tk.PhotoImage(file="spritesheets/" + file + ".png")
        self._image[file] = image

        return image


    @ViewRenderer.draw.register(Player)
    def _draw_player(self, instance: Player, shape: pymunk.Shape,
                     view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:



        if instance.get_name() == "mario":

            if shape.body.velocity.x == 0:
                image = self.load_sprite(file="mario1")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 1:
                image = self.load_sprite(file="mario2")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 2:
                image = self.load_sprite(file="mario3")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 3:
                image = self.load_sprite(file="mario4")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 1:
                image = self.load_sprite(file="flipped_mario2")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 2:
                image = self.load_sprite(file="flipped_mario3")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 3:
                image = self.load_sprite(file="flipped_mario4")

            # I can't use elif here. Otherwise, the statement below will not execute if
            # one of the condition above is true.
            if shape.body.velocity.x >= 0 and shape.body.velocity.y < 0:
                image = self.load_sprite(file="mario6")
            elif shape.body.velocity.x < 0 and shape.body.velocity.y < 0:
                image = self.load_sprite(file="flipped_mario6")
            elif shape.body.velocity.x >= 0 and shape.body.velocity.y > 0:
                image = self.load_sprite(file="mario14")
            elif shape.body.velocity.x < 0 and shape.body.velocity.y > 0:
                image = self.load_sprite(file="flipped_mario14")

        if instance.get_name() == "luigi":

            if shape.body.velocity.x == 0:
                image = self.load_sprite(file="luigi1")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 1:
                image = self.load_sprite(file="luigi2")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 2:
                image = self.load_sprite(file="luigi3")
            elif shape.body.velocity.x > 0 and instance.get_player_render_value() == 3:
                image = self.load_sprite(file="luigi4")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 1:
                image = self.load_sprite(file="flipped_luigi2")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 2:
                image = self.load_sprite(file="flipped_luigi3")
            elif shape.body.velocity.x < 0 and instance.get_player_render_value() == 3:
                image = self.load_sprite(file="flipped_luigi4")

            if shape.body.velocity.x >= 0 and shape.body.velocity.y < 0:
                image = self.load_sprite(file="luigi6")
            elif shape.body.velocity.x < 0 and shape.body.velocity.y < 0:
                image = self.load_sprite(file="flipped_luigi6")
            elif shape.body.velocity.x >= 0 and shape.body.velocity.y > 0:
                image = self.load_sprite(file="luigi14")
            elif shape.body.velocity.x < 0 and shape.body.velocity.y > 0:
                image = self.load_sprite(file="flipped_luigi14")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="player")]

    @ViewRenderer.draw.register(MysteryBlock)
    def _draw_mystery_block(self, instance: MysteryBlock, shape: pymunk.Shape,
                            view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:
        if instance.is_active():
            image = self.load_image("coin")
        else:
            image = self.load_image("coin_used")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="block")]

    @ViewRenderer.draw.register(BounceBlock)
    def _draw_bounce_block(self, instance: BounceBlock, shape: pymunk.Shape,
                            view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        if instance.get_bounce_state() == 1:
            image = self.load_sprite(file="bounce_block3")
        elif instance.get_bounce_state() == 2:
            image = self.load_sprite(file="bounce_block2")
        elif instance.get_bounce_state() == 3:
            image = self.load_sprite(file="bounce_block1")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y - 8,
                                  image=image, tags="block")]

    @ViewRenderer.draw.register(Mushroom)
    def _draw_Mushroom(self, instance: Mushroom, shape: pymunk.Shape,
                       view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        if instance.get_mob_flag():
            image = self.load_sprite(file="mushroom_mob1")
        else:
            image = self.load_sprite(file="mushroom_mob2")

        if instance.is_dead():
            image = self.load_sprite(file="mushroom_mob3")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="mob")]

    @ViewRenderer.draw.register(Flagpole)
    def _draw_Flagpole(self, instance: Flagpole, shape: pymunk.Shape,
                           view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        image = self.load_image("flag")
        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="block")]

    @ViewRenderer.draw.register(Switches)
    def _draw_Switches(self, instance: Switches, shape: pymunk.Shape,
                       view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        if instance.is_active():
            image = self.load_image("switch")
        else:
            image = self.load_image("switch_pressed")
        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="block")]

    @ViewRenderer.draw.register(Coin)
    def _draw_Coin(self, instance: Coin, shape: pymunk.Shape,
                       view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        if instance.get_coin_render_value() == 1:
            image = self.load_sprite(file="coin1")
        elif instance.get_coin_render_value() == 2:
            image = self.load_sprite(file="coin2")
        elif instance.get_coin_render_value() == 3:
            image = self.load_sprite(file="coin3")
        elif instance.get_coin_render_value() == 4:
            image = self.load_sprite(file="coin4")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="item")]

    @ViewRenderer.draw.register(Stick)
    def _draw_Stick(self, instance: Stick, shape: pymunk.Shape,
                   view: tk.Canvas, offset: Tuple[int, int]) -> List[int]:

        if instance.get_stick_state() == 1:
            image = self.load_sprite(file="stick")
        elif instance.get_stick_state() == 2:
            image = self.load_sprite(file="stick90")
        elif instance.get_stick_state() == 3:
            image = self.load_sprite(file="stick180")
        elif instance.get_stick_state() == 4:
            image = self.load_sprite(file="stick270")

        return [view.create_image(shape.bb.center().x + offset[0], shape.bb.center().y,
                                  image=image, tags="item")]


class MarioApp:
    """High-level app class for Mario, a 2d platformer"""

    _world: World

    def __init__(self, master: tk.Tk):
        """Construct a new game of a MarioApp game.

        Parameters:
            master (tk.Tk): tkinter root widget
        """
        self._master = master
        self._master.title("Mario")

        self._filename = None
        self._loading_file = None

        # create a window to load the file
        self._window = tk.Tk()
        self._window.title("Load File")

        prompt_label = tk.Label(self._window, text="Please enter the file path")
        prompt_label.pack()

        self._entry_file_message = tk.Entry(self._window, width=10)
        self._entry_file_message.pack()

        button = tk.Button(self._window, text="confirm", command=self.file_path_to_load)
        button.pack()

        # initialize the attributes which the __init__ method originally has to None
        self._builder = None
        self._player = None
        self._current_world = None
        self._renderer = None
        self._view = None
        self._bottom_frame = None
        self._invincible_flag = None
        self._tunnel = None

        # initialize an attribute to record the time when the player launch a stick
        self._launch_time = 0

    def file_path_to_load(self):
        """ Callback when click on the file confirm button """

        self._filename = self._entry_file_message.get()
        self._window.destroy()
        self.run(master=self._master)

    def run(self, master):
        """ After user clicks on the file confirm button, it handles the game running.

            Parameters:
                master (tk.Tk): tkinter root widget
        """
        try:
            # set up a file instance for loading file
            self._loading_file = LoadingConfiguration(filename=self._filename)
            self._loading_file.load_file()

            world_builder = WorldBuilder(BLOCK_SIZE, gravity=(0, self._loading_file.get_gravity()),
                                         fallback=create_unknown)
            world_builder.register_builders(BLOCKS.keys(), create_block)
            world_builder.register_builders(ITEMS.keys(), create_item)
            world_builder.register_builders(MOBS.keys(), create_mob)
            self._builder = world_builder

            self._player = Player(max_health=self._loading_file.get_health(), name=self._loading_file.get_character())

            # Initialize the current world to the attribute described in the specification.
            # The reset world method will behave according to the self._current_world attribute
            self._current_world = self._loading_file.get_start()
            self.reset_world(new_level=self._current_world)

            self._renderer = MarioViewRenderer(BLOCK_IMAGES, ITEM_IMAGES, MOB_IMAGES)

            size = tuple(map(min, zip(MAX_WINDOW_SIZE, self._world.get_pixel_size())))
            self._view = GameView(master, size, self._renderer)
            self._view.pack()

            self.bind()

            # set up the menu
            menubar = tk.Menu(master)
            master.config(menu=menubar)

            filemenu = tk.Menu(menubar)
            menubar.add_cascade(label="File", menu=filemenu)
            filemenu.add_command(label="Load Level", command=self.load_level_window)
            filemenu.add_command(label="Reset Level", command=self.execute_reset_world)
            filemenu.add_command(label="Exit", command=self.exit)
            filemenu.add_command(label="High Scores", command=self.view_score)

            # set up the bottom frame displaying the player's health and score
            self._bottom_frame = BottomFrame(parent=master)
            self._bottom_frame.pack(side=tk.BOTTOM)

            # set up a flag to determine if the player is currently invincible
            self._invincible_flag = False

            # set up a flag to determine if the player is currently near the tunnel
            self._tunnel = None

            # Wait for window to update before continuing
            master.update_idletasks()
            self.step()

        except Exception as e:

            error_message = tk.messagebox
            is_close = error_message.showinfo(title="Error", message=e)

            if is_close:
                os._exit(0)

    def load_level_window(self):
        """ set up a load level window """
        window = tk.Tk()
        window.title("Load Level")

        prompt_label = tk.Label(window, text="Please load the level")
        prompt_label.pack()

        entry_level_message = tk.Entry(window, width=10)
        entry_level_message.pack()

        def button_confirm():
            """ Callback function for button
                When button are pressed, it should reset the world according to the level string entered
            """
            level_string = entry_level_message.get()
            self.reset_world(new_level=level_string)

            # When users enter different level, the self._current_world needs to change accordingly.
            # So, when users reset the world later, it will load the current world.
            if level_string == 'level1.txt':
                self._current_world = 'level1.txt'
            elif level_string == 'level2.txt':
                self._current_world = 'level2.txt'
            elif level_string == 'level3.txt':
                self._current_world = 'level3.txt'
            elif level_string == 'bonus.txt':
                self._current_world = 'bonus.txt'
            elif level_string == 'small_room.txt':
                self._current_world = 'small_room.txt'
            elif level_string == 'bonus.txt':
                self._current_world = 'bonus.txt'
            elif level_string == 'small_room.txt':
                self._current_world = 'small_room.txt'
            window.destroy()

        button = tk.Button(window, text="confirm", command=button_confirm)
        button.pack()



    def reset_world(self, new_level='level1.txt'):
        """ Reset all player progress (e.g. health, score, etc) in the current level
            As well as set the invincible flag to False
        """
        current_score = self._player.get_score()
        self._player.change_score(change=-current_score) # change player's score to zero

        self._player.change_health(change=self._loading_file.get_health()) # change player's health to the beginning

        self._invincible_flag = False # set the invincible flag to false when reset the world

        self._world = load_world(self._builder, new_level)
        self._world.add_player(player=self._player, x=self._loading_file.get_x_coordinate(),
                               y=self._loading_file.get_y_coordinate(), mass=self._loading_file.get_mass())
        self._builder.clear()

        self._setup_collision_handlers()


    def reset_world_original(self, new_level):
        """ This is the original version of reset world method """
        self._world = load_world(self._builder, new_level)
        self._world.add_player(player=self._player, x=self._loading_file.get_x_coordinate(),
                               y=self._loading_file.get_y_coordinate(), mass=self._loading_file.get_mass())
        self._builder.clear()

        self._setup_collision_handlers()


    def execute_reset_world(self):
        """ Callback for reset level button
            Decide if users want to reset the level
        """

        self.reset_world(new_level=self._current_world)


    def exit(self):
        """ Decide if users want to exit the game"""
        exit_window = tk.messagebox
        end = exit_window.askyesno(title="Alert", message="Do you really want to quick")
        if end:
            os._exit(0)


    def view_score(self):
        """ Callback for High Score button """
        try:
            score_file = ReadFile(filename="score_for_level.txt")
            score_file.read_file()
            score_file.sort_list()
            top_level_window = tk.Toplevel()

            if self._current_world == "level1.txt":
                label1 = tk.Label(top_level_window, text="Level1 Score", width=20)
                label1.pack(side=tk.TOP)

                for index, tuple in enumerate(score_file.show_ordered_level1_list()):
                    if index <= 9:
                        score_label = tk.Label(top_level_window, text="{0}:{1}".format(tuple[0], tuple[1]), width=20)
                        score_label.pack(side=tk.TOP)
                    else:
                        break

            elif self._current_world == "level2.txt":
                label2 = tk.Label(top_level_window, text="Level2 Score", width=20)
                label2.pack(side=tk.TOP)

                for index, tuple in enumerate(score_file.show_ordered_level2_list()):
                    if index <= 9:
                        score_label = tk.Label(top_level_window, text="{0}:{1}".format(tuple[0], tuple[1]), width=20)
                        score_label.pack(side=tk.TOP)
                    else:
                        break

            elif self._current_world == "level3.txt":
                label3 = tk.Label(top_level_window, text="Level3 Score", width=20)
                label3.pack(side=tk.TOP)

                for index, tuple in enumerate(score_file.show_ordered_level3_list()):
                    if index <= 9:
                        score_label = tk.Label(top_level_window, text="{0}:{1}".format(tuple[0], tuple[1]), width=20)
                        score_label.pack(side=tk.TOP)
                    else:
                        break

            elif self._current_world == "bonus.txt":
                bonus_label = tk.Label(top_level_window, text="Bonus Score", width=20)
                bonus_label.pack(side=tk.TOP)

                for index, tuple in enumerate(score_file.show_ordered_bonus_list()):
                    if index <= 9:
                        score_label = tk.Label(top_level_window, text="{0}:{1}".format(tuple[0], tuple[1]), width=20)
                        score_label.pack(side=tk.TOP)
                    else:
                        break

            elif self._current_world == "small_room.txt":
                small_room_label = tk.Label(top_level_window, text="Small Room Score", width=20)
                small_room_label.pack(side=tk.TOP)

                for index, tuple in enumerate(score_file.show_ordered_small_room_list()):
                    if index <= 9:
                        score_label = tk.Label(top_level_window, text="{0}:{1}".format(tuple[0], tuple[1]), width=20)
                        score_label.pack(side=tk.TOP)
                    else:
                        break
        except:
            # If the file cannot be found, it will display an empty window
            error_window = tk.Toplevel()




    def bind(self):
        """Bind all the keyboard events to their event handlers."""

        # left and right
        self._master.bind("<Left>", lambda e: self._move(dx=-1, dy=0))
        self._master.bind("<Right>", lambda e: self._move(dx=1, dy=0))
        self._master.bind("<D>", lambda e: self._move(dx=1, dy=0))
        self._master.bind("<A>", lambda e: self._move(dx=-1, dy=0))
        self._master.bind("<d>", lambda e: self._move(dx=1, dy=0))
        self._master.bind("<a>", lambda e: self._move(dx=-1, dy=0))

        # jump
        self._master.bind("<Up>", lambda e: self._jump())
        self._master.bind("<W>", lambda e: self._jump())
        self._master.bind("<w>", lambda e: self._jump())
        self._master.bind("<space>", lambda e: self._jump())

        # down
        self._master.bind("<Down>", lambda e: self._duck())
        self._master.bind("<S>", lambda e: self._duck())
        self._master.bind("<s>", lambda e: self._duck())

        # attack
        self._master.bind("<z>", lambda e: self.launch_stick())
        self._master.bind("<Z>", lambda e: self.launch_stick())
        self._master.bind("<j>", lambda e: self.launch_stick())
        self._master.bind("<J>", lambda e: self.launch_stick())

    def redraw(self):
        """Redraw all the entities in the game canvas."""
        self._view.delete(tk.ALL)

        # get the player's max health and current health to update the health bar
        max_health = self._player.get_max_health()
        current_health = self._player.get_health()
        self._bottom_frame.current_health_status(max_health=max_health, current_health=current_health)

        # get the player's current score to update the score label
        current_score = self._player.get_score()
        self._bottom_frame.current_score_status(current_score=current_score)

        self._view.draw_entities(self._world.get_all_things())

        # If the player is currently invincible, his health label should become yellow
        if self._invincible_flag:
            self._bottom_frame.invincible_health_status(max_health=max_health, current_health=current_health)

        # If the player is currently invincible and the time difference between collision and now is greater tha 10
        # Set the flag back to false and set the health label back to normal
        if self._invincible_flag and time.time()-self._initial_invincible_time >= 10:
            self._invincible_flag = False

        # If the player runs out of health, it should ask users if they want to restart the game
        if current_health == 0:
            death_message = tk.messagebox
            restart = death_message.askyesno(title="You are died", message="Do you want to restart the game?")

            if restart:  # If users restart the game, the game will restart at the current level
                self.reset_world(new_level=self._current_world)
            else:
                os._exit(0)




    def scroll(self):
        """Scroll the view along with the player in the center unless
        they are near the left or right boundaries
        """
        x_position = self._player.get_position()[0]
        half_screen = self._master.winfo_width() / 2
        world_size = self._world.get_pixel_size()[0] - half_screen

        # Left side
        if x_position <= half_screen:
            self._view.set_offset((0, 0))

        # Between left and right sides
        elif half_screen <= x_position <= world_size:
            self._view.set_offset((half_screen - x_position, 0))

        # Right side
        elif x_position >= world_size:
            self._view.set_offset((half_screen - world_size, 0))

    def step(self):
        """Step the world physics and redraw the canvas."""
        data = (self._world, self._player)
        self._world.step(data)

        self.scroll()
        self.redraw()

        self._master.after(10, self.step)

    def _move(self, dx, dy):
        """ Get the x-velocity (vx) and y-velocity (vy) of the player
            Set the x-velocity and y-velocity of the player accoring to the dx and dy

            Parameters:
                dx (int): times to increase or decrease the x-velocity
                dy (int): times to increase or decrease the y-velocity
        """
        vx, vy = self._player.get_velocity()

        # max velocity constraint
        if vx <= self._loading_file.get_max_velocity():
            self._player.set_velocity(velocity=(vx+dx*60, vy+dy*60))

    def _jump(self):
        """ If the player is currently not jumping, set the y-velocity an appropriate number
            while maintain the x-velocity
        """

        if self._player.get_velocity()[1] != 0:
            self._player.set_jumping(jumping=True)
        else:
            self._player.set_jumping(jumping=False)

        # The player can only jump when he is not currently jumping.
        if not self._player.is_jumping():
            self._player.set_velocity(velocity=(self._player.get_velocity()[0], -190))


    def _duck(self):
        """ Callback when the player is ducking, it also affects the level loaiding in the certain circumstances """
        self._player.set_velocity(velocity=(self._player.get_velocity()[0], 170))

        # If the player is not on the block, press the duck shouldn't load the next level
        if self._tunnel != None:
            if self._current_world == "level1.txt":
                self.reset_world_original(new_level=self._loading_file.get_level1_tunnel())
                self._current_world = self._loading_file.get_level1_tunnel()

            elif self._current_world == "level2.txt":
                self.reset_world_original(new_level=self._loading_file.get_level2_tunnel())
                self._current_world = self._loading_file.get_level2_tunnel()

            elif self._current_world == "level3.txt":
                self.reset_world_original(new_level=self._loading_file.get_level3_tunnel())
                self._current_world = self._loading_file.get_level3_tunnel()
            
            elif self._current_world == "bonus.txt":
                self.reset_world_original(new_level=self._loading_file.get_bonus_tunnel())
                self._current_world = self._loading_file.get_bonus_tunnel()

            elif self._current_world == "small_room.txt":
                self.reset_world_original(new_level=self._loading_file.get_small_room_tunnel())
                self._current_world = self._loading_file.get_small_room_tunnel()

    def launch_stick(self):
        """ Callback for the player to throw a stick """

        # The player can only launch the stick after 1 second
        if time.time() - self._launch_time >= 1:
            player_x, player_y = self._player.get_position()
            stick = Stick()
            if self._player.get_velocity()[0] >= 0:
                self._world.add_mob(mob=stick, x=player_x + 22, y=player_y)
                stick.set_tempo(tempo=100)
                self._launch_time = time.time()
            else:
                self._world.add_mob(mob=stick, x=player_x - 22, y=player_y)
                stick.set_tempo(tempo=-100)
                self._launch_time = time.time()


    def _setup_collision_handlers(self):
        """ set up the cllision handlers, and the following methods handle different collisions """
        self._world.add_collision_handler("player", "item", on_begin=self._handle_player_collide_item)
        self._world.add_collision_handler("player", "block", on_begin=self._handle_player_collide_block,
                                          on_separate=self._handle_player_separate_block)
        self._world.add_collision_handler("player", "mob", on_begin=self._handle_player_collide_mob)
        self._world.add_collision_handler("mob", "block", on_begin=self._handle_mob_collide_block)
        self._world.add_collision_handler("mob", "mob", on_begin=self._handle_mob_collide_mob)
        self._world.add_collision_handler("mob", "item", on_begin=self._handle_mob_collide_item)

    def _handle_mob_collide_block(self, mob: Mob, block: Block, data,
                                  arbiter: pymunk.Arbiter) -> bool:
        if mob.get_id() == "fireball":
            if block.get_id() == "brick":
                self._world.remove_block(block)
            self._world.remove_mob(mob)

        elif mob.get_id() == "mushroom":
            # The mob will reverse its direction whether it collides the block from left or right.
            if get_collision_direction(mob, block) == "L" or get_collision_direction(mob, block) == "R":
                mob.set_tempo(tempo=-mob.get_tempo())

        elif mob.get_id() == "stick":

            if get_collision_direction(mob, block) == "L":
                mob.set_tempo(tempo=-100)
            elif get_collision_direction(mob, block) == "R":
                mob.set_tempo(tempo=100)

        return True

    def _handle_mob_collide_item(self, mob: Mob, block: Block, data,
                                 arbiter: pymunk.Arbiter) -> bool:
        return False

    def _handle_mob_collide_mob(self, mob1: Mob, mob2: Mob, data,
                                arbiter: pymunk.Arbiter) -> bool:
        if mob1.get_id() == "fireball" or mob2.get_id() == "fireball":
            self._world.remove_mob(mob1)
            self._world.remove_mob(mob2)

        elif mob1.get_id() == "mushroom" and mob2.get_id() == "mushroom":
            # When mushrooms collides with other mushrooms, they should reverse their direction.
            mob1.set_tempo(tempo=-mob1.get_tempo())
            mob2.set_tempo(tempo=-mob2.get_tempo())

        elif mob1.get_id() == "stick" and mob2.get_id() == "mushroom":
            self._world.remove_mob(mob=mob1)
            self._world.remove_mob(mob=mob2)

        elif mob1.get_id() == "mushroom" and mob2.get_id() == "stick":
            self._world.remove_mob(mob=mob1)
            self._world.remove_mob(mob=mob2)

        return False

    def _handle_player_collide_item(self, player: Player, dropped_item: DroppedItem,
                                    data, arbiter: pymunk.Arbiter) -> bool:
        """Callback to handle collision between the player and a (dropped) item. If the player has sufficient space in
        their to pick up the item, the item will be removed from the game world.

        Parameters:
            player (Player): The player that was involved in the collision
            dropped_item (DroppedItem): The (dropped) item that the player collided with
            data (dict): data that was added with this collision handler (see data parameter in
                         World.add_collision_handler)
            arbiter (pymunk.Arbiter): Data about a collision
                                      (see http://www.pymunk.org/en/latest/pymunk.html#pymunk.Arbiter)
                                      NOTE: you probably won't need this
        Return:
             bool: False (always ignore this type of collision)
                   (more generally, collision callbacks return True iff the collision should be considered valid; i.e.
                   returning False makes the world ignore the collision)
        """
        if dropped_item.get_id() == "star": # if the player collide the star item, set the invincible flag to true
            self._invincible_flag = True

            # If the player collide the star item, assign the current time to an attribute _initial_invincible_time
            self._initial_invincible_time = time.time()
            self._world.remove_item(dropped_item)

        else:
            dropped_item.collect(self._player)
            self._world.remove_item(dropped_item)
        return False

    def _handle_player_collide_block(self, player: Player, block: Block, data,
                                     arbiter: pymunk.Arbiter) -> bool:

        if block.get_id() == "flagpole":
            block.on_hit(arbiter, (self._world, player))

            if self._current_world == 'level1.txt':
                # When the player enters the next world, the self._current_world needs to change accordingly.
                # So, when users reset the world later, it will reset the current world.
                self._current_world = self._loading_file.get_level1_goal()
                if self._current_world != 'END':

                    # set up a window for users to input their name to output their score
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level1", name=name, score=str(self._player.get_score()))
                        self.reset_world_original(new_level=self._current_world)
                        name_window.destroy()

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)

                else:
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level1", name=name, score=str(self._player.get_score()))
                        name_window.destroy()
                        over_message = tk.messagebox
                        is_close = over_message.showinfo(title="Gave Over", message="Gave is over")

                        if is_close:
                            os._exit(0)

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)


            elif self._current_world == 'level2.txt':
                self._current_world = self._loading_file.get_level2_goal()
                if self._current_world != 'END':
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level2", name=name, score=str(self._player.get_score()))
                        self.reset_world_original(new_level=self._current_world)
                        name_window.destroy()

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)
                else:
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level2", name=name, score=str(self._player.get_score()))
                        name_window.destroy()
                        over_message = tk.messagebox
                        is_close = over_message.showinfo(title="Gave Over", message="Gave is over")

                        if is_close:
                            os._exit(0)

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)

            elif self._current_world == 'level3.txt':
                self._current_world = self._loading_file.get_level3_goal()
                if self._current_world != 'END':
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level3", name=name, score=str(self._player.get_score()))
                        self.reset_world_original(new_level=self._current_world)
                        name_window.destroy()

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)
                else:
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="level3", name=name, score=str(self._player.get_score()))
                        name_window.destroy()
                        over_message = tk.messagebox
                        is_close = over_message.showinfo(title="Gave Over", message="Gave is over")

                        if is_close:
                            os._exit(0)

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)

            elif self._current_world == "bonus.txt":
                self._current_world = self._loading_file.get_bonus_goal()
                if self._current_world != 'END':
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="bonus", name=name, score=str(self._player.get_score()))
                        self.reset_world_original(new_level=self._current_world)
                        name_window.destroy()

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)
                else:
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="bonus", name=name, score=str(self._player.get_score()))
                        name_window.destroy()
                        over_message = tk.messagebox
                        is_close = over_message.showinfo(title="Gave Over", message="Gave is over")

                        if is_close:
                            os._exit(0)

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)

            elif self._current_world == "small_room.txt":
                self._current_world = self._loading_file.get_small_room_goal()
                if self._current_world != 'END':
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="small_room", name=name, score=str(self._player.get_score()))
                        self.reset_world_original(new_level=self._current_world)
                        name_window.destroy()

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)
                else:
                    name_window = tk.Tk()
                    name_window.title("Enter your name")
                    prompt_label = tk.Label(name_window, text="Name: ", width=10)
                    prompt_label.pack(side=tk.LEFT)
                    entry_name = tk.Entry(name_window, width=10)
                    entry_name.pack(side=tk.LEFT)

                    def name_output():
                        """ Callback for output name and score """
                        name = entry_name.get()
                        write_file(level="small_room", name=name, score=str(self._player.get_score()))
                        name_window.destroy()
                        over_message = tk.messagebox
                        is_close = over_message.showinfo(title="Gave Over", message="Gave is over")

                        if is_close:
                            os._exit(0)

                    button = tk.Button(name_window, text="confirm", command=name_output)
                    button.pack(side=tk.LEFT)


        elif block.get_id() == "switches":
            block.on_hit(arbiter, (self._world, player))
            block.step(time_delta=10.0, game_data=(self._world, self._player))

            # If the method returns False, it means that the switch has been pressed.
            if not block.is_active():
                return False  # cancel the interaction between the player and the switch
            else:
                return True

        elif block.get_id() == "tunnel":

            # If the player lands on the tunnel from the top, set the attribute to block
            if get_collision_direction(entity=self._player, other=block) == "A":
                self._tunnel = block

        elif block.get_id() == "bounce":
            block.on_hit(arbiter, (self._world, player))
            block.step(time_delta=1.0, game_data=(self._world, self._player))

        else:
            block.on_hit(arbiter, (self._world, player))

        return True

    def _handle_player_collide_mob(self, player: Player, mob: Mob, data,
                                   arbiter: pymunk.Arbiter) -> bool:

        if self._invincible_flag: # If invincible flag is true, remove the mob directly
            self._world.remove_mob(mob=mob)

        else:
            mob.on_hit(arbiter, (self._world, player))
            mob.step(time_delta=1, game_data=(self._world, self._player))

        return True

    def _handle_player_separate_block(self, player: Player, block: Block, data,
                                      arbiter: pymunk.Arbiter) -> bool:

        # If the player leaves the tunnel, set the attribute back to None
        self._tunnel = None
        return True




class BottomFrame(tk.Frame):
    """ Create a bottom frame displaying health bar and score"""

    def __init__(self, parent):

        super().__init__(parent)

        # set up a score label
        self._score_label = tk.Label(self, text="Score: 0")
        self._score_label.pack(side=tk.BOTTOM)

        # set up a canvas
        self._canvas = tk.Canvas(self, bg="black", width=MAX_WINDOW_SIZE[0], height=25)
        self._canvas.pack(side=tk.BOTTOM)

        # set up a health bar (rectangle) inside the canvas
        self._health_bar = self._canvas.create_rectangle(0, 0, MAX_WINDOW_SIZE[0]+10, 30, fill="green")



    def current_health_status(self, max_health, current_health):
        """ Change the length of health bar based on the ratio of current_health and max_health"""

        ratio = current_health/max_health

        if ratio >= 0.5:
            # When the player has grater than or equal to 50%
            self._canvas.delete(tk.ALL)
            self._health_bar = self._canvas.create_rectangle(0, 0, (MAX_WINDOW_SIZE[0]+3)*ratio, 30, fill="green")

        elif ratio > 0.25:
            self._canvas.delete(tk.ALL)
            self._health_bar = self._canvas.create_rectangle(0, 0, MAX_WINDOW_SIZE[0]*ratio, 30, fill="orange")

        # When the player has less than or equal to 25%
        else:
            self._canvas.delete(tk.ALL)
            self._health_bar = self._canvas.create_rectangle(0, 0, MAX_WINDOW_SIZE[0]*ratio, 30, fill="red")




    def current_score_status(self, current_score):
        """ Change the score based on the current score"""

        self._score_label.config(text="Score: {0}".format(current_score))


    def invincible_health_status(self, max_health, current_health):
        """ If the player is currently invincible, change the color of health bar to yellow based on the ratio

        """

        ratio = current_health/max_health

        if ratio >= 0.5:
            self._canvas.delete(tk.ALL)
            self._health_bar = self._canvas.create_rectangle(0, 0, (MAX_WINDOW_SIZE[0] + 3) * ratio, 30, fill="yellow")

        else:
            self._canvas.delete(tk.ALL)
            self._health_bar = self._canvas.create_rectangle(0, 0, MAX_WINDOW_SIZE[0] * ratio, 30, fill="yellow")



class LoadingConfiguration(object):
    """ The class for loading file """

    def __init__(self, filename):
        """ Construct a series of attributes for storing data

            Parameters:
                filename (str) : The filename to load
        """

        self._filename = filename

        self._gravity = None
        self._start = None

        self._character = None
        self._x = None
        self._y = None
        self._mass = None
        self._health = None
        self._max_velocity = None

        self._level1_tunnel = None
        self._level1_goal = None

        self._bonus_tunnel = None
        self._bonus_goal = None

        self._level2_tunnel = None
        self._level2_goal = None

        self._small_room_tunnel = None
        self._small_room_goal = None

        self._level3_tunnel = None
        self._level3_goal = None


    def load_file(self):
        """ Handle the file loading """

        with open(self._filename, 'r') as reader:

            lines = reader.readlines()

            for index, line in enumerate(lines):

                if line.startswith("==World=="):
                    self._gravity = int(lines[index+1].split(":")[1].strip())
                    self._start = lines[index+2].split(":")[1].strip()

                if line.startswith("==Player=="):
                    self._character = lines[index+1].split(":")[1].strip()
                    self._x = int(lines[index+2].split(":")[1].strip())
                    self._y = int(lines[index+3].split(":")[1].strip())
                    self._mass = int(lines[index+4].split(":")[1].strip())
                    self._health = int(lines[index+5].split(":")[1].strip())
                    self._max_velocity = int(lines[index+6].split(":")[1].strip())

                if line.startswith("==level1.txt=="):
                    if lines[index+1].startswith("tunnel"):
                        self._level1_tunnel = lines[index+1].split(":")[1].strip()
                        self._level1_goal = lines[index+2].split(":")[1].strip()

                    elif lines[index+1].startswith("goal"):
                        self._level1_goal = lines[index+1].split(":")[1].strip()

                    else:
                        raise Exception("No goal property can be found or the line above is wrong")

                if line.startswith("==bonus.txt=="):
                    if lines[index+1].startswith("tunnel"):
                        self._bonus_tunnel = lines[index+1].split(":")[1].strip()
                        self._bonus_goal = lines[index+2].split(":")[1].strip()

                    elif lines[index+1].startswith("goal"):
                        self._bonus_goal = lines[index+1].split(":")[1].strip()

                    else:
                        raise Exception("No goal property can be found or the line above is wrong")

                if line.startswith("==level2.txt=="):
                    if lines[index+1].startswith("tunnel"):
                        self._level2_tunnel = lines[index+1].split(":")[1].strip()
                        self._level2_goal = lines[index+2].split(":")[1].strip()

                    elif lines[index+1].startswith("goal"):
                        self._level2_goal = lines[index+1].split(":")[1].strip()

                    else:
                        raise Exception("No goal property can be found or the line above is wrong")

                if line.startswith("==small_room.txt=="):
                    if lines[index+1].startswith("tunnel"):
                        self._small_room_tunnel = lines[index+1].split(":")[1].strip()
                        self._small_room_goal = lines[index+2].split(":")[1].strip()

                    elif lines[index+1].startswith("goal"):
                        self._small_room_goal = lines[index+1].split(":")[1].strip()

                    else:
                        raise Exception("No goal property can be found or the line above is wrong")

                if line.startswith("==level3.txt=="):
                    if lines[index+1].startswith("tunnel"):
                        self._level3_tunnel = lines[index+1].split(":")[1].strip()
                        self._level3_goal = lines[index+2].split(":")[1].strip()

                    elif lines[index+1].startswith("goal"):
                        self._level3_goal = lines[index+1].split(":")[1].strip()

                    else:
                        raise Exception("No goal property can be found or the line above is wrong")

            # These attributes shouldn't be None after assignment in the for loop
            if self._gravity == None:
                raise Exception("gravity property is missing or world tag is missing")
            if self._start == None:
                raise Exception("start property is missing or world tag is missing")
            if self._character == None:
                raise Exception("character property is missing or player tag is missing")
            if self._x == None:
                raise Exception("x property is missing or player tag is missing")
            if self._y == None:
                raise Exception("y property is missing or player tag is missing")
            if self._mass == None:
                raise Exception("mass property is missing or player tag is missing")
            if self._health == None:
                raise Exception("health property is missing or player tag is missing")
            if self._max_velocity == None:
                raise Exception("max_velocity property is missing or player tag is missing")

            if self._start == 'level1.txt' and self._level1_goal == None:
                raise Exception("starting point missing")
            if self._start == 'level2.txt' and self._level2_goal == None:
                raise Exception("starting point missing")
            if self._start == 'level3.txt' and self._level3_goal == None:
                raise Exception("starting point missing")
            if self._start != 'level1.txt' and self._start != 'level2.txt' and self._start != 'level3.txt':
                raise  Exception("starting point must be level1.txt, level2.txt or level3.txt")


    def get_gravity(self):

        return self._gravity

    def get_start(self):

        return self._start

    def get_character(self):

        return self._character

    def get_x_coordinate(self):

        return self._x

    def get_y_coordinate(self):

        return self._y

    def get_mass(self):

        return self._mass

    def get_health(self):

        return self._health

    def get_max_velocity(self):

        return self._max_velocity

    def get_level1_tunnel(self):

        return self._level1_tunnel

    def get_level1_goal(self):

        return self._level1_goal

    def get_bonus_tunnel(self):

        return self._bonus_tunnel

    def get_bonus_goal(self):

        return self._bonus_goal

    def get_level2_tunnel(self):

        return self._level2_tunnel

    def get_level2_goal(self):

        return self._level2_goal

    def get_small_room_tunnel(self):

        return self._small_room_tunnel

    def get_small_room_goal(self):

        return self._small_room_goal

    def get_level3_tunnel(self):

        return self._level3_tunnel

    def get_level3_goal(self):

        return self._level3_goal


def write_file(level: str, name: str, score: str):
    """ Output level, name and score into a txt file

        Parameters:
            level (str): current level in which the player is playing
            name: the name which input from users
            score: the player's current score
    """

    with open("score_for_level.txt", 'a') as writer:
        writer.write(level+"\n")
        writer.write("name : "+name+"\n")
        writer.write("score : "+score+"\n")
        writer.write("\n")


class ReadFile(object):
    """ A class for reading file from the score_for_level.txt which contains the name and score"""

    def __init__(self, filename):

        self._filename = filename
        self._level1_dict = {}
        self._level2_dict = {}
        self._level3_dict = {}
        self._bonus_dict = {}
        self._small_room_dict = {}
        self._ordered_level1_list = None
        self._ordered_level2_list = None
        self._ordered_level3_list = None
        self._ordered_bonus_list = None
        self._ordered_small_room_list = None

    def read_file(self):
        """ A method for reading the file """

        with open(self._filename, 'r') as reader:
            lines = reader.readlines()

            for index, line in enumerate(lines):

                if line.startswith("level1"):
                    name = lines[index + 1].split(":")[1].strip()
                    score = int(lines[index + 2].split(":")[1].strip())
                    self._level1_dict[name] = score

                elif line.startswith("level2"):
                    name = lines[index + 1].split(":")[1].strip()
                    score = int(lines[index + 2].split(":")[1].strip())
                    self._level2_dict[name] = score

                elif line.startswith("level3"):
                    name = lines[index + 1].split(":")[1].strip()
                    score = int(lines[index + 2].split(":")[1].strip())
                    self._level3_dict[name] = score

                elif line.startswith("bonus"):
                    name = lines[index + 1].split(":")[1].strip()
                    score = int(lines[index + 2].split(":")[1].strip())
                    self._bonus_dict[name] = score

                elif line.startswith("small_room"):
                    name = lines[index + 1].split(":")[1].strip()
                    score = int(lines[index + 2].split(":")[1].strip())
                    self._small_room_dict[name] = score


    def sort_list(self):
        """ sort the scores in order so that they can be displayed on the high score menu"""

        self._ordered_level1_list = sorted(self._level1_dict.items(), key=lambda x:x[1], reverse=True)
        self._ordered_level2_list = sorted(self._level2_dict.items(), key=lambda x:x[1], reverse=True)
        self._ordered_level3_list = sorted(self._level3_dict.items(), key=lambda x:x[1], reverse=True)
        self._ordered_bonus_list = sorted(self._bonus_dict.items(), key=lambda x:x[1], reverse=True)
        self._ordered_small_room_list = sorted(self._small_room_dict.items(), key=lambda x:x[1], reverse=True)

    def show_ordered_level1_list(self):

        return self._ordered_level1_list

    def show_ordered_level2_list(self):

        return self._ordered_level2_list

    def show_ordered_level3_list(self):

        return self._ordered_level3_list

    def show_ordered_bonus_list(self):

        return self._ordered_bonus_list

    def show_ordered_small_room_list(self):

        return self._ordered_small_room_list



def main():

    root = tk.Tk()
    game = MarioApp(master=root)
    root.mainloop()

if __name__ == "__main__":

    main()

