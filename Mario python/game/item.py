"""
Classes to represent items dropped within the game world that players pickup.
"""

from game.entity import DynamicEntity

from player import Player


class DroppedItem(DynamicEntity):
    """An item dropped within the game world.

    Dropped items must implement the collect(Player) method to handle players
    picking up the items.
    """
    _id = None
    _type = 4

    def get_id(self) -> str:
        """(str) Returns the unique id of this block"""
        return self._id

    def collect(self, player: Player):
        """Collect method activated when a player collides with the item.

        Parameters:
            player (Player): The player which collided with the dropped item.
        """
        raise NotImplementedError("Should be overridden in a subclass")

    def __repr__(self):
        return f"{self.__class__.__name__}({self.get_id()})"


class Coin(DroppedItem):
    """A dropped coin item that can be picked up to increment the players score.
    """
    _id = "coin"

    def __init__(self, value: int = 1):
        """Construct a coin with a score value of value.

        Parameters:
            value (int): The value of the coin
        """
        super().__init__()
        self._value = value
        # set up a value to determine the image loading
        self._coin_render_value = 1
        # set up a step to increase
        self._step = 0

    def collect(self, player: Player):
        """Collect the coin and increment the players score by the coin value.

        Parameters:
            player (Player): The player which collided with the dropped item.
        """
        player.change_score(self._value)

    def step(self, time_delta: float, game_data):

        self._step += 1

        # need to use the if statement not the elif. It is a logic issue.
        if self._step >= 0:
            self._coin_render_value = 1
        if self._step >= 10:
            self._coin_render_value = 2
        if self._step >= 20:
            self._coin_render_value = 3
        if self._step >= 30:
            self._coin_render_value = 4

        if self._step == 40:
            self._step = 0

    def get_coin_render_value(self):

        return self._coin_render_value


