/* script */
$(document).ready(function() {
    
    // scroll event and callback for excuting the parallax and onscroll function and change the navbar when
    // scrolling down
    $(window).scroll(function() {

        if ($(window).width() >= 768) {
            parallax();
        }
        else {
            parallax2();
        }
        
        onscroll();

        var scrollPosition = $(document).scrollTop();
        if (scrollPosition > 100) {
            $(".navbar").addClass("version2");
        }
        else {
            $(".navbar").removeClass("version2");
        }

        $(".before_slide").each(function() {
            var elementPosition = $(this).offset().top;
            // We need to declare the scrollPosition again as it is within a new callback function.
            // The previous scrollPosition cannot be used here.
            var scrollPosition = $(document).scrollTop();

            if(elementPosition < scrollPosition + 600) {
                $(this).addClass("slide");
            }
        });
    });
    
    // for creating the parallax effect for desktop side
    function parallax() {
        var topScroll = $(window).scrollTop();
        $("section:nth-of-type(8)").css("background-position", "center " + ((topScroll-4900)*(0.5)) + "px");
    }

    // for creating the parallax effect for mobile side
    function parallax2() {
        var topScroll = $(window).scrollTop();
        $("section:nth-of-type(8)").css("background-position", "center " + ((topScroll-8900)*(0.5)) + "px");
    }

    // update the active state of the navigation
    function onscroll() {
        var scrollPosition = $(document).scrollTop();
        var aboutSectionDistance = $("#about").position().top - 70; // because the height of navbar is 70px
        var servicesSectionDistance = $("#services").position().top - 70;
        var portfolioSectionDistance = $("#portfolio").position().top - 70;
        var pricingSectionDistance = $("#pricing").position().top - 70;
        var contactSectionDistance = $("#contact").position().top - 70;

        if (scrollPosition >= aboutSectionDistance && scrollPosition <= servicesSectionDistance) {
            $("li").removeClass("active");
            $("li:first-of-type").addClass("active");
        }
        else if (scrollPosition >= servicesSectionDistance && scrollPosition <= portfolioSectionDistance) {
            $("li").removeClass("active");
            $("li:nth-of-type(2)").addClass("active");
        }
        else if (scrollPosition >= portfolioSectionDistance && scrollPosition <= pricingSectionDistance) {
            $("li").removeClass("active");
            $("li:nth-of-type(3)").addClass("active");
        }
        else if (scrollPosition >= pricingSectionDistance && scrollPosition <= contactSectionDistance) {
            $("li").removeClass("active");
            $("li:nth-of-type(4)").addClass("active");
        }
        else if (scrollPosition >= contactSectionDistance) {
            $("li").removeClass("active");
            $("li:last-of-type").addClass("active");
        }
        else {
            $("li").removeClass("active");
        }
    }

    // for hide the web_title when the dropdown menu is activited
    var flag = true;
    $(".navbar-toggle").click(function() {
        if (flag) {
            $(".web_title").addClass("hide");
            flag = false;
        }
        else {
            $(".web_title").removeClass("hide");
            flag = true;
        }
    });

});