package deco2800.thomas.trading;

import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ui.InventoryMenu;
import deco2800.thomas.items.AbstractItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles buying, selling, and currency management
 */
public class Trading {

    private final Logger LOG = LoggerFactory.getLogger(Trading.class);

    int money;
    InventoryAbstract inventory;

    public Trading(InventoryAbstract inventory) {
        this.money = 100;
        this.inventory = inventory;
    }

    /**
     * Sells the item from the inventory, adds the necessary amount of money
     * @param x
     * @param y
     */
    public void sell(int x, int y) {
        this.addMoney(inventory.getItem(x, y).getItemValue());
        inventory.dropInventory(x,y);
        try {
            inventory.getMenu().displayCurrency("$" + Integer.toString(getMoney()));
        } catch (Exception ignored){
            LOG.warn("Exception Thrown", ignored);
        }
    }

    /**
     * Adds the item to the inventory, removes the necessary amount of money
     *
     * @param item the item being bought
     */
    public void buy(AbstractItem item) {
        if (item.getItemValue() <= this.getMoney()) {
            inventory.addInventory(item);
            this.subtractMoney(item.getItemValue());
            try {
                inventory.getMenu().displayCurrency("$" + Integer.toString(getMoney()));
            } catch (Exception ignored) {
                LOG.warn("Exception Thrown", ignored);
            }
        }
    }

    /**
     * Set the amount of money the player has
     *
     * @param amount amount of money the player will have
     */
    public void setMoney(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        }
        this.money = amount;
    }

    /**
     * Adds the amount of money to the player
     *
     * @param amount amount of money to add
     */
    public void addMoney(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        }
        this.money += amount;
    }

    /**
     * Reduces the amount of money the player has
     *
     * @param amount amount of money to remove
     * @return whether the player has enough money to subtract
     */
    public boolean subtractMoney(int amount) {
        if (amount > this.money) {
            return false;
        }
        this.money -= amount;
        return true;
    }
    /**
     *  Returns the amount of money the player has
     *
     * @return the player's money count
     */
    public int getMoney() {
        return this.money;
    }

    /**
     * Updates the player's money when an item is sold
     *
     * @param x the column in the bag
     * @param y the row in the bag
     */
    public void updateMoneyOnSell(int x, int y) {
        money += inventory.getItem(x, y).getItemValue();
    }

    /**
     * Updates the player's money when an item is bought
     *
     * @param item the item being bought
     */
    public void updateMoneyOnBuy(AbstractItem item) {
        money -= item.getItemValue();
    }
}
