package deco2800.thomas.managers;

import com.badlogic.gdx.graphics.Texture;
import deco2800.thomas.worlds.WorldTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * Texture manager acts as a cache between the file system and the renderers.
 * This allows all textures to be read into memory at the start of the game saving
 * file reads from being completed during rendering.
 * <p>
 * With this in mind don't load textures you're not going to use.
 * Textures that are not used should probably (at some point) be removed
 * from the list and then read from disk when needed again using some type
 * of reference counting
 *
 * @author Tim Hadwen
 */
public class TextureManager extends AbstractManager {
	
	/**
	 * The width of the tile to use then positioning the tile.
	 */
	public static final int TILE_WIDTH = 320;
	
	/**
	 * The height of the tile to use when positioning the tile.
	 */
	public static final int TILE_HEIGHT = 320;

	
	//private final Logger log = LoggerFactory.getLogger(TextureManager.class);


    /**
     * A HashMap of all textures with string keys
     */
    private Map<String, Texture> textureMap = new HashMap<>();

    /**
     * Constructor
     * Currently loads up all the textures but probably shouldn't/doesn't
     * need to.
     */
    public TextureManager() {
        try {
            textureMap.put("background", new Texture("resources/background.jpg"));
            textureMap.put("spacman_ded", new Texture("resources/spacman.png"));
            textureMap.put("black_bg", new Texture("resources/black_bg.png"));
            // Some simple animations for changing positions
 
            textureMap.put("selection", new Texture("resources/square-select.png"));
            textureMap.put("path", new Texture("resources/yellow_selection.png"));
            
            
            textureMap.put("buildingB", new Texture("resources/building3x2.png"));
            
            textureMap.put("buildingA", new Texture("resources/buildingA.png"));
            textureMap.put("tree", new Texture("resources/tiles/option1_building_1.png"));
            textureMap.put("rock", new Texture("resources/tiles/option1_building_2.png"));

            // Add textures for the minimap
            textureMap.put("minimap", new Texture("resources/minimap/Mini-map3.png"));
            textureMap.put("miniMe", new Texture("resources/minimap/mini-map_blue enemy.png"));
            textureMap.put("miniWiz", new Texture("resources/minimap/witch_minimap.png"));
            textureMap.put("miniEnemy", new Texture("resources/minimap/mini-map_red enemy.png"));
            textureMap.put("miniTree", new Texture("resources/minimap/mini-map_grass item.png"));
            textureMap.put("miniRock", new Texture("resources/minimap/mini-map_rock.png"));
            textureMap.put("miniTeleport", new Texture("resources/minimap/door.png"));

            // Add textures for the Tutorial
            textureMap.put("tuteMan", new Texture("resources/NPC/tutorial NPC right.png"));
            textureMap.put("tute1", new Texture("resources/NPC/Game guide/tutorial1.png"));
            textureMap.put("tute2", new Texture("resources/NPC/Game guide/tutorial2.png"));
            textureMap.put("tute3", new Texture("resources/NPC/Game guide/tutorial 3.png"));
            textureMap.put("tute4", new Texture("resources/NPC/Game guide/tutorial 4.png"));
            textureMap.put("tute5", new Texture("resources/NPC/Game guide/tutorial 5.png"));
            textureMap.put("tute6", new Texture("resources/NPC/Game guide/tutorial 6.png"));
            textureMap.put("tute7", new Texture("resources/NPC/Game guide/tutorial 7.png"));
            textureMap.put("tute8", new Texture("resources/NPC/Game guide/tutorial 8.png"));
            textureMap.put("tute9", new Texture("resources/NPC/Game guide/tutorial 9.png"));
            textureMap.put("tute10", new Texture("resources/NPC/Game guide/tutorial 10.png"));
            textureMap.put("tute11", new Texture("resources/NPC/Game guide/tutorial 11.png"));
            textureMap.put("tute12", new Texture("resources/NPC/Game guide/tutorial12.png"));

            // NPC Teleporter
            textureMap.put("door0", new Texture("resources/minimap/door-frame-1.png"));
            textureMap.put("door1", new Texture("resources/minimap/door-frame-2.png"));
            textureMap.put("door2", new Texture("resources/minimap/door-frame-3.png"));
            textureMap.put("door3", new Texture("resources/minimap/door-frame-4.png"));
            textureMap.put("door4", new Texture("resources/minimap/door-frame-5.png"));

            // Add textures for the Traders
            textureMap.put("traderMan", new Texture("resources/NPC/trader_fixed_size.png"));
            textureMap.put("traderRoom", new Texture("resources/NPC/trader_background.png"));
            textureMap.put("trader0", new Texture("resources/NPC/Dialogue_Welcome.png"));
            textureMap.put("trader1", new Texture("resources/NPC/Dialogue_What.png"));
            textureMap.put("trader2", new Texture("resources/NPC/trader-talk-3.png"));

            // Add texture for the death ghost
            textureMap.put("ghost0", new Texture("resources/NPC/ghost-frame-1.gif"));
            textureMap.put("ghost1", new Texture("resources/NPC/ghost-frame-2.gif"));
            textureMap.put("ghost2", new Texture("resources/NPC/ghost-frame-3.gif"));
            textureMap.put("ghost3", new Texture("resources/NPC/ghost-frame-4.gif"));
            textureMap.put("ghost4", new Texture("resources/NPC/ghost-frame-5.gif"));
            textureMap.put("ghost-0", new Texture("resources/NPC/ghost_down.png"));
            textureMap.put("ghost-1", new Texture("resources/NPC/ghost_up.png"));
            textureMap.put("ghost-talk-1", new Texture("resources/NPC/ghost-talk1.png"));
            textureMap.put("ghostBG", new Texture("resources/NPC/image.png"));

            //Add texture for the game over screen
            textureMap.put("gameOver", new Texture("resources/gameOver.png"));

            // Add textures for the Chests
            textureMap.put("chest_close", new Texture("resources/items/Chest_Close.png"));
            textureMap.put("chest_open", new Texture("resources/items/Chest_Open.png"));

            // Add textures for the Orb Entity
            textureMap.put("orb_blue", new Texture("resources/items/Orb/Orb_Blue.png"));
            textureMap.put("orb_pink", new Texture("resources/items/Orb/Orb_Pink.png"));
            textureMap.put("orb_red", new Texture("resources/items/Orb/Orb_Red.png"));
            textureMap.put("orb_yellow", new Texture("resources/items/Orb/Orb_Yellow.png"));
            textureMap.put("orb_violet", new Texture("resources/items/Orb/Orb_Violet.png"));

            textureMap.put("bomb", new Texture("resources/items/Bomb3.png"));

            // Add textures for the getting orb background
            textureMap.put("bg_orb_blue", new Texture("resources/items/Orb/Get_Blue.png"));
            textureMap.put("bg_orb_pink", new Texture("resources/items/Orb/Get_Pink.png"));
            textureMap.put("bg_orb_red", new Texture("resources/items/Orb/Get_Red.png"));
            textureMap.put("bg_orb_yellow", new Texture("resources/items/Orb/Get_Yellow.png"));
            textureMap.put("bg_orb_violet", new Texture("resources/items/Orb/Get_Violet.png"));

            // textures for combat worlds
            textureMap.put("desert_background", new Texture("resources/Refined combat Scenes/refined_desert.png"));
            textureMap.put("lava_background", new Texture("resources/Combat Scenes Lava/lava.png"));
            textureMap.put("ice_background", new Texture("resources/ice combat scene/ice.png"));
            textureMap.put("city_background", new Texture("resources/Combat Scenes Cities/combat_city.png"));
            textureMap.put("magic_button", new Texture("resources/Combat UI elements/buttons/magical button.png"));
            textureMap.put("red_button", new Texture("resources/Combat UI elements/buttons/red_button.png"));
            textureMap.put("grey_button", new Texture("resources/Combat UI elements/buttons/grey_button.png"));
            textureMap.put("lilypad_desert", new Texture("resources/Refined combat Scenes/lilypad_1_resize2.png"));
            textureMap.put("lilypad_city", new Texture("resources/Combat Scenes Cities/lily_pad_city_resize.png"));
            textureMap.put("lilypad_ice", new Texture("resources/ice combat scene/ice LilyPad-lg.png"));
            textureMap.put("blank", new Texture("resources/Refined combat Scenes/blank.png"));
            textureMap.put("basic_panel", new Texture("resources/Combat UI elements/labels/basic_panel.png"));
            textureMap.put("hpmp_symbols", new Texture("resources/Combat UI elements/labels/health_ and_magic.png"));
            textureMap.put("world_name_panel", new Texture("resources/photoshop_file/backgorund2.png"));
            //textures for enemies
            textureMap.put("skeleton", new Texture("resources/enemy_04.png"));
            textureMap.put("spacesuit", new Texture("resources/enemy_version2_combat scenes/enemy03_outerworld.png"));
            textureMap.put("spacesuit_combat", new Texture("resources/enemy_version2_combat scenes/enemy03_combat screen.png"));

            textureMap.put("core-world", new Texture("resources/enemy_version3/enemy_06.png"));
            textureMap.put("core-battle", new Texture("resources/enemy_version3/enemy_06_3d.png"));

            textureMap.put("jelly-world", new Texture("resources/enemy_07.png"));
            textureMap.put("jelly-battle", new Texture("resources/enemy_version3/enemy_03_3d.png"));

            textureMap.put("cpu-world", new Texture("resources/enemy_10.png"));
            textureMap.put("cpu-battle", new Texture("resources/enemy_version3/enemy_05_3d.png"));

            textureMap.put("seeker-world", new Texture("resources/enemy_version3/Boss_03.png"));
            textureMap.put("seeker-battle", new Texture("resources/enemy_version3/Boss_03_3d.png"));

            //textures for knight and wizard
            textureMap.put("Knight1", new Texture("resources/PlayerCharacterMech/Knights N-01" +
                    ".png"));
            textureMap.put("Wizard1.1", new Texture("resources/PlayerCharacterMech/Witch N 1.png"));
            textureMap.put("Wizard1.2", new Texture("resources/PlayerCharacterMech/Witch N 2.png"));
            textureMap.put("Wizard2", new Texture("resources/PlayerCharacterMech/v1 Witch N 1" +
                    ".png"));
            textureMap.put("Knight2", new Texture("resources/player/knight/knight_idle.png"));
            textureMap.put("Wizard3", new Texture("resources/player/wizard/wizard_idle.png"));

            textureMap.put("knight2walking1", new Texture("resources/player/knight/knight_walk1" +
                    ".png"));
            textureMap.put("knight2walking2", new Texture("resources/player/knight/knight_walk2" +
                    ".png"));
            textureMap.put("knight2walking3", new Texture("resources/player/knight/knight_walk3" +
                    ".png"));

            textureMap.put("wizard3walking1", new Texture("resources/player/wizard/wizard_walk1" +
                    ".png"));
            textureMap.put("wizard3walking2", new Texture("resources/player/wizard/wizard_walk2" +
                    ".png"));
            textureMap.put("wizard3walking3", new Texture("resources/player/wizard/wizard_walk3" +
                    ".png"));

            //textures for Mech
            textureMap.put("mech1", new Texture("resources/player/mech/base_mech.png"));
            textureMap.put("mech2", new Texture("resources/player/mech/blue_mech.png"));
            textureMap.put("mech3", new Texture("resources/player/mech/green_mech.png"));
            textureMap.put("mech4", new Texture("resources/player/mech/red_mech.png"));
            textureMap.put("mech5", new Texture("resources/player/mech/orange_mech.png"));

            //textures for Teleported room
            textureMap.put("desk-1",new Texture("resources/desk-1.png"));
            textureMap.put("desk-2",new Texture("resources/desk-2.png"));
            textureMap.put("desk-3",new Texture("resources/desk-3.png"));
            textureMap.put("desk-4",new Texture("resources/desk-4.png"));
            textureMap.put("desk-5",new Texture("resources/desk-5.png"));
            textureMap.put("desk-6",new Texture("resources/desk-6.png"));
            textureMap.put("door-bottom-left",new Texture("resources/door-bottom-left.png"));
            textureMap.put("door-bottom-middle",new Texture("resources/door-bottom-middle.png"));
            textureMap.put("door-bottom-right",new Texture("resources/door-bottom-right.png"));
            textureMap.put("door-top-left",new Texture("resources/door-top-left.png"));
            textureMap.put("door-top-middle",new Texture("resources/door-top-middle.png"));
            textureMap.put("door-top-right",new Texture("resources/door-top-right.png"));
            textureMap.put("floor-tile",new Texture("resources/floor-tile.png"));
            textureMap.put("mainframe-1",new Texture("resources/mainframe-1.png"));
            textureMap.put("mainframe-2",new Texture("resources/mainframe-2.png"));
            textureMap.put("mainframe-3",new Texture("resources/mainframe-3.png"));
            textureMap.put("mainframe-4",new Texture("resources/mainframe-4.png"));
            textureMap.put("tele-blue-bottom-left",new Texture("resources/tele-blue-bottom-left.png"));
            textureMap.put("tele-blue-bottom-right",new Texture("resources/tele-blue-bottom-right.png"));
            textureMap.put("tele-blue-top-left",new Texture("resources/tele-blue-top-left.png"));
            textureMap.put("tele-blue-top-right",new Texture("resources/tele-blue-top-right.png"));
            textureMap.put("tele-green-bottom-left",new Texture("resources/tele-green-bottom-left.png"));
            textureMap.put("tele-green-bottom-right",new Texture("resources/tele-green-bottom-right.png"));
            textureMap.put("tele-green-top-left",new Texture("resources/tele-green-top-left.png"));
            textureMap.put("tele-green-top-right",new Texture("resources/tele-green-top-right.png"));
            textureMap.put("tele-yellow-bottom-left",new Texture("resources/tele-yellow-bottom-left.png"));
            textureMap.put("tele-yellow-bottom-right",new Texture("resources/tele-yellow-bottom-right.png"));
            textureMap.put("tele-yellow-top-left",new Texture("resources/tele-yellow-top-left.png"));
            textureMap.put("tele-yellow-top-right",new Texture("resources/tele-yello-top-right.png"));
            textureMap.put(WorldTypes.TELEPORTER.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.TUNDRA_DUNGEON.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.TEST.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.VOLCANO_BIOME.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.HUB_WORLD.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.HUB_WORLD_DUNGEON.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put(WorldTypes.FOREST_BIOME.toString(),new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put("teleporter-tile",new Texture("resources/teleporter-tile.png"));
            textureMap.put("wall-bottom",new Texture("resources/wall-bottom.png"));
            textureMap.put("wall-top",new Texture("resources/wall-top.png"));
            //textures for volcano bio
            textureMap.put("volcano_overallBasic",new Texture("resources/volcano_overallBasic.png"));
            textureMap.put("volcano_path_large",new Texture("resources/volcano_path_large.png"));
            textureMap.put("volcano_pathCorner_large",new Texture("resources/volcano_pathCorner_large.png"));
            textureMap.put("volcano_pathEdge_large",new Texture("resources/volcano_pathEdge_large.png"));
            textureMap.put("volcano_side",new Texture("resources/volcano_side.png"));
            textureMap.put("volcano dungeon",new Texture("resources/biomes/volcano/dungeon/volcano dungeon.png"));
            //Texture for Items
            //orb items
            textureMap.put("Orb_Blue", new Texture("resources/items/Orb/Orb_Blue.png"));
            textureMap.put("Orb_Pink", new Texture("resources/items/Orb/Orb_Pink.png"));
            textureMap.put("Orb_Red", new Texture("resources/items/Orb/Orb_Red.png"));
            textureMap.put("Orb_Violet", new Texture("resources/items/Orb/Orb_Violet.png"));
            textureMap.put("Orb_Yellow", new Texture("resources/items/Orb/Orb_Yellow.png"));

            //consumable items
            textureMap.put("Health_big", new Texture("resources/items/Consumable/Big_Health_Potion.png"));
            textureMap.put("Health_medium", new Texture("resources/items/Consumable/Medium_Health_Potion.png"));
            textureMap.put("Health_small", new Texture("resources/items/Consumable/Small_Health_Potion.png"));
            textureMap.put("Mana_big", new Texture("resources/items/Consumable/Big_Health_Potion.png"));
            textureMap.put("Mana_medium", new Texture("resources/items/Consumable/Medium_Health_Potion.png"));
            textureMap.put("Mana_small", new Texture("resources/items/Consumable/Small_Health_Potion.png"));
            textureMap.put("Tp_scroll", new Texture("resources/items/Consumable/Teleport_Scroll.png"));

            //equipment items
            textureMap.put("Sword_wood", new Texture("resources/items/Equipment/Sword_wood.png"));
            textureMap.put("Sword_iron", new Texture("resources/items/Equipment/Sword_iron.png"));
            textureMap.put("Bow_wood", new Texture("resources/items/Equipment/Bow_wood.png"));
            textureMap.put("Bow_iron", new Texture("resources/items/Equipment/Bow_iron.png"));

            //Material items
            textureMap.put("Wood", new Texture("resources/items/Material/Wood.png"));
            textureMap.put("Iron", new Texture("resources/items/Material/Iron.png"));
            textureMap.put("Gold", new Texture("resources/items/Material/Gold.png"));
            textureMap.put("Diamond", new Texture("resources/items/Material/Diamond.png"));

            //Add teleport circle
            textureMap.put("teleport_circle", new Texture("resources/items/TP.png"));

            textureMap.put("tmpBackground", new Texture("resources/Main Menu Designs/Main Menu.png"));

            //Hub world
            textureMap.put("hubworld-base",new Texture("resources/HubWorld/hubworld-base.png"));
            textureMap.put("crossroad-NE",new Texture("resources/HubWorld/crossroad-NE.png"));
            textureMap.put("crossroad-NW",new Texture("resources/HubWorld/crossroad-NW.png"));
            textureMap.put("crossroad-SE",new Texture("resources/HubWorld/crossroad-SE.png"));
            textureMap.put("crossroad-SW",new Texture("resources/HubWorld/crossroad-SW.png"));
            textureMap.put("curb-NE",new Texture("resources/HubWorld/curb-NE.png"));
            textureMap.put("curb-NW",new Texture("resources/HubWorld/curb-NW.png"));
            textureMap.put("curb-SE",new Texture("resources/HubWorld/curb-SE.png"));
            textureMap.put("curb-side",new Texture("resources/HubWorld/curb-side.png"));
            textureMap.put("curb-SW",new Texture("resources/HubWorld/curb-SW.png"));
            textureMap.put("curb-up",new Texture("resources/HubWorld/curb-up.png"));
            textureMap.put("gate-1-1",new Texture("resources/HubWorld/gate-1-1.png"));
            textureMap.put("gate-1-2",new Texture("resources/HubWorld/gate-1-2.png"));
            textureMap.put("gate-1-3",new Texture("resources/HubWorld/gate-1-3.png"));
            textureMap.put("gate-1-4",new Texture("resources/HubWorld/gate-1-4.png"));
            textureMap.put("gate-1-5",new Texture("resources/HubWorld/gate-1-5.png"));
            textureMap.put("gate-1-6",new Texture("resources/HubWorld/gate-1-6.png"));
            textureMap.put("gate-2-1",new Texture("resources/HubWorld/gate-2-1.png"));
            textureMap.put("gate-2-2",new Texture("resources/HubWorld/gate-2-2.png"));
            textureMap.put("gate-2-3",new Texture("resources/HubWorld/gate-2-3.png"));
            textureMap.put("gate-2-4",new Texture("resources/HubWorld/gate-2-4.png"));
            textureMap.put("gate-2-5",new Texture("resources/HubWorld/gate-2-5.png"));
            textureMap.put("gate-2-6",new Texture("resources/HubWorld/gate-2-6.png"));
            textureMap.put("gate-3-1",new Texture("resources/HubWorld/gate-3-1.png"));
            textureMap.put("gate-3-2",new Texture("resources/HubWorld/gate-3-2.png"));
            textureMap.put("gate-3-3",new Texture("resources/HubWorld/gate-3-3.png"));
            textureMap.put("gate-3-4",new Texture("resources/HubWorld/gate-3-4.png"));
            textureMap.put("gate-3-5",new Texture("resources/HubWorld/gate-3-5.png"));
            textureMap.put("gate-3-6",new Texture("resources/HubWorld/gate-3-6.png"));
            textureMap.put("rear-wall",new Texture("resources/HubWorld/rear-wall.png"));
            textureMap.put("road-bottom",new Texture("resources/HubWorld/road-bottom.png"));
            textureMap.put("road-left",new Texture("resources/HubWorld/road-left.png"));
            textureMap.put("road-right",new Texture("resources/HubWorld/road-right.png"));
            textureMap.put("road-top",new Texture("resources/HubWorld/road-top.png"));
            textureMap.put("roof-generic-left",new Texture("resources/HubWorld/roof-generic-left.png"));
            textureMap.put("roof-generic-right",new Texture("resources/HubWorld/roof-generic-right.png"));
            textureMap.put("roof-generic-triangle-bottom",new Texture("resources/HubWorld/roof-generic-triangle-bottom.png"));
            textureMap.put("roof-generic-triangle-top",new Texture("resources/HubWorld/roof-generic-triangle-top.png"));
            textureMap.put("window-generic",new Texture("resources/HubWorld/window-generic.png"));
            textureMap.put("boss-world", new Texture("resources/enemy_version3/Boss_04.png"));
            textureMap.put("boss-battle", new Texture("resources/enemy_version3/Boss_04_3d.png"));
            textureMap.put("robot-world", new Texture("resources/enemy_version2_combat scenes/enemy02_outerworld.png"));
            textureMap.put("robot-battle", new Texture("resources/enemy_version3/enemy_01_3d.png"));
            textureMap.put("enemy-world", new Texture("resources/enemy_version2_combat scenes/enemy01_outerworld.png"));
            textureMap.put("enemy-battle", new Texture("resources/enemy_version2_combat scenes/enemy01_combat screen.png"));

            //Add volcano dungeon
            textureMap.put("volcano_lava_large",new Texture("resources/biomes/volcano/dungeon/volcano_lava_large.png"));
            textureMap.put("volcano_dungeon_normal",new Texture("resources/biomes/volcano/dungeon/volcano_dungeon_normal.png"));
            textureMap.put("volcano_dungeon_Rockwall",new Texture("resources/biomes/volcano/dungeon/volcano_dungeon_Rockwall.png"));
            textureMap.put("bridge_vertical",new Texture("resources/biomes/volcano/dungeon/bridge_vertical.png"));
            textureMap.put("bridge_horizontal",new Texture("resources/biomes/volcano/dungeon/bridge_horizontal.png"));
            textureMap.put("Transparent-tile",new Texture("resources/biomes/volcano/dungeon/Transparent-tile.png"));
            textureMap.put("health_pool_bLeft",new Texture("resources/biomes/volcano/dungeon/health_pool_bLeft.png"));
            textureMap.put("health_pool_bMid",new Texture("resources/biomes/volcano/dungeon/health_pool_bMid.png"));
            textureMap.put("health_pool_bRight",new Texture("resources/biomes/volcano/dungeon/health_pool_bRight.png"));
            textureMap.put("health_pool_mLeft",new Texture("resources/biomes/volcano/dungeon/health_pool_mLeft.png"));
            textureMap.put("health_pool_mMid",new Texture("resources/biomes/volcano/dungeon/health_pool_mMid.png"));
            textureMap.put("health_pool_mRight",new Texture("resources/biomes/volcano/dungeon/health_pool_mRight.png"));
            textureMap.put("health_pool_tLeft",new Texture("resources/biomes/volcano/dungeon/health_pool_tLeft.png"));
            textureMap.put("health_pool_tMid",new Texture("resources/biomes/volcano/dungeon/health_pool_tMid.png"));
            textureMap.put("health_pool_tRight",new Texture("resources/biomes/volcano/dungeon/health_pool_tRight.png"));
            textureMap.put("mana_pool_bLeft",new Texture("resources/biomes/volcano/dungeon/mana_pool_bLeft.png"));
            textureMap.put("mana_pool_bMid",new Texture("resources/biomes/volcano/dungeon/mana_pool_bMid.png"));
            textureMap.put("mana_pool_bRight",new Texture("resources/biomes/volcano/dungeon/mana_pool_bRight.png"));
            textureMap.put("mana_pool_mLeft",new Texture("resources/biomes/volcano/dungeon/mana_pool_mLeft.png"));
            textureMap.put("mana_pool_mMid",new Texture("resources/biomes/volcano/dungeon/mana_pool_mMid.png"));
            textureMap.put("mana_pool_mRight",new Texture("resources/biomes/volcano/dungeon/mana_pool_mRight.png"));
            textureMap.put("mana_pool_tLeft",new Texture("resources/biomes/volcano/dungeon/mana_pool_tLeft.png"));
            textureMap.put("mana_pool_tMid",new Texture("resources/biomes/volcano/dungeon/mana_pool_tMid.png"));
            textureMap.put("mana_pool_tRight",new Texture("resources/biomes/volcano/dungeon/mana_pool_tRight.png"));

            //hubWorld
            textureMap.put("crossroad-NE.png",new Texture("resources/HubWorld/crossroad-NE.png"));
            textureMap.put("crossroad-NW.png",new Texture("resources/HubWorld/crossroad-NW.png"));
            textureMap.put("crossroad-SE.png",new Texture("resources/HubWorld/crossroad-SE.png"));
            textureMap.put("crossroad-SW.png",new Texture("resources/HubWorld/crossroad-SW.png"));
            textureMap.put("curb-NE.png",new Texture("resources/HubWorld/curb-NE.png"));
            textureMap.put("curb-NW.png",new Texture("resources/HubWorld/curb-NW.png"));
            textureMap.put("curb-SE.png",new Texture("resources/HubWorld/curb-SE.png"));
            textureMap.put("curb-side.png",new Texture("resources/HubWorld/curb-side.png"));
            textureMap.put("curb-SW.png",new Texture("resources/HubWorld/curb-SW.png"));
            textureMap.put("curb-up.png",new Texture("resources/HubWorld/curb-up.png"));
            textureMap.put("gate-1-1.png",new Texture("resources/HubWorld/gate-1-1.png"));
            textureMap.put("gate-1-2.png",new Texture("resources/HubWorld/gate-1-2.png"));
            textureMap.put("gate-1-3.png",new Texture("resources/HubWorld/gate-1-3.png"));
            textureMap.put("gate-1-4.png",new Texture("resources/HubWorld/gate-1-4.png"));
            textureMap.put("gate-1-5.png",new Texture("resources/HubWorld/gate-1-5.png"));
            textureMap.put("gate-1-6.png",new Texture("resources/HubWorld/gate-1-6.png"));
            textureMap.put("gate-2-1.png",new Texture("resources/HubWorld/gate-2-1.png"));
            textureMap.put("gate-2-2.png",new Texture("resources/HubWorld/gate-2-2.png"));
            textureMap.put("gate-2-3.png",new Texture("resources/HubWorld/gate-2-3.png"));
            textureMap.put("gate-2-4.png",new Texture("resources/HubWorld/gate-2-4.png"));
            textureMap.put("gate-2-5.png",new Texture("resources/HubWorld/gate-2-5.png"));
            textureMap.put("gate-2-6.png",new Texture("resources/HubWorld/gate-2-6.png"));
            textureMap.put("gate-3-1.png",new Texture("resources/HubWorld/gate-3-1.png"));
            textureMap.put("gate-3-2.png",new Texture("resources/HubWorld/gate-3-2.png"));
            textureMap.put("gate-3-3.png",new Texture("resources/HubWorld/gate-3-3.png"));
            textureMap.put("gate-3-4.png",new Texture("resources/HubWorld/gate-3-4.png"));
            textureMap.put("gate-3-5.png",new Texture("resources/HubWorld/gate-3-5.png"));
            textureMap.put("gate-3-6.png",new Texture("resources/HubWorld/gate-3-6.png"));
            textureMap.put("hubworld-base.png",new Texture("resources/HubWorld/hubworld-base.png"));
            textureMap.put("rear-wall.png",new Texture("resources/HubWorld/rear-wall.png"));
            textureMap.put("road-bottom.png",new Texture("resources/HubWorld/road-bottom.png"));
            textureMap.put("road-left.png",new Texture("resources/HubWorld/road-left.png"));
            textureMap.put("road-right.png",new Texture("resources/HubWorld/road-right.png"));
            textureMap.put("road-top.png",new Texture("resources/HubWorld/road-top.png"));
            textureMap.put("roof-generic-left.png",new Texture("resources/HubWorld/roof-generic-left.png"));
            textureMap.put("roof-generic-right.png",new Texture("resources/HubWorld/roof-generic-right.png"));
            textureMap.put("roof-generic-triangle-bottom.png",new Texture("resources/HubWorld/roof-generic-triangle-bottom.png"));
            textureMap.put("roof-generic-triangle-top.png",new Texture("resources/HubWorld/roof-generic-triangle-top.png"));
            textureMap.put("window-generic.png",new Texture("resources/HubWorld/window-generic.png"));
            textureMap.put("Gus assets/back1-roof1.png",new Texture("resources/HubWorld/Gus assets/back1-roof1.png"));
            textureMap.put("Gus assets/back1-roof2.png",new Texture("resources/HubWorld/Gus assets/back1-roof2.png"));
            textureMap.put("Gus assets/back1-roof3.png",new Texture("resources/HubWorld/Gus assets/back1-roof3.png"));
            textureMap.put("Gus assets/back1-topleft.png",new Texture("resources/HubWorld/Gus assets/back1-topleft.png"));
            textureMap.put("Gus assets/back1-topmid.png",new Texture("resources/HubWorld/Gus assets/back1-topmid.png"));
            textureMap.put("Gus assets/back1-topright.png",new Texture("resources/HubWorld/Gus assets/back1-topright.png"));
            textureMap.put("Gus assets/back1-wallleft.png",new Texture("resources/HubWorld/Gus assets/back1-wallleft.png"));
            textureMap.put("Gus assets/back1-wallmid.png",new Texture("resources/HubWorld/Gus assets/back1-wallmid.png"));
            textureMap.put("Gus assets/back1-wallright.png",new Texture("resources/HubWorld/Gus assets/back1-wallright.png"));
            textureMap.put("Gus assets/back1-windowleft.png",new Texture("resources/HubWorld/Gus assets/back1-windowleft.png"));
            textureMap.put("Gus assets/back1-windowmid.png",new Texture("resources/HubWorld/Gus assets/back1-windowmid.png"));
            textureMap.put("Gus assets/back1-windowright.png",new Texture("resources/HubWorld/Gus assets/back1-windowright.png"));
            textureMap.put("Gus assets/back2-lightleft.png",new Texture("resources/HubWorld/Gus assets/back2-lightleft.png"));
            textureMap.put("Gus assets/back2-lightmid.png",new Texture("resources/HubWorld/Gus assets/back2-lightmid.png"));
            textureMap.put("Gus assets/back2-roof1.png",new Texture("resources/HubWorld/Gus assets/back2-roof1.png"));
            textureMap.put("Gus assets/back2-roof2.png",new Texture("resources/HubWorld/Gus assets/back2-roof2.png"));
            textureMap.put("Gus assets/back2-roof3.png",new Texture("resources/HubWorld/Gus assets/back2-roof3.png"));
            textureMap.put("Gus assets/back2-roof4.png",new Texture("resources/HubWorld/Gus assets/back2-roof4.png"));
            textureMap.put("Gus assets/back2-roof5.png",new Texture("resources/HubWorld/Gus assets/back2-roof5.png"));
            textureMap.put("Gus assets/back2-roof6.png",new Texture("resources/HubWorld/Gus assets/back2-roof6.png"));
            textureMap.put("Gus assets/back2-wall-left.png",new Texture("resources/HubWorld/Gus assets/back2-wall-left.png"));
            textureMap.put("Gus assets/back2-wallmid.png",new Texture("resources/HubWorld/Gus assets/back2-wallmid.png"));
            textureMap.put("Gus assets/back2-wallright.png",new Texture("resources/HubWorld/Gus assets/back2-wallright.png"));
            textureMap.put("Gus assets/back3-roof1.png",new Texture("resources/HubWorld/Gus assets/back3-roof1.png"));
            textureMap.put("Gus assets/back3-roof2.png",new Texture("resources/HubWorld/Gus assets/back3-roof2.png"));
            textureMap.put("Gus assets/back3-roof3.png",new Texture("resources/HubWorld/Gus assets/back3-roof3.png"));
            textureMap.put("Gus assets/back3-windowleft.png",new Texture("resources/HubWorld/Gus assets/back3-windowleft.png"));
            textureMap.put("Gus assets/back3-windowmid.png",new Texture("resources/HubWorld/Gus assets/back3-windowmid.png"));
            textureMap.put("Gus assets/back3-windowright.png",new Texture("resources/HubWorld/Gus assets/back3-windowright.png"));
            textureMap.put("Gus assets/back4-roof1.png",new Texture("resources/HubWorld/Gus assets/back4-roof1.png"));
            textureMap.put("Gus assets/back4-roof2.png",new Texture("resources/HubWorld/Gus assets/back4-roof2.png"));
            textureMap.put("Gus assets/back4-roof3.png",new Texture("resources/HubWorld/Gus assets/back4-roof3.png"));
            textureMap.put("Gus assets/back4-windowleft.png",new Texture("resources/HubWorld/Gus assets/back4-windowleft.png"));
            textureMap.put("Gus assets/back4-windowmid.png",new Texture("resources/HubWorld/Gus assets/back4-windowmid.png"));
            textureMap.put("Gus assets/back4-windowright.png",new Texture("resources/HubWorld/Gus assets/back4-windowright.png"));
            textureMap.put("Gus assets/crafts-doorleft.png",new Texture("resources/HubWorld/Gus assets/crafts-doorleft.png"));
            textureMap.put("Gus assets/crafts-doorright.png",new Texture("resources/HubWorld/Gus assets/crafts-doorright.png"));
            textureMap.put("Gus assets/crafts-roof1.png",new Texture("resources/HubWorld/Gus assets/crafts-roof1.png"));
            textureMap.put("Gus assets/crafts-roof2.png",new Texture("resources/HubWorld/Gus assets/crafts-roof2.png"));
            textureMap.put("Gus assets/crafts-roof3.png",new Texture("resources/HubWorld/Gus assets/crafts-roof3.png"));
            textureMap.put("Gus assets/crafts-sign1.png",new Texture("resources/HubWorld/Gus assets/crafts-sign1.png"));
            textureMap.put("Gus assets/crafts-sign2.png",new Texture("resources/HubWorld/Gus assets/crafts-sign2.png"));
            textureMap.put("Gus assets/crafts-sign3.png",new Texture("resources/HubWorld/Gus assets/crafts-sign3.png"));
            textureMap.put("Gus assets/crafts-sign4.png",new Texture("resources/HubWorld/Gus assets/crafts-sign4.png"));
            textureMap.put("Gus assets/crafts-wallleft.png",new Texture("resources/HubWorld/Gus assets/crafts-wallleft.png"));
            textureMap.put("Gus assets/crafts-wallright.png",new Texture("resources/HubWorld/Gus assets/crafts-wallright.png"));
            textureMap.put("Gus assets/crafts-windowleft.png",new Texture("resources/HubWorld/Gus assets/crafts-windowleft.png"));
            textureMap.put("Gus assets/crafts-windowmid.png",new Texture("resources/HubWorld/Gus assets/crafts-windowmid.png"));
            textureMap.put("Gus assets/crafts-windowright.png",new Texture("resources/HubWorld/Gus assets/crafts-windowright.png"));
            textureMap.put("Gus assets/front1-door.png",new Texture("resources/HubWorld/Gus assets/front1-door.png"));
            textureMap.put("Gus assets/front1-long-bottomleft.png",new Texture("resources/HubWorld/Gus assets/front1-long-bottomleft.png"));
            textureMap.put("Gus assets/front1-long-bottommid.png",new Texture("resources/HubWorld/Gus assets/front1-long-bottommid.png"));
            textureMap.put("Gus assets/front1-long-bottomright.png",new Texture("resources/HubWorld/Gus assets/front1-long-bottomright.png"));
            textureMap.put("Gus assets/front1-long-topleft.png",new Texture("resources/HubWorld/Gus assets/front1-long-topleft.png"));
            textureMap.put("Gus assets/front1-long-topmid.png",new Texture("resources/HubWorld/Gus assets/front1-long-topmid.png"));
            textureMap.put("Gus assets/front1-long-topright.png",new Texture("resources/HubWorld/Gus assets/front1-long-topright.png"));
            textureMap.put("Gus assets/front1-roof1.png",new Texture("resources/HubWorld/Gus assets/front1-roof1.png"));
            textureMap.put("Gus assets/front1-roof2.png",new Texture("resources/HubWorld/Gus assets/front1-roof2.png"));
            textureMap.put("Gus assets/front1-roof3.png",new Texture("resources/HubWorld/Gus assets/front1-roof3.png"));
            textureMap.put("Gus assets/front1-wall-left.png",new Texture("resources/HubWorld/Gus assets/front1-wall-left.png"));
            textureMap.put("Gus assets/front1-wall-mid.png",new Texture("resources/HubWorld/Gus assets/front1-wall-mid.png"));
            textureMap.put("Gus assets/front1-wall-right.png",new Texture("resources/HubWorld/Gus assets/front1-wall-right.png"));
            textureMap.put("Gus assets/front1-window-left.png",new Texture("resources/HubWorld/Gus assets/front1-window-left.png"));
            textureMap.put("Gus assets/front1-window-mid.png",new Texture("resources/HubWorld/Gus assets/front1-window-mid.png"));
            textureMap.put("Gus assets/front1-window-right.png",new Texture("resources/HubWorld/Gus assets/front1-window-right.png"));
            textureMap.put("Gus assets/front2-door.png",new Texture("resources/HubWorld/Gus assets/front2-door.png"));
            textureMap.put("Gus assets/front2-roof1.png",new Texture("resources/HubWorld/Gus assets/front2-roof1.png"));
            textureMap.put("Gus assets/front2-roof2.png",new Texture("resources/HubWorld/Gus assets/front2-roof2.png"));
            textureMap.put("Gus assets/front2-roof3.png",new Texture("resources/HubWorld/Gus assets/front2-roof3.png"));
            textureMap.put("Gus assets/front2-wall-left.png",new Texture("resources/HubWorld/Gus assets/front2-wall-left.png"));
            textureMap.put("Gus assets/front2-wall-mid.png",new Texture("resources/HubWorld/Gus assets/front2-wall-mid.png"));
            textureMap.put("Gus assets/front2-wall-right.png",new Texture("resources/HubWorld/Gus assets/front2-wall-right.png"));
            textureMap.put("Gus assets/front2-window-left.png",new Texture("resources/HubWorld/Gus assets/front2-window-left.png"));
            textureMap.put("Gus assets/front2-window-mid.png",new Texture("resources/HubWorld/Gus assets/front2-window-mid.png"));
            textureMap.put("Gus assets/front2-window-right.png",new Texture("resources/HubWorld/Gus assets/front2-window-right.png"));
            textureMap.put("krish-assets/blue-building-1.png",new Texture("resources/HubWorld/krish-assets/blue-building-1.png"));
            textureMap.put("krish-assets/blue-building-1--01.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--01.png"));
            textureMap.put("krish-assets/blue-building-1--02.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--02.png"));
            textureMap.put("krish-assets/blue-building-1--03.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--03.png"));
            textureMap.put("krish-assets/blue-building-1--04.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--04.png"));
            textureMap.put("krish-assets/blue-building-1--05.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--05.png"));
            textureMap.put("krish-assets/blue-building-1--06.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--06.png"));
            textureMap.put("krish-assets/blue-building-1--07.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--07.png"));
            textureMap.put("krish-assets/blue-building-1--08.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--08.png"));
            textureMap.put("krish-assets/blue-building-1--09.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--09.png"));
            textureMap.put("krish-assets/blue-building-1--10.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--10.png"));
            textureMap.put("krish-assets/blue-building-1--11.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--11.png"));
            textureMap.put("krish-assets/blue-building-1--12.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--12.png"));
            textureMap.put("krish-assets/blue-building-1--13.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--13.png"));
            textureMap.put("krish-assets/blue-building-1--14.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--14.png"));
            textureMap.put("krish-assets/blue-building-1--15.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--15.png"));
            textureMap.put("krish-assets/blue-building-1--16.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--16.png"));
            textureMap.put("krish-assets/blue-building-1--17.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--17.png"));
            textureMap.put("krish-assets/blue-building-1--18.png",new Texture("resources/HubWorld/krish-assets/blue-building-1--18.png"));
            textureMap.put("krish-assets/green-building-1.png",new Texture("resources/HubWorld/krish-assets/green-building-1.png"));
            textureMap.put("krish-assets/green-building-1---01.png",new Texture("resources/HubWorld/krish-assets/green-building-1---01.png"));
            textureMap.put("krish-assets/green-building-1---02.png",new Texture("resources/HubWorld/krish-assets/green-building-1---02.png"));
            textureMap.put("krish-assets/green-building-1---03.png",new Texture("resources/HubWorld/krish-assets/green-building-1---03.png"));
            textureMap.put("krish-assets/green-building-1---04.png",new Texture("resources/HubWorld/krish-assets/green-building-1---04.png"));
            textureMap.put("krish-assets/green-building-1---05.png",new Texture("resources/HubWorld/krish-assets/green-building-1---05.png"));
            textureMap.put("krish-assets/green-building-1---06.png",new Texture("resources/HubWorld/krish-assets/green-building-1---06.png"));
            textureMap.put("krish-assets/green-building-1---07.png",new Texture("resources/HubWorld/krish-assets/green-building-1---07.png"));
            textureMap.put("krish-assets/green-building-1---08.png",new Texture("resources/HubWorld/krish-assets/green-building-1---08.png"));
            textureMap.put("krish-assets/green-building-1---09.png",new Texture("resources/HubWorld/krish-assets/green-building-1---09.png"));
            textureMap.put("krish-assets/green-building-2.png",new Texture("resources/HubWorld/krish-assets/green-building-2.png"));
            textureMap.put("krish-assets/green-building-2---01.png",new Texture("resources/HubWorld/krish-assets/green-building-2---01.png"));
            textureMap.put("krish-assets/green-building-2---02.png",new Texture("resources/HubWorld/krish-assets/green-building-2---02.png"));
            textureMap.put("krish-assets/green-building-2---03.png",new Texture("resources/HubWorld/krish-assets/green-building-2---03.png"));
            textureMap.put("krish-assets/green-building-2---04.png",new Texture("resources/HubWorld/krish-assets/green-building-2---04.png"));
            textureMap.put("krish-assets/green-building-2---05.png",new Texture("resources/HubWorld/krish-assets/green-building-2---05.png"));
            textureMap.put("krish-assets/green-building-2---06.png",new Texture("resources/HubWorld/krish-assets/green-building-2---06.png"));
            textureMap.put("krish-assets/green-building-2---07.png",new Texture("resources/HubWorld/krish-assets/green-building-2---07.png"));
            textureMap.put("krish-assets/green-building-2---08.png",new Texture("resources/HubWorld/krish-assets/green-building-2---08.png"));
            textureMap.put("krish-assets/green-building-2---09.png",new Texture("resources/HubWorld/krish-assets/green-building-2---09.png"));
            textureMap.put("krish-assets/green-building-2---10.png",new Texture("resources/HubWorld/krish-assets/green-building-2---10.png"));
            textureMap.put("krish-assets/green-building-2---11.png",new Texture("resources/HubWorld/krish-assets/green-building-2---11.png"));
            textureMap.put("krish-assets/green-building-2---12.png",new Texture("resources/HubWorld/krish-assets/green-building-2---12.png"));
            textureMap.put("krish-assets/green-building-2---13.png",new Texture("resources/HubWorld/krish-assets/green-building-2---13.png"));
            textureMap.put("krish-assets/green-building-2---14.png",new Texture("resources/HubWorld/krish-assets/green-building-2---14.png"));
            textureMap.put("krish-assets/green-building-2---15.png",new Texture("resources/HubWorld/krish-assets/green-building-2---15.png"));
            textureMap.put("krish-assets/green-building-2---16.png",new Texture("resources/HubWorld/krish-assets/green-building-2---16.png"));
            textureMap.put("krish-assets/green-building-2---17.png",new Texture("resources/HubWorld/krish-assets/green-building-2---17.png"));
            textureMap.put("krish-assets/green-building-2---18.png",new Texture("resources/HubWorld/krish-assets/green-building-2---18.png"));
            textureMap.put("krish-assets/pink-building-1.jpg",new Texture("resources/HubWorld/krish-assets/pink-building-1.jpg"));
            textureMap.put("krish-assets/pink-building-1---01.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---01.png"));
            textureMap.put("krish-assets/pink-building-1---02.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---02.png"));
            textureMap.put("krish-assets/pink-building-1---03.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---03.png"));
            textureMap.put("krish-assets/pink-building-1---04.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---04.png"));
            textureMap.put("krish-assets/pink-building-1---05.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---05.png"));
            textureMap.put("krish-assets/pink-building-1---06.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---06.png"));
            textureMap.put("krish-assets/pink-building-1---07.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---07.png"));
            textureMap.put("krish-assets/pink-building-1---08.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---08.png"));
            textureMap.put("krish-assets/pink-building-1---09.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---09.png"));
            textureMap.put("krish-assets/pink-building-1---10.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---10.png"));
            textureMap.put("krish-assets/pink-building-1---11.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---11.png"));
            textureMap.put("krish-assets/pink-building-1---12.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---12.png"));
            textureMap.put("krish-assets/pink-building-1---13.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---13.png"));
            textureMap.put("krish-assets/pink-building-1---14.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---14.png"));
            textureMap.put("krish-assets/pink-building-1---15.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---15.png"));
            textureMap.put("krish-assets/pink-building-1---16.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---16.png"));
            textureMap.put("krish-assets/pink-building-1---17.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---17.png"));
            textureMap.put("krish-assets/pink-building-1---18.png",new Texture("resources/HubWorld/krish-assets/pink-building-1---18.png"));
            textureMap.put("krish-assets/pink-building-2.png",new Texture("resources/HubWorld/krish-assets/pink-building-2.png"));
            textureMap.put("krish-assets/pink-building-2---03.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---03.png"));
            textureMap.put("krish-assets/pink-building-2---04.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---04.png"));
            textureMap.put("krish-assets/pink-building-2---05.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---05.png"));
            textureMap.put("krish-assets/pink-building-2---06.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---06.png"));
            textureMap.put("krish-assets/pink-building-2---07.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---07.png"));
            textureMap.put("krish-assets/pink-building-2---08.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---08.png"));
            textureMap.put("krish-assets/pixel_city_scape-upscaled.png",new Texture("resources/HubWorld/krish-assets/pixel_city_scape-upscaled.png"));
            textureMap.put("krish-assets/red-building-1.png",new Texture("resources/HubWorld/krish-assets/red-building-1.png"));
            textureMap.put("krish-assets/red-building-1--01.png",new Texture("resources/HubWorld/krish-assets/red-building-1--01.png"));
            textureMap.put("krish-assets/red-building-1--02.png",new Texture("resources/HubWorld/krish-assets/red-building-1--02.png"));
            textureMap.put("krish-assets/red-building-1--03.png",new Texture("resources/HubWorld/krish-assets/red-building-1--03.png"));
            textureMap.put("krish-assets/red-building-1--04.png",new Texture("resources/HubWorld/krish-assets/red-building-1--04.png"));
            textureMap.put("krish-assets/red-building-1--05.png",new Texture("resources/HubWorld/krish-assets/red-building-1--05.png"));
            textureMap.put("krish-assets/red-building-1--06.png",new Texture("resources/HubWorld/krish-assets/red-building-1--06.png"));
            textureMap.put("krish-assets/red-building-2.png",new Texture("resources/HubWorld/krish-assets/red-building-2.png"));
            textureMap.put("krish-assets/red-building-2--01.png",new Texture("resources/HubWorld/krish-assets/red-building-2--01.png"));
            textureMap.put("krish-assets/red-building-2--02.png",new Texture("resources/HubWorld/krish-assets/red-building-2--02.png"));
            textureMap.put("krish-assets/red-building-2--03.png",new Texture("resources/HubWorld/krish-assets/red-building-2--03.png"));
            textureMap.put("krish-assets/red-building-2--04.png",new Texture("resources/HubWorld/krish-assets/red-building-2--04.png"));
            textureMap.put("krish-assets/red-building-2--05.png",new Texture("resources/HubWorld/krish-assets/red-building-2--05.png"));
            textureMap.put("krish-assets/red-building-2--06.png",new Texture("resources/HubWorld/krish-assets/red-building-2--06.png"));
            textureMap.put("krish-assets/red-building-2--07.png",new Texture("resources/HubWorld/krish-assets/red-building-2--07.png"));
            textureMap.put("krish-assets/red-building-2--08.png",new Texture("resources/HubWorld/krish-assets/red-building-2--08.png"));
            textureMap.put("krish-assets/red-building-2--09.png",new Texture("resources/HubWorld/krish-assets/red-building-2--09.png"));
            textureMap.put("krish-assets/red-building-2--10.png",new Texture("resources/HubWorld/krish-assets/red-building-2--10.png"));
            textureMap.put("Mark assets/blue-roof-angled-bottom-left.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-bottom-left.png"));
            textureMap.put("Mark assets/blue-roof-angled-bottom-mid.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-bottom-mid.png"));
            textureMap.put("Mark assets/blue-roof-angled-bottom-right.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-bottom-right.png"));
            textureMap.put("Mark assets/blue-roof-angled-left-top.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-left-top.png"));
            textureMap.put("Mark assets/blue-roof-angled-top-mid.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-top-mid.png"));
            textureMap.put("Mark assets/blue-roof-angled-top-right.png",new Texture("resources/HubWorld/Mark assets/blue-roof-angled-top-right.png"));
            textureMap.put("Mark assets/blue-roof-left.png",new Texture("resources/HubWorld/Mark assets/blue-roof-left.png"));
            textureMap.put("Mark assets/blue-roof-middle.png",new Texture("resources/HubWorld/Mark assets/blue-roof-middle.png"));
            textureMap.put("Mark assets/blue-roof-right.png",new Texture("resources/HubWorld/Mark assets/blue-roof-right.png"));
            textureMap.put("Mark assets/blue-window-larg-left.png",new Texture("resources/HubWorld/Mark assets/blue-window-larg-left.png"));
            textureMap.put("Mark assets/blue-window-larg-middle.png",new Texture("resources/HubWorld/Mark assets/blue-window-larg-middle.png"));
            textureMap.put("Mark assets/blue-window-larg-right.png",new Texture("resources/HubWorld/Mark assets/blue-window-larg-right.png"));
            textureMap.put("Mark assets/blue-window-medium.png",new Texture("resources/HubWorld/Mark assets/blue-window-medium.png"));
            textureMap.put("Mark assets/blue-window-small.png",new Texture("resources/HubWorld/Mark assets/blue-window-small.png"));
            textureMap.put("Mark assets/circle-base-left.png",new Texture("resources/HubWorld/Mark assets/circle-base-left.png"));
            textureMap.put("Mark assets/circle-base-middle.png",new Texture("resources/HubWorld/Mark assets/circle-base-middle.png"));
            textureMap.put("Mark assets/circle-base-right.png",new Texture("resources/HubWorld/Mark assets/circle-base-right.png"));
            textureMap.put("Mark assets/circle-roof-left.png",new Texture("resources/HubWorld/Mark assets/circle-roof-left.png"));
            textureMap.put("Mark assets/circle-roof-middle.png",new Texture("resources/HubWorld/Mark assets/circle-roof-middle.png"));
            textureMap.put("Mark assets/circle-roof-rightpng.png",new Texture("resources/HubWorld/Mark assets/circle-roof-rightpng.png"));
            textureMap.put("Mark assets/circle-window-left.png",new Texture("resources/HubWorld/Mark assets/circle-window-left.png"));
            textureMap.put("Mark assets/circle-window-middle.png",new Texture("resources/HubWorld/Mark assets/circle-window-middle.png"));
            textureMap.put("Mark assets/circle-window-right.png",new Texture("resources/HubWorld/Mark assets/circle-window-right.png"));
            textureMap.put("Mark assets/diamond-bottom-left.png",new Texture("resources/HubWorld/Mark assets/diamond-bottom-left.png"));
            textureMap.put("Mark assets/diamond-bottom-right.png",new Texture("resources/HubWorld/Mark assets/diamond-bottom-right.png"));
            textureMap.put("Mark assets/diamond-top-left.png",new Texture("resources/HubWorld/Mark assets/diamond-top-left.png"));
            textureMap.put("Mark assets/diamond-top-right.png",new Texture("resources/HubWorld/Mark assets/diamond-top-right.png"));
            textureMap.put("Mark assets/door-bottom-left.png",new Texture("resources/HubWorld/Mark assets/door-bottom-left.png"));
            textureMap.put("Mark assets/door-bottom-right.png",new Texture("resources/HubWorld/Mark assets/door-bottom-right.png"));
            textureMap.put("Mark assets/door-top-left.png",new Texture("resources/HubWorld/Mark assets/door-top-left.png"));
            textureMap.put("Mark assets/door-top-right.png",new Texture("resources/HubWorld/Mark assets/door-top-right.png"));
            textureMap.put("Mark assets/EM.png",new Texture("resources/HubWorld/Mark assets/EM.png"));
            textureMap.put("Mark assets/grey-door.png",new Texture("resources/HubWorld/Mark assets/grey-door.png"));
            textureMap.put("Mark assets/grey-roof-left.png",new Texture("resources/HubWorld/Mark assets/grey-roof-left.png"));
            textureMap.put("Mark assets/grey-roof-middle.png",new Texture("resources/HubWorld/Mark assets/grey-roof-middle.png"));
            textureMap.put("Mark assets/grey-roof-right.png",new Texture("resources/HubWorld/Mark assets/grey-roof-right.png"));
            textureMap.put("Mark assets/grey-wall.png",new Texture("resources/HubWorld/Mark assets/grey-wall.png"));
            textureMap.put("Mark assets/grey-window.png",new Texture("resources/HubWorld/Mark assets/grey-window.png"));
            textureMap.put("Mark assets/insignia-bottom-left.png",new Texture("resources/HubWorld/Mark assets/insignia-bottom-left.png"));
            textureMap.put("Mark assets/insignia-bottom-right.png",new Texture("resources/HubWorld/Mark assets/insignia-bottom-right.png"));
            textureMap.put("Mark assets/insignia-top-left.png",new Texture("resources/HubWorld/Mark assets/insignia-top-left.png"));
            textureMap.put("Mark assets/insignia-top-right.png",new Texture("resources/HubWorld/Mark assets/insignia-top-right.png"));
            textureMap.put("Mark assets/IT.png",new Texture("resources/HubWorld/Mark assets/IT.png"));
            textureMap.put("Mark assets/ministry-wall.png",new Texture("resources/HubWorld/Mark assets/ministry-wall.png"));
            textureMap.put("Mark assets/ministry-window.png",new Texture("resources/HubWorld/Mark assets/ministry-window.png"));
            textureMap.put("Mark assets/ministry-window-NE.png",new Texture("resources/HubWorld/Mark assets/ministry-window-NE.png"));
            textureMap.put("Mark assets/ministry-window-NW.png",new Texture("resources/HubWorld/Mark assets/ministry-window-NW.png"));
            textureMap.put("Mark assets/ministry-window-SE.png",new Texture("resources/HubWorld/Mark assets/ministry-window-SE.png"));
            textureMap.put("Mark assets/ministry-window-SW.png",new Texture("resources/HubWorld/Mark assets/ministry-window-SW.png"));
            textureMap.put("Mark assets/MS.png",new Texture("resources/HubWorld/Mark assets/MS.png"));
            textureMap.put("Mark assets/purple window-1.png",new Texture("resources/HubWorld/Mark assets/purple window-1.png"));
            textureMap.put("Mark assets/purple-door.png",new Texture("resources/HubWorld/Mark assets/purple-door.png"));
            textureMap.put("Mark assets/purple-door-left.png",new Texture("resources/HubWorld/Mark assets/purple-door-left.png"));
            textureMap.put("Mark assets/purple-door-right.png",new Texture("resources/HubWorld/Mark assets/purple-door-right.png"));
            textureMap.put("Mark assets/purple-roof-left.png",new Texture("resources/HubWorld/Mark assets/purple-roof-left.png"));
            textureMap.put("Mark assets/purple-roof-middle.png",new Texture("resources/HubWorld/Mark assets/purple-roof-middle.png"));
            textureMap.put("Mark assets/purple-roof-right.png",new Texture("resources/HubWorld/Mark assets/purple-roof-right.png"));
            textureMap.put("Mark assets/purple-window-2.png",new Texture("resources/HubWorld/Mark assets/purple-window-2.png"));
            textureMap.put("Mark assets/purple-window-3.png",new Texture("resources/HubWorld/Mark assets/purple-window-3.png"));
            textureMap.put("Mark assets/red-roof-angled-bottom.png",new Texture("resources/HubWorld/Mark assets/red-roof-angled-bottom.png"));
            textureMap.put("Mark assets/red-roof-angled-top.png",new Texture("resources/HubWorld/Mark assets/red-roof-angled-top.png"));
            textureMap.put("Mark assets/red-roof-left.png",new Texture("resources/HubWorld/Mark assets/red-roof-left.png"));
            textureMap.put("Mark assets/red-roof-right.png",new Texture("resources/HubWorld/Mark assets/red-roof-right.png"));
            textureMap.put("Mark assets/red-window.png",new Texture("resources/HubWorld/Mark assets/red-window.png"));
            textureMap.put("Mark assets/red-window-left.png",new Texture("resources/HubWorld/Mark assets/red-window-left.png"));
            textureMap.put("Mark assets/red-window-right.png",new Texture("resources/HubWorld/Mark assets/red-window-right.png"));
            textureMap.put("Mark assets/roof.png",new Texture("resources/HubWorld/Mark assets/roof.png"));
            textureMap.put("Mark assets/roof-bottom.png",new Texture("resources/HubWorld/Mark assets/roof-bottom.png"));
            textureMap.put("Mark assets/roof-bottom-left.png",new Texture("resources/HubWorld/Mark assets/roof-bottom-left.png"));
            textureMap.put("Mark assets/roof-bottom-right.png",new Texture("resources/HubWorld/Mark assets/roof-bottom-right.png"));
            textureMap.put("Mark assets/roof-left.png",new Texture("resources/HubWorld/Mark assets/roof-left.png"));
            textureMap.put("Mark assets/roof-right.png",new Texture("resources/HubWorld/Mark assets/roof-right.png"));
            textureMap.put("Mark assets/roof-top.png",new Texture("resources/HubWorld/Mark assets/roof-top.png"));
            textureMap.put("Mark assets/roof-top-left.png",new Texture("resources/HubWorld/Mark assets/roof-top-left.png"));
            textureMap.put("Mark assets/roof-top-right.png",new Texture("resources/HubWorld/Mark assets/roof-top-right.png"));
            textureMap.put("Mark assets/small-door-left.png",new Texture("resources/HubWorld/Mark assets/small-door-left.png"));
            textureMap.put("Mark assets/small-door-right.png",new Texture("resources/HubWorld/Mark assets/small-door-right.png"));
            textureMap.put("Mark assets/small-roof.png",new Texture("resources/HubWorld/Mark assets/small-roof.png"));
            textureMap.put("Mark assets/small-roof-bottom-left.png",new Texture("resources/HubWorld/Mark assets/small-roof-bottom-left.png"));
            textureMap.put("Mark assets/small-roof-bottom-right.png",new Texture("resources/HubWorld/Mark assets/small-roof-bottom-right.png"));
            textureMap.put("Mark assets/small-roof-top-left.png",new Texture("resources/HubWorld/Mark assets/small-roof-top-left.png"));
            textureMap.put("Mark assets/small-roof-top-right.png",new Texture("resources/HubWorld/Mark assets/small-roof-top-right.png"));
            textureMap.put("Mark assets/blue-window-angled.png",new Texture("resources/HubWorld/Mark assets/blue-window-angled.png"));
            textureMap.put("Mark assets/purple-wall.png",new Texture("resources/HubWorld/Mark assets/purple-wall.png"));
            textureMap.put("Mark assets/purple-dark-wall.png",new Texture("resources/HubWorld/Mark assets/purple-dark-wall.png"));
            textureMap.put("krish-assets/green-building-2---19.png",new Texture("resources/HubWorld/krish-assets/green-building-2---19.png"));
            textureMap.put("krish-assets/pink-building-2---01.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---01.png"));
            textureMap.put("krish-assets/pink-building-2---02.png",new Texture("resources/HubWorld/krish-assets/pink-building-2---02.png"));

            // new teleporter room
            textureMap.put("blue-tele-1.png",new Texture("resources/teleporter-room-assets/blue-tele-1.png"));
            textureMap.put("blue-tele-2.png",new Texture("resources/teleporter-room-assets/blue-tele-2.png"));
            textureMap.put("blue-tele-3.png",new Texture("resources/teleporter-room-assets/blue-tele-3.png"));
            textureMap.put("blue-tele-4.png",new Texture("resources/teleporter-room-assets/blue-tele-4.png"));
            textureMap.put("desk-1.png",new Texture("resources/teleporter-room-assets/desk-1.png"));
            textureMap.put("desk-2.png",new Texture("resources/teleporter-room-assets/desk-2.png"));
            textureMap.put("desk-3.png",new Texture("resources/teleporter-room-assets/desk-3.png"));
            textureMap.put("desk-4.png",new Texture("resources/teleporter-room-assets/desk-4.png"));
            textureMap.put("desk-5.png",new Texture("resources/teleporter-room-assets/desk-5.png"));
            textureMap.put("desk-6.png",new Texture("resources/teleporter-room-assets/desk-6.png"));
            textureMap.put("door-1.png",new Texture("resources/teleporter-room-assets/door-1.png"));
            textureMap.put("door-2.png",new Texture("resources/teleporter-room-assets/door-2.png"));
            textureMap.put("door-3.png",new Texture("resources/teleporter-room-assets/door-3.png"));
            textureMap.put("door-4.png",new Texture("resources/teleporter-room-assets/door-4.png"));
            textureMap.put("floor-generic.png",new Texture("resources/teleporter-room-assets/floor-generic.png"));
            textureMap.put("green-tele-1.png",new Texture("resources/teleporter-room-assets/green-tele-1.png"));
            textureMap.put("green-tele-2.png",new Texture("resources/teleporter-room-assets/green-tele-2.png"));
            textureMap.put("green-tele-3.png",new Texture("resources/teleporter-room-assets/green-tele-3.png"));
            textureMap.put("green-tele-4.png",new Texture("resources/teleporter-room-assets/green-tele-4.png"));
            textureMap.put("grey-purple-wire.png",new Texture("resources/teleporter-room-assets/grey-purple-wire.png"));
            textureMap.put("grey-purple-wire-2.png",new Texture("resources/teleporter-room-assets/grey-purple-wire-2.png"));
            textureMap.put("grey-wire-corner-NW.png",new Texture("resources/teleporter-room-assets/grey-wire-corner-NW.png"));
            textureMap.put("grey-wire-corner-SW.png",new Texture("resources/teleporter-room-assets/grey-wire-corner-SW.png"));
            textureMap.put("grey-wire-left.png",new Texture("resources/teleporter-room-assets/grey-wire-left.png"));
            textureMap.put("grey-wire-right.png",new Texture("resources/teleporter-room-assets/grey-wire-right.png"));
            textureMap.put("grey-wire-top.png",new Texture("resources/teleporter-room-assets/grey-wire-top.png"));
            textureMap.put("mainframe-1.png",new Texture("resources/teleporter-room-assets/mainframe-1.png"));
            textureMap.put("mainframe-2.png",new Texture("resources/teleporter-room-assets/mainframe-2.png"));
            textureMap.put("mainframe-3.png",new Texture("resources/teleporter-room-assets/mainframe-3.png"));
            textureMap.put("pipe-up-1.png",new Texture("resources/teleporter-room-assets/pipe-up-1.png"));
            textureMap.put("pipe-up-2.png",new Texture("resources/teleporter-room-assets/pipe-up-2.png"));
            textureMap.put("pipe-up-wire-1.png",new Texture("resources/teleporter-room-assets/pipe-up-wire-1.png"));
            textureMap.put("pipe-up-wire-2.png",new Texture("resources/teleporter-room-assets/pipe-up-wire-2.png"));
            textureMap.put("pipe-wire-1.png",new Texture("resources/teleporter-room-assets/pipe-wire-1.png"));
            textureMap.put("pipe-wire-2.png",new Texture("resources/teleporter-room-assets/pipe-wire-2.png"));
            textureMap.put("purple-tele-1.png",new Texture("resources/teleporter-room-assets/purple-tele-1.png"));
            textureMap.put("purple-tele-2.png",new Texture("resources/teleporter-room-assets/purple-tele-2.png"));
            textureMap.put("purple-tele-3.png",new Texture("resources/teleporter-room-assets/purple-tele-3.png"));
            textureMap.put("purple-tele-4.png",new Texture("resources/teleporter-room-assets/purple-tele-4.png"));
            textureMap.put("purple-wire-middle.png",new Texture("resources/teleporter-room-assets/purple-wire-middle.png"));
            textureMap.put("purple-wire-up.png",new Texture("resources/teleporter-room-assets/purple-wire-up.png"));
            textureMap.put("red-tele-1.png",new Texture("resources/teleporter-room-assets/red-tele-1.png"));
            textureMap.put("red-tele-2.png",new Texture("resources/teleporter-room-assets/red-tele-2.png"));
            textureMap.put("red-tele-3.png",new Texture("resources/teleporter-room-assets/red-tele-3.png"));
            textureMap.put("red-tele-4.png",new Texture("resources/teleporter-room-assets/red-tele-4.png"));
            textureMap.put("wall-generic.png",new Texture("resources/teleporter-room-assets/wall-generic.png"));
            textureMap.put("wire-angle-1.png",new Texture("resources/teleporter-room-assets/wire-angle-1.png"));
            textureMap.put("wire-angle-2.png",new Texture("resources/teleporter-room-assets/wire-angle-2.png"));
            textureMap.put("wire-angle-3.png",new Texture("resources/teleporter-room-assets/wire-angle-3.png"));
            textureMap.put("wire-angle-4.png",new Texture("resources/teleporter-room-assets/wire-angle-4.png"));

            //Forest
            textureMap.put("dungeon-tower-tiles.png",new Texture("resources/biome.forest/dungeon-tower-tiles.png"));
            textureMap.put("floor-tile-1.png",new Texture("resources/biome.forest/floor-tile-1.png"));
            textureMap.put("floor-tile-2.png",new Texture("resources/biome.forest/floor-tile-2.png"));
            textureMap.put("leaves-tile.png",new Texture("resources/biome.forest/leaves-tile.png"));
            textureMap.put("wall-tile.png",new Texture("resources/biome.forest/wall-tile.png"));
            textureMap.put("bush-tile.png",new Texture("resources/biome.forest/bush-tile.png"));
            textureMap.put("dungeon-tile-1.png",new Texture("resources/biome.forest/dungeon-tile-1.png"));
            textureMap.put("dungeon-tile-2.png",new Texture("resources/biome.forest/dungeon-tile-2.png"));

            //HubWorldDungeon
            textureMap.put("option1_building_1.png",new Texture("resources/HubWorldDungeon/option1_building_1.png"));
            textureMap.put("option1_building_2.png",new Texture("resources/HubWorldDungeon/option1_building_2.png"));
            textureMap.put("option1_tile_1.png",new Texture("resources/HubWorldDungeon/option1_tile_1.png"));
            textureMap.put("option1_tile_2.png",new Texture("resources/HubWorldDungeon/option1_tile_2.png"));
            textureMap.put("option1_tile_3.png",new Texture("resources/HubWorldDungeon/option1_tile_3.png"));
            textureMap.put("option1_tile_4.png",new Texture("resources/HubWorldDungeon/option1_tile_4.png"));
            textureMap.put("option2_building-1.png",new Texture("resources/HubWorldDungeon/option2_building-1.png"));
            textureMap.put("option2_building-2.png",new Texture("resources/HubWorldDungeon/option2_building-2.png"));
            textureMap.put("option2_tile_1.png",new Texture("resources/HubWorldDungeon/option2_tile_1.png"));
            textureMap.put("option2_tile_2.png",new Texture("resources/HubWorldDungeon/option2_tile_2.png"));
            textureMap.put("option2_tile_3.png",new Texture("resources/HubWorldDungeon/option2_tile_3.png"));
            textureMap.put("option2_tile_4.png",new Texture("resources/HubWorldDungeon/option2_tile_4.png"));
            textureMap.put("wall_1.png",new Texture("resources/HubWorldDungeon/wall_1.png"));
            textureMap.put("wall_2.png",new Texture("resources/HubWorldDungeon/wall_2.png"));

            //TundraDungeon
            textureMap.put("dungeon-tile 1.png",new Texture("resources/biomes.tundra/dungeon-tile 1.png"));
            textureMap.put("dungeon-tile 2.png",new Texture("resources/biomes.tundra/dungeon-tile 2.png"));
            textureMap.put("dungeon-tower-lower.png",new Texture("resources/biomes.tundra/dungeon-tower-lower.png"));
            textureMap.put("dungeon-tower-upper.png",new Texture("resources/biomes.tundra/dungeon-tower-upper.png"));
            textureMap.put("floor-tile.png",new Texture("resources/biomes.tundra/floor-tile.png"));
            textureMap.put("floor-tile2.png",new Texture("resources/biomes.tundra/floor-tile2.png"));
            textureMap.put("ice-tile.png",new Texture("resources/biomes.tundra/ice-tile.png"));
            textureMap.put("icicle-tile.png",new Texture("resources/biomes.tundra/icicle-tile.png"));
            textureMap.put(".wall-tile.png",new Texture("resources/biomes.tundra/wall-tile.png"));
            textureMap.put("0-1.png",new Texture("resources/biomes.tundra/0-1.png"));
            textureMap.put("0-2.png",new Texture("resources/biomes.tundra/0-2.png"));
            textureMap.put("0-3.png",new Texture("resources/biomes.tundra/0-3.png"));
            textureMap.put("0-4.png",new Texture("resources/biomes.tundra/0-4.png"));
            textureMap.put("2-1.png",new Texture("resources/biomes.tundra/2-1.png"));
            textureMap.put("2-2.png",new Texture("resources/biomes.tundra/2-2.png"));
            textureMap.put("2-3.png",new Texture("resources/biomes.tundra/2-3.png"));
            textureMap.put("2-4.png",new Texture("resources/biomes.tundra/2-4.png"));
            textureMap.put("3-1.png",new Texture("resources/biomes.tundra/3-1.png"));
            textureMap.put("3-2.png",new Texture("resources/biomes.tundra/3-2.png"));
            textureMap.put("3-3.png",new Texture("resources/biomes.tundra/3-3.png"));
            textureMap.put("3-4.png",new Texture("resources/biomes.tundra/3-4.png"));
            textureMap.put("4-1.png",new Texture("resources/biomes.tundra/4-1.png"));
            textureMap.put("4-2.png",new Texture("resources/biomes.tundra/4-2.png"));
            textureMap.put("4-3.png",new Texture("resources/biomes.tundra/4-3.png"));
            textureMap.put("4-4.png",new Texture("resources/biomes.tundra/4-4.png"));
            textureMap.put("5-1.png",new Texture("resources/biomes.tundra/5-1.png"));
            textureMap.put("5-2.png",new Texture("resources/biomes.tundra/5-2.png"));
            textureMap.put("5-3.png",new Texture("resources/biomes.tundra/5-3.png"));
            textureMap.put("5-4.png",new Texture("resources/biomes.tundra/5-4.png"));
            textureMap.put("6-1.png",new Texture("resources/biomes.tundra/6-1.png"));
            textureMap.put("6-2.png",new Texture("resources/biomes.tundra/6-2.png"));
            textureMap.put("6-3.png",new Texture("resources/biomes.tundra/6-3.png"));
            textureMap.put("6-4.png",new Texture("resources/biomes.tundra/6-4.png"));



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a texture object for a given string id
     *
     * @param id Texture identifier
     * @return Texture for given id
     */
    public Texture getTexture(String id) {
        if (textureMap.containsKey(id)) {
            return textureMap.get(id);
        } else {
        	//log.info("Texture map does not contain P{}, returning default texture.", id);
            return textureMap.get("spacman_ded");
        }

    }

    /**
     * Checks whether or not a texture is available.
     *
     * @param id Texture identifier
     * @return If texture is available or not.
     */
    public boolean hasTexture(String id) {
        return textureMap.containsKey(id);

    }

    /**
     * Saves a texture with a given id
     *
     * @param id       Texture id
     * @param filename Filename within the assets folder
     */
    public void saveTexture(String id, String filename) {
        if (!textureMap.containsKey(id)) {
            textureMap.put(id, new Texture(filename));
        }
    }

    /**
     * Saves a texture object with a given id
     *
     * @param id texture id
     * @param texture texture object
     */
    public void addTexture(String id, Texture texture) {
        if (!textureMap.containsKey(id)) {
            textureMap.put(id, texture);
        }
    }

    /**
     * Replaces a texture object with a given id with a new texture object
     *
     * @param id texture id
     * @param texture texture object
     */
    public void replaceTexture(String id, Texture texture) {
        if (textureMap.containsKey(id)) {
            textureMap.replace(id, texture);
        } else {
            addTexture(id, texture);
        }
    }

    /**
     * Removes a texture with a given id from the map
     *
     * @param id texture id
     */
    public void removeTexture(String id) {
        if (textureMap.containsKey(id)) {
            textureMap.remove(id);
        }
    }

}
