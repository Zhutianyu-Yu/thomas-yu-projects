package deco2800.thomas.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.quest.*;
import deco2800.thomas.worlds.AbstractWorld;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

public class QuestManager extends AbstractManager {
    private static final String mainDbConnectionString = "jdbc:sqlite:Quest.db";
    protected static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(QuestManager.class);

    private final List<Quest> currentQuests;
    private final List<Quest> completedQuests;

    private static QuestManager instance;

    private QuestManager() {
        this.currentQuests = new ArrayList<>();
        this.completedQuests = new ArrayList<>();
    }

    /**
     * Returns an instance of the QuestManager
     *
     * @return QuestManager
     */
    public static QuestManager get() {
        if (instance == null) {
            instance = new QuestManager();
            try {
                QuestTrackDB.initDatabase(mainDbConnectionString);
                QuestTrackDB.questClear();
                if (!instance.loadQuestsIntoDb())
                    LOGGER.error("Loading quests into db failed");
            } catch (SQLException ex) {
                LOGGER.error("Quest database initialisation failed: " + ex.getMessage());
            }
        }
        return instance;
    }

    /**
     * @return Read-only list of current quests
     */
    public List<Quest> getCurrentQuests() {
        return Collections.unmodifiableList(currentQuests);
    }

    /**
     * @return Read-only list of completed quests
     */
    public List<Quest> getCompletedQuests() {
        return Collections.unmodifiableList(completedQuests);
    }

    /**
     * Starts the quest if it exists, is available to the player, all of the prerequisites were completed,
     * is not being tracked currently or completed prior
     *
     * @param name Name of a quest to start
     * @return True if quest exists and has been started, False otherwise
     */
    public boolean startQuest(String name) {
        //If we already have this quest or have completed it prior - we can't start it
        for (Quest q : currentQuests) {
            if (q.getName().equals(name))
                return false;
        }
        for (Quest q : completedQuests) {
            if (q.getName().equals(name))
                return false;
        }
        Quest newQuest = loadQuestFromDb(name);
        //Well, it does not exists, what can we do?
        if (newQuest == null)
            return false;
        //Go do other things first, would you?
        if (!isQuestAvailable(newQuest))
            return false;
        LOGGER.info("Quest successfully started: " + name);
        return currentQuests.add(newQuest);
    }

    /**
     * Checks whether any of the NPC related quests require provided action done to the provided NPC
     *
     * @param npcId  Numerical Id of the NPC player interacted with
     * @param action Action used by player
     */
    public void updateQuestStatus(int npcId, String action) {
        // Check every quest in the list of current quests, if any of the tasks require the player to make a certain
        // action to the NPC - check whether it is the right action and the right NPC. If it is - check whether player
        // has done it enough times. If so - complete the quest and issue the reward
        Iterator<Quest> i = currentQuests.iterator();
        while (i.hasNext()) {
            Quest quest = i.next();
            for (QuestTask task : quest.getQuestTasks()) {
                if (task instanceof NPCInteractionQuestTask && task.isAvailable() && !task.isComplete()) {
                    NPCInteractionQuestTask taskDetailed = (NPCInteractionQuestTask) task;
                    if (taskDetailed.getNpcID() == npcId && taskDetailed.getAction().equals(action)) {
                        taskDetailed.incrementProgress(1);
                        if (taskDetailed.isComplete()) {
                            if (quest.isComplete()) {
                                completedQuests.add(quest);
                                assignQuestRewards(quest.getRewards());
                                i.remove();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks whether any of the Enemy related quests require provided action done to the provided Enemy type
     *
     * @param enemy  The enemy player killed\interacted with
     * @param action Action used by player
     */
    public void updateQuestStatus(EnemyPeon enemy, String action) {
        // Check every quest in the list of current quests, if any of the tasks require the enemy to be killed
        // check whether it is the right enemy. If it is - check whether player has killed enough of them and if so
        // complete the quest and issue the reward
        Iterator<Quest> i = currentQuests.iterator();
        while (i.hasNext()) {
            Quest quest = i.next();
            for (QuestTask task : quest.getQuestTasks()) {
                if (task instanceof KillQuestTask && task.isAvailable() && !task.isComplete()) {
                    KillQuestTask taskDetailed = (KillQuestTask) task;
                    if (taskDetailed.getEnemyType().equals(enemy.getTypeIdentifier()) && action.equals(QuestAction.Kill)) {
                        taskDetailed.incrementProgress(1);
                        if (taskDetailed.isComplete()) {
                            LOGGER.info("Task complete: " + taskDetailed.getTaskDescription());
                            if (quest.isComplete()) {
                                LOGGER.info("Quest complete: " + quest.getName());
                                completedQuests.add(quest);
                                assignQuestRewards(quest.getRewards());
                                i.remove();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks whether any of the item related quests require provided action done to the provided item
     *
     * @param item     Item the player has interacted with
     * @param action   Action used by player
     * @param quantity Amount of objects obtained or lost, must be a positive number
     */
    public void updateQuestStatus(AbstractItem item, String action, int quantity) {
        // Check every quest in the list of current quests, if any of the tasks require the player to make a certain
        // action to the item - check whether it is the right action and the right item. If it is - check whether player
        // has done it enough times. If so - complete the quest and issue the reward
        Iterator<Quest> i = currentQuests.iterator();
        while (i.hasNext()) {
            Quest quest = i.next();
            for (QuestTask task : quest.getQuestTasks()) {
                if (task instanceof ObjectInteractionQuestTask && task.isAvailable() && !task.isComplete()) {
                    ObjectInteractionQuestTask taskDetailed = (ObjectInteractionQuestTask) task;
                    if (taskDetailed.getItemId().equals(item.getItemIdString())) {
                        switch (action) {
                            case QuestAction.PickUpItem:
                                taskDetailed.incrementProgress(quantity);
                                break;
                            case QuestAction.DropItem:
                            case QuestAction.UseItem:
                                taskDetailed.incrementProgress(-quantity);
                                break;
                            default:
                                //Do nothing, this action is not supported yet
                                break;
                        }
                        if (taskDetailed.isComplete()) {
                            if (quest.isComplete()) {
                                completedQuests.add(quest);
                                assignQuestRewards(quest.getRewards());
                                i.remove();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Rewards the player with provided rewards
     *
     * @param rewards List of items or XP to give the player for being awesome
     */
    private void assignQuestRewards(List<QuestReward> rewards) {
        AbstractEntity potentialPlayer = GameManager.get().getWorld().getPlayerPeon();
        InventoryAbstract inventory = GameManager.getManagerFromInstance(InputManager.class).getInventory();
        if (potentialPlayer instanceof PlayerPeon) {
            PlayerPeon player = (PlayerPeon) potentialPlayer;
            for (QuestReward reward : rewards) {
                player.addXP(reward.getXpReward());
                String itemTypeId = reward.getItemTypeId();
                int quantity = reward.getItemQuantity();
                if (itemTypeId != null && quantity > 0) {
                    for (int i = 0; i < quantity; i++) {
                        AbstractItem item = AbstractItem.createItemById(itemTypeId);
                        //Only add an item if the id is valid
                        if (item != null)
                        {
                            inventory.addInventory(item);
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks whether it is possible to start the quest evaluating the player characteristics and prerequisites for the
     * quest itself
     *
     * @param quest Quest to assess
     * @return True if all of the prerequisites are met, false otherwise
     */
    private boolean isQuestAvailable(Quest quest) {
        //They must complete all of the prior quests before proceeding so check the list of completed quests
        for (int prerequisiteId : quest.getQuestPrerequisites()) {
            boolean found = false;
            for (Quest q : completedQuests) {
                if (q.getId() == prerequisiteId) {
                    found = true;
                    break;
                }
            }
            if (!found) return false;
        }
        AbstractWorld world = GameManager.get().getWorld();
        //Can probably only happen during testing
        if (world == null)
            return true;
        AbstractEntity potentialPlayer = world.getPlayerPeon();
        //Can probably only happen during testing
        if (potentialPlayer == null)
            return true;
        //You don't exist hence you can't accept quests, bro
        //this handles nulls too
        if (!(potentialPlayer instanceof PlayerPeon)) return false;
        PlayerPeon player = (PlayerPeon) potentialPlayer;
        //You must be the right level and maybe possess certain items to proceed
        InventoryAbstract inventory = GameManager.getManagerFromInstance(InputManager.class).getInventory();
        for (PlayerPrerequisite p : quest.getPlayerPrerequisites()) {
            if (player.getLevel() < p.getPlayerLevel()) return false;
            if (p.getItemTypeId() != null && p.getItemQuantity() > 0) {
                //let's just pray that those lists match until they write a better search methods that supports
                //multiple stacks and searches for at least X items as opposed to exactly X items
                List<AbstractItem> inventoryContents = inventory.get1DInventoryList();
                List<Integer> inventoryQuantities = inventory.get1DQuantityList();
                boolean found = false;
                for (int i = 0; i < inventoryContents.size(); i++) {
                    AbstractItem item = inventoryContents.get(i);
                    Integer quantity = inventoryQuantities.get(i);
                    if (item == null || quantity == null) continue;
                    if (item.getItemIdString().equals(p.getItemTypeId()) && quantity >= p.getItemQuantity()) {
                        found = true;
                        break;
                    }
                }
                if (!found) return false;
            }
        }
        return true;
    }

    /**
     * Loads multiple quests from the database by their names
     *
     * @param name Name of the quest to load
     * @return Requested quest or null if nothing was found
     */
    private Quest loadQuestFromDb(String name) {
        try {
            Quest quest = QuestTrackDB.getQuest(name);
            if (quest != null)
                LOGGER.info("Quest exists in the database");
            return quest;
        } catch (SQLException e) {
            LOGGER.info("Quest loading failed:" + e.getMessage());
            return null;
        }
    }

    public boolean loadQuestsIntoDb() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(QuestTask.class, new QuestTaskDeserializer())
                .create();
        File folder = new File("resources/Quest store");
        if (!folder.exists()) {
            LOGGER.error("Quest folder does not exist");
            return false;
        }
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile() && !file.getName().equals("template.json")) {
                    try (Reader reader = new FileReader(file.getAbsolutePath())) {
                        Quest questBack = gson.fromJson(reader, Quest.class);
                        if (questBack != null) {
                            try {
                                QuestTrackDB.questInsertFull(questBack);
                            } catch (SQLException e) {
                                LOGGER.info("Inserting quest failed: " + e.getMessage());
                                return false;
                            }
                        }
                    } catch (IOException e) {
                        LOGGER.info("Reading file failed");
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
