package deco2800.thomas.managers;

import com.badlogic.gdx.InputProcessor;

import deco2800.thomas.entities.Peon;
import deco2800.thomas.entities.inforBoard.MainInformation;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.MechInventory;
import deco2800.thomas.inventory.ui.InventoryMenu;
import deco2800.thomas.items.ItemUI.IllustrationUI;
import deco2800.thomas.observers.*;
import deco2800.thomas.pause.PauseInterface;
import deco2800.thomas.quest.ui.QuestInterface;
import deco2800.thomas.traderUI.TraderInterface;
import deco2800.thomas.util.Vector2;
import deco2800.thomas.worlds.MenuMenu;
import deco2800.thomas.worlds.staticworlds.*;
import deco2800.thomas.worlds.rooms.*;

import java.util.ArrayList;

/**
 * Created by woody on 30-Jul-17.
 */
public class InputManager extends AbstractManager implements InputProcessor {

	private ArrayList<KeyDownObserver> keyDownListeners = new ArrayList<>();

	private ArrayList<KeyUpObserver> keyUpListeners = new ArrayList<>();

	private ArrayList<TouchDownObserver> touchDownListeners = new ArrayList<>();

	private MenuMenu menuMenu;
	/** Holds inventory actors*/
	private ArrayList<TouchDownObserver> touchDownListenersInventory = new ArrayList<>();

	private ArrayList<TouchDownObserver> touchDownListenersMenuButtons = new ArrayList<>();

	private ArrayList<TouchDownObserver> touchDownListenersTutorial = new ArrayList<>();
	private ArrayList<TouchDownObserver> touchDownListenersTrader = new ArrayList<>();
	private ArrayList<TouchDownObserver> touchDownListenersFinish = new ArrayList<>();

	private ArrayList<TouchUpObserver> touchUpListeners = new ArrayList<>();

	/** Holds inventory actors*/
	private ArrayList<TouchUpObserver> touchUpListenersInventory = new ArrayList<>();

	private ArrayList<TouchDraggedObserver> touchDragegdListeners = new ArrayList<>();

	private ArrayList<MouseMovedObserver> mouseMovedListeners = new ArrayList<>();

	private ArrayList<ScrollObserver> scrollListeners = new ArrayList<>();

	private Vector2 mousePosition = new Vector2(0, 0);

	private InventoryMenu inventoryMenu;

	/** Holds quest actors */
	private QuestInterface questInterface;

	private PauseInterface pauseInterface;

	private TraderInterface traderInterface;

	private IllustrationUI itemIllustrationInterface;

	private MainInformation characterInformation;

	private ArrayList<TouchDownObserver> touchDownListenersQuest = new ArrayList<>();

	private ArrayList<TouchDownObserver> touchDownListenersPause = new ArrayList<>();

	private ArrayList<TouchDownObserver> touchDownListenersTrade = new ArrayList<>();

	private TutorialWorld tuteWorld;
	private TraderWorld tradeWorld;
	private FinishWorld finishWorld;


	/**
	 * Adds InventoryMenu to member variables
	 * @param menu global instance of the inventory menu
	 */
	public void addInventoryMenu(InventoryMenu menu) {
		this.inventoryMenu = menu;
	}

	public void setMenuMenu(MenuMenu menuMenu) {
		this.menuMenu = menuMenu;
	}

	public QuestInterface getQuestInterface() {
		return this.questInterface;
	}

	public void setItemIllustrationInterface(IllustrationUI itemIllustrationInterface) {
		this.itemIllustrationInterface = itemIllustrationInterface;
	}

	public IllustrationUI getItemIllustrationInterface() {
		return this.itemIllustrationInterface;
	}

	public void setCharacterInformation(MainInformation characterInformation) {
		this.characterInformation = characterInformation;
	}

	public MainInformation getCharacterInformation() {
		return this.characterInformation;
	}

	/**
	 * Adds QuestInterface to member variables
	 * @param questInterface global instance of the quest interface
	 */
	public void addQuestInterface(QuestInterface questInterface) {
		this.questInterface = questInterface;
	}


	public void addPauseInterface(PauseInterface pauseInterface) {
		this.pauseInterface = pauseInterface;
	}

	public void addTradeInterface(TraderInterface tradeInterface) {
		this.traderInterface = tradeInterface;
	}

	public TraderInterface getTraderInterface() {
		return this.traderInterface;
	}

	public PauseInterface getPauseInterface() {
		return this.pauseInterface;
	}


	public void addTradeWorld(TraderWorld traderWorld) {
		this.tradeWorld = traderWorld;
	}

	public void addTuteWorld(TutorialWorld tuteWorld) {
		this.tuteWorld = tuteWorld;
	}

	public void addFinishWorld(FinishWorld finWorld) {
		this.finishWorld = finWorld;
	}

	/**
	 *
	 * @return Instance of the inventory
	 */
	public InventoryAbstract getInventory() {
		return this.inventoryMenu.getInventory();
	}

	public MechInventory getMech(){
		if (this.inventoryMenu != null)
			return this.inventoryMenu.getMechInventory();
		else
			return null;

	}

	/**
	 * Adds an observer to observe the keyDown action
	 * @param observer the observer to add
     */
	public void addKeyDownListener(KeyDownObserver observer) {
		keyDownListeners.add(observer);
	}

	/**
	 * Removes an observer from observing the KeyDown action
	 * @param observer the observer to remove
	 */
	public void removeKeyDownListener(KeyDownObserver observer) {
		keyDownListeners.remove(observer);
	}

	public void removeListeners() {
		for (Object observer : keyDownListeners.toArray()) {
			if (observer instanceof Peon) {
				keyDownListeners.remove((KeyDownObserver) observer);
			}
		}

		for (Object observer : keyUpListeners.toArray()) {
			if (observer instanceof Peon) {
				keyUpListeners.remove((KeyUpObserver) observer);
			}
		}

		for (Object observer : touchUpListeners.toArray()) {
			if (observer instanceof Peon) {
				touchUpListeners.remove((TouchUpObserver) observer);
			}
		}

		for (Object observer : touchDownListeners.toArray()) {
			if (observer instanceof Peon) {
				touchDownListeners.remove((TouchDownObserver) observer);
			}
		}
	}

	/**
	 * Adds an observer to observe the keyUp action
	 * @param observer the observer to add
	 */
	public void addKeyUpListener(KeyUpObserver observer) {
		keyUpListeners.add(observer);
	}

	/**
	 * removes an observer from observing the keyUp action
	 * @param observer the observer to remove
	 */
	public void removeKeyUpListener(KeyUpObserver observer) {
		keyUpListeners.remove(observer);
	}

	/**
	 * Adds an observer to observe the touchDown action
	 * @param observer the observer to add
	 */
	public void addTouchDownListener(TouchDownObserver observer) {
		 touchDownListeners.add(observer);
	}

	/**
	 * Adds an observer that is part of InventoryMenu to observe the touchDown action
	 * @param observer the observer to add
	 */
	public void addTouchDownListenerInventory(TouchDownObserver observer) {
		touchDownListenersInventory.add(0, observer);
	}

	public void addTouchDownListenersMenuButtons(TouchDownObserver observer) {
		touchDownListenersMenuButtons.add(0, observer);
	}

	public void addTouchDownListenerTrader(TouchDownObserver observer) {
		touchDownListenersTrader.add(0, observer);
	}

	public void addTouchDownListenerTutorial(TouchDownObserver observer) {
		touchDownListenersTutorial.add(0, observer);
	}

	public void addTouchDownListenerFinish(TouchDownObserver observer) {
		touchDownListenersFinish.add(0, observer);
	}

	/**
	 * Adds an observer that is a part of QuestInterface to observer the touchDown action
	 * @param observer the observer to add
	 */
	public void addTouchDownListenerQuest(TouchDownObserver observer) {
		touchDownListenersQuest.add(observer);
	}


	public void addTouchDownListenerTradeInterface(TouchDownObserver observer) {
		touchDownListenersTrade.add(observer);
	}

	/**
	 * removes an observer from observing the touchDown action
	 * @param observer the observer to remove
	 */
	public void removeTouchDownListener(TouchDownObserver observer) {
		touchDownListeners.remove(observer);
	}

	public void removeTouchDownListenerTrader(TouchDownObserver observer) {
		touchDownListenersTrader.remove(observer);
	}

	public void removeTouchDownListenerTutorial(TouchDownObserver observer) {
		touchDownListenersTutorial.remove(observer);
	}

	public void removeTouchDownListenerFinish(TouchDownObserver observer) {
		touchDownListenersFinish.remove(observer);
	}

	/**
	 * Adds an observer to observe the touchUp action
	 * @param observer the observer to add
	 */
	public void addTouchUpListener(TouchUpObserver observer) {
		touchUpListeners.add(observer);
	}

	/**
	 * Adds an observer that is part of InventoryMenu to observe the touchUp action
	 * @param observer the observer to add
	 */
	public void addTouchUpListenerInventory(TouchUpObserver observer) {
		touchUpListenersInventory.add(observer);
	}

	/**
	 * removes an observer from observing the touchUp action
	 * @param observer the observer to remove
	 */
	public void removeTouchUpListener(TouchUpObserver observer) {
		touchUpListeners.remove(observer);
	}

	/**
	 * Adds an observer to observe the TouchDragged action
	 * @param observer the observer to add
	 */
	public void addTouchDraggedListener(TouchDraggedObserver observer) {
		touchDragegdListeners.add(observer);
	}

	/**
	 * removes an observer from observing the touchDragged action
	 * @param observer the observer to remove
	 */
	public void removeTouchDraggedListener(TouchDraggedObserver observer) {
		touchDragegdListeners.remove(observer);
	}

	/**
	 * Adds an observer to observe the mouseMoved action
	 * @param observer the observer to add
	 */
	public void addMouseMovedListener(MouseMovedObserver observer) {
		mouseMovedListeners.add(observer);
	}

	/**
	 * removes an observer from observing the mouseMoved action
	 * @param observer the observer to remove
	 */
	public void removeMouseMovedListener(MouseMovedObserver observer) {
		mouseMovedListeners.remove(observer);
	}

	/**
	 * Adds an observer to observe the scroll action
	 * @param observer the observer to add
	 */
	public void addScrollListener(ScrollObserver observer) {
		scrollListeners.add(observer);
	}

	/**
	 * removes an observer from observing the scrolling action
	 * @param observer the observer to remove
	 */
	public void removeScrollListener(ScrollObserver observer) {
		scrollListeners.remove(observer);
	}

	@Override
	public boolean keyDown(int keycode) {
		for (KeyDownObserver observer : keyDownListeners) {
			observer.notifyKeyDown(keycode);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		for (KeyUpObserver observer : keyUpListeners) {
			observer.notifyKeyUp(keycode);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		this.menuMenu.setUsed();

		if (this.inventoryMenu.isInventoryDisplayed()) { //Checks if Inventory is being displayed
			for (TouchDownObserver observer : touchDownListenersInventory) {
				observer.notifyTouchDown(screenX, screenY, pointer, button);
			}
		} else if (this.questInterface.getQuestInterfaceDisplayed()) {
			for (TouchDownObserver observer : touchDownListenersQuest) {
				observer.notifyTouchDown(screenX, screenY, pointer, button);
			}
		} else if (this.pauseInterface.getPauseInterfaceDisplayed()) {
			for (TouchDownObserver observer : touchDownListenersPause) {
				observer.notifyTouchDown(screenX, screenY, pointer, button);
			}
		} else if (this.traderInterface.getTraderInterfaceDisplayed()) {
			for (TouchDownObserver observer : touchDownListenersTrade) {
				observer.notifyTouchDown(screenX, screenY, pointer, button);
			}
		} else if (GameManager.get().getWorld() instanceof TraderWorld) {
			touchDownListenersTrader.get(0).notifyTouchDown(screenX, screenY, pointer, button);
		} else if (GameManager.get().getWorld() instanceof TutorialWorld) {
			touchDownListenersTutorial.get(0).notifyTouchDown(screenX, screenY, pointer, button);
		} else if (GameManager.get().getWorld() instanceof FinishWorld) {
			touchDownListenersFinish.get(0).notifyTouchDown(screenX, screenY, pointer, button);
		} else {
			for (TouchDownObserver observer : touchDownListeners) {
				observer.notifyTouchDown(screenX, screenY, pointer, button);
			}
		}

		for (TouchDownObserver observer : touchDownListenersMenuButtons) {
			observer.notifyTouchDown(screenX, screenY, pointer, button);
		}

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (this.inventoryMenu.isInventoryDisplayed()) {  //Checks if Inventory is being displayed
			for (TouchUpObserver observer : touchUpListenersInventory) {
				observer.notifyTouchUp(screenX, screenY, pointer, button);
			}
		} else {
			for (TouchUpObserver observer : touchUpListeners) {
				observer.notifyTouchUp(screenX, screenY, pointer, button);
			}
		}
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		for (TouchDraggedObserver observer : touchDragegdListeners) {
			observer.notifyTouchDragged(screenX, screenY, pointer);
		}
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		for (MouseMovedObserver observer : mouseMovedListeners) {
			observer.notifyMouseMoved(screenX, screenY);
		}

		mousePosition.setX(screenX);
		mousePosition.setY(screenY);
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		for (ScrollObserver observer : scrollListeners) {
			observer.notifyScrolled(amount);
		}
		return true;
	}

	public Vector2 getMousePosition() {
		return mousePosition;
	}

	public TraderWorld getTradeWorld() {
		return this.tradeWorld;
	}

	public FinishWorld getFinishedWorld() {
		return this.finishWorld;
	}

	public TutorialWorld getTuteWorld() {
		return this.tuteWorld;
	}
}
