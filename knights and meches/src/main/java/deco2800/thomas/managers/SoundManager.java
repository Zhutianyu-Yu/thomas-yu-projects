package deco2800.thomas.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

public class SoundManager extends AbstractManager {

    float volume = 1f;
    HashMap<String, Sound> sounds = new HashMap<>();
	
    public void playSound(String soundName) {
        Sound sound = Gdx.audio.newSound(Gdx.files.internal("resources/sounds/" + soundName));
        sound.play(volume);
        sounds.put(soundName, sound);
    }

    public void mute() {
        if (volume == 0f) {
            volume = 1f;
            sounds.get("ost.mp3").resume();
        } else {
            volume = 0f;
            sounds.get("ost.mp3").pause();
        }
    }

    public void stopSound(String soundName) {
        sounds.get(soundName).stop();
        sounds.get(soundName).dispose();
        sounds.remove(soundName);
    }
}
