package deco2800.thomas.managers;

import com.badlogic.gdx.Gdx;
import deco2800.thomas.entities.*;
import deco2800.thomas.util.ButtonView;
import deco2800.thomas.worlds.dungeons.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import deco2800.thomas.GameScreen;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.items.Orb.Orb;
import deco2800.thomas.moves.MoveManager;
import deco2800.thomas.renderers.PotateCamera;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.util.Party;
import deco2800.thomas.worlds.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameManager {
	//debug values stored here
	private int entitiesRendered;
	private int entitiesCount;
	private int tilesRendered;
	private int tilesCount;

	private static final Logger LOGGER = LoggerFactory.getLogger(GameManager.class);

	private static GameManager instance = null;

	// The Party which is currently seen in the overworld
	private static Party party;

	// The mech currently seen in the overworld
	private static Mech inGameMech;

	// The Players Stored in the game
	private  ArrayList<PlayerPeon> players = new ArrayList<>();

	// The list of all instantiated managers classes.
	private List<AbstractManager> managers = new ArrayList<>();

	// The game world currently being played on.
	private WorldTypes gameWorldType;
	private static AbstractWorld currentWorld;

	// The previous game world for re-entering main world after combat
	private WorldTypes previousWorld;

	// The camera being used by the Game Screen to project the game world.
	private PotateCamera camera;

	// The stage the game world is being rendered on to.
	private Stage stage;

	// The UI skin being used by the game for libGDX elements.
	private Skin skin;

	private boolean debugMode = true;

	private OrbEntity currentOrbEntity;

	private MoveManager moveManager = new MoveManager();
	/**
	 * Whether or not we render info over the tiles.
	 */
	// Whether or not we render the movement path for Players.
	private boolean showCoords = false;
	
	// The game screen for a game that's currently running.
	private boolean showPath = false;

	/**
	 * Whether or not we render info over the entities
	 */
	private boolean showCoordsEntity = false;

	/**
	 * Stores visuals to display world name at top of screen
	 */
	private ButtonView worldName;

	/**
	 * Returns an instance of the GM
	 *
	 * @return GameManager
	 */
	public static GameManager get() {
		if (instance == null) {
			instance = new GameManager();
		}
		return instance;
	}

	/**
	 * Private constructor to enforce use of get()
	 */
	private GameManager() {

	}

	/**
	 * Creates object which will show world name at the top of the screen
	 */
	public void createWorldName() {
		worldName = new ButtonView(stage, getManager(TextureManager.class),
				Gdx.graphics.getWidth() / 2 - 60, Gdx.graphics.getHeight() - 100);
		worldName.addLabel(currentWorld.getName(), "world_name_panel",
				skin.getFont("default-font"));
	}

	/**
	 * Updates the object at the top of the screen to display current world
	 */
	public void updateWorldName() {
		//Index is 0 because only one label describing world
		worldName.updateLabel(0, currentWorld.getName());
		worldName.makeVisible(true);
	}

	/**
	 * Changes the world rendered by the screen
	 * @param world
	 * 			World to change to
	 */
	public void changeWorld(WorldTypes world)
	{
		ScreenManager manager = getManagerFromInstance(ScreenManager.class);
		GameScreen screen = manager.getCurrentScreen();
		screen.changeWorld(world);

		camera.setPotate(world == WorldTypes.COMBAT || world == WorldTypes.GHOST_WORLD
				|| world == WorldTypes.TRADE);
		updateWorldName();
	}

	/**
	 * Resets the camera to be centered on the player with original zoom
	 */
	public void resetCam(float zoom) {
		camera.zoom = zoom;
		float playerX = GameManager.getParty().getMainPlayer().getPosition().getRow();
		float playerY = GameManager.getParty().getMainPlayer().getPosition().getCol();
		float[] move = WorldUtil.colRowToWorldCords(playerY, playerX);
		camera.translate(move[0] - camera.position.x, move[1] - camera.position.y);
	}

	public MoveManager getMoveManager()
	{
		return this.moveManager;
	}

	public void setMoveManager(MoveManager moveManager)
	{
		this.moveManager = moveManager;
	}

	public QuestManager getQuestManager() { return QuestManager.get(); }


	public void roomCam() {
		camera.zoom = 1.4f;
		float[] move = WorldUtil.colRowToWorldCords(1, 0);
		camera.translate(move[0] - camera.position.x, move[1] - camera.position.y);
	}

	public void roomCam(float row, float col) {
		camera.zoom = 1.4f;
		float[] move = WorldUtil.colRowToWorldCords(col, row);
		camera.translate(move[0] - camera.position.x, move[1] - camera.position.y);
	}

	/**
	 * Restores previous world
	 * @param enemy enemy to remove
	 */
	public void restoreWorld(EnemyPeon enemy, CombatWorld.CombatLeaveType type, float previousCol, float previousRow)
	{
		if (!isInBossBattle() || type != CombatWorld.CombatLeaveType.DEATH)
		{
			WorldUtil.getWorld(previousWorld).removeEntity(enemy);
		}
		else
		{
			enemy.setCol(previousCol);
			enemy.setRow(previousRow);
			enemy.setNormalTexture();
		}

		if (isInBossBattle())
		{
			restoreWorld(currentOrbEntity, type);
		}

		changeWorld(previousWorld);
	}

	/**
	 * Restores over-world from boss-battle
	 * @param orbEntity current orb to be picked up
	 * @param type Leaving type
	 */
	public void restoreWorld(OrbEntity orbEntity, CombatWorld.CombatLeaveType type)
	{
		if (type == CombatWorld.CombatLeaveType.VICTORY)
		{
			Orb orb;
			switch (orbEntity.getTexture()){
				case "orb_violet":
					TestWorld.ORBS[0] = "null";
					orb = new Orb("Violet");
					WorldUtil.orbCollected();
					break;
				case "orb_blue":
					TestWorld.ORBS[1] = "null";
					orb = new Orb("Blue");
					WorldUtil.orbCollected();
					break;
				case "orb_pink":
					TestWorld.ORBS[2] = "null";
					orb = new Orb("Pink");
					WorldUtil.orbCollected();
					break;
				case "orb_red":
					TestWorld.ORBS[3] = "null";
					orb = new Orb("Red");
					WorldUtil.orbCollected();
					break;
				case "orb_yellow":
					TestWorld.ORBS[4] = "null";
					orb = new Orb("Yellow");
					WorldUtil.orbCollected();
					break;
				default:
					orb = null;
					LOGGER.debug("Orb is null, invalid texture set");
					break;
			}

			if (WorldUtil.getWorld(previousWorld).getEntities().contains(orbEntity) && orb != null){
				WorldUtil.getWorld(previousWorld).removeEntity(orbEntity);
				GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getGetOrbUi().createGetOrbUi(orb);
				GameManager.getManagerFromInstance(InputManager.class).getInventory().addInventory(orb);
			}
		}

		this.setCurrentOrbEntity(null);
	}

	/**
	 * Sets the game's current boss-battle orb
	 * @param orb current orb
	 */
	public void setCurrentOrbEntity(OrbEntity orb)
	{
		this.currentOrbEntity = orb;
	}

	/**
	 * Gets the game's current boss-battle orb
	 * @return current orb
	 */
	public OrbEntity getCurrentOrbEntity()
	{
		return currentOrbEntity;
	}

	/**
	 * Gets whether the player is in a boss battle
	 * @return true if in battle, else false
	 */
	public boolean isInBossBattle()
	{
		return currentOrbEntity != null;
	}

	public void restoreWorld() {

		changeWorld(previousWorld);
	}

	/**
	 * Add a manager to the current instance, making a new instance if none
	 * exist
	 *
	 * @param manager
	 */
	public static void addManagerToInstance(AbstractManager manager) {
		get().addManager(manager);
	}

	/**
	 * Adds a manager component to the GM
	 *
	 * @param manager
	 */
	public void addManager(AbstractManager manager) {
		managers.add(manager);
	}

	/**
	 * Removes the manager component to the GM
	 * @param manager
	 * 			The manager to remove
	 */
	public void removeManager(AbstractManager manager) {
		managers.remove(manager);
	}

	public void removeAllManagers() {
		for (Object manager : managers.toArray()) {
			if (manager instanceof ScreenManager) {
				managers.remove((ScreenManager) manager);
			}
		}
	}

	/**
	 * Retrieves a manager from the list.
	 * If the manager does not exist one will be created, added to the list and returned
	 *
	 * @param type The class type (ie SoundManager.class)
	 * @return A Manager component of the requested type
	 */
	@SuppressWarnings("unchecked")
	public <T extends AbstractManager> T getManager(Class<T> type) {
		/* Check if the manager exists */
		for (AbstractManager m : managers) {
			if (m.getClass() == type) {
				return (T) m;
			}
		}
		LOGGER.info("creating new manager instance");
		/* Otherwise create one */
		AbstractManager newInstance;
		try {
			Constructor<?> ctor = type.getConstructor();
			newInstance = (AbstractManager) ctor.newInstance();
			this.addManager(newInstance);
			return (T) newInstance;
		} catch (Exception e) {
			// Gotta catch 'em all
			LOGGER.error("Exception occurred when adding Manager.");
		}

		LOGGER.warn("GameManager.getManager returned null! It shouldn't have!");
		return null;
	}

	/**
	 * Retrieve a manager from the current GameManager instance, making a new
	 * instance when none are available.
	 *
	 * @param type The class type (ie SoundManager.class)
	 * @return A Manager component of the requested type
	 */
	public static <T extends AbstractManager> T getManagerFromInstance(Class<T> type) {
		return get().getManager(type);
	}

	
	/* ------------------------------------------------------------------------
	 * 				GETTERS AND SETTERS BELOW THIS COMMENT.
	 * ------------------------------------------------------------------------ */

	/**Get entities rendered count
	 * @return entities rendered count
	 */
	public int getEntitiesRendered() {
		return this.entitiesRendered;
	}

	/** Set entities rendered to new amount
	 * @param entitiesRendered the new amount
	 */
	public void setEntitiesRendered(int entitiesRendered) {
		this.entitiesRendered = entitiesRendered;
	}
	/**Get number of entities
	 * @return entities count
	 */
	public int getEntitiesCount() {
		return this.entitiesCount;
	}

	/** Set entities count to new amount
	 * @param entitiesCount the new amount
	 */
	public void setEntitiesCount(int entitiesCount) {
		this.entitiesCount = entitiesCount;
	}

	/**Get tiles rendered count
	 * @return tiles rendered count
	 */
	public int getTilesRendered() {
		return this.tilesRendered;
	}

	/** Set tiles rendered to new amount
	 * @param tilesRendered the new amount
	 */
	public void setTilesRendered(int tilesRendered) {
		this.tilesRendered = tilesRendered;
	}

	/**Get number of tiles
	 * @return tiles count
	 */
	public int getTilesCount() {
		return this.tilesCount;
	}

	/** Set tiles count to new amount
	 * @param tilesCount the new amount
	 */
	public void setTilesCount(int tilesCount) {
		this.tilesCount = tilesCount;
	}
	
	/**
	 * Sets the current game world
	 *
	 * @param world
	 */
	public void setWorld(WorldTypes world) {
		this.previousWorld = this.gameWorldType;
		this.gameWorldType = world;
		this.currentWorld = WorldUtil.createNewWorld(world);
	}

	/**
	 * Change the current world to the ghost world
	 * @param enemy
	 */
	public void setGhostWorld(EnemyPeon enemy) {
		WorldUtil.getWorld(previousWorld).removeEntity(enemy);
		zeroCamera();
		this.currentWorld = WorldUtil.createNewWorld(WorldTypes.GHOST_WORLD);
		gameWorldType = WorldTypes.GHOST_WORLD;
	}

	public void zeroCamera() {
		camera.position.x = 0;
		camera.position.y = 0;
		camera.zoom = 1f;
	}

	public void ghostCamera() {
		camera.position.x = 300;
		camera.position.y = 300;
	}

	/**
	 * Gets the current game world
	 *
	 * @return the current game world
	 */
	public static AbstractWorld getWorld() {
		return currentWorld;
	}

	public WorldTypes getWorldType() {
		return gameWorldType;
	}

	public AbstractWorld getPreviousWorld()
	{
		return WorldUtil.getWorld(previousWorld);
	}

	public boolean getDebugMode() {
		return debugMode;
	}

	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}

	public void setShowCoords(boolean showCoords) {
		this.showCoords = showCoords;
	}

	public boolean getShowCoords() {
		return this.showCoords;
	}

	public void setShowPath(boolean showPath) {
		this.showPath = showPath;
	}

	public boolean getShowPath() {
		return this.showPath;
	}

	public boolean getShowCoordsEntity() {
		return this.showCoordsEntity;
	}

	public void setPreviousWorld(WorldTypes world)
	{
		previousWorld = world;
	}

	public void setCamera(PotateCamera camera) {
		this.camera = camera;
	}

	/**
	 * @return current game's stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage - the current game's stage
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return current game's skin
	 */
	public Skin getSkin() {
		return skin;
	}

	/**
	 * @param skin - the current game's skin
	 */
	public void setSkin(Skin skin) {
		this.skin = skin;
	}

	public PotateCamera getCamera() {
		return camera;
	}

	/**
	 * On tick method for ticking managers with the TickableManager interface
	 *
	 * @param i
	 */
	public void onTick(long i) {
		for (AbstractManager m : managers) {
			if (m instanceof TickableManager) {

				((TickableManager) m).onTick(0);
			}
		}
		currentWorld.onTick(0);
	}

	/**
	 * Getter method for the party in the game
	 * @return
	 */
	public static Party getParty() {
		return party;
	}

	/**
	 * Method for modifying the position of the party, as you change worlds
	 * @param row
	 * @param col
	 */
	public void updatePartyPosition(float row, float col) {
		for (PlayerPeon player : party.getPlayersList()) {
			player.setRow(row);
			player.setCol(col);
		}
	}

	/**
	 * To set the party that is currently in the game
	 * @param gameParty (Party) the party we want to add.
	 */
	public static void setUpParty(Party gameParty) {
		party = gameParty;
	}

	public void swapPartyMembers(PlayerPeon playerSwappedIn) {
		PlayerPeon playerSwappedOut = party.getPlayer(playerSwappedIn.getType());

		//set position of the player to be added into the game
		playerSwappedIn.setPosition(playerSwappedOut.getPosition().getCol(),
				playerSwappedOut.getRow(), 1);

		//remove old player and add in new player
		getWorld().removeEntity(playerSwappedOut);
		getWorld().addEntity(playerSwappedIn);

		party.swapPlayer(playerSwappedIn.getType(), playerSwappedIn);
	}

	public static void setInGameMech(Mech mech) {
		inGameMech = mech;
	}

	public static Mech getInGameMech() {
		return inGameMech;
	}

	public void addPlayersToGame(PlayerPeon... playersToAdd) {
		players.addAll(Arrays.asList(playersToAdd));

	}

	public List<PlayerPeon> getPlayersInGame() {
		return players;
	}
}
