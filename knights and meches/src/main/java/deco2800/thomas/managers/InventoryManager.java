package deco2800.thomas.managers;

public class InventoryManager extends TickableManager {

    private int stone = 0;

    @Override
    public void onTick(long i) {

    }

    public int getStone() {
        return stone;
    }

    public void addStone(int stone) {
        this.stone += stone;
    }
}
