package deco2800.thomas.tasks;

import deco2800.thomas.entities.AgentEntity;

/**
 * A class used to replace movement when we stop moving. Intended to be a placeholder task.
 */
public class StopMovementTask extends AbstractTask{
	
	protected AgentEntity entity;
	
	
	public StopMovementTask(AgentEntity entity) {
		super(entity);
		this.entity = entity;
	}

	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	public void onTick(long tick) {
		// Do nothing. 
	}

	@Override
	public boolean isAlive() {
		return false;
	};

}
