package deco2800.thomas.tasks;

import java.util.List;

import deco2800.thomas.GameScreen;
import deco2800.thomas.entities.AgentEntity;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.PathFindingService;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.Tile;
import deco2800.thomas.worlds.WorldTypes;

public class MovementTask extends AbstractTask{
	
	private boolean complete;
	
	private boolean computingPath = false;
	private boolean taskAlive = true;
	
	AgentEntity entity;
	SquareVector destination;
	private Direction direction;
	
	private List<Tile> path;

	public MovementTask(AgentEntity entity, SquareVector destination) {
		super(entity);
		
		this.entity = entity;
		this.destination = destination;
		this.direction = Direction.NONE;
		this.complete = false;    //path == null || path.isEmpty();
	}

	public MovementTask(AgentEntity entity, Direction direction) {
		super(entity);
		
		this.entity = entity;
		this.destination = null;
		this.direction = direction;
		this.complete = false;    //path == null || path.isEmpty();

		if (this.direction == Direction.UP) {
			this.destination = new SquareVector(entity.getCol(), entity.getRow() + 1);
		} else if (this.direction == Direction.DOWN) {
			this.destination = new SquareVector(entity.getCol(), entity.getRow() - 1);
		} else if (this.direction == Direction.LEFT) {
			this.destination = new SquareVector(entity.getCol() - 1, entity.getRow());
		} else if (this.direction == Direction.RIGHT) {
			this.destination = new SquareVector(entity.getCol() + 1, entity.getRow());
		}

	}

	public Direction getDirection() {
		return this.direction;
	}

	@Override
	public void onTick(long tick) {
		GameManager game = GameManager.get();
		ScreenManager manager = game.getManagerFromInstance(ScreenManager.class);
		GameScreen screen = manager.getCurrentScreen();
		if (screen.getCameraDisabled() && GameManager.get().getWorldType() != WorldTypes.COMBAT) {
			GameManager.get().resetCam(1.3f);
		}
		if(path != null) {
			// We have a path.
			if(path.isEmpty()) {
				complete = true;
			} else {
				entity.moveTowards(path.get(0).getCoordinates());
				// This is a bit of a hack.
				if(entity.getPosition().isCloseEnoughToBeTheSame(path.get(0).getCoordinates())) {
					path.remove(0);
				}
			}			
		} else if (computingPath) {
			// Change sprite to show waiting??

		} else if (this.direction != Direction.NONE) {
			if (this.direction == Direction.UP) {
				if (WorldUtil.isWalkable((float) Math.round(entity.getCol()), (float) Math.round(entity.getRow() + 0.25f))) {
					entity.moveTowards(new SquareVector(entity.getCol(), entity.getRow() + 0.25f));
				}
			} else if (this.direction == Direction.DOWN) {
				if (WorldUtil.isWalkable((float) Math.round(entity.getCol()), (float) Math.round(entity.getRow() - 0.25f))) {
					entity.moveTowards(new SquareVector(entity.getCol(), entity.getRow() - 0.25f));
				}

			} else if (this.direction == Direction.LEFT) {
				if (WorldUtil.isWalkable((float) Math.round(entity.getCol() - 0.25f), (float) Math.round(entity.getRow()))) {
					entity.moveTowards(new SquareVector(entity.getCol() - 0.25f, entity.getRow()));
				}

			} else if (this.direction == Direction.RIGHT && WorldUtil.isWalkable
					((float) Math.round(entity.getCol() + 0.25f), (float) Math.round(entity.getRow()))) {
				entity.moveTowards(new SquareVector(entity.getCol() + 0.25f, entity.getRow()));
			}
			// For indefinite movement in this direction. 
			
		} else {
			// Ask for a path.
			computingPath = true;
			GameManager.get().getManager(PathFindingService.class).addPath(this);
		}
	}

	@Override
	public boolean isComplete() {
		return complete;
	}

	public void setPath(List<Tile> path) {
		if (path == null) {
			taskAlive = false;
		}
		this.path = path;
		computingPath = false;
	}
	
	public List<Tile> getPath() {
		return path;
	}

	@Override
	public boolean isAlive() {
		return taskAlive;
	}

}
