package deco2800.thomas.tasks;

// Public enum for MovementTask and PlayerPeon for moving in directions. 

public enum Direction 
{ 
    NONE, UP, DOWN, LEFT, RIGHT
}