package deco2800.thomas.items;

/**
 * Abstract Class for Material Items
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public abstract class MaterialItem extends AbstractItem{

    /**
     * Constructor for Material Items
     * @param name name for Material Items
     * @param value value for Material Items
     * @param id id of Material Items
     * @param description Description for Material Items
     */
    public MaterialItem(String name,  String id,Integer value, String description, int size, String imgName) {
        super(name, id, value, description, size, imgName);
    }

    /**
     * Abstract Method for getCapacity()
     * @return String Capacity of Material Items
     */
    public abstract String getAbility();

    /**
     * Abstract Method for use()
     */
    public abstract void use();
}
