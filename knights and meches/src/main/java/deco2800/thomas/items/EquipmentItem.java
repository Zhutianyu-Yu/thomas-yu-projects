package deco2800.thomas.items;

/**
 * Items that player can equipped
 */
public abstract class EquipmentItem extends AbstractItem {

    //Attack number of the equipment. Could improve attack/ATK of the user.
    private int damage;

    //Defense number of the equipment. Could improve defense/DEF of the user.
    private int armour;

    /**
     * Constructor for Body
     *
     * @param name        name of this equipment
     * @param value       gold value of this equipment
     * @param id          id of this equipment
     * @param description description of the equipment
     * @param size        max number of this equipments can save in one inventory box
     * @param damage      damage of this equipment, it will be added to user
     * @param armour      armour of this equipment, it will be added to user
     *
     */
    public EquipmentItem(String name, String id, int value, String description, int size , String imgName, int damage, int armour) {

        super(name, id, value, description, size, imgName);
        this.damage = damage;
        this.armour = armour;
    }

    @Override
    public String getType(){
        return "EquipmentItem";
    }

    @Override
    public int getDamage(){
        return this.damage;
    }

    @Override
    public int getArmour() {
        return this.armour;
    }
}
