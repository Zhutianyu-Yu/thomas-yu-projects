package deco2800.thomas.items;

import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.items.Consumable.*;
import deco2800.thomas.items.Material.*;
import deco2800.thomas.items.Equipment.*;
import deco2800.thomas.items.MechComponent.Body;
import deco2800.thomas.items.Other.*;


/**
 * AbstractItem is an item that can be take or used.
 * AbstractItem does not need to be rendered.
 */
public abstract class AbstractItem {
    private String name;
    private String id;
    private int value;
    private String description;
    private int size;
    private String imgName;

    /**
     * Constructor for Body
     *
     * @param name        String name for the item
     * @param value       Gold value for the item
     * @param id          String id for the item. Start from "001"
     * @param description Description for the item
     * @param size        Max number of same items can save in one inventory box
     */
    public AbstractItem(String name, String id, int value, String description, int size, String imgName) {
        this.name = name;
        this.value = value;
        this.id = id;
        this.description = description;
        this.size = size;
        this.imgName = imgName;
    }

    public String getItemIdString() {
        return this.id;
    }

    public String getItemName() {
        return this.name;
    }

    public int getItemValue() {
        return this.value;
    }

    public String getItemDescription() {
        return this.description;
    }

    public int getItemSize() {
        return this.size;
    }

    public String getImgName() { return this.imgName; }

    public void setItemSize(int size){
        this.size = size;
    }

    public void setItemValue(int value){
        this.value = value;
    }


    public String getType(){return "";}

    public void upgrade() {
        if (this.id.equals("015")) {
            this.name = "Gold Body";
            this.value = 200;
            this.id = "019";
            this.imgName = "Gold_Body.png";
            ((Body)this).setHP(20);
        }
        if (this.id.equals("017")) {
            this.name = "Gold Legs";
            this.value = 200;
            this.id ="021";
            this.imgName = "Gold_Leg.png";
        }
        if (this.id.equals("016")) {
            this.name = "Gold Arm";
            this.value = 200;
            this.id="020";
            this.imgName = "Gold_Arm.png";
        }
        if (this.id.equals("018")) {
            this.name = "Rare energy storage ";
            this.value = 200;
            this.id="022";
            this.imgName = "Rare_energy_storage .png";
        }
    }

    public static AbstractItem createItemById(String id) {

        switch (id) {
            case "002":
                return new WoodenSword();
            case "004":
                return new WoodenArmour();
            case "008":
                return new HealthPotion("small");
            case "108":
                return new HealthPotion("medium");
            case "208":
                return new HealthPotion("large");
            case "009":
                return new ManaPotion("small");
            case "109":
                return new ManaPotion("medium");
            case "209":
                return new ManaPotion("large");
            case "010":
                return new ResurrectionScroll();
            case "011":
                return new Wood();
            case "012":
                return new Iron();
            case "013":
                return new Gold();
            case "014":
                return new Diamond();
            case "023":
                return new TeleportScroll();
            default:
                return null;
        }
    }

    public String getEquipmentType() {
        return "Not Equipment";
    }

    public int getDamage() {return 0;}

    public int getArmour() {return 0;}

    public String getOrbColour() {return "None";}

    public void setOrbColour(String colour) {}

    public void backPosition(PlayerPeon playerPeon){}

    public void use(float col, float row, int height){}

    public void setBomb(){}
    public void useBomb(){}
    /*
     Hi Items Team, I wrote these following functions to help with checking if
     items are the same in the inventory. Please contact me about this if you
     want/need to change it
        - Alex (Inventory Team)
     */

    /**
     * Checks if the two given items are the same with respect for crafting
     * @return Returns true if they are the same with respect to crafting, false
     * otherwise
     */
    public static boolean checkIfSame(AbstractItem item1, AbstractItem item2) {
        if (item1 == null || item2 == null) {
            return (item1 == item2);
        } else {
            return (item1.id.equals(item2.id) && item1.size == item2.size);
        }
    }

    /**
     * Checks if this item id the same as the given item with respect to
     * crafting
     * @return Returns true if this item is the same as the given one with
     * respect to crafting
     */
    public boolean checkIfSame(AbstractItem otherItem) {
        return AbstractItem.checkIfSame(this, otherItem);
    }

    @Override
    public int hashCode() {
        int prime = 17;
        int hash = 0;
        hash += (value + size) * prime;
        hash += name.hashCode() + id.hashCode() + description.hashCode() +
                imgName.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (!(obj instanceof AbstractItem)) {
            return false;
        }
        else {
            AbstractItem otherItem = (AbstractItem) obj;
            if (otherItem.hashCode() != this.hashCode()) {
                return false;
            } else {
                return (this.name.equals(otherItem.name) &&
                        this.id.equals(otherItem.id) &&
                        this.description.equals(otherItem.description) &&
                        this.imgName.equals(otherItem.imgName) &&
                        this.value == otherItem.value &&
                        this.size == otherItem.size);
            }
        }
    }
}


