package deco2800.thomas.items;

/**
 * Abstract class for MechComponent items
 */
public abstract class MechComponentItem extends AbstractItem{

    private int hp;

    /**
     * Constructor for MechComponent items
     * @param name name for MechComponent items
     * @param value value for MechComponent items
     * @param id id of MechComponent items
     * @param description Description for MechComponent items
     * @param size Max number for same items can save in one inventory box
     */
    public MechComponentItem(String name,  String id, Integer value, String description, int size,String imgName,
                             int hp,int mp,
                             int attack, int Defense) {
        super(name, id, value, description, size, imgName);
        this.hp = hp;
    }

    public String getType(){
        return "MechComponentItem";
    }

    public  int getHP() {
        return hp;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

}
