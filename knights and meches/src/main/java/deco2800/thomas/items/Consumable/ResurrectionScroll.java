package deco2800.thomas.items.Consumable;

import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.items.ConsumableItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Resurrection Scroll
 *
 * @author Yonghao Wu
 * @since <pre>15 Sep 2020</pre>
 * @version 1.0
 */
public class ResurrectionScroll extends ConsumableItem {

    /**
     * Constructor for Resurrection Scroll
     */
    public ResurrectionScroll() {
        super("ResurrectionScroll",
                "010",
                20,
                "Scroll to a Different World",
                1,
                "Resurrection_Scroll.png");

        Logger LOG = LoggerFactory.getLogger(ResurrectionScroll.class);
        LOG.info("Resurrection Scroll Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Resurrection Scroll has no capacity
     */
    @Override
    public String getCapacity() {
        return null;
    }

    /**
     * Method to use the Resurrection Scroll
     */
    public void use(PlayerPeon playerPeon){
        // use() method will be implemented in later sprints
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    /**
     * Overrides the .equals() method for ResurrectionScroll Class
     * @param other Other ResurrectionScroll object to be compared
     * @return True if the ResurrectionScroll objects are the same
     */
    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof ResurrectionScroll)) {
            return false;
        }

        ResurrectionScroll scroll = (ResurrectionScroll) other;
        return scroll.getItemValue() == this.getItemValue() && scroll.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Resurrection Scroll
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}