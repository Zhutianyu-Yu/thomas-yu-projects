package deco2800.thomas.items.Consumable;

import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.items.ConsumableItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Health Potion
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class HealthPotion extends ConsumableItem {

    private final String capacity;

    /**
     * Constructor for Health Potion
     * @param capacity String: small or medium or large
     */
    public HealthPotion(String capacity) {
        super("HealthPotion", 
                "008", 
                100, 
                "A Refresher After Battle", 
                5, 
                "Small_Health_Potion.png");

        this.capacity = capacity;

        // Capacity of the Item depends on its size
        switch (capacity) {
            case "small":
                this.setItemSize(3);
                this.setItemValue(10);
                break;
            case "medium":
                this.setItemSize(2);
                this.setItemValue(20);
                break;
            case "large":
                this.setItemSize(1);
                this.setItemValue(30);
                break;
            default:
        }

        Logger LOG = LoggerFactory.getLogger(HealthPotion.class);
        LOG.info("HealthPotion Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue());
    }

    /**
     * Get the Capacity of the Potion
     * @return String: small, medium or large
     */
    public String getCapacity(){
        return capacity;
    }

    /**
     * Method to use the mana potion
     * Effects of the potion depends on its capacity
     */
    public void use(PlayerPeon playerPeon){
        switch (capacity) {
            case "small":
                playerPeon.setHealth(playerPeon.getHealth() + 100);
                break;
            case "medium":
                playerPeon.setHealth(playerPeon.getHealth() + 200);
                break;
            case "large":
                playerPeon.setHealth(playerPeon.getHealth() + 300);
                break;
            default:
        }
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    /**
     * Overrides the .equals() method for HealthPotion Class
     * @param other Other HealthPotion object to be compared
     * @return True if the HealthPotion objects are the same
     */
    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof HealthPotion)) {
            return false;
        }

        HealthPotion hp = (HealthPotion) other;
        return hp.capacity.equals(this.capacity) && hp.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of HealthPotion
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}