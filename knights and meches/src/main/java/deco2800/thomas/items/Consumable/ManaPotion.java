package deco2800.thomas.items.Consumable;

import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.items.ConsumableItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Mana Potion
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class ManaPotion extends ConsumableItem {

    private final String capacity;

    /**
     * Constructor for Mana Potion
     * @param capacity String: small or medium or large
     */
    public ManaPotion(String capacity) {
        super("ManaPotion",
                "009",
                100,
                "See The Mana Magic!",
                5,
                "Small_Mana_Potion.png");

        this.capacity = capacity;

        // Capacity of the Item depends on its size
        switch (capacity) {
            case "small":
                this.setItemSize(3);
                this.setItemValue(10);
                break;
            case "medium":
                this.setItemSize(2);
                this.setItemValue(20);
                break;
            case "large":
                this.setItemSize(1);
                this.setItemValue(30);
                break;
            default:
        }

        Logger LOG = LoggerFactory.getLogger(ManaPotion.class);
        LOG.info("ManaPotion Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Get the Capacity of the Potion
     * @return String: small, medium or large
     */
    public String getCapacity(){
        return capacity;
    }

    /**
     * Method to use the mana potion
     * Effects of the potion depends on its capacity
     */
    public void use(PlayerPeon playerPeon){
        switch (capacity) {
            case "small":
                playerPeon.addMana(10);
                break;
            case "medium":
                playerPeon.addMana(20);
                break;
            case "large":
                playerPeon.addMana(30);
                break;
            default:
        }
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    /**
     * Overrides the .equals() method for ManaPotion Class
     * @param other Other ManaPotion object to be compared
     * @return True if the ManaPotion objects are the same
     */
    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof ManaPotion)) {
            return false;
        }

        ManaPotion mp = (ManaPotion) other;
        return mp.capacity.equals(this.capacity) && mp.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of ManaPotion
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}