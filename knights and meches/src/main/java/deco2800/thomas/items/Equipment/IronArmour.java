package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.EquipmentItem;

public class IronArmour extends EquipmentItem {

    public IronArmour() {
        super("Iron Armour",
                "007",
                50,
                "",
                1,
                "Armor_Iron.png",
                0,
                10
        );
    }

    @Override
    public String getEquipmentType() {
        return "Armour";
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof IronArmour)) {
            return false;
        }

        IronArmour ironAm = (IronArmour) other;

        return ironAm.getArmour() == this.getArmour() &&
                ironAm.getDamage() == this.getDamage() &&
                ironAm.getItemIdString().equals(this.getItemIdString())
                && ironAm.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Wooden Armour
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}
