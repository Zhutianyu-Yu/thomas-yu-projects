package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.EquipmentItem;

public class IronSword extends EquipmentItem {

    public IronSword() {
        super("Iron Sword",
                "005",
                50,
                "",
                1,
                "Sword_iron.png",
                10,
                0
        );
    }

    @Override
    public String getEquipmentType() {
        return "Weapon";
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof IronSword)) {
            return false;
        }

        IronSword ironSw = (IronSword) other;

        return ironSw.getArmour() == this.getArmour() &&
                ironSw.getDamage() == this.getDamage() &&
                ironSw.getItemIdString().equals(this.getItemIdString())
                && ironSw.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Wooden Armour
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}
