package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.EquipmentItem;

public class WoodenSword extends EquipmentItem {

    public WoodenSword() {
        super("Wooden Sword",
                "002",
                10,
                "Every legendary adventure begins with a wooden sword.",
                3,
                "Sword_wood.png",
                3,
                0
        );
    }

    @Override
    public String getEquipmentType() {
        return "Weapon";
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof WoodenSword)) {
            return false;
        }

        WoodenSword woodSw = (WoodenSword) other;

        return woodSw.getArmour() == this.getArmour() &&
                woodSw.getDamage() == this.getDamage() &&
                woodSw.getItemIdString().equals(this.getItemIdString())
                && woodSw.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Wooden Sword
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();

    }
}
