package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.EquipmentItem;

public class WoodenArmour extends EquipmentItem {

    public WoodenArmour() {
        super("Wooden Armour",
                "004",
                10,
                "The strong never need armour, but luckily you are not one of them.",
                1,
                 "Armor_Wood.png",
                0,
                3
        );
    }

    @Override
    public String getEquipmentType() {
        return "Armour";
    }


    public static WoodenArmour createWoodenArmour() {
        return new WoodenArmour();
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof WoodenArmour)) {
            return false;
        }

        WoodenArmour woodAm = (WoodenArmour) other;

        return woodAm.getArmour() == this.getArmour() &&
                woodAm.getDamage() == this.getDamage() &&
                woodAm.getItemIdString().equals(this.getItemIdString())
                && woodAm.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Wooden Armour
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();

    }
}
