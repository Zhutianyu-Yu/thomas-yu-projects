package deco2800.thomas.items.MechComponent;
import deco2800.thomas.items.MechComponentItem;

public class Body extends MechComponentItem {


    final public static String id = "015";
    final public static String name = "Iron Body";
    final public static int value = 100;
    final public static String description = ".";
    final public static int size = 1;
    final public static int hp = 10;
    final public static int mp = 10;
    final public static int attack = 10;
    final public static int defence = 10;
    final public  static String imgName ="Iron_Body.png" ;



    public Body() {
        super(name, id, value, description, size,imgName, hp,mp,attack,defence);
    }





    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Body) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }

    /**
     * @return A String Representation of Body
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }

}


