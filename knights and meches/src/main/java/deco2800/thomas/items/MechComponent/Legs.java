package deco2800.thomas.items.MechComponent;
import deco2800.thomas.items.MechComponentItem;


public class Legs extends MechComponentItem {

    private static String id= "017";
    private static String name= "Iron leg";
    private static int value = 100;
    private static String description="Iron made leg mech";
    private static int size = 1;
    private static int hp = 10;
    private static int mp = 10;
    private static int attack = 10;
    private static int defence = 10;
    private  static String imgName ="Iron_leg.png";

    public Legs() {
        super(name,  id, value, description, size,imgName,hp,mp,attack,defence);
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Legs) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }

    /**
     * @return A String Representation of Legs
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}