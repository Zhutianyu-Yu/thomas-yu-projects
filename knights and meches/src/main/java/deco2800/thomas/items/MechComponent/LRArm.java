package deco2800.thomas.items.MechComponent;
import deco2800.thomas.items.MechComponentItem;


public class LRArm extends MechComponentItem {
    private static String id = "016";
    private static String name = "Iron Arm";
    private static int value = 100;
    private static String description = "Iron made arm mech";
    private static int size = 1;
    private static int HP = 10;
    private static int MP = 10;
    private static int Attack = 10;
    private static int Defence = 10;
    private static String imgName ="Iron_Arm.png";

    public LRArm() {
        super(name, id, value, description, size,imgName, HP, MP, Attack, Defence);
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LRArm) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }

    /**
     * @return A String Representation of LRArm
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}