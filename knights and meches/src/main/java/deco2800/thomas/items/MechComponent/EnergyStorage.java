package deco2800.thomas.items.MechComponent;
import deco2800.thomas.items.MechComponentItem;

public class EnergyStorage extends MechComponentItem {
    private static String id = "018";
    private static String name = "Common energy storage";
    private static int value = 100;
    private static int size = 1;
    private static String description = "Common energy storage";

    private static int hp = 10;
    private static int mp = 10;
    private static int attack = 10;
    private static int defence = 10;
    final public static String imgName ="Common_energy_storage.png";


    public EnergyStorage() {
        super(name, id, value, description, size,imgName, hp,mp,attack,defence);
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EnergyStorage) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }

    /**
     * @return A String Representation of Energy Storage
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}
