package deco2800.thomas.items.Orb;

import deco2800.thomas.items.QuestRelatedItem;

public class Orb extends QuestRelatedItem {

    /**
     * Orb colour for distinguishing different orbs:
     * tentative: Red, Blue, Violet, Yellow, Pink
     */
    private String colour;
    private int damage;
    private int armour;

    public Orb(String colour) {
        super(  "Orb",
                "001",
                -1,
                "The necessary quest items to win the game.",
                1,
                "Orb_"+colour+".png"
        );
        if(colour.equals("Violet")) {
            this.damage = 30;
            this.armour = 30;
        } else if (colour.equals("Red")) {
            this.damage = 30;
            this.armour = 30;
        } else if (colour.equals("Yellow")) {
            this.damage = 30;
            this.armour = 30;
        } else if (colour.equals("Blue")) {
            this.damage = 30;
            this.armour = 30;
        } else if (colour.equals("Pink")) {
            this.damage = 30;
            this.armour = 30;
        }
        this.colour = colour;
    }

    public String getOrbColour() {
        return this.colour;
    }

    public void setOrbColour(String newColour) {
        this.colour = newColour;
    }

    @Override
    public String getEquipmentType() {
        return "Orb";
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof Orb)) {
            return false;
        }

        Orb orb = (Orb) other;

        return orb.colour.equals(this.colour) && orb.armour == this.armour && orb.damage ==
                this.damage && orb.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Orb
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }

    @Override
    public int getDamage(){
        return this.damage;
    }

    @Override
    public int getArmour(){
        return this.armour;
    }
}