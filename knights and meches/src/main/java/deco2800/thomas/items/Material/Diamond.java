package deco2800.thomas.items.Material;

import deco2800.thomas.items.MaterialItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Diamond
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class Diamond extends MaterialItem {

    private final String ability;

    /**
     * Constructor for Diamond
     */
    public Diamond() {
        super("Diamond",
                "014",
                70,
                "Precious Gem",
                3,
                "Diamond.png");

        this.ability = "D";

        Logger LOG = LoggerFactory.getLogger(Diamond.class);
        LOG.info("Diamond Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Get the Ability of the Diamond
     * @return String: Ability will be implemented in the next sprint
     */
    public String getAbility(){
        return ability;
    }

    /**
     * Method to use the Diamond
     */
    public void use(){
        // use() method will be implemented in later sprints
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof Diamond)) {
            return false;
        }

        Diamond material = (Diamond) other;

        return material.ability.equals(this.ability) &&
                material.getItemValue() == this.getItemValue() &&
                material.getItemIdString().equals(this.getItemIdString())
                && material.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Diamond
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}