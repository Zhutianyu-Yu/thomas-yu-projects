package deco2800.thomas.items.Material;

import deco2800.thomas.items.MaterialItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Gold Item
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class Gold extends MaterialItem {
    private final String ability;

    /**
     * Constructor for Gold Item
     */
    public Gold() {
        super("Gold",
                "013",
                50,
                "Valuable Metal",
                3,
                "Gold.png");

        this.ability = "G";

        Logger LOG = LoggerFactory.getLogger(Gold.class);
        LOG.info("Gold Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Get the Ability of the Gold
     * @return String: Ability will be implemented in the next sprint
     */
    public String getAbility(){
        return ability;
    }

    /**
     * Method to use the Gold Item
     */
    public void use(){
        // use() method will be implemented in later sprints
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof Gold)) {
            return false;
        }

        Gold material = (Gold) other;

        return material.ability.equals(this.ability) &&
                material.getItemValue() == this.getItemValue() &&
                material.getItemIdString().equals(this.getItemIdString())
                && material.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Gold
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}
