package deco2800.thomas.items.Material;

import deco2800.thomas.items.MaterialItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Wood
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class Wood extends MaterialItem {
    private final String ability;

    /**
     * Constructor for Wood
     */
    public Wood() {
        super(
                "Wood",
                "011",
                10,
                "Nature's Pillar",
                3,
                "Wood.png");

        this.ability = "W";

        Logger LOG = LoggerFactory.getLogger(Wood.class);
        LOG.info("Wood Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Get the Ability of the Wood
     * @return String: Ability will be implemented in the next sprint
     */
    public String getAbility(){
        return ability;
    }

    /**
     * Method to use the Iron
     * Effects of the Iron depends on its capacity
     */
    public void use(){
        // use() method will be implemented in later sprints
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof Wood)) {
            return false;
        }

        Wood material = (Wood) other;

        return material.ability.equals(this.ability) &&
                material.getItemValue() == this.getItemValue() &&
                material.getItemIdString().equals(this.getItemIdString())
                && material.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Wood
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}