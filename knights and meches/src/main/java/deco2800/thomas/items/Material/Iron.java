package deco2800.thomas.items.Material;

import deco2800.thomas.items.MaterialItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Iron
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public class Iron extends MaterialItem {
    private final String ability;

    /**
     * Constructor for Iron
     */
    public Iron() {
        super("Iron",
                "012",
                30,
                "Industrial Good",
                3,
                "Iron.png");

        this.ability = "I";

        Logger LOG = LoggerFactory.getLogger(Iron.class);
        LOG.info("Iron Created with size: {} and Value: {}", this.getItemSize(), this.getItemValue() );
    }

    /**
     * Get the Ability of the Iron
     * @return String: Ability will be implemented in the next sprint
     */
    public String getAbility(){
        return ability;
    }

    /**
     * Method to use the Iron
     * Effects of the Iron depends on its capacity
     */
    public void use(){
        // use() method will be implemented in later sprints
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (!(other instanceof Iron)) {
            return false;
        }

        Iron material = (Iron) other;

        return material.ability.equals(this.ability) &&
                material.getItemValue() == this.getItemValue() &&
                material.getItemIdString().equals(this.getItemIdString())
                && material.hashCode() == this.hashCode();
    }

    /**
     * @return A String Representation of Iron
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}