package deco2800.thomas.items.ItemUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import deco2800.thomas.items.Orb.Orb;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;

public class GetOrbUi extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver {


    private Image image;

    /**
     * Constructor for Congratulation UI
     */
    public GetOrbUi(){
        this.image = null;
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     * Method to display the Congratulation Image
     */
    public void createGetOrbUi(Orb orb) {
        String imgPath = "items/Orb/Get_" + orb.getOrbColour() + ".png";
        this.image = new Image(new Texture(Gdx.files.internal(imgPath)));
        this.image.setPosition((float) (Gdx.graphics.getWidth() * 0.17), (float) (Gdx.graphics.getHeight() * 0.2));
        this.image.setSize((float) (Gdx.graphics.getWidth() * 0.7), (float) (Gdx.graphics.getHeight() * 0.7));
        this.addActor(this.image);

        // Touch the image then image display
        image.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                image.clear();
                image.remove();
                removeActor(image);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });
    }

    @Override
    public void notifyKeyDown(int keycode){

    }

    @Override
    public void notifyKeyUp(int keycode) {

    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {

    }
}
