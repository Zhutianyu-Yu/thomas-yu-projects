package deco2800.thomas.items.ItemUI;

import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Creates an Illustration of all Items when the user press "O" key in the game
 * @author Yonghao Wu
 * @since <pre>17 Sep 2020</pre>
 * @version 1.0
 */
public class IllustrationUI extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver {
    private boolean click;
    private Image image;

    /**
     * Constructor for IllustrationUI
     */
    public IllustrationUI() {
        this.image = null;
        this.click = false;
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     * Method to display the Illustration Image
     */
    public void createIllustration() {

        if (!this.click) {
            this.image = new Image(new Texture(Gdx.files.internal("items/Illustration.jpg")));
            this.image.setPosition(100, 200);
            this.addActor(this.image);
            this.click = true;
        } else {
            this.removeActor(this.image);
            this.click = false;
        }
    }

    /**
     * Display Illustration Image when the "O" key is pressed
     * @param keycode the key being pressed
     */
    @Override
    public void notifyKeyDown(int keycode) {
        if (keycode == Input.Keys.O) {
            this.createIllustration();
            GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");
        }
    }

    @Override
    public void notifyKeyUp(int keycode) {
        // no keyboard interaction needed
    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
        // no keyboard interaction needed
    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {
        // no keyboard interaction needed
    }

    public boolean getDisplayed() {
        return this.click;
    }
}
