package deco2800.thomas.items.ItemUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


import deco2800.thomas.entities.AbstractEntity;

import deco2800.thomas.entities.Chest;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;


import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.entities.combat.EnemyPeon;

import deco2800.thomas.items.*;
import deco2800.thomas.items.Other.TeleportScroll;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;
import deco2800.thomas.worlds.RoomWorld;

import java.util.List;

/**
 * Class for present the open chest information
 * Click the right top button close
 * Click L to open and close
 */
public class BoxUi extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver{
    /** The background  image to display */
    private Image image;
    /** show item icon */
    private ImageButton item1Thum;
    /** show item icon */
    private ImageButton item2Thum;
    /** show item icon */
    private ImageButton item3Thum;
    /** show item icon */
    private ImageButton item4Thum;
    /** show close icon */
    private ImageButton closeBtn;

    private Chest bomb = null;
    private boolean setBomb = false;

    private int itemNum;


    /** The constructor of this class*/
    public BoxUi(){
        this.image = null;
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }


    public Chest getBomb() {
        return this.bomb;
    }

    public boolean getSetBomb() {
        return this.setBomb;
    }

    /** Class use to generate the chest UI*/


    public void createBox(Chest chest, int itemNum) {

        //background
        this.image = new Image(new Texture(Gdx.files.internal("items/box_ui.png")));
        this.image.setPosition((float) (Gdx.graphics.getWidth() * 0.3),(float) (Gdx.graphics.getHeight() * 0.4));
        this.image.setSize((float) (Gdx.graphics.getWidth() * 0.5), (float) (Gdx.graphics.getHeight() * 0.4));
        this.addActor(this.image);

        //thumbnails
        item1Thum = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(this.generateImgPath(chest.getItems().get(0))))));
        item1Thum.setPosition((float) (Gdx.graphics.getWidth() * 0.32),(float) (Gdx.graphics.getHeight() * 0.6));
        item1Thum.setSize((float) (Gdx.graphics.getWidth() * 0.09), (float) (Gdx.graphics.getHeight() * 0.09));

        item1Thum.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                item1Thum.clear();
                AbstractItem item = chest.getItems().get(0);
                GameManager.getManagerFromInstance(InputManager.class).getInventory().addInventory(item);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        //thumbnails
        item2Thum = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(this.generateImgPath(chest.getItems().get(1))))));
        item2Thum.setPosition((float) (Gdx.graphics.getWidth() * 0.45),(float) (Gdx.graphics.getHeight() * 0.6));
        item2Thum.setSize((float) (Gdx.graphics.getWidth() * 0.09), (float) (Gdx.graphics.getHeight() * 0.09));

        item2Thum.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                item2Thum.clear();
                AbstractItem item = chest.getItems().get(1);
                GameManager.getManagerFromInstance(InputManager.class).getInventory().addInventory(item);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });


        item3Thum = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(this.generateImgPath(chest.getItems().get(2))))));
        item3Thum.setPosition((float) (Gdx.graphics.getWidth() * 0.57),(float) (Gdx.graphics.getHeight() * 0.6));
        item3Thum.setSize((float) (Gdx.graphics.getWidth() * 0.09), (float) (Gdx.graphics.getHeight() * 0.09));

        item3Thum.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                item3Thum.clear();
                AbstractItem item = chest.getItems().get(2);
                GameManager.getManagerFromInstance(InputManager.class).getInventory().addInventory(item);

            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });
        //thumbnails
        item4Thum = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(this.generateImgPath(chest.getItems().get(3))))));
        item4Thum.setPosition((float) (Gdx.graphics.getWidth() * 0.7),(float) (Gdx.graphics.getHeight() * 0.6));
        item4Thum.setSize((float) (Gdx.graphics.getWidth() * 0.09), (float) (Gdx.graphics.getHeight() * 0.09));

        item4Thum.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                item4Thum.clear();
                AbstractItem item = chest.getItems().get(3);
                GameManager.getManagerFromInstance(InputManager.class).getInventory().addInventory(item);

            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");
                super.touchDown(event, x, y, pointer, button);
                return true;

            }
        });
        // Close thumbnails
        closeBtn = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/Close_Button.png"))));
        closeBtn.setPosition((float) (Gdx.graphics.getWidth() * 0.7),(float) (Gdx.graphics.getHeight() * 0.7));
        closeBtn.setSize((float) (Gdx.graphics.getWidth() * 0.1), (float) (Gdx.graphics.getHeight() * 0.1));

        closeBtn.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                item1Thum.clear();
                item2Thum.clear();
                item3Thum.clear();
                item4Thum.clear();
                closeBtn.clear();
                image.remove();
                removeActor(image);
                item1Thum.remove();
                item2Thum.remove();
                item3Thum.remove();
                item4Thum.remove();
                closeBtn.remove();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        this.addActor(item1Thum);
        this.addActor(item2Thum);
        this.addActor(item3Thum);
        this.addActor(item4Thum);
        if(itemNum ==3){
            item4Thum.remove();
            this.removeActor(item4Thum);
        }
        else if(itemNum ==2){
            item4Thum.remove();
            this.removeActor(item4Thum);
            item3Thum.remove();
            this.removeActor(item3Thum);
        }
        else if(itemNum ==1){
            item4Thum.remove();
            this.removeActor(item4Thum);
            item3Thum.remove();
            this.removeActor(item3Thum);
            item2Thum.remove();
            this.removeActor(item1Thum);
        }
        else if(itemNum ==0){
            item4Thum.remove();
            this.removeActor(item4Thum);
            item3Thum.remove();
            this.removeActor(item3Thum);
            item2Thum.remove();
            this.removeActor(item2Thum);
            item1Thum.remove();
            this.removeActor(item1Thum);
        }
        this.addActor(closeBtn);
    }

    /**
     *
     * Pressing L on keyboard can show the chest
     *
     * @param keycode the key being pressed
     */
    @Override
    public void notifyKeyDown(int keycode){
        if (keycode == Input.Keys.R){
            if (!setBomb){
                int col = 0;
                int row = 0;
                List<AbstractEntity> entities = GameManager.get().getWorld().getEntities();
                for (AbstractEntity ae : entities) {
                    if(ae instanceof PlayerPeon){
                       col = Math.round(ae.getCol());
                       row = Math.round(ae.getRow());

                        System.out.println(col+" "+row);
                       break;
                    }
                }
                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().bomb = ((RoomWorld)GameManager.get().getWorld()).addChest(col, row,"bomb");
                setBomb = true;
            } else {
                List<AbstractEntity> entities = GameManager.get().getWorld().getEntities();
                for (AbstractEntity ae : entities){
                    if(ae instanceof EnemyPeon && Math.abs(ae.getRow()-this.bomb.getRow()) <= 3 && Math.abs(ae.getCol()-this.bomb.getCol())<=3 ){
                        GameManager.get().getWorld().removeEntity(ae);
                    }
                }
                GameManager.get().getWorld().removeEntity(this.bomb);
                setBomb = false;
                this.bomb = null;
            }
        }
    }

    @Override
    public void notifyKeyUp(int keycode) {

    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {
    }

    public String generateImgPath(AbstractItem item) {
        if(item instanceof ConsumableItem){
            return "resources/items/Consumable/" + item.getImgName();
        }else if(item instanceof EquipmentItem){
            return "resources/items/Equipment/" + item.getImgName();
        }else if(item instanceof MaterialItem){
            return "resources/items/Material/" + item.getImgName();
        }else if(item instanceof OtherItem){
            return "resources/items/Other/" + item.getImgName();
        }
        else {
            return null;
        }
    }


}
