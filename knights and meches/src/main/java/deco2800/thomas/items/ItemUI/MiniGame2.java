package deco2800.thomas.items.ItemUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.entities.Chest;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;

import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.managers.SoundManager;

import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;
import java.util.*;

public class MiniGame2 extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver {

    private Image image;
    private ImageButton closeBtn;
    private ImageButton hint;
    private ImageButton hintBoard;
    private String[][] imageList;
    private ArrayList<ImageButton> buttonList;
    private Chest chest;
    private boolean isOpen;
    private int AiSelectRow;
    private int aiSelectCol;
    private boolean isFirstPlay;

    public MiniGame2(){

        isFirstPlay = true;
        imageList = new String[2][3];
        buttonList = new ArrayList<>();
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    public void createMiniGame2(Chest chest) {
        this.chest = chest;
        isOpen = false;
        aiSelectCol = -1;
        AiSelectRow = -1;
        image = null;
        hint = null;
        hintBoard = null;
        buttonList.clear();


        imageList[0][0] = "items/Game_scissors.png";
        imageList[0][1] = "items/Game_rock.png";
        imageList[0][2] = "items/Game_paper.png";
        imageList[1][0] = "items/Game_scissors.png";
        imageList[1][1] = "items/Game_rock.png";
        imageList[1][2] = "items/Game_paper.png";

        this.image = new Image(new Texture(Gdx.files.internal("items/MiniGame/minigame_bg2.png")));
        this.image.setSize((float) (Gdx.graphics.getWidth() * 0.8), (float) (Gdx.graphics.getHeight() * 0.8));
        image.setPosition((float) (Gdx.graphics.getWidth() * 0.1),(float) (Gdx.graphics.getHeight() * 0.1));
        this.addActor(image);
        image.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });


        closeBtn = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/Close_Button.png"))));
        closeBtn.setPosition((float) (Gdx.graphics.getWidth() * 0.8),(float) (Gdx.graphics.getHeight() * 0.8));
        closeBtn.setSize((float) (Gdx.graphics.getWidth() * 0.08), (float) (Gdx.graphics.getHeight() * 0.08));
        this.addActor(closeBtn);
        closeBtn.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame2().gameClose();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        this.hint = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/MiniGame/minigame2arrow2.png"))));
        this.hint.setSize((float) (Gdx.graphics.getWidth() * 0.7), (float) (Gdx.graphics.getHeight() * 0.7));
        this.hint.setPosition((float) (Gdx.graphics.getWidth() * 0.2),(float) (Gdx.graphics.getHeight() * 0));
        this.addActor(hint);
        buttonList.add(hint);
        hint.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        this.addButton(0,0,false, true);
        this.addButton(0,1,false, true);
        this.addButton(0,2,false, true);
        this.addButton(1,0,false, false);
        this.addButton(1,1,false, false);
        this.addButton(1,2,false, false);

        hintBoard = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/MiniGame/minigame2hint3.png"))));
        hintBoard.setPosition((float) (Gdx.graphics.getWidth() * -0.1),(float) (Gdx.graphics.getHeight() * -0.15));
        hintBoard.setSize((float) (Gdx.graphics.getWidth() *1.2), (float) (Gdx.graphics.getHeight() *1.2));
        this.addActor(hintBoard);
        this.buttonList.add(hintBoard);
        hintBoard.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                hintBoard.remove();
                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame2().removeActor(hintBoard);
                isFirstPlay = false;
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        if(!isFirstPlay){
            hintBoard.remove();
            this.removeActor(hintBoard);
        }

    }


    private ImageButton addButton(int row, int col, boolean isCard, boolean isUI){
        double buttonHeight = 0;
        double buttonWidth = 0;
        switch (row) {
            case 0:
                buttonHeight = 0.65;
                break;
            case 1:
                buttonHeight = 0.15;
                break;
            default:
                break;
        }
        switch (col) {
            case 0:
                buttonWidth = 0.2;
                break;
            case 1:
                buttonWidth = 0.45;
                break;
            case 2:
                buttonWidth = 0.7;
                break;
            default:
                break;
        }
        String imagePath;
        if(isCard){
            imagePath = imageList[row][col];
        } else if (isUI){
            imagePath = "items/MiniGame/AiCard.png";
        } else {
            imagePath = "items/MiniGame/UserCard3.png";
        }
        ImageButton button1 = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(imagePath))));
        button1.setPosition((float) (Gdx.graphics.getWidth() * buttonWidth),(float) (Gdx.graphics.getHeight() * buttonHeight));
        button1.setSize((float) (Gdx.graphics.getWidth() * 0.1), (float) (Gdx.graphics.getHeight() * 0.2));
        buttonList.add(button1);
        this.addActor(button1);
        button1.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if(!isUI) {
                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame2().click(row,col,button1);
                }
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });
        return button1;
    }



    public void click(int i, int j, ImageButton button) {
        GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");

        button.remove();
        this.removeActor(button);
        double random = Math.floor(Math.random() * 10);
        if(!isOpen) {
            while (random == 0) {
                random = Math.floor(Math.random() * 10);
            }
            if(random <= 3) {
                this.addButton(0, 0, true, true);
                AiSelectRow = 0;
                aiSelectCol = 0;
            } else if (4 <= random && random <= 6){
                this.addButton(0, 1, true, true);
                AiSelectRow = 0;
                aiSelectCol = 1;
            } else {
                this.addButton(0, 2, true, true);
                AiSelectRow = 0;
                aiSelectCol = 2;
            }
            isOpen = true;
            addButton(i, j, true, false);
        } else if (aiSelectCol==j) {
            this.gameEnd(3);
        } else if ((aiSelectCol==0&&j==1) || (aiSelectCol==1&&j==2) || (aiSelectCol==2&&j==0)) {
            this.gameEnd(4);
        } else {
            this.gameEnd(2);
        }
    }

    private void gameEnd(int score){
        image.remove();
        this.removeActor(image);
        closeBtn.remove();
        this.removeActor(closeBtn);
        hint.remove();
        this.removeActor(hint);
        for (ImageButton b:buttonList){
            b.remove();
            this.removeActor(b);
        }
        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(this.chest,score);
    }

    private void gameClose(){
        image.remove();
        this.removeActor(image);
        closeBtn.remove();
        this.removeActor(closeBtn);
        hint.remove();
        this.removeActor(hint);
        for (ImageButton b:buttonList){
            b.remove();
            this.removeActor(b);
        }
    }

    @Override
    public void notifyKeyDown(int keycode){
        if (keycode == Input.Keys.R) {
            this.removeActor(image);
            for (ImageButton b:buttonList){
                this.removeActor(b);
            }
        }
    }

    @Override
    public void notifyKeyUp(int keycode) {

    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {

    }

}
