package deco2800.thomas.items.ItemUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.GameScreen;
import deco2800.thomas.entities.Chest;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;

import deco2800.thomas.managers.ScreenManager;

import deco2800.thomas.managers.SoundManager;

import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;
import java.util.*;

public class MiniGame extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver {

    private String[][] imageList;
    private ArrayList<String> initImageList;
    ArrayList<ImageButton> buttonList;
    private boolean isFirstPlay;
    private int restNum;
    private Chest chest;
    private int openedCards;
    private int lastRow;
    private int lastCol;
    private int currentRow;
    private int currentCol;
    private int failTimes;
    private ImageButton lastImage;
    private ImageButton currentImage;
    private String lastImagePath;
    private Image image;
    private ImageButton hintBoard;
    private ImageButton closeBtn;
    private ImageButton image2;
    private ImageButton card9Thum;

    /**
     * Constructor for Congratulation UI
     */
    public MiniGame(){

        this.chest = null;
        this.openedCards = 0;
        this.initImageList = null;
        this.lastImage = null;
        this.currentImage = null;
        this.image = null;
        this.lastImagePath = null;
        this.restNum = 9;
        this.isFirstPlay = true;
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     *
     * GameScreen can display a screen for mini-game1
     *
     * @param chest prepare a chest for target chest
     */
    public void createMiniGame(Chest chest) {

        this.failTimes = 0;
        this.restNum = 9;
        this.buttonList = new ArrayList<>();
        this.chest = chest;
        hintBoard = null;
        lastRow = -1;
        lastCol = -1;
        currentRow = -1;
        currentCol = -1;
        initImageList = new ArrayList<>();
        initImageList.add("items/Orb/Orb_Red.png");
        initImageList.add("items/Orb/Orb_Red.png");
        initImageList.add("items/Orb/Orb_Blue.png");
        initImageList.add("items/Orb/Orb_Blue.png");
        initImageList.add("items/Orb/Orb_Yellow.png");
        initImageList.add("items/Orb/Orb_Yellow.png");
        initImageList.add("items/Orb/Orb_Violet.png");
        initImageList.add("items/Orb/Orb_Violet.png");
        initImageList.add("items/Orb/Orb_Red.png");
        Collections.shuffle(initImageList);
        imageList = new String[3][3];
        for (int i=0; i<imageList.length; ++i) {
            for (int j=0; j<imageList[0].length; ++j) {
                imageList[i][j] = initImageList.get(3*i+j);
            }
        }

        this.image = new Image(new Texture(Gdx.files.internal("items/MiniGame/minigame_bg2.png")));
        this.image.setSize((float) (Gdx.graphics.getWidth() * 0.8), (float) (Gdx.graphics.getHeight() * 0.8));
        image.setPosition((float) (Gdx.graphics.getWidth() * 0.1),(float) (Gdx.graphics.getHeight() * 0.1));
        this.addActor(image);

        this.image2 = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/MiniGame/minigame_bg2.png"))));
        this.image2.setSize((float) (Gdx.graphics.getWidth() * 0.8), (float) (Gdx.graphics.getHeight() * 0.8));
        image2.setPosition((float) (Gdx.graphics.getWidth() * 0.1),(float) (Gdx.graphics.getHeight() * 0.1));
        this.addActor(this.image2);

        image2.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if(lastRow>=0 && lastCol>=0 && currentRow>=0 && currentCol>=0) {
                    currentImage.remove();
                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(currentImage);
                    lastImage.remove();
                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(lastImage);
                    if (!imageList[lastRow][lastCol].equals(imageList[currentRow][currentCol])) {
                        addButton(currentRow, currentCol, false);
                        addButton(lastRow, lastCol, false);
                        failTimes++;
                    } else {
                        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().decreaseRestNum();

                        if (GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getRestnum() == 1) {

                            image.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(image);
                            image2.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(image2);
                            closeBtn.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(closeBtn);

                            lastImage.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(lastImage);
                            currentImage.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(currentImage);

                            hintBoard.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(hintBoard);

                            for (ImageButton cardThum : buttonList) {
                                cardThum.remove();
                                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(cardThum);
                            }

                            if(failTimes<=3) {
                                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 4);
                            } else if (failTimes<=6) {
                                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 3);
                            } else {
                                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 2);
                            }

                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().clearChest();

                        }
                    }

                    lastRow = -1;
                    lastCol = -1;
                    currentRow = -1;
                    currentCol = -1;
                    lastImage = null;
                    currentImage = null;

                }
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        closeBtn = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/Close_Button.png"))));
        closeBtn.setPosition((float) (Gdx.graphics.getWidth() * 0.8),(float) (Gdx.graphics.getHeight() * 0.8));
        closeBtn.setSize((float) (Gdx.graphics.getWidth() * 0.08), (float) (Gdx.graphics.getHeight() * 0.08));
        this.addActor(closeBtn);
        closeBtn.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().gameClose();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });

        // Add 9 cards on the game
        this.addButton(0, 0, false);    // Card1Thum
        this.addButton(0, 1, false);    // Card2Thum
        this.addButton(0, 2, false);    // Card3Thum
        this.addButton(1, 0, false);    // Card4Thum
        this.addButton(1, 1, false);    // Card5Thum
        this.addButton(1, 2, false);    // Card6Thum
        this.addButton(2, 0, false);    // Card7Thum
        this.addButton(2, 1, false);    // Card8Thum
        card9Thum = this.addButton(2,2,false);


        this.hintBoard = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("items/MiniGame/minigame1hint3.png"))));
        this.hintBoard.setSize((float) (Gdx.graphics.getWidth() * 1.3), (float) (Gdx.graphics.getHeight() * -0.1));
        this.hintBoard.setPosition((float) (Gdx.graphics.getWidth() * -0.1),(float) (Gdx.graphics.getHeight() * -0.1));
        this.addActor(hintBoard);
        hintBoard.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                hintBoard.remove();
                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(hintBoard);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });
        if(!isFirstPlay){
            hintBoard.remove();
            this.removeActor(hintBoard);
        }

        isFirstPlay = false;
    }

    /**
     *
     * Add card on the screen
     *
     * @param i the row of the added card
     * @param j the col of the added card
     * @param isCard Decide the front or back of the card
     */
    public ImageButton addButton(int i, int j, boolean isCard){

        double buttonHeight = 0;
        switch (i) {
            case 0:
                buttonHeight = 0.65;
                break;
            case 1:
                buttonHeight = 0.4;
                break;
            case 2:
                buttonHeight = 0.15;
                break;
            default:
                break;
        }
        double buttonWidth = 0;
        switch (j) {
            case 0:
                buttonWidth = 0.2;
                break;
            case 1:
                buttonWidth = 0.45;
                break;
            case 2:
                buttonWidth = 0.7;
                break;
            default:
                break;
        }
        String imgPath;
        if(!isCard){
            imgPath = "items/MiniGame/AiCard.png";
        } else {
            imgPath = imageList[i][j];
        }

        ImageButton button1 = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(imgPath))));
        button1.setPosition((float) (Gdx.graphics.getWidth() * buttonWidth),(float) (Gdx.graphics.getHeight() * buttonHeight));
        button1.setSize((float) (Gdx.graphics.getWidth() * 0.1), (float) (Gdx.graphics.getHeight() * 0.2));
        this.buttonList.add(button1);
        if(!isCard) {
            button1.addListener(new InputListener() {
                    @Override
                    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                        super.touchUp(event, x, y, pointer, button);
                        if (lastRow>=0 && lastCol>=0 && currentRow>=0 && currentCol>=0){
                            currentImage.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(currentImage);
                            lastImage.remove();
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(lastImage);
                            if (!imageList[lastRow][lastCol].equals(imageList[currentRow][currentCol])) {
                                addButton(currentRow, currentCol, false);
                                addButton(lastRow, lastCol, false);
                                failTimes++;
                            } else {
                                GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().decreaseRestNum();
                                if (GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getRestnum() == 1) {

                                    image.remove();
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(image);
                                    image2.remove();
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(image2);
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(card9Thum);
                                    lastImage.remove();
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(lastImage);
                                    currentImage.remove();
                                    System.out.println(buttonList);
                                    for (ImageButton cardThum : buttonList) {
                                        cardThum.remove();
                                        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(cardThum);
                                    }
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(currentImage);

                                    if(failTimes<=3) {
                                        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 4);
                                    } else if (failTimes<=6) {
                                        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 3);
                                    } else {
                                        GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getBoxUi().createBox(GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().getChest(), 2);
                                    }

                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().clearChest();
                                    closeBtn.remove();
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(closeBtn);
                                    hintBoard.remove();
                                    GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().removeActor(hintBoard);
                                }
                            }

                            lastRow = -1;
                            lastCol = -1;
                            currentRow = -1;
                            currentCol = -1;
                            lastImage = null;
                            currentImage = null;

                        } else {
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().click(i, j, button1);
                        }
                    }

                    @Override
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        super.touchDown(event, x, y, pointer, button);
                        return true;
                    }
                });
            }

        this.addActor(button1);
        return button1;
    }


    /**
     *
     * Click a card
     *
     * @param i the row of the clicked card
     * @param j the col of the clicked card
     * @param cardThum clicked card
     */
    public void click(int i,int j, ImageButton cardThum) {

        GameManager.get().getManager(SoundManager.class).playSound("pk.mp3");

        cardThum.remove();
        this.removeActor(cardThum);
        ImageButton cardImage = addButton(i, j,true);

        if (lastRow<0 && lastCol<0){
            lastRow = i;
            lastCol = j;
            lastImage = cardImage;
        } else if (currentRow<0 && currentCol<0) {
            currentRow = i;
            currentCol = j;
            currentImage = cardImage;
        }
    }

    public void decreaseRestNum(){
        this.restNum -= 2;
    }

    public int getRestnum() {
        return this.restNum;
    }

    public Chest getChest() {
        return this.chest;
    }

    private void clearChest() {
        this.chest = null;
        this.buttonList.clear();
    }

    private void gameClose(){
        this.removeActor(image);
        this.removeActor(image2);
        this.removeActor(lastImage);
        this.removeActor(currentImage);
        this.removeActor(closeBtn);
        hintBoard.remove();
        this.removeActor(hintBoard);
        for (ImageButton cardThum : buttonList) {
            this.removeActor(cardThum);
        }
        hintBoard.remove();
        this.removeActor(hintBoard);
        this.clearChest();
    }

    @Override
    public void notifyKeyDown(int keycode){

    }

    @Override
    public void notifyKeyUp(int keycode) {

    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {

    }

}
