package deco2800.thomas.items;

import deco2800.thomas.entities.PlayerPeon;

/**
 * Abstract Class for Consumable Items
 *
 * @author Yonghao Wu
 * @since <pre>2 Sep 2020</pre>
 * @version 1.0
 */
public abstract class ConsumableItem extends AbstractItem{

    /**
     * Constructor for Consumable items
     * @param name name for Consumable items
     * @param value value for Consumable items
     * @param id id of Consumable items
     * @param description Description for Consumable items
     */
    public ConsumableItem(String name,  String id,Integer value, String description, int size, String imgName) {
        super(name, id, value, description, size, imgName);
    }

    /**
     * Abstract Method for getCapacity()
     * @return String Capacity of consumable Items
     */
    public abstract String getCapacity();

    /**
     * Abstract Method for use()
     */
    public abstract void use(PlayerPeon playerPeon);
}
