package deco2800.thomas.items;

/**
 * Items that only used for complete certain quests
 */
public abstract class QuestRelatedItem extends AbstractItem {

    /**
     *
     * @param name Name of this quest related item
     * @param id Id of this quest related item
     * @param value Value of this quest related item
     * @param description Description of this quest related item
     * @param size Max number of the same items save in one inventory box
     */
    public QuestRelatedItem(String name, String id, int value, String description, int size, String imgName) {
        super(name, id, value, description, size, imgName);
    }

    public String getType(){
        return "Orb";
    }
}
