package deco2800.thomas.items.Other;

import deco2800.thomas.items.OtherItem;

public class Bomb extends OtherItem {

    public Bomb() {
        super("Bomb", "929", 100, "Pree 'R' to set or detonate a bomb", 5,"Bomb3.png");
    }
}
