package deco2800.thomas.items.Other;
import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.entities.TeleportCircle;
import deco2800.thomas.items.OtherItem;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.worlds.Tile;

import java.util.List;

public class TeleportScroll extends OtherItem {

    private String status = "off";
    private float col = 0;
    private float row = 0;
    private int high = 0;

    public TeleportScroll() {
        super("Teleport", "023", 100, "Use to teleport", 5,"Teleport_Scroll.png");
    }

    @Override
    public  void use(float col, float row, int height) {

        this.col = col;
        this.row = row;
        this.high = height;
        Tile t = GameManager.getWorld().getTile(col, row);
        TeleportCircle tpc = new TeleportCircle(t);
        GameManager.getWorld().addEntity(tpc);
    }

    @Override
    public void backPosition(PlayerPeon playerPeon){
        playerPeon.setPosition(col,row,high);
        List<AbstractEntity> entities = GameManager.get().getWorld().getEntities();
        for (AbstractEntity entity:entities){
            if(entity instanceof TeleportCircle) {
                GameManager.get().getWorld().removeEntity(entity);
            }
        }
    }

    @Override
    public int hashCode(){
        return this.getImgName().hashCode() * this.getItemIdString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TeleportScroll) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }

    /**
     * @return A String Representation of Teleport
     */
    @Override
    public String toString(){
        return this.getImgName() + ":" + this.getItemIdString() + ":" + this.getItemDescription();
    }
}
