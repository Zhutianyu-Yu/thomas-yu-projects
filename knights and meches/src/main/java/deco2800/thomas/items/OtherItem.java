package deco2800.thomas.items;

/**
 * Anything that doesn't fall into the above categories is classified as "other"
 */

public abstract class OtherItem extends AbstractItem {
    /**
     * Constructor for special item
     * @param name name for special item
     * @param value value for special item
     * @param id id of  special item
     * @param description Description for special item
     */
    public OtherItem(String name,  String id, Integer value,String description, int size,String imgName) {
        super(name,  id, value, description, size,imgName);
    }

    public String getType(){
        return "OtherItem";
    }

}
