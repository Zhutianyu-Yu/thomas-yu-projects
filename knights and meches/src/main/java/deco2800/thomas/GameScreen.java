package deco2800.thomas;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.inforBoard.MainInformation;
import deco2800.thomas.handlers.KeyboardManager;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ui.InventoryMenu;
import deco2800.thomas.items.ItemUI.*;
import deco2800.thomas.managers.*;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.pause.PauseInterface;
import deco2800.thomas.quest.ui.QuestInterface;
import deco2800.thomas.renderers.MinimapRenderer;
import deco2800.thomas.renderers.PotateCamera;
import deco2800.thomas.renderers.OverlayRenderer;
import deco2800.thomas.renderers.Renderer3D;
import deco2800.thomas.traderUI.TraderInterface;
import deco2800.thomas.worlds.*;
import deco2800.thomas.worlds.dungeons.TestWorld;

import deco2800.thomas.worlds.rooms.HubWorld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameScreen implements Screen, KeyDownObserver {
	private final Logger LOG = LoggerFactory.getLogger(GameScreen.class);
	@SuppressWarnings("unused")
	private final ThomasGame game;
	/**
	 * Set the renderer.
	 * 3D is for Isometric worlds
	 * Check the documentation for each renderer to see how it handles WorldEntity coordinates
	 */
	Renderer3D renderer = new Renderer3D();

	Texture backgroundImage = null;
	OverlayRenderer rendererDebug = new OverlayRenderer();
	// An object to render the minimap
	MinimapRenderer rendererMinimap = new MinimapRenderer();
	static Skin skin;

	/**
	 * Create a camera for panning and zooming.
	 * Camera must be updated every render cycle.
	 */
	PotateCamera camera;
	PotateCamera cameraDebug;
	PotateCamera cameraMinimap;

	private Stage stage = new Stage(new ExtendViewport(1280, 720));

	long lastGameTick = 0;


	private boolean cameraDisable;
	private MainInformation mainInformation;

	private BoxUi boxUi;
	private GetOrbUi getOrbUi;
	private MiniGame miniGame;
	private MiniGame2 miniGame2;

	private boolean isMinimapVisible = true;
	private boolean isRenderDebugVisible = false;

	public enum gameType {
		LOAD_GAME{
			@Override
			public AbstractWorld method() {
				AbstractWorld world = new TestWorld();
				DatabaseManager.loadWorld(world);
				GameManager.get().getManager(NetworkManager.class).startHosting("host");
				return world;
			}
		},
		NEW_GAME{
			@Override
			public HubWorld method() {
				HubWorld world = new HubWorld();
				world.initialiseParty();
				GameManager.get().getManager(NetworkManager.class).startHosting("host");
				return world;
			}
		};
		public abstract AbstractWorld method(); // could also be in an interface that MyEnum implements
	  }

	public Texture getBackgroundImage() {
		return backgroundImage;
	}
	public void setBackgroundImage(Texture backgroundImage)
	{
		this.backgroundImage = backgroundImage;
	}

	public Stage getStage(Stage stage) {

		return this.stage;
	}

	public GameScreen(final ThomasGame game, final gameType startType) {

		/* Game BGM */
		GameManager.get().getManager(SoundManager.class).playSound("ost.mp3");

		/* Create an example world for the engine */
		this.game = game;
		this.cameraDisable = true;

		// Add first peon to the world
		camera = new PotateCamera(1920, 1080);
		cameraDebug = new PotateCamera(1920, 1080);
		cameraMinimap = new PotateCamera(1920, 1080);

		/* Add the window to the stage */
		GameManager.get().setStage(stage);
		GameManager.get().setCamera(camera);

		GameManager gameManager = GameManager.get();

		startType.method();
		ScreenManager screen = GameManager.get().getManager(ScreenManager.class);
		if (screen != null)
		{
			screen.setCurrentScreen(this);
		}

		gameManager.setWorld(WorldTypes.HUB_WORLD);
		gameManager.createWorldName();

		QuestInterface questInterface = new QuestInterface();
		this.stage.addActor(questInterface);

		PauseInterface pauseInterface = new PauseInterface();
		this.stage.addActor(pauseInterface);

		QuestManager questManager = QuestManager.get();
		gameManager.addManager(questManager);

		InventoryAbstract inventoryAbstract = new InventoryAbstract(4,9);
		InventoryMenu inventoryMenu = new InventoryMenu(inventoryAbstract, inventoryAbstract.getMechInventory());
		inventoryAbstract.setMenu(inventoryMenu);
		inventoryAbstract.createDummyInventory();

		this.stage.addActor(inventoryAbstract.getMenu());
		inventoryAbstract.getMenu();

		// add information board to the stage
		this.mainInformation = new MainInformation();


		this.stage.addActor(this.mainInformation);

		// add box board to the stage
		boxUi = new BoxUi();
		this.stage.addActor(boxUi);

		// Add ui of getting orbs
		getOrbUi = new GetOrbUi();
		this.stage.addActor(getOrbUi);

		// Add ui of MiniGame
		miniGame = new MiniGame();
		this.stage.addActor(miniGame);

		// Add ui of MiniGame2
		miniGame2 = new MiniGame2();
		this.stage.addActor(miniGame2);

		// add Illustration UI to the stage
		IllustrationUI illustrationUi = new IllustrationUI();
		this.stage.addActor(illustrationUi);


		MenuMenu menuMenu= new MenuMenu();
		stage.addActor(menuMenu);



		PathFindingService pathFindingService = new PathFindingService();
		GameManager.get().addManager(pathFindingService);

		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(GameManager.get().getManager(KeyboardManager.class));
		multiplexer.addProcessor(GameManager.get().getManager(InputManager.class));

		Gdx.input.setInputProcessor(multiplexer);

		GameManager.get().getManager(InputManager.class).addInventoryMenu(inventoryAbstract.getMenu());

		GameManager.get().getManager(InputManager.class).setItemIllustrationInterface(illustrationUi);

		GameManager.get().getManager(InputManager.class).setCharacterInformation(this.mainInformation);

		GameManager.get().getManager(InputManager.class).addQuestInterface(questInterface);

		GameManager.get().getManager(InputManager.class).addPauseInterface(pauseInterface);

		TraderInterface tradeInterface = new TraderInterface();
		this.stage.addActor(tradeInterface);
		GameManager.get().getManager(InputManager.class).addTradeInterface(tradeInterface);

		GameManager.get().getManager(KeyboardManager.class).registerForKeyDown(this);
	}

	/**
	 * Renderer thread
	 * Must update all displayed elements using a Renderer
	 */
	@Override
	public void render(float delta) {
		handleRenderables();

		moveCamera();

		cameraDebug.position.set(camera.position);
		cameraDebug.update();
		// Update the minimap camera position
		cameraMinimap.position.set(camera.position);
		cameraMinimap.update();
		camera.update();

		SpriteBatch batchDebug = new SpriteBatch();
		batchDebug.setProjectionMatrix(cameraDebug.combined);

		SpriteBatch batchMinimap = new SpriteBatch();
		batchMinimap.setProjectionMatrix(cameraMinimap.combined);

		SpriteBatch batch = new SpriteBatch();
		batch.setProjectionMatrix(camera.combined);

		// Clear the entire display as we are using lazy rendering
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (backgroundImage != null) {

			batch.begin();
			batch.draw(backgroundImage,
					-camera.viewportWidth/2 + camera.position.x,
					-camera.viewportHeight/2 + camera.position.y,
					camera.viewportWidth, camera.viewportHeight);

			batch.end();
		}

		rerenderMapObjects(batch, camera);

		//Render the debug information
		if (isRenderDebugVisible) {
			rendererDebug.render(batchDebug, cameraDebug);
		}

		// Render the minimap
		if (getMinimapVisible())
		{
			rendererMinimap.render(batchMinimap, cameraMinimap);
		}


		/* Refresh the experience UI for if information was updated */
		stage.act(delta);
		stage.draw();
		batch.dispose();
	}

	private void handleRenderables() {
		if (System.currentTimeMillis() - lastGameTick > 20) {
			lastGameTick = System.currentTimeMillis();
			GameManager.get().onTick(0);
		}
	}

	/**
	 * Use the selected renderer to render objects onto the map
	 */
	private void rerenderMapObjects(SpriteBatch batch, OrthographicCamera camera) {
		renderer.render(batch, camera);
	}



	@Override
	public void show() {
		// can be null because of parent class
	}

	/**
	 * Resizes the viewport
	 *
	 * @param width
	 * @param height
	 */
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.update();

		cameraDebug.viewportWidth = width;
		cameraDebug.viewportHeight = height;
		cameraDebug.update();

		cameraMinimap.viewportWidth = width;
		cameraMinimap.viewportHeight = height;
		cameraMinimap.update();
	}

	@Override
	public void pause() {
		//do nothing
	}

	@Override
	public void resume() {
		//do nothing
	}

	@Override
	public void hide() {
		//do nothing
	}

	/**
	 * Disposes of assets etc when the rendering system is stopped.
	 */
	@Override
	public void dispose() {
		// Don't need this at the moment
		System.exit(0);
	}

	/**
	 * Changes current screen world
	 * @param newWorld new world for screen to render
	 */
	public void changeWorld(WorldTypes newWorld)
	{
		AbstractEntity.resetID();
		Tile.resetID();
		GameManager gameManager = GameManager.get();
		gameManager.setWorld(newWorld);

		this.setMinimapVisible(gameManager.getWorld() instanceof LevelWorld);
	}

	@Override
	public void notifyKeyDown(int keycode) {
		if (keycode == Input.Keys.F12) {
			GameManager.get().setDebugMode(!GameManager.get().getDebugMode());
		}

		if (keycode == Input.Keys.F11) { // F11
			GameManager.get().setShowCoords(!GameManager.get().getShowCoords());
			LOG.info("Show coords is now {}", GameManager.get().getShowCoords());
		}


		if (keycode == Input.Keys.C) { // F11
			GameManager.get().setShowCoords(!GameManager.get().getShowCoords());
			LOG.info("Show coords is now {}", GameManager.get().getShowCoords());
		}

		if (keycode == Input.Keys.F10) { // F10
			GameManager.get().setShowPath(!GameManager.get().getShowPath());
			LOG.info("Show Path is now {}", GameManager.get().getShowPath());
		}

		if (keycode == Input.Keys.F3) { // F3
			// Save the world to the DB
			DatabaseManager.saveWorld(null);
		}

		if (keycode == Input.Keys.F4) { // F4
			// Load the world to the DB
			DatabaseManager.loadWorld(null);
		}


		if (keycode == Input.Keys.Y) {
			// Disable the camera
			this.cameraDisable = !this.cameraDisable;
		}

		if (keycode == Input.Keys.G) {
			isRenderDebugVisible = !isRenderDebugVisible;
		}

	}

	/**
	 * Checks if minimap is currently being rendered
	 * @return true if rendered, else false
	 */
	private boolean getMinimapVisible()
	{
		return isMinimapVisible;
	}

	/**
	 * Sets whether minimap should be rendered
	 * @param visible true if rendered, else false
	 * @return
	 */
	private void setMinimapVisible(boolean visible)
	{
		this.isMinimapVisible = visible;
	}

	/**
	 * Gets whether the camera is disabled
	 * @return true if the camera is disabled, false otherwise
	 */
	public boolean getCameraDisabled() {
		return cameraDisable;
	}

	/**
	 * Gets whether the inventory menu is displayed
	 */
	public boolean getInventoryDisplayed() {
		return 	GameManager.get().getManager(InputManager.class).getInventory().getMenu().isInventoryDisplayed();
	}

	public BoxUi getBoxUi() {
		return boxUi;
	}

	public GetOrbUi getGetOrbUi() {
		return getOrbUi;
	}

	public MiniGame getMiniGame() {
		return miniGame;
	}

	public MiniGame2 getMiniGame2() {
		return miniGame2;
	}

	public void moveCamera() {
	//timmeh to fix hack.  // fps is not updated cycle by cycle
		float normilisedGameSpeed = (60.0f/Gdx.graphics.getFramesPerSecond());

		int goFastSpeed = (int) (5 * normilisedGameSpeed * camera.zoom);

		if (!camera.isPotate() && !cameraDisable) {

			if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
				goFastSpeed *= goFastSpeed * goFastSpeed;
			}

			if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
				camera.translate(-goFastSpeed, 0, 0);
			}

			if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				camera.translate(goFastSpeed, 0, 0);
			}

			if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
				camera.translate(0, -goFastSpeed, 0);
			}

			if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
				camera.translate(0, goFastSpeed, 0);
			}

			if (Gdx.input.isKeyPressed(Input.Keys.EQUALS)) {
				camera.zoom *=1-0.01*normilisedGameSpeed;
				if (camera.zoom < 0.5) {
					camera.zoom = 0.5f;
				}
			}

			if (Gdx.input.isKeyPressed(Input.Keys.MINUS)) {
				camera.zoom *=1+0.01*normilisedGameSpeed;
			}

		}
	}
}