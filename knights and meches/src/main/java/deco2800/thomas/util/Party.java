package deco2800.thomas.util;

import deco2800.thomas.entities.*;

import java.util.*;

/**
 * Party of playable characters
 */
public class Party {
    private HashMap<PlayerType, PlayerPeon> players;

    public Party(PlayerPeon... players) {
        this.players = new HashMap<>();
        for (PlayerPeon player : players)
        {
            this.players.put(player.getType(), player);
        }
    }

    public PlayerPeon getMainPlayer() {
        PlayerPeon player = this.players.getOrDefault(PlayerType.KNIGHT, null);

        if (player == null) {
            return this.players.get(PlayerType.WIZARD);
        }
        return player;
    }

    public Collection<PlayerPeon> getPlayersList() {
        return this.players.values();
    }

    /**
     * Swaps out either the wizard or Knight in the party
     * @param playerType Wizard or knight type to be swapped
     * @param player Individual playerPeon to be swapped
     */
    public void swapPlayer(PlayerType playerType, PlayerPeon player){
        if (playerType == PlayerType.MECH) {
            return;
        }
        this.players.replace(playerType, player);
    }

    /**
     * Gets current players with their corresponding type
     * @return mapping from player type to player object
     */
    public Map<PlayerType, PlayerPeon> getPlayers() {
        return this.players;
    }

    public PlayerPeon getPlayer(PlayerType playerType) {
        return this.players.get(playerType);
    }
}
