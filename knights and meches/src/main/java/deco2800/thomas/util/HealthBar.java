package deco2800.thomas.util;

import com.badlogic.gdx.graphics.Color;

/**
 * User-interface element that visualises an entity's health
 */
public class HealthBar extends AbstractHealthBar
{

    /**
     * Constructor for HealthBar
     * @param value current health value
     * @param maximum maximum health
     */
    public HealthBar(int value, int maximum)
    {
        super(value, maximum, Color.GREEN, Color.RED);
    }
}
