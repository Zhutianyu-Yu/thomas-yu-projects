package deco2800.thomas.util;

import com.badlogic.gdx.graphics.Color;

public class ManaBar extends AbstractHealthBar
{
    /**
     * Constructor for ManaBar
     * @param value current mana value
     * @param maximum maximum mana value
     */
    public ManaBar(int value, int maximum)
    {
        super(value, maximum, Color.CYAN, Color.LIGHT_GRAY);
    }
}
