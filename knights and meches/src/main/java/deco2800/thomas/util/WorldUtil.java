package deco2800.thomas.util;

import java.util.*;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import deco2800.thomas.entities.*;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.managers.QuestManager;
import deco2800.thomas.worlds.*;
import deco2800.thomas.worlds.dungeons.*;
import deco2800.thomas.worlds.rooms.*;
import deco2800.thomas.worlds.staticworlds.*;

import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.TextureManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class for the World instances.
 * Created by BradleyKing on 15/6/18.
 */
public class WorldUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorldUtil.class);

	private static final float SCALE = 0.3f; // Scales the assets to reasonable sizes
	
	private static final boolean ISO_MODE = false;
	private static int orbsCollected = 0;
	
	public static final float SCALE_X = SCALE;
	public static final float SCALE_Y = SCALE * (ISO_MODE ? 5/8f : 1f);
	
    //base sizes for calculating grid placement.
	public static final float TILE_WIDTH = TextureManager.TILE_WIDTH * SCALE_X;
	public static final float TILE_HEIGHT = TextureManager.TILE_HEIGHT * SCALE_Y;

	//Save the initialized world classes in a mapping. 
	private static final HashMap<WorldTypes, AbstractWorld> worldMappings;
	static {
		worldMappings = new HashMap<>();
		worldMappings.put(WorldTypes.TELEPORTER, null);
		worldMappings.put(WorldTypes.TEST, null);
		worldMappings.put(WorldTypes.HUB_WORLD, null);
		worldMappings.put(WorldTypes.VOLCANO_BIOME, null);
	}
	
	private WorldUtil() {}

	/**
     * Converts screen coordinates to world coordinates.
     * @param x the x coordinate on the screen
     * @param y the y coordinate on the screen
     * @return a float array of the world coordinates
     */
	public static float[] screenToWorldCoordinates(float x, float y) {
		Vector3 result = GameManager.get().getCamera().unproject(new Vector3(x, y, 0));

		return new float[]{result.x, result.y};
	}

	/**
	 * Gets the entities at a specific screen location
	 * @param x the x coordinate on the screen
	 * @param y the y coordinate on the screen
	 * @return a list of entities at the coordinates
	 */
	public static List<AbstractEntity> screenToEntities(float x, float y)
	{
		float[] worldCoordinates = screenToWorldCoordinates(x, y);
		float[] coordinates = worldCoordinatesToColRow(worldCoordinates[0], worldCoordinates[1]);

		AbstractWorld world = GameManager.get().getWorld();
		List<AbstractEntity> allEntities = world.getEntities();
		LinkedList<AbstractEntity> entitiesAtCoords = new LinkedList<>();

		for (AbstractEntity entity : allEntities)
		{
			boolean sameColumn = MathUtil.floatEquality(entity.getCol(), coordinates[0], 1.0f);
			boolean sameRow = MathUtil.floatEquality(entity.getRow(), coordinates[1], 1.0f);
			if (sameColumn && sameRow) {
				entitiesAtCoords.add(entity);
			}
		}

		return entitiesAtCoords;
	}

	/**
	 * Generates new worlds based on type
	 * @param worldType type of world
	 * @return newly generated world
	 */
	public static AbstractWorld createNewWorld(WorldTypes worldType) {
		if (worldMappings.containsKey(worldType) && !worldType.equals(WorldTypes.TRADE)) {
			AbstractWorld world = worldMappings.getOrDefault(worldType, null);
			if (world != null) {
				if (worldType.equals(WorldTypes.TRADE)) {
					((TraderWorld) world).worldSetup();
				} else if (worldType.equals(WorldTypes.GHOST_WORLD)) {
					((GhostWorld) world).setupWorld();
				} else if (worldType.equals(WorldTypes.TUTORIAL)) {
					((TutorialWorld) world).setupWorld();
				}
				return world;
			} 
		}
		// Prepares to return a new instance of that world. 
		switch (worldType) {
			case TEST:
				worldMappings.put(WorldTypes.TEST, new TestWorld());
				break;
			case TUNDRA_DUNGEON:
				worldMappings.put(WorldTypes.TUNDRA_DUNGEON, new TundraDungeon());
				break;
			case GHOST_WORLD:
				worldMappings.put(WorldTypes.GHOST_WORLD, new GhostWorld());
				break;
			case VOLCANO_BIOME:
				worldMappings.put(WorldTypes.VOLCANO_BIOME, new VolcanoBiome());
				break;
			case TELEPORTER:
				worldMappings.put(WorldTypes.TELEPORTER, new TeleportedRoom());
				break;
			case FOREST_BIOME:
				worldMappings.put(WorldTypes.FOREST_BIOME, new ForestDungeon());
				break;
			case HUB_WORLD:
				worldMappings.put(WorldTypes.HUB_WORLD, new HubWorld());
				break;
			case HUB_WORLD_DUNGEON:
				worldMappings.put(WorldTypes.HUB_WORLD_DUNGEON, new HubWorldDungeon());
				break;
			case TRADE:
				worldMappings.put(WorldTypes.TRADE, new TraderWorld());
				break;
			case TUTORIAL:
				worldMappings.put(WorldTypes.TUTORIAL, new TutorialWorld());
			default:
				break;
		}
		AbstractWorld world = worldMappings.get(worldType);
		if (world instanceof StaticWorld) {
			((StaticWorld) world).setup();
		}

		return worldMappings.getOrDefault(worldType, null);
	}

	public static float[] worldCoordinatesToColRow(float x, float y) {
		float row;
		float col;
		float actualRow; 
		x -= TILE_WIDTH / 2;
		y -= TILE_HEIGHT / 2;
		col = Math.round(x / TILE_WIDTH);
		actualRow = y / TILE_HEIGHT;
		row = Math.round(actualRow);
		return new float[]{col, row};
	}

	/**
	 * Same function as above, but returns a primitive type. Much faster for rendering.
	 * @param col coordinate column
	 * @param row coordinate row
	 * @return a float array containing the world coordinates
	 */
	public static float[] colRowToWorldCords(float col, float row) {
		float squareX = col * TILE_WIDTH; //sprite ratio to fix sprite
		float squareY = row * TILE_HEIGHT;

		return new float[]{squareX, squareY};
	}


    public static boolean validColRow(SquareVector pos) {
    	if (pos.getCol() % 1 !=0 ) {
			 return false;
		}

		return (pos.getRow()) % 1 == 0;
	}

	/**
	 * Determines if the position is safe to walk on or place machines on.
	 * @param col the x coordinate of a position
	 * @param row the y coordinate of a position
	 * @return A boolean stating if the position is safe to walk on.
	 */
	public static boolean isWalkable(float col, float row) {
		if (GameManager.get().getWorld() == null || GameManager.get().getWorld().getTile(col, row) == null)
			return false;

		return !GameManager.get().getWorld().getTile(col, row).isObstructed();
	}


  	public static boolean areCoordinatesOffScreen(float squareX, float squareY, OrthographicCamera camera) {
		float bufferWidth = 1.1f;
  		return squareX < (camera.position.x - camera.viewportWidth * camera.zoom / 2 - 2 * TILE_WIDTH * camera.zoom * bufferWidth)
  				|| squareX > (camera.position.x + camera.viewportWidth * camera.zoom / 2 + TILE_WIDTH * camera.zoom * bufferWidth + 50)
  				|| squareY < (camera.position.y - camera.viewportHeight * camera.zoom / 2 - 4 * TILE_HEIGHT * camera.zoom * bufferWidth)
  				|| squareY > (camera.position.y + camera.viewportHeight * camera.zoom / 2 + TILE_HEIGHT * camera.zoom * bufferWidth - 50);
  	}

	/**
	 * Method to return the co-ordinates of the top right corner of the minimap
	 * @param camera
	 * 			The camera being used
	 * @return The co-orinates of where the minimap should be placed
	 */
	public static float[] minimapCoords(OrthographicCamera camera) {

		return new float[]{(camera.position.x + camera.viewportWidth/ 2.6f) ,
				(camera.position.y + camera.viewportHeight / 2.8f)};

	}

    /**
     * Attempts to find the closest Walkable tile to a location.
     * NO LOGGING BECAUSE OF HORRENDOUS SPAM
     * @param startLocation the location of a tile to start the search
     * @return a location of a walkable tile or null if none where found
     */
    public static SquareVector closestWalkable(SquareVector startLocation) {
		LinkedList<SquareVector> queue = new LinkedList<>();
		// A set of all the tiles that have been added to the queue
        // Used to verify no duplicates are added for infinite loops
		Set<SquareVector> closedSet = new HashSet<>();
		queue.add(startLocation);
		while (!queue.isEmpty()) {
		    // Make the first in queue the default start
			SquareVector nextLocation = queue.get(0);

			// Attempt to get the closest to the start location out of the queue
			for (SquareVector neighbour: queue) {
				// Improve this as it has some funky outcomes
				if(Math.abs(neighbour.distance(startLocation.getCol(), startLocation.getRow())) < Math.abs(nextLocation.distance(startLocation.getCol(), startLocation.getRow()))) {
					nextLocation = neighbour;
				}
			}
			// If the closest is walkable then return it
			if (isWalkable(nextLocation.getCol(), nextLocation.getRow())) {
				return nextLocation;
			}
			// Get neighbours of the closest and add to queue if they haven't already been checked
			getNeighbours(nextLocation, closedSet, queue);
			// Remove the invalid location
			queue.remove(nextLocation);
		}
		return null;
	}

	private static void getNeighbours(SquareVector nextLocation, Set<SquareVector> closedSet, LinkedList<SquareVector> queue) {
		Tile nextLocationTile = GameManager.get().getWorld().getTile(nextLocation.getCol(), nextLocation.getRow());
		if (nextLocationTile != null) {
			for (int i = 0; i < 6; i++) {
				Tile neighbour = nextLocationTile.getNeighbour(i);
				if (!closedSet.contains(neighbour.getCoordinates())) {
					closedSet.add(neighbour.getCoordinates());
					queue.add(neighbour.getCoordinates());
				}
			}
		}
	}

	public static AbstractWorld getWorld(WorldTypes worldType) {
    	AbstractWorld world = worldMappings.getOrDefault(worldType, null);
    	if (world != null) {
    		return world;
		} else {
    		return worldMappings.get(WorldTypes.HUB_WORLD);
		}
	}

//	/**
//	 * Creates a tutorial world for the NPC tutorial
//	 */
//	public static void createTutorialWorld() {
//
//    	GameManager manager = GameManager.get();
//    	manager.resetCam(1);
//    	TutorialWorld tute = new TutorialWorld();
//    	manager.changeWorld(tute);
//    	tute.setup();
//	}

//	public static void createFinishWorld() {
//
//		GameManager manager = GameManager.get();
//		roomSetup(WorldTypes.Test, GameManager.get().getWorld());
//		manager.zeroCamera();
//		FinishWorld finished = new FinishWorld();
//		manager.changeWorld(finished);
//		orbsCollected = 0;
//		finished.setup();
//	}

	public static void orbCollected() {
		orbsCollected++;
	}

	public static int getOrbsCollected() {
		return orbsCollected;
	}

	/**
	 * Creates a CombatWorld and focuses screen
	 * @param enemy enemy in battle
	 * @param player user-controlled player
	 */
	public static void createCombatWorld(EnemyPeon enemy, PlayerPeon player) {
		GameManager manager = GameManager.get();
		manager.resetCam(1); //Focus camera on player
		CombatWorld world = new CombatWorld(enemy, GameManager.get().getParty());
		worldMappings.put(WorldTypes.COMBAT, world);
		manager.changeWorld(WorldTypes.COMBAT);
		world.setup();
	}
//
//	/**
//	 * Unused
//	 * Creates a OrbWorld and focuses screen
//	 * @param orb The orb about this world
//	 */
//	public static void createOrbWorld(OrbEntity orb) {
//		GameManager manager = GameManager.get();
//		manager.resetCam(1);
//		OrbWorld world = new OrbWorld(orb);
//		manager.changeWorld(world);
//		world.setup();
//	}

	/**
	 * Detects if enemy and player are in range, if so initiates encounter
	 * @param world
	 * 			current world
	 */
	public static void detectEnemyCombat(LevelWorld world) {
		//Checks every enemy in this world and identifies if any are in range of the player
		AbstractEntity enemy = world.isEnemyInRange();

		//An enemy is in rage of the player
		if (enemy != null) {
			LOGGER.info("Enemy spotted the player");
			createCombatWorld((EnemyPeon) enemy, (PlayerPeon) world.getPlayerPeon());
		}
	}


	public static void detectTraderNPC(RoomWorld world) {

		AbstractEntity npc = world.isEntityInRange(new TraderNPC(new Tile("",0,0), ""));

		if (npc != null) {
			((TraderNPC) npc).found();
		} else {
			if (GameManager.get().getWorldType().equals(WorldTypes.HUB_WORLD)) {
				((HubWorld) GameManager.getWorld()).getTrader().gone();
			}
		}
	}

//	/**
//	 * Unused
//	 * Detects if the orb is not existed in the world, create a new test world.
//	 * @param orb The orb which is be detected.
//	 */
//	public static void detectOrb(AbstractEntity orb) {
//		for (String orb_str : TestWorld.ORBS) {
//			if(orb.getTexture().equals(orb_str)) {
//				return;
//			}
//		}
//		GameManager manager = GameManager.get();
//		TestWorld testWorld = new TestWorld();
//		testWorld.getPlayerPeon().setCol(orb.getCol());
//		testWorld.getPlayerPeon().setRow(orb.getRow());
//		manager.changeWorld(testWorld);
//	}

	public static void detectTeleporter(AbstractWorld world){
		WorldTypes worldTypes =  world.isTeleporterInRange();
    	if (worldTypes == null) {
			return;
		}
		GameManager manager = GameManager.get();
    	switch (worldTypes){
			case TEST:
				TestWorld testWorld = (TestWorld) createNewWorld(WorldTypes.TEST);
				roomSetup(WorldTypes.TEST, testWorld);
				manager.changeWorld(WorldTypes.TEST);
				GameManager.get().resetCam(1.3f);
				break;
			case VOLCANO_BIOME:
				VolcanoBiome volcanoBiome = (VolcanoBiome) createNewWorld(WorldTypes.VOLCANO_BIOME);
				roomSetup(WorldTypes.VOLCANO_BIOME, volcanoBiome);
				manager.changeWorld(WorldTypes.VOLCANO_BIOME);
				manager.resetCam(1.3f);
				break;
			case TELEPORTER:
				TeleportedRoom teleportedRoom = (TeleportedRoom) createNewWorld(WorldTypes.TELEPORTER);
				roomSetup(WorldTypes.TELEPORTER, teleportedRoom);
				manager.changeWorld(WorldTypes.TELEPORTER);
				manager.resetCam(1.3f);
				break;
			case HUB_WORLD:
				HubWorld hubWorld = (HubWorld)createNewWorld(WorldTypes.HUB_WORLD);
				roomSetup(WorldTypes.HUB_WORLD, hubWorld);
				manager.changeWorld(WorldTypes.HUB_WORLD);
				manager.resetCam(1.3f);
				break;
			case HUB_WORLD_DUNGEON:
				HubWorldDungeon hubWorldDungeon = (HubWorldDungeon) createNewWorld(WorldTypes.HUB_WORLD_DUNGEON);
				roomSetup(WorldTypes.HUB_WORLD_DUNGEON, hubWorldDungeon);
				manager.changeWorld(WorldTypes.HUB_WORLD_DUNGEON);
				manager.resetCam(1.3f);
				break;
			case FOREST_BIOME:
				ForestDungeon forestDungeon = (ForestDungeon) createNewWorld(WorldTypes.FOREST_BIOME);
				roomSetup(WorldTypes.FOREST_BIOME, forestDungeon);
				manager.changeWorld(WorldTypes.FOREST_BIOME);
				manager.resetCam(1.3f);
				break;
			case TUNDRA_DUNGEON:
				TundraDungeon tundraDungeon = (TundraDungeon) createNewWorld(WorldTypes.TUNDRA_DUNGEON);
				roomSetup(WorldTypes.TUNDRA_DUNGEON, tundraDungeon);
				manager.changeWorld(WorldTypes.TUNDRA_DUNGEON);
				manager.resetCam(1.3f);
			default:
				break;
		}
	}

	public  static void detectQuestActivator(AbstractWorld world){
		String questName = world.isQuestActivatorInRange();
		if(questName != null)
		{
			QuestManager manager = QuestManager.get();
			if(manager != null)
				manager.startQuest(questName);
		}
	}

	private static int oldCol, oldRow;

	private static void roomSetup(WorldTypes room, AbstractWorld world) {

		LinkedList<WorldTypes> types = new LinkedList<>();
		types.add(WorldTypes.VOLCANO_BIOME);
		types.add(WorldTypes.HUB_WORLD);
		types.add(WorldTypes.TELEPORTER);
		types.add(WorldTypes.FOREST_BIOME);
		types.add(WorldTypes.HUB_WORLD_DUNGEON);
		types.add(WorldTypes.TUNDRA_DUNGEON);


		for (PlayerPeon playerPeon : GameManager.getParty().getPlayersList()) {
			if (!(playerPeon instanceof Mech)) {
				if (WorldTypes.TEST == room) {
					playerPeon.setCol(oldCol + 1);
					playerPeon.setRow(oldRow + 1);
					playerPeon.stopMovement();
				} else if (types.contains(room)) {
					world.addEntity(playerPeon);
					oldRow = (int)playerPeon.getRow();
					oldCol = (int)playerPeon.getCol();
					oldCol += oldCol < 0 ? 1 : 0;
					oldRow += oldRow < 0 ? 1 : 0;

					int[] birthPoint = world.getBirthPointPosition();
					playerPeon.setCol(birthPoint[0]);
					playerPeon.setRow(birthPoint[1]);
					playerPeon.stopMovement();
				}
			}
		}
	}
}
