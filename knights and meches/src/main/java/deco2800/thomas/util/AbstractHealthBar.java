package deco2800.thomas.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.managers.GameManager;

/**
 * Base object progress bar
 */
public class AbstractHealthBar extends Actor
{
    protected int value;
    private int maximum;
    private ProgressBar healthBar;
    private Label label;
    private ProgressBar.ProgressBarStyle style;
    private static final float DEFAULT_BARS = 1000;
    private static final String healthFormat = "%d / %d";
    /**
     * Base bar for tracking health and mana
     * @param value current progressbar value
     * @param maximum maximum progressbar value
     * @param remaining color of remaining bar
     * @param lost color of lost bar
     */
    public AbstractHealthBar(int value, int maximum, Color remaining, Color lost)
    {
        final int height = 20;
        final int width = 100;

        this.value = value;
        this.maximum = maximum;

        //lost pixmap
        Pixmap lostHealth = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        lostHealth.setColor(lost);
        lostHealth.fill();

        //remaining pixmap
        Pixmap remainingHealth = new Pixmap(0, height, Pixmap.Format.RGBA8888);
        remainingHealth.setColor(remaining);
        remainingHealth.fill();

        //before pixmap
        Pixmap beforeHealth = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        beforeHealth.setColor(remaining);
        beforeHealth.fill();

        this.style = new ProgressBar.ProgressBarStyle();

        addDrawable(lostHealth, AbstractHealthBar.DrawingType.BACKGROUND, style);
        addDrawable(remainingHealth, AbstractHealthBar.DrawingType.KNOB, style);
        addDrawable(beforeHealth, AbstractHealthBar.DrawingType.BEFORE, style);

        setupLabel(value, maximum);

        healthBar = new ProgressBar(0.0f, DEFAULT_BARS, 1f, false, style);
        healthBar.setValue(value * DEFAULT_BARS / maximum);
        healthBar.setSize(width, height);
    }

    /**
     * Sets up the health bar label
     * @param value current health bar value
     * @param maximum maximum health bar value
     */
    private void setupLabel(int value, int maximum)
    {
        BitmapFont font = GameManager.get().getSkin().getFont("default-font");

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = font;
        labelStyle.fontColor = Color.BLACK;


        String text = String.format(healthFormat, value, maximum);
        label = new Label(text, labelStyle);
    }


    /**
     * Adds a pixmap to the healthbar
     * @param pixmap image to add
     * @param type type to assign image to
     * @param style style of progressbar
     */
    private void addDrawable(Pixmap pixmap, AbstractHealthBar.DrawingType type, ProgressBar.ProgressBarStyle style)
    {
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        pixmap.dispose();

        if (type == AbstractHealthBar.DrawingType.BACKGROUND)
        {
            style.background = drawable;
        }
        else if (type == AbstractHealthBar.DrawingType.BEFORE)
        {
            style.knobBefore = drawable;
        }
        else
        {
            style.knob = drawable;
        }
    }

    /**
     * Sets the bar's position
     * @param x the bar's x-coordinate
     * @param y the bar's y-coordinate
     */
    public void setPosition(float x, float y)
    {
        healthBar.setPosition(x, y);
        label.setPosition(x, y - healthBar.getHeight());
    }

    /**
     * Sets the progress bar's current value
     * @param health progress bar's current value
     */
    public void setHealth(int health) {
        this.value = health;
    }

    /**
     * Gets the width of the health bar
     * @return bar's width
     */
    public float getWidth()
    {
        return healthBar.getWidth();
    }

    /**
     * Updates label text and progress bar
     * @param value bar value
     */
    public void updateValue(int value)
    {
        this.value = value;
        this.healthBar.setValue(value * DEFAULT_BARS / maximum);
        this.label.setText(String.format(healthFormat, value, maximum));
    }

    public void updateValue(int value, int maximum)
    {
        this.maximum = maximum;
        this.value = Math.round(value * DEFAULT_BARS / maximum);
        this.healthBar.setValue(this.value);
        this.label.setText(String.format(healthFormat, value, maximum));
    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        this.healthBar.draw(batch, parentAlpha);
        this.label.draw(batch, parentAlpha);
    }

    /**
     * Basic enum that stipulates the usage of a pixmap
     */
    private enum DrawingType
    {
        BEFORE,
        KNOB,
        BACKGROUND
    }
}
