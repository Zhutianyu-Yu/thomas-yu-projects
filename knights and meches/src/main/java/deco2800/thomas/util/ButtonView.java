package deco2800.thomas.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import deco2800.thomas.managers.TextureManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import java.util.LinkedList;

/**
 * Represents a list of buttons that are viewable on screen
 */
public class ButtonView
{
    /**
     * Current labels viewed
     */
    private LinkedList<Label> labels = new LinkedList<>();

    /**
     * Game's current stage
     */
    private Stage stage;

    /**
     * Origin X point to draw labels from
     */
    private static final int START_X = 80;

    /**
     * Origin Y point to draw labels from
     */
    private final int START_Y;

    /**
     * Default width of buttons
     */
    private static final int DEFAULT_WIDTH = 200;

    /**
     * Default height of buttons
     */
    private static final int DEFAULT_HEIGHT = 100;

    /**
     * Default margin between buttons
     */
    private static final int DEFAULT_MARGIN = 20;

    /**
     * Current X coordinate to draw next label
     */
    private int currentX = START_X;

    /**
     * Label's texture manager
     */
    private TextureManager textures;

    public ButtonView(Stage stage, TextureManager textures)
    {
        this.stage = stage;
        this.textures = textures;
        this.START_Y = Gdx.graphics.getHeight() / 6;
    }

    public ButtonView(Stage stage, TextureManager textures, int startX, int startY)
    {
        this.stage = stage;
        this.textures = textures;
        currentX = startX;
        this.START_Y = startY;
    }

    /**
     * Adds label to view
     * @param name text on label
     * @param textureName texture name to draw background
     * @param font font for text
     * @return new label created
     */
    public Label addLabel(String name, String textureName, BitmapFont font)
    {
        return addLabel(name, textureName, font, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Adds label to view
     * @param name text on label
     * @param textureName texture name to draw background
     * @param font font for text
     * @param width width of the surrounding button
     * @param height height of the surrounding button
     * @return new label created
     */
    public Label addLabel(String name, String textureName, BitmapFont font,
                          int width, int height)
    {
        return addLabel(name, textureName, font, width, height, Align.center, Color.WHITE);
    }

    /**
     * Adds label to view
     * @param name text on label
     * @param textureName texture name to draw background
     * @param font font for text
     * @param width width of the surrounding button
     * @param height height of the surrounding button
     * @param align position of the button text
     * @param color the colour of the text
     * @return new label created
     */
    public Label addLabel(String name, String textureName, BitmapFont font,
                          int width, int height, int align, Color color)
    {
        Texture labelTexture = textures.getTexture(textureName);
        TextureRegionDrawable region = new TextureRegionDrawable(labelTexture);
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = font;
        style.background = region;
        style.fontColor = color;

        Label label = new Label(name, style);
        label.setAlignment(align);
        label.setSize(width, height);
        label.setPosition(currentX, this.START_Y);

        currentX += width + DEFAULT_MARGIN;
        stage.addActor(label);
        this.labels.add(label);

        return label;
    }

    /**
     * Adds label to view
     * @param name text on label
     * @param textureName texture name to draw background
     * @param font font for text
     * @return new label created
     * @param clickListener
     * @return new label created
     */
    public Label addLabel(String name, String textureName, BitmapFont font, ClickListener clickListener)
    {
        Label label = addLabel(name, textureName, font);
        label.addListener(clickListener);
        return label;
    }

    /**
     * Removes labels from view
     */
    public void removeLabels()
    {
        for (Label label : labels)
        {
            stage.getActors().removeValue(label, true);
        }
    }

    /**
     * Toggles label visibility
     * @param visible determines if labels are visible
     */
    public void makeVisible(boolean visible)
    {
        for (Label label : labels)
        {
            label.setVisible(visible);
        }
    }

    /**
     * Update a label of a button by index to show new information
     * @param index the index of label within the list
     * @param info the new info to be present as the label
     */
    public void updateLabel(int index, String info) {
        if (labels.get(index) != null) {
            labels.get(index).setText(info);
        }
    }
}
