package deco2800.thomas.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import deco2800.thomas.tasks.Direction;

import deco2800.thomas.worlds.AbstractWorld;
import deco2800.thomas.worlds.Tile;

public class DirectionPathfinder extends Pathfinder {
	private final int direction;
	private static final Map<Direction, Integer> DIRECTION_TO_COMPASS;
    static {
        Map<Direction, Integer> map = new HashMap<>();
        map.put(Direction.UP, Tile.NORTH);
        map.put(Direction.DOWN, Tile.SOUTH);
        map.put(Direction.LEFT, Tile.WEST);
		map.put(Direction.RIGHT, Tile.EAST);
		DIRECTION_TO_COMPASS = Collections.unmodifiableMap(map);
	}

	public DirectionPathfinder(Direction direction) {
		this.direction = DIRECTION_TO_COMPASS.get(direction);
	}

	public List pathfind(AbstractWorld world, SquareVector origin, SquareVector destination) {
		List<Tile> path = new ArrayList<>();
		
		Tile root = getTileByHexVector(world, origin);

		while (true) {
			
			if (root.getCoordinates().equals(destination)) {
				return path;
			}

			Tile child = root.getNeighbour(this.direction);
			
			if (child.isObstructed()) {
				break;
			}

			path.add(child);
			root = child;
		}
		return new ArrayList<Tile>();
	}

	// custom tile find to allow for the chartactor to not be centered over the square.
	protected Tile getTileByHexVector(AbstractWorld world, SquareVector vector) {
		for (Tile tile : world.getTileMap()) {
			if (vector.isCloseEnoughToBeTheSame(tile.getCoordinates(),0.5f) ) {
				return tile;
			}
		}
		return null;

	}

}
