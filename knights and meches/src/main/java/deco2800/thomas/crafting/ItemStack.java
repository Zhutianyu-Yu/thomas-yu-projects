package deco2800.thomas.crafting;

import deco2800.thomas.items.AbstractItem;

/**
 * A stack of an item
 * (ie 64 dirt, 12 apples, 1 sword)
 * (the same as a minecraft stack)
 */
public class ItemStack {

    /**
     * the item of the stack
     */
    private AbstractItem item;

    /**
     * the amount of the item in the stack
     */
    private int quantity;

    //Constructor ==============================================================

    /**
     * Constructor
     * @param item the item of the stack
     * @param quantity the amount of said item in the stack
     */
    public ItemStack(AbstractItem item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    //Get Methods ==============================================================

    /**
     * Gets the stack's item
     * @return Returns the item held in the stack
     */
    public AbstractItem getItem() {
        return this.item;
    }

    /**
     * Get the quantity of item in this stack
     * @return Returns the current quantity of this stack
     */
    public int getQuantity() {
        return this.quantity;
    }

    /**
     * Gets the maximum stack size as given by the current item
     * @return Returns the integer of the maximum stack size
     */
    public int getMaxQuantity() {
        return this.item.getItemSize();
    }

    //Set, Add and Remove Methods ==============================================

    /**
     * Changes the quantity by the given amount
     * @param amount the number to add to the quantity (a negative integer
     *               will subtract to a minimum of 0)
     */
    public void changeQuantity(int amount) {
        this.quantity += amount;
        if (this.quantity < 0) {
            this.quantity = 0;
        }
    }

    //Other Methods ============================================================

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (!(obj instanceof ItemStack)) {
            return false;
        } else {
            ItemStack otherStack = (ItemStack) obj;
            if (this.hashCode() != otherStack.hashCode()) {
                return false;
            } else {
                return (this.item.equals(otherStack.item) &&
                        this.quantity == otherStack.quantity);
            }
        }
    }

    @Override
    public int hashCode() {
        return this.item.hashCode() + this.quantity;
    }
}
