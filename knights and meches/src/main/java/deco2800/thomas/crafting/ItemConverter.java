package deco2800.thomas.crafting;

import java.util.ArrayList;
import java.util.List;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ItemNotInInventoryException;

/**
 * An abstract class that will be implemented by Crafter, Trader, etc
 * It holds a list of recipes to craft/trade etc and has methods to make use of
 * them
 */
public abstract class ItemConverter {

    /**
     * A list that holds all the recipes that this converter knows
     */
    private List<Recipe> recipeList;

    /**
     * The number of recipes in the recipeList
     */
    private int numRecipesHeld;

    /**
     * The inventory to take items from when crafting
     */
    private InventoryAbstract converterInventory;

    //Constructor ==============================================================

    /**
     * Constructor
     * @param playerIn the inventory that recipes take their inputs
     *                       from and give their outputs to
     */
    public ItemConverter(InventoryAbstract playerIn) {
        this.converterInventory = playerIn;
        //currently no recipes held
        this.emptyConverter();
    }

    //Get Methods===============================================================

    /**
     * Returns all recipes stored in this converter
     * @return return a list of all recipes known to this instance
     */
    public List<Recipe> getRecipeList() {
        return recipeList;
    }

    /**
     * Shows all recipes that are unlocked at the given level
     * @param level the level that the recipes to be listed will all be
     *              available to
     * @return returns a list of all recipes unlocked at/before the given level
     */
    public List<Recipe> getUnlockedRecipeList(int level) {
        List<Recipe> unlockedList = new ArrayList<>();
        for (Recipe currentRecipe: this.recipeList) {
            if (isUnlocked(currentRecipe, level)) {
                unlockedList.add(currentRecipe);
            }
        }
        return unlockedList;
    }

    /**
     * Gives a list of all recipes that are currently unlocked by the player
     * @return An ArrayList of recipes that are unlocked for the player
     */
    public List<Recipe> getCurrentlyUnlockedRecipeList() {
        int currentLevel = 10;
        return getUnlockedRecipeList(currentLevel);
    }

    /**
     * Gets the number of recipes currently held in the recipe list
     * @return the count of recipes in the recipe list
     */
    public int getNumRecipesHeld() {
        return this.numRecipesHeld;
    }

    /**
     * Checks if the given recipe is currently craftable
     * Being Craftable means that the required inputs of the recipe are in
     * the inventory, there's enough empty space in the inventory for the
     * outputs to be places, and the players level is higher than the
     * required level of that recipe
     * @param recipe the recipe to be checked if it is craftable
     * @return true if the recipe is currently craftable. false otherwise
     */
    public boolean isCraftable(Recipe recipe) {
        //checks to make sure all inputs exist in the inventory
        for (ItemStack itemStack : recipe.getInputs()) {
            try {
                this.converterInventory.findInInventory(itemStack);
            } catch (ItemNotInInventoryException e) {
                //Inputs not in inventory
                return false;
            }
        }
        //check if the players level is higher than the recipes level
        if (!isUnlocked(recipe, 10)) {
            return false;
        }

        //Check if there's enough space for the outputs
        int numOutputs = recipe.getInventorySpacesNeeded();
        int spacesAvailable;
        try {
            spacesAvailable = converterInventory.findQuantityInInventory(null);
        } catch (ItemNotInInventoryException e) {
            spacesAvailable = 0;
        }
        return numOutputs <= spacesAvailable;
    }

    /**
     * Checks if the given recipe is unlocked at the given level
     * @param recipe the recipe to be checked if it is unlocked
     * @param level the level to check at
     * @return true if the recipes level is less than or equal to the given
     * level. false otherwise
     */
    public boolean isUnlocked(Recipe recipe, int level) {
        int levelReq = recipe.getLevelRequirement();
        return (levelReq <= level);
    }

    //Set, add and remove methods ==============================================

    /**
     * Adds the given recipe to the
     * @param newRecipe the recipe to add to the recipe list
     */
    public void addRecipe(Recipe newRecipe) {
        recipeList.add(newRecipe);
        this.numRecipesHeld++;
    }

    /**
     * removes the given recipe from the recipe list
     * @param recipe the recipe to remove from the list
     */
    public void removeRecipe(Recipe recipe) {
        if (recipeList.remove(recipe)) { //recipe existed in the list
            this.numRecipesHeld--;
        }
    }

    /**
     * Empties the crafter of all recipes held
     */
    protected void emptyConverter() {
        this.recipeList = new ArrayList<>();
        this.numRecipesHeld = 0;
    }

    //Other Methods ============================================================

    /**
     * Crafts the given recipe by removing the input items from playerInventory
     *      and adding the output items to Inventory
     * @param recipe the recipe that is to be crafted
     * @return true if the craft was successful, false if it was not
     */
    public boolean craft(Recipe recipe) {
        if (!isCraftable(recipe)) {
            return false;
        }
        //removes the inputs from the inventory
        for (ItemStack input : recipe.getInputs()) {
            converterInventory.removeQuantityFromInventory(
                    input.getItem(), input.getQuantity());
        }

        //add the output items to the inventory
        for (ItemStack outputs : recipe.getOutputs()) {
            //add output items
            for (int i = 1; i <= outputs.getQuantity(); i++) {
                //add the items i times (for when 2 or more are made)
                converterInventory.addInventory(outputs.getItem());
            }
        }
        return true;
    }
}
