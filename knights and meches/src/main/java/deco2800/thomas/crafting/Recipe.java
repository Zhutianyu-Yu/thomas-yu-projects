package deco2800.thomas.crafting;

import java.util.ArrayList;
import java.util.List;
import deco2800.thomas.items.*;

/**
 * A recipe that you can craft.
 * Takes the input items out of your inventory and adds the output items to it
 * whenever it is crafted
 */
public class Recipe {

    /**
     * Items that will be used up in this recipe
     */
    private List<ItemStack> inputItems;

    /**
     *  Items that will be created by this recipe
     */
    private List<ItemStack> outputItems;

    /**
     * The level that the player must be in order to unlock this recipe
     */
    private int levelRequirement;

     /**
     * The address of the image to be shown in the crafting menu
     */
    private String imageAddress;

    /**
     * The address of the image if the recipe has not been assigned an image yet
     */
    public static final String DEFAULT_IMAGE =
            "inventory/crafting/placeholder.png";

    /**
     * The name of the recipe
     */
    private String recipeName;

    //Constructor methods ======================================================

    /**
     * Constructor of recipe
     * @param recipeName The name of the recipe
     */
    public Recipe(String recipeName) {
        //set recipeName to the given name
        this.recipeName = recipeName;
        //make sure all class variables are initialised
        //IO item lists are empty upon creation
        this.inputItems  = new ArrayList<>();
        this.outputItems = new ArrayList<>();
        //the required level defaults to 0
        this.levelRequirement = 0;
        //set the default image
        this.imageAddress = DEFAULT_IMAGE;
    }

    /**
     * Constructor of recipe
     * @param recipeName The name of the recipe
     * @param imageAddress the address of the recipes image
     */
    public Recipe(String recipeName, String imageAddress) {
        this(recipeName);
        this.imageAddress = imageAddress;
    }

    /**
     * Constructor of recipe
     * @param recipeName The name of the recipe
     * @param imageAddress the address of the recipes image
     * @param inputs A list of items for the input of the recipe
     * @param outputs A list of items for the output of the recipe
     */
    public Recipe(String recipeName, String imageAddress, List<ItemStack> inputs,
                  List<ItemStack> outputs) {
        this(recipeName, imageAddress);
        for (ItemStack input : inputs) {
            this.addInput(input);
        }
        for (ItemStack outut : outputs) {
            this.addOutput(outut);
        }
    }

    //Get methods ==============================================================

    /**
     * Gets the recipes inputs
     * @return a list of the inputs for this recipe
     */
    public List<ItemStack> getInputs() {
        return inputItems;
    }

    /**
     * Gets the recipes outputs
     * @return a list of the outputs of this recipe
     */
    public List<ItemStack> getOutputs() {
        return outputItems;
    }

    /**
     * Gets the recipes level requirement
     * @return An int of the required level
     */
    public int getLevelRequirement() {
        return this.levelRequirement;
    }

    /**
     * Counts the spaces of the inventory needed to use the recipe
     * @return An integer of the spaces needed
     */
    public int getInventorySpacesNeeded() {
        int spacesNeeded = 0;
        spacesNeeded += outputItems.size();
        return spacesNeeded;
    }

    /**
     * Returns the image address for this recipe
     * @return the String of the path to the image that represents this recipe
     */
    public String getImageAddress() {
        return this.imageAddress;
    }

    /**
     * Gets the name of this recipe
     * @return String holding the name of this recipe
     */
    public String getRecipeName() {
        return this.recipeName;
    }

    //Set and add methods ======================================================

    /**
     * Adds the given item stack to the input
     * @param itemsInput the itemStack to add
     */
    public void addInput(ItemStack itemsInput) {
        this.inputItems.add(itemsInput);
    }
    /**
     * Adds the given item of quantity to the inputs list
     * @param item the item to be added
     * @param quantity the amount to be added
     */
    public void addInput(AbstractItem item, int quantity) {
        ItemStack itemsInput = new ItemStack(item, quantity);
        this.addInput(itemsInput);
    }

    /**
     * Adds the given item stack to the output
     * @param itemsOutput the itemStack to add
     */
    public void addOutput(ItemStack itemsOutput) {
        this.outputItems.add(itemsOutput);
    }
    /**
     * add the given item of quantity to the outputs list
     * @param item the item to be added
     * @param quantity the amount to be added
     */
    public void addOutput(AbstractItem item, int quantity) {
        ItemStack itemsOutput = new ItemStack(item, quantity);
        this.addOutput(itemsOutput);
    }

    /**
     * sets the required level to use this recipe
     * @param level the new level requirement
     */
    public void setLevelRequirement(int level) {
        this.levelRequirement = level;
    }

    /**
     * Sets the image of this recipe to the image at the given address
     * If the given string is empty then it is set to the default image
     * @param imageAddress the address of the image
     */
    public void setImageAddress(String imageAddress) {
        if (imageAddress.equals("")) {
            this.imageAddress = DEFAULT_IMAGE;
        } else {
            this.imageAddress = imageAddress;
        }
    }
}
