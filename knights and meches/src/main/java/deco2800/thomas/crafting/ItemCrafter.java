package deco2800.thomas.crafting;

import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Consumable.ResurrectionScroll;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Equipment.WoodenSword;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.items.Material.Wood;

import java.io.File;
import java.util.ArrayList;

/**
 * A converter that is used for crafting items
 * Currently Identical to ItemConverter
 */
public class ItemCrafter extends ItemConverter {

    private static final String IMAGE_FORMAT = ".png";

    //Constructor ==============================================================

    /**
     * Constructor
     * @param playerInventoryAbstract the players inventory
     */
    public ItemCrafter(InventoryAbstract playerInventoryAbstract) {
        super(playerInventoryAbstract);
    }

    //Other Methods ============================================================

    /**
     * Sets the crafter to an empty crafter and fills it with starting items
     */
    public void startingCrafter() {
        this.emptyConverter();
        //Swords
        //Wood Sword
        Recipe woodSword = createNewItem("Sword_Wood");
        woodSword.addOutput(new WoodenSword(), 1);
        this.addRecipe(woodSword);
        //Iron Sword
        Recipe ironSword = createNewItem("Sword_Iron");
        ironSword.addOutput(new IronSword(), 1);
        this.addRecipe(ironSword);
        //Armours
        //Wood Armour
        Recipe woodArmour = createNewItem("Armor_Wood");
        woodArmour.addOutput(new WoodenArmour(), 1);
        this.addRecipe(woodArmour);
        //Iron Armour
        Recipe ironArmour = createNewItem("Armor_Iron");
        ironArmour.addOutput(new IronArmour(), 1);
        this.addRecipe(ironArmour);

        //Health Potions
        Recipe smallHealthPotion = createNewItem("Small_Health_Potion");
        smallHealthPotion.addOutput(new HealthPotion("small"), 1);
        this.addRecipe(smallHealthPotion);
        Recipe mediumHealthPotion = createNewItem("Medium_Health_Potion");
        mediumHealthPotion.addOutput(new HealthPotion("medium"), 1);
        this.addRecipe(mediumHealthPotion);
        Recipe largeHealthPotion = createNewItem("Big_Health_Potion");
        largeHealthPotion.addOutput(new HealthPotion("large"), 1);
        this.addRecipe(largeHealthPotion);

        //Mana Potions
        Recipe smallManaPotion = createNewItem("Small_Mana_Potion");
        smallManaPotion.addOutput(new ManaPotion("small"), 1);
        this.addRecipe(smallManaPotion);
        Recipe mediumManaPotion = createNewItem("Medium_Mana_Potion");
        mediumManaPotion.addOutput(new ManaPotion("medium"), 1);
        this.addRecipe(mediumManaPotion);
        Recipe largeManaPotion = createNewItem("Big_Mana_Potion");
        largeManaPotion.addOutput(new ManaPotion("large"), 1);
        this.addRecipe(largeManaPotion);
    }

    private static Recipe createNewItem(String name) {
        String category;
        if (name.contains("Sword") || name.contains("Armor")) {
            category = "Equipment";
        } else if (name.contains("Potion")) {
            category = "Consumable";
        } else {
            category = "Material";
        }

        Recipe newItem = new Recipe(name);
        newItem.setImageAddress(
                String.join(File.separator, "items", category, name)
                        .concat(IMAGE_FORMAT));
        createInputs(newItem);
        return newItem;
    }

    private static void createInputs(Recipe recipe) {
        ArrayList<AbstractItem> craftingMaterials = new ArrayList<>();
        craftingMaterials.add(new Wood());
        craftingMaterials.add(new Iron());
        craftingMaterials.add(new Gold());
        craftingMaterials.add(new Diamond());

        AbstractItem mainMaterial = new Wood();
        for (AbstractItem material : craftingMaterials) {
            if (recipe.getRecipeName().contains(material.getItemName())) {
                recipe.addInput(material, 3);
                mainMaterial = material;
            }
        }
        if (recipe.getRecipeName().contains("Sword")) {
            recipe.addInput(new Wood(), 1);
            recipe.addInput(mainMaterial, 2);
        }
        if (recipe.getRecipeName().contains("Armor")) {
            recipe.addInput(mainMaterial, 3);
        }
        if (recipe.getRecipeName().contains("Potion")) {
            recipe.addInput(new Wood(), 2);
        }
    }
}
