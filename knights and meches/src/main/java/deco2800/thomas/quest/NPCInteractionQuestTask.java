package deco2800.thomas.quest;

import java.util.List;

/**
 * {@inheritDoc} Subclass of a QuestTask with a primary goal to interact with a certain NPC in a certain way
 */
public class NPCInteractionQuestTask extends QuestTask {
    private final int npcID;
    private final String action;

    /**
     * Creates a QuestTask tracking the NPC to undertake an action over
     * @param npcID Numerical id of an NPC in the game
     * @param action Action to undertake over said NPC
     */
    public NPCInteractionQuestTask(int taskID,
                                   String description,
                                   String location,
                                   int requiredQuantity,
                                   List<QuestTask> prerequisiteTasks,
                                   int currentProgress,
                                   int npcID,
                                   String action) {
        super(taskID, description, location, requiredQuantity, prerequisiteTasks, currentProgress);
        this.npcID = npcID;
        this.action = action;
    }

    /**
     *
     * @return Numerical id of an NPC to undertake the quest action over
     */
    public int getNpcID() {
        return npcID;
    }

    /**
     *
     * @return An action to undertake over the quest NPC
     */
    public String getAction() {
        return action;
    }
}
