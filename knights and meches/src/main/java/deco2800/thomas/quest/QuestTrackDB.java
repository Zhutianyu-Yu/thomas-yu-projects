package deco2800.thomas.quest;

import org.javatuples.Pair;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class QuestTrackDB {

    private static final String DESCRIPTION = "Description";

    private  QuestTrackDB(){}

    public static  boolean isInitialized(){
        return connectionString != null;
    }

    protected static final org.slf4j.Logger LOGGER =  LoggerFactory.getLogger(QuestTrackDB.class);

    private static String connectionString;

    public static void initDatabase(String connectionString) throws SQLException {
        QuestTrackDB.connectionString = connectionString;
        //Make sure database actually exist after we initialise it
        createQuestDataTable();
    }

    /**
     * Provides connection to the database
     * @return Connection to the database
     */
    private static Connection connect() throws SQLException {
        if(!isInitialized())
            throw new SQLException("Connection string is not set");
        return DriverManager.getConnection(connectionString);
    }

    /**
     * Creates Tables in .db file
     */
    public static void createQuestDataTable() throws SQLException {

        // SQL statement for creating a new table
        String quest = "CREATE TABLE IF NOT EXISTS Quest (\n"
                + " Id integer PRIMARY KEY,\n"
                + " Name text NOT NULL,\n"
                + " Description text NOT NULL,\n"
                + " IsMainQuest integer NOT NULL\n"
                + ");";

        String questTask = "CREATE TABLE IF NOT EXISTS QuestTask (\n"
                + " Id integer,\n"
                + " QuestId integer,\n"
                + " Description text NOT NULL,\n"
                + " Location text NOT NULL,\n"
                + " RequiredQuantity integer NOT NULL,\n"
                + " PRIMARY KEY (Id, QuestId),\n"
                + " FOREIGN KEY (QuestId) REFERENCES Quest (Id) ON DELETE CASCADE\n"
                + ");";

        String objectInteractionQuestTask = "CREATE TABLE IF NOT EXISTS ObjectInteractionQuestTask (\n"
                + " QuestId integer,\n"
                + " TaskId integer,\n"
                + "	ItemId text NOT NULL,\n"
                + "	Action text NOT NULL,\n"
                + "	FOREIGN KEY (QuestId, TaskId) REFERENCES QuestTask (Id, QuestId) ON DELETE CASCADE,\n"
                + " PRIMARY KEY (QuestId, TaskId)\n"
                + ");";

        String npcInteractionQuestTask = "CREATE TABLE IF NOT EXISTS NPCInteractionQuestTask (\n"
                + " QuestId integer,\n"
                + " TaskId integer,\n"
                + "	NPCId integer NOT NULL,\n"
                + "	Action text NOT NULL, \n"
                + "	FOREIGN KEY (QuestId, TaskId) REFERENCES QuestTask (Id, QuestId) ON DELETE CASCADE,\n"
                + " PRIMARY KEY (TaskId)\n"
                + ");";

        String killQuestTask = "CREATE TABLE IF NOT EXISTS KillQuestTask (\n"
                + " QuestId integer,\n"
                + " TaskId integer,\n"
                + "	EnemyType text NOT NULL,\n"
                + "	FOREIGN KEY (QuestId, TaskId) REFERENCES QuestTask (Id, QuestId) ON DELETE CASCADE,\n"
                + " PRIMARY KEY (TaskId)\n"
                + ");";

        String questTaskPrerequisite = "CREATE TABLE IF NOT EXISTS QuestTaskPrerequisite (\n"
                + " Id integer PRIMARY KEY,\n"
                + " TaskId integer,\n"
                + " PrerequisiteTaskId,\n"
                + " FOREIGN KEY (TaskId) REFERENCES QuestTask (Id),\n"
                + " FOREIGN KEY (PrerequisiteTaskId) REFERENCES QuestTask (Id) ON DELETE CASCADE\n"
                + ");";

        String questPrerequisite = "CREATE TABLE IF NOT EXISTS QuestPrerequisite (\n"
                + " Id integer PRIMARY KEY,\n"
                + " QuestId integer,\n"
                + " PrerequisiteId integer,\n"
                + " FOREIGN KEY (QuestId) REFERENCES Quest (Id),\n"
                + " FOREIGN KEY (PrerequisiteId) REFERENCES Quest (Id) ON DELETE CASCADE\n"
                + ");";

        String playerPrerequisite = "CREATE TABLE IF NOT EXISTS PlayerPrerequisite (\n"
                + " Id integer PRIMARY KEY,\n"
                + " QuestId integer,\n"
                + " PlayerLevel integer NOT NULL,\n"
                + " ItemId text,\n"
                + " RequiredQuantity integer,\n"
                + " FOREIGN KEY (QuestId) REFERENCES Quest (Id) ON DELETE CASCADE\n"
                + ");";

        String questReward = "CREATE TABLE IF NOT EXISTS QuestReward (\n"
                + " Id integer PRIMARY KEY,\n"
                + " QuestId integer,\n"
                + " XpReward integer,\n"
                + " ItemTypeId text,\n"
                + " ItemQuantity integer NOT NULL,\n"
                + " FOREIGN KEY (QuestId) REFERENCES Quest (Id) ON DELETE CASCADE\n"
                + ");";

        try (Connection conn = connect()) {
            try (Statement stmt = conn.createStatement()) {
                stmt.execute(quest);
                stmt.execute(questTask);
                stmt.execute(objectInteractionQuestTask);
                stmt.execute(npcInteractionQuestTask);
                stmt.execute(killQuestTask);
                stmt.execute(questTaskPrerequisite);
                stmt.execute(questPrerequisite);
                stmt.execute(playerPrerequisite);
                stmt.execute(questReward);
            }
        }
    }

    /**
     * Deletes QuestReward table from the database
     */
    public static void dropAllTable() throws SQLException {
        String dropQuestReward = "DROP TABLE IF EXISTS QuestReward;";
        String dropObjectInteractionQuestTask = "DROP TABLE IF EXISTS ObjectInteractionQuestTask;";
        String dropNPCInteractionQuestTask  = "DROP TABLE IF EXISTS NPCInteractionQuestTask;";
        String dropKillQuestTask = "DROP TABLE IF EXISTS KillQuestTask;";
        String dropQuestTaskPrerequisite = "DROP TABLE IF EXISTS QuestTaskPrerequisite;";
        String dropQuestPrerequisite = "DROP TABLE IF EXISTS QuestPrerequisite;";
        String dropPlayerPrerequisite = "DROP TABLE IF EXISTS PlayerPrerequisite;";
        String dropQuestTask = "DROP TABLE IF EXISTS QuestTask;";
        String dropQuest = "DROP TABLE IF EXISTS Quest;";

        try (Connection conn = connect()) {
            try (Statement stmt = conn.createStatement()) {
                stmt.execute(dropQuestReward);
                stmt.execute(dropObjectInteractionQuestTask);
                stmt.execute(dropNPCInteractionQuestTask);
                stmt.execute(dropKillQuestTask);
                stmt.execute(dropQuestTaskPrerequisite);
                stmt.execute(dropQuestPrerequisite);
                stmt.execute(dropPlayerPrerequisite);
                stmt.execute(dropQuestTask);
                stmt.execute(dropQuest);
            }
        }
    }

    /**
     * Extracts quest information from a quest object and stores in the Quest Table
     * @param quest object information to be inserted
     */
    public static void questInsert(Quest quest) throws SQLException {
        String query = "INSERT INTO Quest(Id,Name,Description,IsMainQuest) VALUES(?,?,?,?)";
        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, quest.getId());
                pstmt.setString(2, quest.getName());
                pstmt.setString(3, quest.getDescription());
                pstmt.setBoolean(4, quest.isMainQuest());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts full quest information from a quest object and stores it in the corresponding tables
     * @param quest Quest to be inserted
     */
    public static void questInsertFull(Quest quest) throws SQLException {
        questInsert(quest);
        for(Integer prereq : quest.getQuestPrerequisites()){
            questPrerequisiteInsert(quest.getId(), prereq);
        }
        for (QuestReward reward: quest.getRewards()) {
            questRewardInsert(reward, quest);
        }
        for(PlayerPrerequisite prereq: quest.getPlayerPrerequisites()){
            playerPrerequisiteInsert(prereq, quest);
        }
        for(QuestTask task : quest.getQuestTasks()){
            questTaskInsert(task, quest);
            if(task instanceof KillQuestTask)
                killQuestTaskInsert((KillQuestTask) task, quest.getId());
            else if(task instanceof ObjectInteractionQuestTask)
                objectInteractionQuestTaskInsert((ObjectInteractionQuestTask)task, quest.getId());
            else if(task instanceof NPCInteractionQuestTask)
                npcInteractionQuestTaskInsert((NPCInteractionQuestTask)task, quest.getId());
        }
        for(QuestTask task : quest.getQuestTasks()) {
            for(QuestTask preTask : task.getTaskPrerequisites())
                questTaskPrerequisiteInsert(task, preTask);
        }
    }

    /**
     * Deletes the quest entity from the database
     * @param quest object information to be deleted
     */
    public static void questDelete(Quest quest) throws SQLException {
        String query = "DELETE FROM Quest WHERE Id = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, quest.getId());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Wipes out all of the current quests
     */
    public static void questClear() throws SQLException {
        String query = "DELETE FROM Quest;\n" +
                "DELETE FROM QuestTask;\n" +
                "DELETE FROM KillQuestTask;\n" +
                "DELETE FROM NPCInteractionQuestTask;\n" +
                "DELETE FROM ObjectInteractionQuestTask;\n" +
                "DELETE FROM QuestReward;\n" +
                "DELETE FROM QuestTaskPrerequisite;\n" +
                "DELETE FROM QuestPrerequisite;\n" +
                "DELETE FROM PlayerPrerequisite;\n";
        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from questTask object and stores in QuestTask Table
     * QuestTask entity maps to quest Id
     * @param questTask object to be inserted
     * @param quest object to be inserted
     */
    public static void questTaskInsert(QuestTask questTask, Quest quest) throws SQLException{
        String query = "INSERT INTO QuestTask(Id, QuestId, Description, Location, RequiredQuantity) VALUES(?,?,?,?,?)";
        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questTask.getTaskID());
                pstmt.setInt(2, quest.getId());
                pstmt.setString(3, questTask.getTaskDescription());
                pstmt.setString(4, questTask.getTaskLocation());
                pstmt.setInt(5, questTask.getRequiredQuantity());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Deletes the questTasks entity from the database
     * @param quest object to be deleted
     */
    public static void questTasksDelete(Quest quest) throws SQLException {
        String query = "DELETE FROM QuestTask WHERE questId = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, quest.getId());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from objectInteractionQuestTask object and stores in ObjectInteractionTask Table
     * @param objectInteractionTask object to be inserted
     */
    public static void objectInteractionQuestTaskInsert(ObjectInteractionQuestTask objectInteractionTask, int questId)
            throws SQLException{
        String query = "INSERT INTO ObjectInteractionQuestTask(QuestId, TaskId, ItemId, Action) VALUES(?,?,?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                pstmt.setInt(2, objectInteractionTask.getTaskID());
                pstmt.setString(3, objectInteractionTask.getItemId());
                pstmt.setString(4, objectInteractionTask.getAction());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from NPCInteractionQuestTask object and stores in NPCInteractionQuestTask Table
     * @param npcTask object to be inserted
     */
    public static void npcInteractionQuestTaskInsert(NPCInteractionQuestTask npcTask, int questId)
            throws SQLException{
        String query = "INSERT INTO NPCInteractionQuestTask(QuestId, TaskId, NPCId, Action) VALUES(?,?,?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                pstmt.setInt(2, npcTask.getTaskID());
                pstmt.setInt(3, npcTask.getNpcID());
                pstmt.setString(4, npcTask.getAction());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from KillQuestTask object and stores in KillQuestTask Table
     * @param killTask object to be inserted
     *
     */
    public static void killQuestTaskInsert(KillQuestTask killTask, int questId) throws SQLException {
        String query = "INSERT INTO KillQuestTask(QuestId, TaskId, EnemyType) VALUES(?,?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                pstmt.setInt(2, killTask.getTaskID());
                pstmt.setString(3, killTask.getEnemyType());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from preTask and currentTask and stores in QuestTaskPrerequisite Table
     * @param currentTask object of the current task
     * @param preTask object of the prerequisite of the current task
     */
    public static void questTaskPrerequisiteInsert(QuestTask currentTask,
                                                   QuestTask preTask) throws SQLException {
        String query = "INSERT INTO QuestTaskPrerequisite(TaskId,PrerequisiteTaskId) VALUES(?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, currentTask.getTaskID());
                pstmt.setInt(2, preTask.getTaskID());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Deletes the questTaskPrerequisite entity from the database
     * @param id unique integer identifying the entity
     */
    public static void questTaskPrerequisiteDelete(int id) throws SQLException {
        String query = "DELETE FROM QuestTaskPrerequisite WHERE Id = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, id);
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from preQuest and stores in QuestPrerequisite Table
     * @param currentQuestId integer id of the current quest
     * @param preQuestId integer id of the prerequisite quest of the current quest
     */
    public static void questPrerequisiteInsert(Integer currentQuestId,
                                               Integer preQuestId) throws SQLException {
        String query = "INSERT INTO QuestPrerequisite(QuestId,PrerequisiteId) VALUES(?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, currentQuestId);
                pstmt.setInt(2, preQuestId);
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Deletes the preQuest entity from the database
     * @param id unique integer identifying the entity
     */
    public static void questPrerequisiteDelete(int id) throws SQLException {
        String query = "DELETE FROM QuestPrerequisite WHERE Id = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, id);
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from playerPrerequisite and stores in PlayerPrerequisite Table
     * @param playerPrerequisite object of playerPrerequisite
     * @param quest quest of the playerPrerequisite
     */
    public static void playerPrerequisiteInsert(PlayerPrerequisite playerPrerequisite,
                                                Quest quest) throws SQLException {
        String query = "INSERT INTO PlayerPrerequisite(QuestId,PlayerLevel,ItemId,RequiredQuantity) VALUES(?,?,?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, quest.getId());
                pstmt.setInt(2, playerPrerequisite.getPlayerLevel());
                pstmt.setString(3, playerPrerequisite.getItemTypeId());
                pstmt.setInt(4, playerPrerequisite.getItemQuantity());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Deletes the playerPrerequisite entity from the database
     * @param id unique integer identifying the playerPrerequisite entity from the database
     */
    public static void playerPrerequisiteDelete(int id) throws SQLException {
        String query = "DELETE FROM PlayerPrerequisite WHERE Id = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, id);
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Extracts quest task information from questReward object and stores in QuestReward Table
     * @param questReward object of to be inserted
     * @param quest quest of the questReward
     */
    public static void questRewardInsert(QuestReward questReward,
                                         Quest quest) throws SQLException {
        String query = "INSERT INTO QuestReward(QuestId,XpReward,ItemTypeId,ItemQuantity) VALUES(?,?,?,?)";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, quest.getId());
                pstmt.setInt(2, questReward.getXpReward());
                pstmt.setString(3, questReward.getItemTypeId());
                pstmt.setInt(4, questReward.getItemQuantity());
                pstmt.executeUpdate();
            }
        }
    }

    /**
     * Deletes the questReward entity from the database
     * @param id unique integer identifying the questReward entity from the database
     */
    public static void questRewardDelete(int id) throws SQLException {
        String query = "DELETE FROM QuestReward WHERE Id = ?";

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, id);
                pstmt.executeUpdate();
            }
        }
    }

    /**
     *
     * @param id integer value of the quest Id
     * @return returns Quest object from Database by taking id
     * @throws SQLException for illegal queries
     */
    public static Quest getQuest(int id) throws SQLException{
        String query = "SELECT * FROM Quest WHERE Id = ?";
        Quest quest;
        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, id);
                //Name is a unique key so we only get one set back
                try (ResultSet rs = pstmt.executeQuery()) {
                    //However if we have no matching rows - return null
                    if (!rs.next())
                        return null;
                    int questId = rs.getInt("Id");
                    //Quest id must be positive and bigger than 0
                    if (questId <= 0)
                        return null;
                    //After we make sure quest exists - get its parameters from linked tables
                    List<QuestReward> rewards = getQuestReward(questId);
                    List<QuestTask> questTasks = mapQuestTaskPrerequisite(questId);
                    List<Integer> questPrerequisites = getPrerequisiteQuest(questId);
                    List<PlayerPrerequisite> playerPrerequisites = getPlayerPrerequisite(questId);
                    quest = new Quest(questId,
                            rs.getString("Name"),
                            rs.getString(DESCRIPTION),
                            rs.getBoolean("IsMainQuest"),
                            rewards,
                            questTasks,
                            questPrerequisites,
                            playerPrerequisites,
                            true,
                            false,
                            false);
                    return quest;
                }
            }
        }
    }

    /**
     *
     * @param questName String value of the name of the Quest
     * @return Quest object by taking the quest name
     * @throws SQLException for illegal sql queries
     */
    public static Quest getQuest(String questName) throws SQLException{
        String query = "SELECT * FROM Quest WHERE Name = ?";
        try(Connection conn = connect()){
            try(PreparedStatement pstmt = conn.prepareStatement(query)){
                pstmt.setString(1, questName);
                //Name is a unique key so we only get one set back
                try (ResultSet rs = pstmt.executeQuery()) {
                    //However if we have no matching rows - return null
                    if (!rs.next())
                        return null;
                    int questId = rs.getInt("Id");
                    //Quest id must be positive and bigger than 0
                    if (questId <= 0)
                        return null;
                    //After we make sure quest exists - get its parameters from linked tables
                    List<QuestReward> rewards = getQuestReward(questId);
                    List<QuestTask> questTasks = mapQuestTaskPrerequisite(questId);
                    List<Integer> questPrerequisites = getPrerequisiteQuest(questId);
                    List<PlayerPrerequisite> playerPrerequisites = getPlayerPrerequisite(questId);
                    return new Quest(questId,
                            rs.getString("Name"),
                            rs.getString(DESCRIPTION),
                            rs.getBoolean("IsMainQuest"),
                            rewards,
                            questTasks,
                            questPrerequisites,
                            playerPrerequisites,
                            true,
                            false,
                            false);
                }
            }
        }
    }

    /**
     * Extracts playerPrerequisite information from the database and returns a list of PlayerPrerequisite
     * @param questId referring the relevant quest
     * @return All player prerequisites for the questId
     */
    public static ArrayList<PlayerPrerequisite> getPlayerPrerequisite(int questId)
            throws SQLException {
        String query = "SELECT * FROM PlayerPrerequisite WHERE QuestId = ?";
        ArrayList<PlayerPrerequisite> playerPrerequisites = new ArrayList<>();

        try(Connection conn = connect()) {
            try(PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        playerPrerequisites.add(new PlayerPrerequisite(rs.getInt("PlayerLevel"),
                                rs.getString("ItemId"),
                                rs.getInt("RequiredQuantity")));
                    }
                }
                return playerPrerequisites;
            }
        }
    }

    /**
     * Extracts Id of the PrerequisiteQuest of a quest
     * @param questId referring the relevant quest
     * @return List of Prerequisite Quest Ids
     */
    public static ArrayList<Integer> getPrerequisiteQuest(int questId) throws SQLException {
        String query = "SELECT * FROM QuestPrerequisite WHERE QuestId = ?";
        ArrayList<Integer> prerequisiteQuests = new ArrayList<>();

        try(Connection conn = connect()) {
            try(PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        prerequisiteQuests.add(rs.getInt("PrerequisiteId"));
                    }
                }
                return prerequisiteQuests;
            }
        }
    }

    /**
     * Extracts QuestReward information from the database and returns the list of QuestReward objects
     * @param questId referring the relevant quest
     * @return List of all quest rewards for completing the quest
     */
    public static ArrayList<QuestReward> getQuestReward(int questId) throws SQLException {
        String query = "SELECT * FROM QuestReward WHERE QuestId = ?";
        ArrayList<QuestReward> questRewards = new ArrayList<>();

        try(Connection conn = connect()) {
            try(PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setInt(1, questId);
                try(ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        questRewards.add(new QuestReward(rs.getInt("XpReward"),
                                rs.getString("ItemTypeId"),
                                rs.getInt("ItemQuantity")));
                    }
                }
                return questRewards;
            }
        }
    }

    /**
     * Extracts all QuestTasks required to complete the quest
     * @param questId referring the relevant quest
     * @return List of all questTasks to complete the quest
     */
    public static ArrayList<QuestTask> getQuestTasks(int questId) throws SQLException {

        String killQuery = "SELECT * FROM QuestTask INNER JOIN KillQuestTask on KillQuestTask.TaskId = QuestTask.Id WHERE QuestTask.QuestId = ? ";
        String objectQuery = "SELECT * FROM QuestTask INNER JOIN ObjectInteractionQuestTask on ObjectInteractionQuestTask.TaskId = QuestTask.Id WHERE QuestTask.QuestId = ? ";
        String npcQuery = "SELECT * FROM QuestTask INNER JOIN NPCInteractionQuestTask on NPCInteractionQuestTask.TaskId = QuestTask.Id WHERE QuestTask.QuestId = ? ";
        ArrayList<QuestTask> qt = new ArrayList<>();

        try (Connection conn = connect()) {
            try (PreparedStatement pstmt = conn.prepareStatement(killQuery)) {
                pstmt.setInt(1, questId);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        qt.add(new KillQuestTask(rs.getInt("Id"),
                                rs.getString(DESCRIPTION),
                                rs.getString("Location"),
                                rs.getInt("RequiredQuantity"),
                                new ArrayList<>(),
                                0,
                                rs.getString("EnemyType")));
                    }
                }
            }
            try (PreparedStatement pstmt = conn.prepareStatement(npcQuery)) {
                pstmt.setInt(1, questId);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        qt.add(new NPCInteractionQuestTask(rs.getInt("Id"),
                                rs.getString(DESCRIPTION),
                                rs.getString("Location"),
                                rs.getInt("RequiredQuantity"),
                                new ArrayList<>(),
                                0,
                                rs.getInt("NPCId"),
                                rs.getString("Action")));
                    }
                }
            }
            try (PreparedStatement pstmt = conn.prepareStatement(objectQuery)) {
                pstmt.setInt(1, questId);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        qt.add(new ObjectInteractionQuestTask(rs.getInt("Id"),
                                rs.getString(DESCRIPTION),
                                rs.getString("Location"),
                                rs.getInt("RequiredQuantity"),
                                new ArrayList<>(),
                                0,
                                rs.getString("ItemId"),
                                rs.getString("Action")));
                    }
                }
            }
            return qt;
        }
    }

    /**
     * Pairs each QuestTask of a Quest taken from the getQuestReward method to its relevant prerequisiteQuestTasks
     * @param questTasks List of all quest tasks
     * @return pair of questTask and list of integer of questTask Ids
     */
    private static ArrayList<Pair<QuestTask, ArrayList<Integer>>> pairPrerequisiteTask(ArrayList<QuestTask> questTasks)
            throws SQLException {
        ArrayList<Pair<QuestTask, ArrayList<Integer>>> qtPair = new ArrayList<>();
        try (Connection conn = connect()) {
            for (QuestTask qt : questTasks) {
                ArrayList<Integer> prerequisites = new ArrayList<>();
                String query = "SELECT PrerequisiteTaskId FROM QuestTaskPrerequisite WHERE TaskId = ?";
                try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                    pstmt.setInt(1, qt.getTaskID());
                    try (ResultSet rs = pstmt.executeQuery()) {
                        while (rs.next())
                            prerequisites.add(rs.getInt("PrerequisiteTaskId"));
                        qtPair.add(new Pair<>(qt, prerequisites));
                    }
                }
            }
            return qtPair;
        }
    }

    /**
     * Maps all QuestTasks to QuestTaskPrerequisites taken from the getPrerequisiteTask of a relevant quest
     * @param questId referring the relevant quest
     * @return List of all QuestTask objects mapped to its relevant prerequisiteTasks.
     */
    private static ArrayList<QuestTask> mapQuestTaskPrerequisite(int questId) throws SQLException {
        ArrayList<QuestTask> qt = getQuestTasks(questId);
        ArrayList<Pair<QuestTask, ArrayList<Integer>>> questTaskPrerequisites = pairPrerequisiteTask(qt);
        for (Pair<QuestTask, ArrayList<Integer>> outerPair: questTaskPrerequisites) {
            ArrayList<QuestTask> dependencies = new ArrayList<>();
            ArrayList<Integer> dependenciesIds = outerPair.getValue1();
            for (Pair<QuestTask, ArrayList<Integer>> innerPair: questTaskPrerequisites){
                if(innerPair.equals(outerPair))
                    continue;
                QuestTask innerTask = innerPair.getValue0();
                if(dependenciesIds.contains(innerTask.getTaskID()))
                    dependencies.add(innerTask);
            }
            QuestTask outerTask = outerPair.getValue0();
            outerTask.setTaskPrerequisites(dependencies);
        }
        return qt;
    }
}