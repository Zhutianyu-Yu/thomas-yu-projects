package deco2800.thomas.quest;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class QuestTaskSerializer implements JsonSerializer<QuestTask> {

    @Override
    public JsonElement serialize(QuestTask questTask, Type type, JsonSerializationContext jsonSerializationContext) {
        if (questTask == null)
            return null;
        else
            return jsonSerializationContext.serialize(questTask, questTask.getClass());
    }

}

