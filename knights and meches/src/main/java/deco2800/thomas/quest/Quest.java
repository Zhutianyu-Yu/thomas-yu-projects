package deco2800.thomas.quest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Quest {
    private final int id;
    private final String name;
    private final String description;
    private final boolean isMainQuest;
    private final List<QuestReward> rewards;
    private final List<QuestTask> questTasks;
    private final List<Integer> questPrerequisites;
    private final List<PlayerPrerequisite> playerPrerequisites;
    private boolean isActive;
    private boolean isForceComplete;
    private boolean isFailed;

    public Quest(int id,
                 String name,
                 String description,
                 boolean isMainQuest,
                 List<QuestReward> rewards,
                 List<QuestTask> questTasks,
                 List<Integer> questPrerequisites,
                 List<PlayerPrerequisite> playerPrerequisites,
                 boolean isActive,
                 boolean isForceComplete,
                 boolean isFailed){
        this.id = id;
        this.name = name;
        this.description = description;
        this.isMainQuest = isMainQuest;
        this.rewards = rewards != null
                        ? Collections.unmodifiableList(rewards)
                        : Collections.unmodifiableList(new ArrayList<QuestReward>());
        this.questTasks = Collections.unmodifiableList(questTasks);
        this.questPrerequisites = questPrerequisites != null
                                    ? Collections.unmodifiableList(questPrerequisites)
                                    : Collections.unmodifiableList(new ArrayList<Integer>());
        this.playerPrerequisites = playerPrerequisites != null
                                    ? Collections.unmodifiableList(playerPrerequisites)
                                    : Collections.unmodifiableList(new ArrayList<PlayerPrerequisite>());
        this.isActive = isActive;
        this.isForceComplete = isForceComplete;
        this.isFailed = isFailed;
    }

    public List<QuestReward> getRewards() {
        return rewards;
    }

    public List<Integer> getQuestPrerequisites() {
        return questPrerequisites;
    }

    public List<PlayerPrerequisite> getPlayerPrerequisites() {
        return playerPrerequisites;
    }

    public boolean isMainQuest() {
        return isMainQuest;
    }

    public String getName() {
        return name;
    }

    public String getDescription(){ return description; }

    public int getId() {
        return id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isForceComplete() {
        return isForceComplete;
    }

    public boolean isFailed() {
        return isFailed;
    }

    public void markAsComplete() {
        isForceComplete = true;
    }

    public void markAsFailed(){
        isFailed = true;
    }

    public boolean isComplete(){
        if(isFailed) return false;
        if(isForceComplete) return true;
        for (QuestTask task:questTasks) {
            if(!task.isComplete())
                return false;
        }
        return true;
    }

    public List<QuestTask> getQuestTasks() {
        return questTasks;
    }

    public List<QuestTask> getAvailableTasks(){
        ArrayList<QuestTask> availableTasks = new ArrayList<QuestTask>();
        for(QuestTask task: questTasks){
            if(task.isAvailable())
                availableTasks.add(task);
        }
        return Collections.unmodifiableList(availableTasks);
    }
}
