package deco2800.thomas.quest.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.QuestManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.quest.*;
import deco2800.thomas.worlds.MenuMenu;
import org.slf4j.LoggerFactory;
import java.util.List;


/**
 * Creates group that displays the quest UI.
 */
public class QuestInterface extends Group implements KeyDownObserver {
    /** Tracks whether the UI is currently displayed or not */
    private boolean questInterfaceDisplayed = false;

    /** Tracks whether the 'in progress' or 'completed' buttons are selected or not */
    private boolean isInProgressSelected = false;
    private boolean isCompletedSelected = false;

    /** Tracks whether the 'in progress' or 'completed' buttons are pressed or not */
    private boolean isInProgressPressed = false;
    private boolean isCompletedPressed = false;

    /** Tracks whether the 'expansion' buttons are pressed or not */
    private boolean isMainQuestPressed = false;
    private boolean isSideQuestPressed = false;

    /** Tracks whether the 'quest' buttons are pressed or not */
    private boolean isQuest1Pressed = false;
    private boolean isQuest2Pressed = false;
    private boolean isQuest3Pressed = false;

    /** Image declarations */
    private Image background;
    private Image inProgressSelected;
    private Image completedSelected;
    private Image inProgressMainQuestSelected;
    private Image inProgressSideQuestSelected;
    private Image completedMainQuestSelected;
    private Image completedSideQuestSelected;

    /** ImageButton declarations */
    private ImageButton closeButton;
    private ImageButton inProgressButton;
    private ImageButton completedButton;
    private ImageButton mainQuestExpansionButton;
    private ImageButton sideQuestExpansionButton;

    /** Label declarations */
    private Label rewards;
    private Label xp;
    private Label item;

    private Label quest1Name;
    private Label quest1Title;
    private Label quest1Description;
    private Label quest1Task;
    private Label quest1TaskProgress;
    private Label quest1RewardsXP;
    private Label quest1RewardsItem;
    private Label quest1RewardsItemQuantity;

    private BitmapFont newTimesRomanFontBold = new BitmapFont(Gdx.files.internal("inventory/FontFiles/NewTimesRoman_Bold_20.fnt"));
    private Skin skin = GameManager.get().getSkin();

    private QuestManager manager = QuestManager.get();
    private List<Quest> currentQuests = manager.getCurrentQuests();

    protected final static org.slf4j.Logger LOGGER =  LoggerFactory.getLogger(QuestInterface.class);

    /**
     * Main function.
     * Creates group for the quest UI.
     */
    public QuestInterface() {
        buildTemplate();
        buildButtons();
        buildQuestText();
        buildText();

        /* Getting input manager for the key down observer */
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     * Creates the overall layout of the quest UI.
     */
    private void buildTemplate() {
        this.background = new Image(new Texture(Gdx.files.internal("quests/background.png")));
        setImageSize(background);
        background.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(background);

        this.inProgressSelected = new Image(new Texture(Gdx.files.internal("quests/inProgress_selected.png")));
        setImageSize(inProgressSelected);
        this.inProgressSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 2),
                (float) (Gdx.graphics.getHeight() - background.getHeight() + 217.5));

        this.completedSelected = new Image(new Texture(Gdx.files.internal("quests/completed_selected.png")));
        setImageSize(completedSelected);
        this.completedSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 2),
                (Gdx.graphics.getHeight() - background.getHeight() + 143));

        this.inProgressMainQuestSelected = new Image(new Texture(Gdx.files.internal("quests/inProgress_mainQuest_expansion.png")));
        setImageSize(inProgressMainQuestSelected);
        inProgressMainQuestSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(inProgressMainQuestSelected);

        this.inProgressSideQuestSelected = new Image(new Texture(Gdx.files.internal("quests/inProgress_sideQuest_expansion.png")));
        setImageSize(inProgressSideQuestSelected);
        inProgressSideQuestSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(inProgressSideQuestSelected);

        this.completedMainQuestSelected = new Image(new Texture(Gdx.files.internal("quests/completed_mainQuest_expansion.png")));
        setImageSize(completedMainQuestSelected);
        completedMainQuestSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(completedMainQuestSelected);

        this.completedSideQuestSelected = new Image(new Texture(Gdx.files.internal("quests/completed_sideQuest_expansion.png")));
        setImageSize(completedSideQuestSelected);
        completedSideQuestSelected.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(completedSideQuestSelected);
    }

    /**
     * Creates the buttons to be used for the quest UI.
     */
    private void buildButtons() {
        Texture closeButtonTexture = new Texture(Gdx.files.internal("quests/transparent_closeButton.png"));
        TextureRegion closeButtonTextureRegion = new TextureRegion(closeButtonTexture);
        TextureRegionDrawable closeButtonTextureRegionDrawable = new TextureRegionDrawable(closeButtonTextureRegion);
        this.closeButton = new ImageButton(closeButtonTextureRegionDrawable);
        setButtonSize(closeButton);
        closeButton.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 880),
                (Gdx.graphics.getHeight() - background.getHeight() + 300));
        this.addActor(closeButton);

        closeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                closeQuestsDisplayed();
                closeInProgressSelected();
                closeCompletedSelected();
            }
        });

        Texture inProgressButtonTexture = new Texture(Gdx.files.internal("quests/transparent_inProgress-completedButton.png"));
        TextureRegion inProgressButtonTextureRegion = new TextureRegion(inProgressButtonTexture);
        TextureRegionDrawable inProgressButtonTextureRegionDrawable = new TextureRegionDrawable(inProgressButtonTextureRegion);
        this.inProgressButton = new ImageButton(inProgressButtonTextureRegionDrawable);
        setButtonSize(inProgressButton);
        inProgressButton.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 2),
                (float) (Gdx.graphics.getHeight() - background.getHeight() + 217.5));
        this.addActor(inProgressButton);

        inProgressButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isCompletedPressed && !isMainQuestPressed && !isSideQuestPressed) {
                    isInProgressSelected = !isInProgressSelected;
                    isInProgressPressed = !isInProgressPressed;
                }
            }
        });

        Texture completedButtonTexture = new Texture(Gdx.files.internal("quests/transparent_inProgress-completedButton.png"));
        TextureRegion completedButtonTextureRegion = new TextureRegion(completedButtonTexture);
        TextureRegionDrawable completedButtonTextureRegionDrawable = new TextureRegionDrawable(completedButtonTextureRegion);
        this.completedButton = new ImageButton(completedButtonTextureRegionDrawable);
        setButtonSize(completedButton);
        completedButton.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 2),
                (Gdx.graphics.getHeight() - background.getHeight() + 143));
        this.addActor(completedButton);

        completedButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isInProgressPressed && !isMainQuestPressed && !isSideQuestPressed) {
                    isCompletedSelected = !isCompletedSelected;
                    isCompletedPressed = !isCompletedPressed;
                }
            }
        });

        Texture mainQuestExpansionButtonTexture = new Texture(Gdx.files.internal("quests/transparent_closeButton.png"));
        TextureRegion mainQuestExpansionButtonTextureRegion = new TextureRegion(mainQuestExpansionButtonTexture);
        TextureRegionDrawable mainQuestExpansionButtonTextureRegionDrawable = new TextureRegionDrawable(mainQuestExpansionButtonTextureRegion);
        this.mainQuestExpansionButton = new ImageButton(mainQuestExpansionButtonTextureRegionDrawable);
        setButtonSize(mainQuestExpansionButton);
        mainQuestExpansionButton.setPosition((Gdx.graphics.getWidth() - background.getWidth() - 33),
                (Gdx.graphics.getHeight() - background.getHeight() + 60));
        this.addActor(mainQuestExpansionButton);

        mainQuestExpansionButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (isInProgressPressed || isCompletedPressed) {
                    if (!isSideQuestPressed) {
                        isMainQuestPressed = !isMainQuestPressed;
                    } else {
                        isSideQuestPressed = false;
                    }
                }
            }
        });

        Texture sideQuestExpansionButtonTexture = new Texture(Gdx.files.internal("quests/transparent_closeButton.png"));
        TextureRegion sideQuestExpansionButtonTextureRegion = new TextureRegion(sideQuestExpansionButtonTexture);
        TextureRegionDrawable sideQuestExpansionButtonTextureRegionDrawable = new TextureRegionDrawable(sideQuestExpansionButtonTextureRegion);
        this.sideQuestExpansionButton = new ImageButton(sideQuestExpansionButtonTextureRegionDrawable);
        setButtonSize(sideQuestExpansionButton);
        sideQuestExpansionButton.setPosition((Gdx.graphics.getWidth() - background.getWidth() - 33),
                (Gdx.graphics.getHeight() - background.getHeight() - 20));
        this.addActor(sideQuestExpansionButton);

        sideQuestExpansionButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (isInProgressPressed || isCompletedPressed) {
                    if (!isMainQuestPressed) {
                        isSideQuestPressed = true;
                    }
                }
            }
        });
    }

    /**
     * Creates quest text.
     */
    private void buildText() {
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = this.newTimesRomanFontBold;
        style.fontColor = Color.BLACK;

        this.rewards = new Label("Rewards", skin);
        rewards.setStyle(style);
        rewards.setFontScale(2.0f);
        rewards.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                (Gdx.graphics.getHeight() - background.getHeight() + 15));
        this.addActor(rewards);

        this.xp = new Label("XP", skin);
        xp.setStyle(style);
        xp.setFontScale(1.6f);
        xp.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                (Gdx.graphics.getHeight() - background.getHeight() - 40));
        this.addActor(xp);

        currentQuests = manager.getCurrentQuests();

        if (!currentQuests.isEmpty()) {
            String quest1 = currentQuests.get(0).getName();
            this.quest1Title = new Label(quest1, skin);
            quest1Title.setStyle(style);
            quest1Title.setFontScale(2.0f);
            quest1Title.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                    (Gdx.graphics.getHeight() - background.getHeight() + 265));
            this.addActor(quest1Title);

            String quest1DescriptionText = currentQuests.get(0).getDescription();
            this.quest1Description = new Label(quest1DescriptionText, skin);
            quest1Description.setStyle(style);
            quest1Description.setFontScale(1.6f);
            quest1Description.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                    (Gdx.graphics.getHeight() - background.getHeight() + 210));
            this.addActor(quest1Description);

            String quest1TaskText = currentQuests.get(0).getQuestTasks().get(0).getTaskDescription();
            this.quest1Task = new Label(quest1TaskText, skin);
            quest1Task.setStyle(style);
            quest1Task.setFontScale(1.6f);
            quest1Task.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                    (Gdx.graphics.getHeight() - background.getHeight() + 155));
            this.addActor(quest1Task);

            String quest1TaskProgressText = currentQuests.get(0).getQuestTasks().get(0).getCurrentProgress() +
                    "/" + currentQuests.get(0).getQuestTasks().get(0).getRequiredQuantity();
            this.quest1TaskProgress = new Label(quest1TaskProgressText, skin);
            quest1TaskProgress.setStyle(style);
            quest1TaskProgress.setFontScale(1.6f);
            quest1TaskProgress.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 850),
                    (Gdx.graphics.getHeight() - background.getHeight() + 155));
            this.addActor(quest1TaskProgress);

            String quest1RewardsXPText = String.valueOf(currentQuests.get(0).getRewards().get(0).getXpReward());
            this.quest1RewardsXP = new Label(quest1RewardsXPText, skin);
            quest1RewardsXP.setStyle(style);
            quest1RewardsXP.setFontScale(1.6f);
            quest1RewardsXP.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 850),
                    (Gdx.graphics.getHeight() - background.getHeight() - 40));
            this.addActor(quest1RewardsXP);

            String itemID = currentQuests.get(0).getRewards().get(0).getItemTypeId();
            AbstractItem item = AbstractItem.createItemById(itemID);

            String quest1RewardsItemText;
            if (item != null) {
                quest1RewardsItemText = item.getItemName();
            } else {
                quest1RewardsItemText = currentQuests.get(0).getRewards().get(0).getItemTypeId();
            }

            this.quest1RewardsItem = new Label(quest1RewardsItemText, skin);
            quest1RewardsItem.setStyle(style);
            quest1RewardsItem.setFontScale(1.6f);
            quest1RewardsItem.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 280),
                    (Gdx.graphics.getHeight() - background.getHeight() - 95));
            this.addActor(quest1RewardsItem);

            String quest1RewardsItemQuantityText = String.valueOf(currentQuests.get(0).getRewards().get(0).getItemQuantity());
            this.quest1RewardsItemQuantity = new Label(quest1RewardsItemQuantityText, skin);
            quest1RewardsItemQuantity.setStyle(style);
            quest1RewardsItemQuantity.setFontScale(1.6f);
            quest1RewardsItemQuantity.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 850),
                    (Gdx.graphics.getHeight() - background.getHeight() - 95));
            this.addActor(quest1RewardsItemQuantity);
        }
    }

    /**
     * Creates selectable text for quests.
     */
    private void buildQuestText() {
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = this.newTimesRomanFontBold;

        currentQuests = manager.getCurrentQuests();
        if (!currentQuests.isEmpty()) {
            String quest1 = currentQuests.get(0).getName();
            this.quest1Name = new Label(quest1, skin);
            quest1Name.setStyle(style);
            quest1Name.setColor(Color.BLACK);
            quest1Name.setFontScale(1.6f);
            quest1Name.setPosition(180, 390);
            this.addActor(quest1Name);

            quest1Name.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (isMainQuestPressed || isSideQuestPressed) {
                        if (!isQuest2Pressed && !isQuest3Pressed) {
                            isQuest1Pressed = !isQuest1Pressed;
                        }
                    }
                }
            });
        }
    }

    /**
     * Sets image size by multiplying the height and width by a factor of 0.8.
     * @param image to set size of.
     */
    private void setImageSize(Image image) {
        image.setHeight((float) (image.getHeight() * 0.8));
        image.setWidth((float) (image.getWidth() * 0.8));
    }

    /**
     * Sets button size by multiplying the height and width by a factor of 0.8.
     * @param imageButton to set size of.
     */
    private void setButtonSize(ImageButton imageButton) {
        imageButton.setHeight((float) (imageButton.getHeight() * 0.8));
        imageButton.setWidth((float) (imageButton.getWidth() * 0.8));
    }

    /**
     * Public getter method to be used in TestWorld to stop tick whilst UI is displayed.
     * @return boolean on whether the quest UI is currently displayed or not.
     */
    public boolean getQuestInterfaceDisplayed() {
        return this.questInterfaceDisplayed;
    }

    /**
     * Closes the quest UI.
     */
    private void closeQuestsDisplayed() {
        this.questInterfaceDisplayed = false;
    }

    /**
     * Closes the 'in progress selected' image.
     */
    private void closeInProgressSelected() {
        this.isInProgressSelected = false;
    }

    /**
     * Closes the 'completed selected' image.
     */
    private void closeCompletedSelected() {
        this.isCompletedSelected = false;
    }

    private void drawQuests(Batch batch, float parentAlpha) {
        if (quest1Name != null) {
            this.quest1Name.draw(batch, parentAlpha);
        }
    }

    private void drawQuest1(Batch batch, float parentAlpha) {
        if (currentQuests != null) {
            this.quest1Title.draw(batch, parentAlpha);
            this.quest1Description.draw(batch, parentAlpha);
            this.quest1Task.draw(batch, parentAlpha);
            this.quest1TaskProgress.draw(batch, parentAlpha);
            this.rewards.draw(batch, parentAlpha);
            this.xp.draw(batch, parentAlpha);
            this.quest1RewardsXP.draw(batch, parentAlpha);
            this.quest1RewardsItem.draw(batch, parentAlpha);
            this.quest1RewardsItemQuantity.draw(batch, parentAlpha);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (this.questInterfaceDisplayed) {
            this.background.draw(batch, parentAlpha);

            if (this.isInProgressPressed) {
                this.isInProgressSelected = true;
            }
            if (this.isCompletedPressed) {
                this.isCompletedSelected = true;
            }
        }

        if (this.isInProgressSelected) {
            this.inProgressSelected.draw(batch, parentAlpha);
            if (this.isMainQuestPressed) {
                this.inProgressMainQuestSelected.draw(batch, parentAlpha);
                drawQuests(batch, parentAlpha);

                if (this.isQuest1Pressed) {
                    drawQuest1(batch, parentAlpha);
                }

            } else if (this.isSideQuestPressed) {
                this.inProgressSideQuestSelected.draw(batch, parentAlpha);
                drawQuests(batch, parentAlpha);

                if (this.isQuest1Pressed) {
                    drawQuest1(batch, parentAlpha);
                }
            }
        } else if (this.isCompletedSelected) {
            this.completedSelected.draw(batch, parentAlpha);
            if (this.isMainQuestPressed) {
                this.completedMainQuestSelected.draw(batch, parentAlpha);
                drawQuests(batch, parentAlpha);

                if (this.isQuest1Pressed) {
                    drawQuest1(batch, parentAlpha);
                }

            } else if (this.isSideQuestPressed) {
                this.completedSideQuestSelected.draw(batch, parentAlpha);
                drawQuests(batch, parentAlpha);

                if (this.isQuest1Pressed) {
                    drawQuest1(batch, parentAlpha);
                }
            }
        }
    }

    @Override
    public void notifyKeyDown(int keycode) {
        if (keycode == Input.Keys.Q) {
            if (this.questInterfaceDisplayed) {
                this.questInterfaceDisplayed = false;
                for (Actor actor : GameManager.get().getStage().getActors()) {
                    actor.setVisible(true);
                }
            } else {
                this.currentQuests = manager.getCurrentQuests();
                buildQuestText();
                buildText();

                this.questInterfaceDisplayed = true;
                for (Actor actor : GameManager.get().getStage().getActors()) {
                    if (actor instanceof QuestInterface) {
                        continue;
                    }
                    if (actor instanceof MenuMenu) {
                        continue;
                    }
                    actor.setVisible(false);
                }
            }
            closeInProgressSelected();
            closeCompletedSelected();
        }
    }
}