package deco2800.thomas.quest;

import java.util.List;

/**
 * {@inheritDoc} Subclass of a QuestTask with a primary goal to kill a certain amount of enemies
 */
public class KillQuestTask extends QuestTask {
    private final String enemyType;

    /**
     * Creates a QuestTask tracking the type of a killed enemy
     * @param enemyType Numerical id of an enemy type for a player to kill
     */
    public KillQuestTask(int taskID,
                         String description,
                         String location,
                         int requiredQuantity,
                         List<QuestTask> prerequisiteTasks,
                         int currentProgress,
                         String enemyType) {
        super(taskID, description, location, requiredQuantity, prerequisiteTasks, currentProgress);
        this.enemyType = enemyType;
    }

    /**
     *
     * @return Numerical id of an enemy type for a player to kill
     */
    public String getEnemyType() {
        return enemyType;
    }
}
