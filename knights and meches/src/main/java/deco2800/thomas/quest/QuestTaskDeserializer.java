package deco2800.thomas.quest;

import com.google.gson.*;

import java.lang.reflect.Type;

public class QuestTaskDeserializer implements JsonDeserializer<QuestTask> {

    @Override
    public QuestTask deserialize(JsonElement paramJsonElement, Type paramType,
                                 JsonDeserializationContext paramJsonDeserializationContext) throws JsonParseException {
        String taskType = paramJsonElement.getAsJsonObject().get("taskType").getAsString();
        String className = QuestTask.class.getPackageName() + "." + taskType;
        try {
            Object obj = new Gson().fromJson(paramJsonElement.getAsJsonObject(), Class.forName(className));
            if (obj instanceof QuestTask)
                return (QuestTask) obj;
            else
                return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

}
