package deco2800.thomas.quest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Represents a task in a quest. Each task is one simple action for player to execute a specific amount of times,
 * at a particular location to a particular part of a game world
 * e.g. kill an enemy, pick up an item, talk to an NPC e.t.c.
 */
public abstract class QuestTask {

    private final int taskID;
    private final String description;
    private final String location;
    private final int requiredQuantity;
    private List<QuestTask> prerequisiteTasks;
    private int currentProgress;
    /**
     * Attribute used for serialisation and deserialization to make sure we match the child class, not the parent
     */
    private final String taskType;

    /**
     * @param taskID Numeric id of a task
     * @param description Short description on what to do in a task e.g. "Kill 10 guards"
     * @param location Description of an in-game location to complete the task in
     * @param requiredQuantity Amount of actions to undertake to complete the task
     * @param prerequisiteTasks Tasks that have to be complete prior to displaying and completing this one
     * @param currentProgress Numerical progress of a task e.g. amount of items picked out of the required amount
     */
    public QuestTask(int taskID,
                     String description,
                     String location,
                     int requiredQuantity,
                     List<QuestTask> prerequisiteTasks,
                     int currentProgress) {

        this.taskID = taskID;
        this.description = description;
        this.location = location;
        this.requiredQuantity = requiredQuantity;
        this.prerequisiteTasks = Collections.unmodifiableList(Objects.requireNonNullElseGet(prerequisiteTasks, ArrayList::new));
        this.currentProgress = currentProgress;
        this.taskType = this.getClass().getSimpleName();
    }

    /**
     *
     * @return Numerical id of a current task
     */
    public int getTaskID() {
        return taskID;
    }

    /**
     *
     * @return Short description of this task's goal
     */
    public String getTaskDescription() {
        return description;
    }

    /**
     *
     * @return Amount of items required to complete the task
     */
    public Integer getRequiredQuantity() {
        return requiredQuantity;
    }

    /**
     *
     * @return Location at which the task is to be completed
     */
    public String getTaskLocation() {
        return location;
    }

    /**
     *
     * @return QuestTasks inside the same quest to complete before this one
     */
    public List<QuestTask> getTaskPrerequisites() {
        return prerequisiteTasks;
    }

    /**
     * Replaces the list of prerequisites
     * @param prerequisites List of quest tasks to complete before the current one
     */
    public void setTaskPrerequisites(ArrayList<QuestTask> prerequisites){
        this.prerequisiteTasks = Collections.unmodifiableList(prerequisites);
    }

    /**
     *
     * @return Amount of currently completed quest items
     */
    public int getCurrentProgress() {
        return currentProgress;
    }

    /**
     *
     * @return A boolean indicating whether prerequisites of this task has been complete
     */
    public boolean isAvailable() {
        if(prerequisiteTasks.isEmpty()) return true;
        for (QuestTask task: prerequisiteTasks){
            if(!task.isComplete())
                return false;
        }
        return true;
    }

    /**
     *
     * @return A boolean indicating whether this task is complete
     */
    public boolean isComplete() {
        return currentProgress == requiredQuantity;
    }

    /**
     * Increments the current progress by provided amount. Can be both positive and negative. The value of
     * current progress changes in a range from 0 to requiredQuantity
     * @param amount Amount to increment the current progress by. Can be both positive and negative.
     */
    public void incrementProgress(int amount) {
        if (currentProgress + amount > requiredQuantity)
            currentProgress = requiredQuantity;
        else if (currentProgress + amount < 0)
            currentProgress = 0;
        else
            currentProgress += amount;
    }
}
