package deco2800.thomas.quest;

/**
 * Data storage class representing a reward for a quest.
 * Reward includes XP and may or may not include items.
 */
public class QuestReward {
    private final int xpReward;
    private final String itemTypeId;
    private final int itemQuantity;

    /**
     * @param xpReward Amount of in-game experience gained by player for completing the quest
     * @param itemTypeId Numerical id of a reward item in game, might be null
     * @param itemQuantity Amount of items of the aforementioned type the player will receive on quest completion
     */
    public QuestReward(int xpReward, String itemTypeId, int itemQuantity) {
        this.xpReward = xpReward;
        this.itemTypeId = itemTypeId;
        this.itemQuantity = itemQuantity;
    }

    /**
     * @return Returns an amount of XP player should receive on quest completion
     */
    public int getXpReward() {
        return xpReward;
    }

    /**
     * @return Returns an item id for player to receive on quest completion or null if quest reward is XP only
     */
    public String getItemTypeId() {
        return itemTypeId;
    }

    /**
     * @return Returns the amount of item for player to receive on quest completion
     */
    public int getItemQuantity() {
        return itemQuantity;
    }
}
