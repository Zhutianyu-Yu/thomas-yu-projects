package deco2800.thomas.quest;

/**
 * Represents a requirement for a player to have a certain in-game level
 * or possess a certain amount of items to start the quest
 */
public class PlayerPrerequisite {
    private final int playerLevel;
    private final String itemTypeId;
    private final int itemQuantity;

    /**
     * @param playerLevel In-game level of a player
     * @param itemTypeId Numerical id of an item in-game. Accepts null for no requirement
     * @param itemQuantity Amount of aforementioned items required for a player to have to start a quest
     */
    public PlayerPrerequisite(int playerLevel, String itemTypeId, int itemQuantity) {

        this.playerLevel = playerLevel;
        this.itemTypeId = itemTypeId;
        this.itemQuantity = itemQuantity;
    }

    /**
     *
     * @return Minimum level of a player to accept the quest
     */
    public int getPlayerLevel() {
        return playerLevel;
    }

    /**
     *
     * @return Item id of an item required to start the quest or null if no such item is required
     */
    public String getItemTypeId() {
        return itemTypeId;
    }

    /**
     *
     * @return Amount of certain items required to start a certain quest
     */
    public int getItemQuantity() {
        return itemQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerPrerequisite that = (PlayerPrerequisite) o;

        if (playerLevel != that.playerLevel) return false;
        if (itemQuantity != that.itemQuantity) return false;
        return itemTypeId.equals(that.itemTypeId);
    }

    @Override
    public int hashCode() {
        return itemTypeId.hashCode() + playerLevel + itemQuantity;
    }
}
