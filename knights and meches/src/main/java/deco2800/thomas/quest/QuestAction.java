package deco2800.thomas.quest;

public final class QuestAction {
    public static final String Kill = "KILLACTION";
    public static final String PickUpItem = "PICKUPITEMACTION";
    public static final String DropItem = "DROPITEMACTION";
    public  static final String UseItem = "USEITEMACTION";
}
