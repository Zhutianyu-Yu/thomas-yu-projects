package deco2800.thomas.quest;

import java.util.List;

/**
 * {@inheritDoc} Subclass of a QuestTask with a primary goal to interact with a certain object in the game world
 * in a certain way
 */
public class ObjectInteractionQuestTask extends QuestTask {
    private final String action;
    private final String itemId;

    /**
     * Creates a QuestTask tracking the object to undertake an action over
     * @param itemId Id of an object in the game
     * @param action Action to undertake over said object
     */
    public ObjectInteractionQuestTask(int taskID,
                                      String description,
                                      String location,
                                      int requiredQuantity,
                                      List<QuestTask> prerequisiteTasks,
                                      int currentProgress,
                                      String itemId,
                                      String action) {
        super(taskID, description, location, requiredQuantity, prerequisiteTasks, currentProgress);
        this.itemId = itemId;
        this.action = action;
    }

    /**
     *
     * @return Numerical id of an object to undertake the quest action over
     */
    public String getItemId() {
        return itemId;
    }

    /**
     *
     * @return An action to undertake over the quest NPC
     */
    public String getAction() {
        return action;
    }
}
