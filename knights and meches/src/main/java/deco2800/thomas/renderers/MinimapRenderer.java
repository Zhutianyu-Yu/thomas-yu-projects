package deco2800.thomas.renderers;
import java.util.*;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import deco2800.thomas.entities.*;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.util.SquareVector;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.TextureManager;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.RoomWorld;
import deco2800.thomas.worlds.Wall;

/**
 * A renderer for the Minimap
 */
public class MinimapRenderer implements Renderer {

    private TextureManager textureManager = GameManager.getManagerFromInstance(TextureManager.class);
    private ShapeRenderer shapeRenderer;

    private float mapWidth;
    private float mapHeight;
    private float[] mapCoords;

    private static final float mapScale = 1.5f; // a constant to scale the map image size
    private static final float mapZoom = 10f;

    /**
     * Renders onto a batch, given a renderables with entities It is expected
     * that AbstractWorld contains some entities and a Map to read tiles from
     *
     * @param batch  Batch to render onto
     * @param camera The camera of the minimap
     */
    @Override
    public void render(SpriteBatch batch, OrthographicCamera camera) {

        if (shapeRenderer == null) {
            shapeRenderer = new ShapeRenderer();
        }

        // If we are in the npc world don't render anything
        if (!(GameManager.getWorld() instanceof RoomWorld)) {
            return;
        }

        // Get the current edge co-ords
        this.mapCoords = WorldUtil.minimapCoords(camera);
        Texture tex = textureManager.getTexture("minimap");
        batch.begin();
        this.mapWidth = mapScale * tex.getWidth() * WorldUtil.SCALE_X;
        this.mapHeight = mapScale * tex.getHeight() * WorldUtil.SCALE_Y;
        // draw the foundation for the minimap
        batch.draw(tex, mapCoords[0] - mapWidth / 2,
                mapCoords[1] - mapHeight / 2,
                mapWidth, mapHeight);

        // draw the boundaries of the minimap 
        //batch.draw(textureManager.getTexture("black_bg"), )


        renderMiniEntities(batch);
        batch.end();

    }


    /**
     * A method to render the entities onto the minimap
     *
     * @param batch  Batch to render onto
     */
    private void renderMiniEntities(SpriteBatch batch) {

        // Store the world information/minimap information
        int minimapImageBorder = 18;
        float left = (mapCoords[0] - this.mapWidth / 2 + minimapImageBorder);
        float bottom = (mapCoords[1] - this.mapHeight / 2 + minimapImageBorder);
        int diameter = (int) Math.min(this.mapHeight, this.mapWidth) - 2 * minimapImageBorder;
        float factor = diameter / 2f;

        float entityDiameter = diameter / mapZoom - 1; // 1 is arbitrary adjustment constant.

        float[] playerCoord = {GameManager.getWorld().getPlayerPeon().getCol(),
                GameManager.getWorld().getPlayerPeon().getRow()};

        float wallSize = entityDiameter/2 + 3;
        for (Wall wall : GameManager.getWorld().getWalls()) {
            float[] wallCoords = {wall.getCol(), wall.getRow()};
            wallCoords = calcCoord(wallCoords, playerCoord, left, factor, bottom);
            if (wallCoords[2] < mapZoom - 1) {
                batch.draw(textureManager.getTexture(wall.getTextureName()), wallCoords[0],
                        wallCoords[1], wallSize, wallSize);
            }
        }

        renderEntitiesOnMini(playerCoord, left, factor, bottom, batch, entityDiameter);
    }


    private void renderEntitiesOnMini(float[] playerCoord, float left, float
            factor, float bottom, SpriteBatch batch, float entityDiameter) {
        // Get all entities that are in the game
        List<AbstractEntity> entities = GameManager.getWorld().getSortedEntities();
        // Loop through the entities
        for (AbstractEntity entity : entities) {

            float[] entityInformation = getCoords(entity, playerCoord, left,
                    factor, bottom);

            if (entityInformation[2] > mapZoom - 1) {
                continue;
            }

            // What type of entity are we
            if (entity instanceof StaticEntity && ((StaticEntity) entity).getPartsConstruct()) {
                renderStaticEntity((StaticEntity) entity, batch, playerCoord, left, factor, bottom, entityDiameter);
            } else {
                LinkedList<Float> texDiameter = new LinkedList<>();
                texDiameter.addFirst(entityDiameter);
                Texture texture = renderKnownEntities(entity, texDiameter);
                batch.draw(texture, entityInformation[0],
                        entityInformation[1], texDiameter.getFirst(), texDiameter.getFirst());
            }
        }
    }

    private Texture renderKnownEntities(AbstractEntity entity, LinkedList<Float> texDiamater) {
        // Standard texture cases
        Texture texture = null;
        if (entity instanceof Tree) {
            texture = textureManager.getTexture("tree");
        } else if (entity instanceof Generator) {
            texture = textureManager.getTexture("rock");
        } else if (entity instanceof Wizard) {
            texture = textureManager.getTexture("miniWiz");
        } else if (entity instanceof PlayerPeon) {
            texture = textureManager.getTexture("miniMe");
        } else if (entity instanceof EnemyPeon) {
            texture = textureManager.getTexture("miniEnemy");
        } else {
            float entityDiameter = texDiamater.getFirst();
            texDiamater.removeFirst();
            texDiamater.addFirst(entityDiameter + mapScale);
            texture = renderOthers(entity);
        }
        return texture;
    }

    private Texture renderOthers(AbstractEntity entity) {
        // The zoomed texture cases
        Texture texture = null;
        if (entity instanceof Teleporter ||
                ((entity instanceof TraderNPC || entity instanceof TutorialNPC)
                        && entity.getTexture().startsWith("door"))) {
            texture = textureManager.getTexture("miniTeleport");
        } else if (entity instanceof OrbEntity || entity instanceof Chest || entity instanceof QuestActivator) {
            texture = textureManager.getTexture(entity.getTexture());
        }
        return texture;
    }

    private void renderStaticEntity(StaticEntity staticEntity, SpriteBatch batch,
            float[] playerCoord, float left, float factor, float bottom, float entityDiameter) {

        Map<SquareVector, String> children = staticEntity.getChildren();
        Set<SquareVector> posis = staticEntity.getChildrenPositions();

        for (SquareVector posi : posis) {
            String tex = children.get(posi);
            float [] entityInformation = calcCoord(new float[]{posi.getCol(), posi.getRow()},
                    playerCoord, left, factor, bottom);
            batch.draw(textureManager.getTexture(tex), entityInformation[0],
                    entityInformation[1], entityDiameter, entityDiameter);
        }
    }

    private float[] getCoords(AbstractEntity entity, float[] playerCoord,
            float left, float factor, float bottom) {


        float[] entityCoord = {entity.getCol(), entity.getRow()};

        return calcCoord(entityCoord, playerCoord, left, factor, bottom);

    }

    private float[] calcCoord(float[] entityCoord, float[] playerCoord, float left, float factor, float bottom) {

        double distance = Math.sqrt(Math.pow((entityCoord[0] - playerCoord[0]), 2)
                + Math.pow((entityCoord[1] - playerCoord[1]), 2));

        float miniEntityZoom = 0.1f;
        float[] entityWorldCoord = {(entityCoord[0] - playerCoord[0]) / mapZoom - miniEntityZoom,
                (entityCoord[1] - playerCoord[1]) / mapZoom - miniEntityZoom};
        float xPosition = left + factor + entityWorldCoord[0] * factor;
        float yPosition = bottom + factor + entityWorldCoord[1] * factor;

        return new float[]{xPosition, yPosition, (float) distance};
    }
}