package deco2800.thomas.moves;

import deco2800.thomas.entities.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MoveManager {

    /** Store all moves for peons to use*/
    private List<Move> moves = new ArrayList<>();

    /**
     * Private constructor so no class can make this static class an object
     */
    public MoveManager() {
        addDefaultMoves();
    }

    /**
     * Retrieves an instantiated move object by its string name
     * @param name name of the move
     * @return the move object if it exists, null otherwise
     */
    public Move getMove(String name) {
        for (Move move : moves) {
            if (move.getName().equals(name)) {
                return move;
            }
        }

        return null;
    }

    /**
     * Method to initialise all moves which are present within the game
     */
    public void addDefaultMoves() {
        moves.add(new Move("Sword Swing", PlayerStatistics.HEALTH,10, MoveType.PHYSICAL, 5));
        moves.add(new Move("Fireball", PlayerStatistics.HEALTH,15, MoveType.MAGICAL, 10));
        moves.add(new Move("Rocket Uppercut", PlayerStatistics.HEALTH,15, MoveType.PHYSICAL, 5));
        moves.add(new Move("Charge Beam", PlayerStatistics.HEALTH,20, MoveType.MAGICAL, 10));
        moves.add(new Move("Enemy Punch", PlayerStatistics.HEALTH, 60, MoveType.PHYSICAL, 0));
        moves.add(new Move("Enemy Blast", PlayerStatistics.HEALTH, 100, MoveType.MAGICAL, 10));
        moves.add(new Move("Boss Smash", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Boss Spell", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 100));
        moves.add(new Move("EMP", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 10));
        moves.add(new Move("Electrocute", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Helix Rocket", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Gun Blast", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Dissolve", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 10));
        moves.add(new Move("Core Crash", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Poison Wave", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 10));
        moves.add(new Move("Sting", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Zap", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 10));
        moves.add(new Move("Bandwidth Bash", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
        moves.add(new Move("Death Gaze", PlayerStatistics.HEALTH, 200, MoveType.MAGICAL, 10));
        moves.add(new Move("Stomp", PlayerStatistics.HEALTH, 200, MoveType.PHYSICAL, 0));
    }

    /**
     * Adds specified move to manager
     * @param move move to add to manager
     */
    public void addMove(Move move)
    {
        moves.add(move);
    }

    /**
     * Gets the moves currently in manager
     * @return list of manager's moves
     */
    public List<Move> getMoves()
    {
        return Collections.unmodifiableList(moves);
    }

    /**
     * Clears all moves from the manager
     */
    public void clearMoves()
    {
        moves.clear();
    }

    /**
     * Calculates the damage needed to be dealt to a target by a tacker
     * and reduces respective health and mana values from each object
     *
     * @param move the move which the attacker is using
     * @param attacker the attacker object
     * @param attacked the attacked object
     * @return true if method worked,false if any parameters are null or attacker
     * does not have enough mana
     */
    public boolean calcDamage(Move move, Attacker attacker, HasHealth attacked) {

        if (move == null || attacker == null || attacked == null) {
            return false;
        }

        int damage = move.getAmount() + attacker.getDamage();

        if (move.getType() == MoveType.PHYSICAL) {
            damage -= attacked.getArmour();
        }

        if (move.getManaCost() > attacker.getMana()) {
            return false;
        }

        if (move.getStatistics() == PlayerStatistics.HEALTH)
        {
            attacker.setMana(attacker.getMana() - move.getManaCost());
            attacked.setHealth(attacked.getHealth() - damage);
        }

        return true;
    }

}
