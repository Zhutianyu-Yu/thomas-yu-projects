package deco2800.thomas.moves;

public enum MoveType {
    PHYSICAL,
    MAGICAL,
    STATUS
}
