package deco2800.thomas.moves;

import deco2800.thomas.entities.PlayerStatistics;

/**
 * Object representing an attacker's moves
 */
public class Move {

    /** The name of the move*/
    private String name;

    /** Amount of damage the move can inflict*/
    private int amount;

    /** Whether the move will deal damage to health or mana*/
    private PlayerStatistics statistics;

    /** Whether the move is magical or physical*/
    private MoveType type;

    /** How much mana this move costs to use*/
    private int manaCost;

    /**
     * Constructor for move
     * @param name move's name
     * @param statistics what statistics move attacks
     * @param amount how much that statistic is impacted
     * @param type determines what type the attack is
     * @param manaCost determines the cost of the move
     */
    public Move(String name, PlayerStatistics statistics, int amount, MoveType type, int manaCost) {
        this.name = name;
        this.amount = amount;
        this.statistics = statistics;
        this.type = type;
        this.manaCost = manaCost;
    }

    /**
     * Constructor for move
     * @param name move's name
     * @param amount how much that statistic is impacted
     * @param manaCost determines the cost of the move
     */
    public Move(String name, int amount, int manaCost)
    {
        this.name = name;
        this.amount = amount;
        this.manaCost = manaCost;
        this.statistics = PlayerStatistics.HEALTH;
        this.type = MoveType.PHYSICAL;
    }

    /**
     * Gets the move's name
     * @return string representation of name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the move's attack amount
     * @return amount the attack inflicts
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Gets the statistic the move attacks
     * @return statistic of move
     */
    public PlayerStatistics getStatistics() {
        return statistics;
    }

    /**
     * Gets the type of the move
     * @return type of move
     */
    public MoveType getType() { return this.type; }

    /**
     * Gets the mana cost of move
     * @return move's mana cost
     */
    public int getManaCost() { return manaCost; }
}
