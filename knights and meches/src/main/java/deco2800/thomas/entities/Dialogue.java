package deco2800.thomas.entities;

import deco2800.thomas.worlds.Tile;

public class Dialogue extends Peon {

    private static final String ENTITY_ID_STRING = "staticEntityID";


    public Dialogue() {

        super();
        this.setTexture("ghost1");
    }

    public Dialogue(Tile tile, int renderOrder, String texture, boolean obstructed) {
        super(tile.getRow(), tile.getCol(), 0);
        this.setObjectName(ENTITY_ID_STRING);
        this.setTexture(texture);
    }
}
