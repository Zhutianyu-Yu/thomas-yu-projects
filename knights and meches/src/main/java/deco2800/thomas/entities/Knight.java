package deco2800.thomas.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Knight extends PlayerPeon implements HasHealth {

    private static final String knightTypeA = "typeA";
    private final ArrayList<String> walkingTextures = new ArrayList<>();
    private final String knighttype;
    private String defaultTexture = "Knight1";
    // What walking image we are showing
    private int walkingImgNum = 0;
    // Have we been moved once
    private boolean set = false;
    // The previous tick's position
    private int old;

    private final String[] types = new String[]{knightTypeA, "typeB"};

    /**
     * the class for the Knight, there are two (maybe more in future) Knight, which defined
     * by the argument knighttype.
     * <p>
     * the key different between those two Knights is that they have different skills.
     * <p>
     * Knight typeA get twice damage and do the attack to all enemies, then change back
     * <p>
     * Knight typeB can make all players in team get extra 15 armour in next two turn combat
     * (when the turn based combat system completed, this will work)
     *
     * @param row        row location of the Wizard
     * @param col        column location of the Wizard
     * @param speed      movement speed of the Wizard
     * @param knighttype type of the Wizard
     */
    public Knight(float row, float col, float speed, String knighttype) {
        super(row, col, speed);
        this.type = PlayerType.KNIGHT;

        if (knighttype.equals(knightTypeA) || knighttype.equals("typeB"))
            this.knighttype = knighttype;
        else
            this.knighttype = types[knighttype.hashCode() % 2];
        this.setTexture(defaultTexture);
        this.speed = 0.1f;
    }


    /**
     * Knight typeA get twice damage and do the attack to all enemies, then change to before
     * <p>
     * Knight typeB can make all players in team get extra 15 armour in next two turn combat
     * (when the turn based combat system completed, this will work)
     *
     * @param playerinTeam team members
     * @param teamEnemy    enemy tem
     */
    @Override
    public void skill(List<HasHealth> playerinTeam, List<HasHealth> teamEnemy) {
        int manaNeed = 0;
        if (this.knighttype.equals(knightTypeA)) {
            manaNeed = 90;
        } else {
            manaNeed = 50;
        }

        if (manaNeed <= this.getMana()) {
            if (this.knighttype.equals(knightTypeA)) {
                int initDam = this.getDamage();
                this.setDamage(this.getDamage() * 2);
                for (HasHealth playerEnemy : teamEnemy) {
                    this.doDamage(playerEnemy);
                }
                this.setDamage(initDam);
            } else {
                for (HasHealth teamPlayer : playerinTeam) {
                    teamPlayer.setArmour(teamPlayer.getArmour() + 15);
                }
            }
            this.addMana(0 - manaNeed);

        }
    }

    @Override
    public String getSkillInfor() {
        if (this.knighttype.equals(knightTypeA)) {
            return "Knight gets twice the damage \nand do the attack to all enemies, \nthen change to before";
        }
        return "Knight can make all \nplayers in the team get extra \n15 armour in the next two turn combat";
    }

    @Override
    public String getTypeDetail() {
        return this.knighttype;
    }

    @Override
    public void onTick(long i) {
        if (getTask() != null && getTask().isAlive()) {
            getTask().onTick(i);

            if (getTask().isComplete()) {
                this.setTexture(defaultTexture);
                setTask(null);
            }
        }

        // Have we moved at least once and are we in a different position
        // compared to the previous tick
        if (set && this.getPosition().hashCode() != old && !walkingTextures.isEmpty()) {
            switch (walkingImgNum) {
                case 0:
                    this.setTexture(walkingTextures.get(0));
                    walkingImgNum++;
                    break;
                case 1:
                    this.setTexture(walkingTextures.get(1));
                    walkingImgNum++;
                    break;
                case 2:
                    this.setTexture(walkingTextures.get(2));
                    walkingImgNum = 0;
                    break;
                default:
                    this.setTexture(defaultTexture);
                    break;
            }
        }
        // Store the current position
        old = this.getPosition().hashCode();
        // We have moved a position at least once
        set = true;

    }

    /**
     * set default texture of Knight character
     *
     * @param texture the texture that will be default for this character
     */
    public void setDefaultTexture(String texture) {
        this.defaultTexture = texture;
    }

    /**
     * the walking animations that will be used on tick for this character
     *
     * @param textureNames The names of the textures to be used for the animation seen in
     *                     TextureManager
     */
    public void setWalkingAnimations(String... textureNames) {
        this.walkingTextures.addAll(Arrays.asList(textureNames));
    }
}
