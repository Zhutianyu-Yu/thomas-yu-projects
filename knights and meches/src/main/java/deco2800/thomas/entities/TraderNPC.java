package deco2800.thomas.entities;

import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.tasks.StopMovementTask;
import deco2800.thomas.worlds.*;


public class TraderNPC extends Peon {

    private boolean found = false;
    private long prevTick = 0;
    private long currentTick = 0;
    private int doorNo = 0;
    private boolean down = false;
    private static final String ENTITY_ID_STRING = "traderNpcID";

    public TraderNPC(Tile position, String texture) {

        super(position.getRow(), position.getCol(), 0);
        this.setTexture(texture);
        this.setRenderOrder(RenderConstants.PEON_RENDER - 1);
    }

    public void found() {
        this.found = true;
    }

    public void gone() {
        this.found = false;
    }

    private void doorAnimation(long i) {
            if (this.down) {
                this.setTexture("door" + this.doorNo);
                this.doorNo--;
                if (this.doorNo < 0) {
                    try {
                        GameManager.getManagerFromInstance(SoundManager.class).stopSound("shortDoor.mp3");
                    } catch (NullPointerException ignored) {} // Ignore the null pointer exception
                    this.down = false;
                }
                prevTick = i;
            } else {
                this.doorNo = (this.doorNo + 1) % 5;
                this.setTexture("door" + this.doorNo);
                prevTick = i;
            }
    }

    @Override
    public void onTick(long i) {
        if (this.getTexture().substring(0, 4).equals("door")) {

            if (this.found) {
                this.down = false;
                if (this.doorNo == 0) {
                    GameManager.getManagerFromInstance(SoundManager.class).playSound("shortDoor.mp3");
                }
                if (currentTick - prevTick > 30 && this.doorNo != 4) {
                    doorAnimation(i);
                }
            } else if (this.doorNo == 4) {
                this.down = true;
                this.doorNo--;
            } else if (this.down && currentTick - prevTick > 30) {
                doorAnimation(i);
            } else {
                this.doorNo = 0;
            }
        }

        detectPlayer();

        currentTick++;
    }


    private void detectPlayer() {
        PlayerPeon mainPlayer = GameManager.getParty().getMainPlayer();
        double colDistance = this.getCol() - mainPlayer.getCol();
        double rowDistance = this.getRow() - mainPlayer.getRow();

        if (colDistance < 0.5 && colDistance > -0.5 && rowDistance < 0.5 &&
                rowDistance > -0.5) {
            GameManager.get().changeWorld(WorldTypes.TRADE);
            for (PlayerPeon player : GameManager.getParty().getPlayersList()) {
                player.setCol(player.getCol() - 1);
                player.setRow(player.getRow() - 1);
                player.setTask(new StopMovementTask(player));
            }
        }
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {

            return false;

        }

        if (!(other instanceof TraderNPC)) {

            return false;

        }

        TraderNPC otherTraderNPC = (TraderNPC) other;

        return this.getCol() == otherTraderNPC.getCol() && this.getRow() ==
                otherTraderNPC.getRow() && this.getHeight() == otherTraderNPC.getHeight();
    }


    /**
     * Gets the hashCode of the tree.
     *
     * @return the hashCode of the tree
     */
    @Override
    public int hashCode() {
        final float prime = 31;
        float result = 1;
        result = (result + super.getCol()) * prime;
        result = (result + super.getRow()) * prime;
        result = (result + super.getHeight()) * prime;
        return (int) result;
    }
}
