package deco2800.thomas.entities.combat;

/**
 * Representation of object enemy
 */
public class RobotEnemy extends EnemyPeon
{
    private static final String ENEMY_PHYSICAL = "EMP";
    private static final String ENEMY_MAGICAL = "Electrocute";

    /**
     * Creates a new Robot enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public RobotEnemy(int row, int column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.ROBOT;
        this.combatTexture = "robot-battle";
        this.setTexture("robot-world");
        this.setHealth(100);
    }
}
