package deco2800.thomas.entities.combat;

public class CPUEnemy extends EnemyPeon {

    private static final String ENEMY_PHYSICAL = "Bandwidth Bash";
    private static final String ENEMY_MAGICAL = "Zap";

    /**
     * Creates a new CPU enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public CPUEnemy(int row, int column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.CPU;
        this.combatTexture = "cpu-battle";
        this.setTexture("cpu-world");
        this.setHealth(200);
    }
}
