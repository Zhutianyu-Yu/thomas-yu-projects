package deco2800.thomas.entities.combat;

/**
 * Representation of Soldier enemy
 */
public class SoldierEnemy extends EnemyPeon
{
    private static final String ENEMY_PHYSICAL = "Gun Blast";
    private static final String ENEMY_MAGICAL = "Helix Rocket";

    /**
     * Creates a new Soldier enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public SoldierEnemy(int row, int column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.SOLDIER;
        this.combatTexture = "enemy-battle";
        this.setTexture("enemy-world");
        this.setHealth(100);
    }
}
