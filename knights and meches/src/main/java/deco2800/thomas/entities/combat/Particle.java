package deco2800.thomas.entities.combat;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import deco2800.thomas.Tickable;
import deco2800.thomas.entities.Peon;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.TextureManager;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.worlds.AbstractWorld;

public class Particle extends Peon implements Tickable {

    private static long lastParticleId = 0;
    private long particleId;
    private long tick = 0;
    private static final int HEIGHT = 50;
    private static final int WIDTH = 50;
    private static final String PATH = "particle";
    private float alpha;
    private float r;
    private float g;
    private float b;
    private int vertical;
    private int horizontal;
    private AbstractWorld world;

    /**
     * Create a particle that travels upwards
     *
     * @param row spawn row
     * @param col spawn col
     * @param speed movement speed
     * @param r RBG value of red
     * @param g RGB value of green
     * @param b RGB value of blue
     * @param world world this particle is present in
     */
    public Particle(float row, float col, float speed, float r, float g, float b, AbstractWorld world) {
        super(row, col, speed);
        setup(r,g,b,world,1,0);
    }

    /**
     * Create a particle that travels in a specified direction
     *
     *  @param row spawn row
     *  @param col spawn col
     *  @param speed movement speed
     *  @param r RBG value of red
     *  @param g RGB value of green
     *  @param b RGB value of blue
     *  @param world world this particle is present in
     * @param vertical -1, 0, or 1, if its moving down, not moving vertically or up
     * @param horizontal -1, 0 or 1, if its moving left, not moving horizontally or right
     */
    public Particle(float row, float col, float speed, float r, float g, float b, AbstractWorld world, int vertical, int horizontal) {
        super(row, col, speed);
        setup(r,g,b,world,vertical,horizontal);
    }

    /**
     * Setup to store correct member variables of a particle
     * @param r RBG value of red
     * @param g RGB value of green
     * @param b RGB value of blue
     * @param world world this particle is present in
     * @param vertical -1, 0, or 1, if its moving down, not moving vertically or up
     * @param horizontal -1, 0 or 1, if its moving left, not moving horizontally or right
     */
    private void setup(float r, float g, float b, AbstractWorld world, int vertical, int horizontal) {
        particleId = giveParticleId();
        alpha = 1f;
        this.r = r;
        this.g = g;
        this.b = b;
        this.world = world;
        this.vertical = vertical;
        this.horizontal = horizontal;
        updateTexture();
    }

    /**
     * Returns a unique id for a particle instance
     *
     * @return a unique id
     */
    private static long giveParticleId() {
        return ++lastParticleId;
    }

    /**
     * Updates the texture of the particle to the current values depicted by member variables
     */
    private void updateTexture() {
        Pixmap pmap = new Pixmap(WIDTH, HEIGHT, Pixmap.Format.RGBA8888);
        pmap.setBlending(Pixmap.Blending.None);
        pmap.setColor(r, g, b, alpha);
        pmap.fillCircle(25,25,15);
        Texture text = new Texture(pmap);
        GameManager.getManagerFromInstance(TextureManager.class).replaceTexture(PATH + particleId, text);
        this.setTexture(PATH + particleId);
        pmap.dispose();
    }

    /**
     * Removes the particle from everywhere it is stored
     */
    public void remove() {
        world.removeEntity(this);
        GameManager.getManagerFromInstance(TextureManager.class).removeTexture(PATH + particleId);
    }

    @Override
    public void onTick(long i) {

        tick++;

        this.moveTowards(new SquareVector(this.getCol() + 0.02f * horizontal,
                this.getRow() + 0.02f * vertical));

        if (tick % 5 == 0) {

            alpha -= 0.1f;

            if (alpha <= 0) {
                alpha = 0;
            }

            updateTexture();
        }

        if (alpha <= 0) {
            remove();
        }
    }
}
