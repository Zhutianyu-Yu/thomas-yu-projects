package deco2800.thomas.entities.combat;

public class SeekerEnemy extends EnemyPeon {

    private static final String ENEMY_PHYSICAL = "Stomp";
    private static final String ENEMY_MAGICAL = "Death Gaze";

    /**
     * Creates a new Seeker enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public SeekerEnemy(int row, int column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.SEEKER;
        this.combatTexture = "seeker-battle";
        this.setTexture("seeker-world");
        this.setHealth(200);
    }
}
