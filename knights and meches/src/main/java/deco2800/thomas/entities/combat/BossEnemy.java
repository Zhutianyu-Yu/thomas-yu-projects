package deco2800.thomas.entities.combat;

/**
 * Representation of Boss Enemy
 */
public class BossEnemy extends EnemyPeon
{
    private static final String ENEMY_PHYSICAL = "Boss Smash";
    private static final String ENEMY_MAGICAL = "Boss Spell";

    /**
     * Creates a new boss enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public BossEnemy(int row, int column, int speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.BOSS;
        this.combatTexture = "boss-battle";
        this.normalTexture = "boss-world";
        this.setTexture("boss-world");
        this.setHealth(100);
        this.canDetect = false;
    }
}
