package deco2800.thomas.entities.combat;
import deco2800.thomas.Tickable;
import deco2800.thomas.entities.*;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.QuestManager;
import deco2800.thomas.moves.Move;
import deco2800.thomas.quest.QuestAction;
import deco2800.thomas.tasks.AbstractTask;
import deco2800.thomas.tasks.MovementTask;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.AbstractWorld;
import deco2800.thomas.worlds.WorldTypes;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class EnemyPeon extends Peon implements Tickable, HasHealth, Attacker
{

    private static final int DEFAULT_HEIGHT = 1;
    private static final int DEFAULT_HEALTH = 200;
    private static final int DEFAULT_ARMOUR = 30;
    private static  final int DEFAULT_MANA = 30;
    private static final int DEFAULT_DAMAGE = 70;
    private static final int MAX_HEALTH = 200;
    private static final int MAX_MANA = 50;

    //Moves
    private static final String ENEMY_PHYSICAL = "Enemy Punch";
    private static final String ENEMY_MAGICAL = "Enemy Blast";

    /*Appearance in combat screen*/
    protected String combatTexture = "spacesuit_combat";
    protected String normalTexture = "spacesuit";
    /*Magical ability name*/
    protected String enemyMagical;
    /*Physical ability name*/
    protected String enemyPhysical;

    private transient AbstractTask task;
    //Stats
    private int health;
    private int armour;
    private int mana;
    private int damage;

    //Determine how far away from enemy spawn the enemy can move to
    private float spawnRow;
    private float spawnCol;
    private int roamRowRange;
    private int roamColRange;
    protected boolean canDetect = true;
    private boolean isMoving; //If the enemy should be moving or stationary
    protected EnemyType type = EnemyType.BASE;
    protected WorldTypes biome = WorldTypes.TEST;
    private HashMap<String, Move> moves;
    protected final org.slf4j.Logger logger =  LoggerFactory.getLogger(AbstractWorld.class);

    /**
     * Creates a new enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public EnemyPeon(float row, float column, float speed, int roamRowRange, int roamColRange,
                     String physical, String magical)
    {
        super(row, column, speed);
        setup(row, column, speed, roamRowRange, roamColRange, physical, magical);
    }

    /**
     * Creates a new enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public EnemyPeon(float row, float column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed);
        setup(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
    }

    /**
     * Sets up an enemy
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     * @param physical name of physical attack
     * @param magical name of magical attack
     */
    private void setup(float row, float column, float speed, int roamRowRange, int roamColRange,
                       String physical, String magical)
    {
        this.setTexture("spacesuit");
        this.setObjectName("enemyPeon");
        this.setHeight(DEFAULT_HEIGHT);
        this.health = DEFAULT_HEALTH;
        this.mana = DEFAULT_MANA;
        this.armour = DEFAULT_ARMOUR;
        this.damage = DEFAULT_DAMAGE;

        this.enemyMagical = magical;
        this.enemyPhysical = physical;

        moves = new HashMap<>();
        moves.put(physical, GameManager.get().getMoveManager().getMove(physical));
        moves.put(magical, GameManager.get().getMoveManager().getMove(magical));

        this.speed = speed;
        this.roamColRange = roamColRange;
        this.roamRowRange = roamRowRange;
        this.spawnRow = row;
        this.spawnCol = column;

        isMoving = speed > 0;
        setTask(null);
    }

    /**
     * Gets the type of enemy
     * @return type of enemy
     */
    public EnemyType getType()
    {
        return this.type;
    }

    /**
     * Gets the biome of the enemy.
     * NOTE: in future this will apply only cosmetic changes
     * @return the biome of the enemy
     */
    public WorldTypes getBiome()
    {
        return this.biome;
    }

    /**
     * Gets a string for simple comparison of types and biomes
     * @return String representing the EnemyPeon's type
     */
    public String getTypeIdentifier()
    {
        return this.type.name() + this.biome.name();
    }

    public void setTask(AbstractTask task)
    {
        this.task = task;
    }

    public AbstractTask getTask()
    {
        return task;
    }

    @Override
    public int getHealth()
    {
        return health;
    }

    @Override
    public int getMaxHealth() {
        return MAX_HEALTH;
    }

    @Override
    public void setHealth(int health)
    {
        this.health = health;

        if (health > MAX_HEALTH) {
            this.health = MAX_HEALTH;
        }

        if (health < 0) {
            this.health = 0;
            //Notify the quest manager that the enemy was killed
            QuestManager questManager = GameManager.get().getQuestManager();
            if(questManager != null)
                questManager.updateQuestStatus(this, QuestAction.Kill);
        }
    }

    public boolean getCanDetect() { return this.canDetect; }

    @Override
    public boolean aliveOrNot() {
        return this.health > 0;
    }

    @Override
    public int getArmour() {
        return armour;
    }

    @Override
    public void setArmour(int armour) {
        this.armour = armour;
    }


    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String[] getMoveNames() {
        return moves.keySet().toArray(new String[moves.size()]);
    }

    @Override
    public int getMaxMana() {
        return MAX_MANA;
    }

    @Override
    public int getMana() {
        return mana;
    }

    @Override
    public void setMana(int mana) {
        this.mana = mana;

        if (mana > MAX_MANA) {
            this.mana = MAX_MANA;
        }
    }

    @Override
    public void addAttack(String name)
    {
        if (!moves.containsKey(name))
        {
            moves.put(name, GameManager.get().getMoveManager().getMove(name));
            logger.debug(String.format("%s move added to Player%d", name, this.getEntityID()));
        }
        else
        {
            logger.debug(String.format("%s already in Player%d", name, this.getEntityID()));
        }
    }

    @Override
    public Map<String, Move> getMoves()
    {
        return moves;
    }

    /** Sets the texture of an enemy to be their combat version*/
    public void setCombatTexture() {
        this.setTexture(combatTexture);
    }

    /**
     * Sets the texture of an enenym in the overworld
     */
    public void setNormalTexture() { this.setTexture(normalTexture); }

    /**
     * Returns the name of the move an enemy will use to attack the player with
     *
     * @return name of the move to attack the player
     */
    public String chooseMove() {
        if (mana >= moves.get(enemyMagical).getManaCost()) {
            Random rand = new Random();
            int move = rand.nextInt(4);

            if (move == 0) {
                return enemyMagical;
            } else {
                return enemyPhysical;
            }
        } else {
            return enemyPhysical;
        }
    }

    /**
    If the enemy is currently supposed to move, this function picks random
    locations in the enemies range to roam around to.
     */
    private void roam() {


        if (isMoving) {

            //Choose a random location
            Random rand = new Random();
            float newRow = rand.nextInt(roamRowRange) + spawnRow;
            float newCol = rand.nextInt(roamColRange) + spawnCol;


            //Ensure the new position is valid
            if (!WorldUtil.isWalkable(newCol, newRow)) {
                return;
            }

            //Make the enemy move there
            setTask(new MovementTask(this, new SquareVector(newCol, newRow)));
        }
    }

    @Override
    public void onTick(long i)
    {
        //If the enemy doesn't have a task, let it roam around
        if (getTask() == null) {
            roam();
        }

        if(getTask() != null && getTask().isAlive()) {
            getTask().onTick(i);

            if (getTask().isComplete()) {
                setTask(null);
            }
        }
    }
}
