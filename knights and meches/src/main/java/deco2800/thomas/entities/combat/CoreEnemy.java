package deco2800.thomas.entities.combat;

/**
 * Representation of core enemy as object
 */
public class CoreEnemy extends EnemyPeon {

    private static final String ENEMY_PHYSICAL = "Dissolve";
    private static final String ENEMY_MAGICAL = "Core Crash";

    /**
     * Creates a new Core enemy object which default moves around the map
     *
     * @param row the spawn row of the enemy
     * @param column the spawn column of the enemy
     * @param speed the speed the enemy moves
     * @param roamRowRange how far across in rows the enemy can move from spawn
     * @param roamColRange how far across in columns the enemy can move from spawn
     */
    public CoreEnemy(int row, int column, float speed, int roamRowRange, int roamColRange)
    {
        super(row, column, speed, roamRowRange, roamColRange, ENEMY_PHYSICAL, ENEMY_MAGICAL);
        this.type = EnemyType.CORE;
        this.combatTexture = "core-battle";
        this.setTexture("core-world");
        this.setHealth(200);
    }
}
