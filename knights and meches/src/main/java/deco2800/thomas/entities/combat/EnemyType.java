package deco2800.thomas.entities.combat;

/**
 * Enum that represents the list of enemy types
 */
public enum EnemyType
{
    /*
    Normal base enemy type
     */
    BASE,
    /*
    Boss enemy
     */
    BOSS,
    /*
    Robot enemy type
     */
    ROBOT,
    /*
    Soldier enemy
     */
    SOLDIER,
    /*
    Core Enemy
     */
    CORE,
    /*
    Jellyfish enemy
     */
    JELLYFISH,
    /*
    CPU Enemy
     */
    CPU,
    /*
    Seeker enemy
     */
    SEEKER
}
