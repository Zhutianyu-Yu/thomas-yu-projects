package deco2800.thomas.entities.combat;

import deco2800.thomas.entities.RenderConstants;
import deco2800.thomas.entities.StaticEntity;
import deco2800.thomas.worlds.Tile;

public class Lilypad extends StaticEntity {

    /**
     * Creates a lilypad for the combat world which players and enemies can
     * stand on with a specific look
     *
     * @param tile the tile at which the lily pad will be located
     * @param texture the name of the texture of this lily pad instance
     */
    public Lilypad(Tile tile, String texture) {
        super(tile, RenderConstants.LILYPAD_RENDER, texture, true);
    }
}
