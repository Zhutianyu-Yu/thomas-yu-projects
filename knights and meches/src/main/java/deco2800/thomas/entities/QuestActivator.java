package deco2800.thomas.entities;

import deco2800.thomas.Tickable;
import deco2800.thomas.worlds.Tile;

public class QuestActivator extends StaticEntity implements Tickable {
    private final String questName;
    public QuestActivator(Tile t, String texture, String questName) {
        super(t, RenderConstants.TELEPORTER_RENDER, texture, false);
        this.setTexture(texture);
        this.questName = questName;
    }

    @Override
    public void onTick(long i) {

    }

    public String getQuestName() {
        return questName;
    }
}
