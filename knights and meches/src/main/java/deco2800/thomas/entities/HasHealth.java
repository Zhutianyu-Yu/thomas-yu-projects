package deco2800.thomas.entities;

public interface HasHealth {

    /**
     * Get the health of the entity
     *
     * @return the HP value of the entity
     */
    int getHealth();

    /**
     * Update the health of the entity
     *
     * @param health New HP value of the entity
     */
    void setHealth(int health);

    /**
     * show the entity is alive or not
     *
     * @return true if the HP of the entity is greater than 0, otherwise false
     */
    boolean aliveOrNot();

    /**
     * Get the armour of the entity
     *
     * @return the value of the armour
     */
    int getArmour();


    /**
     * Update the armour of the entity
     *
     * @param armour New armour of the entity
     */
    void setArmour(int armour);

    /**
     * Gets the entity's maximum health
     * @return entity's maximum health
     */
    int getMaxHealth();


}