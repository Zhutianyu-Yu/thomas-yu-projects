package deco2800.thomas.entities;


import deco2800.thomas.worlds.Tile;

/**
 * A class for the Orb Entity
 */
public class OrbEntity extends StaticEntity {

    public OrbEntity(Tile t, String texture) {
        super(t, RenderConstants.ORB_RENDER, texture, false);
        this.setTexture(texture);
    }
}
