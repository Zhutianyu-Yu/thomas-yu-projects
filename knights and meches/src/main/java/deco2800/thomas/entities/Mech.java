package deco2800.thomas.entities;

import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.moves.Move;
import deco2800.thomas.worlds.AbstractWorld;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mech extends PlayerPeon implements HasHealth, Attacker {
    private static final int MAX_MANA = 200;
    private static final int MAX_HEALTH = 2000;
    private static final String DEFAULT_PHYSICAL_ATTACK = "Rocket Uppercut";
    private static final String DEFAULT_MAGICAL_ATTACK = "Charge Beam";
    protected final org.slf4j.Logger logger = LoggerFactory.getLogger(AbstractWorld.class);
    private Wizard mechWizard = null;
    private Knight mechKnight = null;
    private final String defaultTexture = "mech1";
    private int hp;
    private int damage;
    private int armour;
    private int mana;
    private final int mechtype;
    private final HashMap<String, Move> attacks;

    /**
     * Mech Constructor. Mech is initiated with values based on the Wizard and the Knight it is
     * housing
     */
    public Mech(float row, float col, float speed, Wizard wizard, Knight knight) {
        super(row, col, speed);
        this.type = PlayerType.MECH;
        this.mechWizard = wizard;
        this.mechKnight = knight;
        this.mechtype = (wizard.getTypeDetail().hashCode() + knight.getTypeDetail().hashCode()) % 4;
        double ratio = getFunctionRatio(wizard, knight);

        this.damage = wizard.getDamage() + knight.getDamage();
        this.damage = (mechtype == 0 || mechtype == 2) ? (int) (this.damage * (1 + ratio)) : (int) (this.damage * (1 - ratio));
        this.armour = wizard.getArmour() + knight.getArmour();
        this.armour = (mechtype == 1 || mechtype == 3) ? (int) (this.armour * (1 + ratio)) : (int) (this.armour * (1 - ratio));
        this.hp = wizard.getHP() + knight.getHP();
        this.hp = (mechtype == 0 || mechtype == 1) ? (int) (this.hp * (1 + ratio)) : (int) (this.hp * (1 - ratio));
        this.hp = Math.min(this.hp, MAX_HEALTH);
        this.mana = wizard.getMana() + knight.getMana();
        this.mana = (mechtype == 2 || mechtype == 3) ? (int) (this.mana * (1 + ratio)) : (int) (this.mana * (1 - ratio));

        attacks = new HashMap<>();
        attacks.put(DEFAULT_PHYSICAL_ATTACK, GameManager.get().getMoveManager().getMove(DEFAULT_PHYSICAL_ATTACK));
        attacks.put(DEFAULT_MAGICAL_ATTACK, GameManager.get().getMoveManager().getMove(DEFAULT_MAGICAL_ATTACK));

        this.setObjectName("mech");
        this.setTexture(defaultTexture);

        this.setHeight(1);
    }

    /**
     * used for determining statistic values of the mech according ot the wizard and knight
     *
     * @param wizard the wizard that is stored in the mech
     * @param knight the Knight that is stored in the mech
     * @return double which is the ratio used for relevant statistics
     */
    public double getFunctionRatio(Wizard wizard, Knight knight) {
        int uniValue = wizard.getTypeDetail().hashCode() + knight.getTypeDetail().hashCode();
        int one = uniValue % 3;
        int two = uniValue % 10;
        return (double) (two + one * 10) / 100;
    }

    @Override
    public void addAttack(String name) {
        if (!attacks.containsKey(name)) {
            attacks.put(name, GameManager.get().getMoveManager().getMove(name));
            LOGGER.debug(String.format("%s move added to Mech%d", name, this.getEntityID()));
        } else {
            LOGGER.debug(String.format("%s already in Mech%d", name, this.getEntityID()));
        }
    }

    /**
     * This method changes the MechKnight, if the mech already contains the wizard being added, do
     * nothing
     *
     * @param wizard Wizard that is changed into the mech
     */
    public void changeMechWizard(Wizard wizard) {
        if (this.mechWizard != wizard) {
            this.mechWizard = wizard;
        }
    }

    /**
     * This method changes the MechKnight, if the mech already contains the wizard being added do
     * nothing
     *
     * @param knight Knight that is changed into the mech
     */
    public void changeMechKnight(Knight knight) {
        if (this.mechKnight != knight) {
            this.mechKnight = knight;
        }
    }

    /**
     * get the Wizard stored in the mech
     *
     * @return the Wizard that is currently stored in the mech
     */
    public Wizard getMechWizard() {
        return this.mechWizard;
    }

    /**
     * get the Knight stored in the mech
     *
     * @return the Knight that is currently stored in the Mech
     */
    public Knight getMechKnight() {
        return this.mechKnight;
    }

    /**
     * get the type of mech
     *
     * @return
     */
    public int getMechType() {
        return this.mechtype;
    }

    /**
     * change the mech texture according to the type of mech
     */
    public void changeMechTexture() {
        switch (this.mechtype) {
            case 0:
                setTexture(defaultTexture);
                break;
            case 1:
                setTexture("mech2");
                break;
            case 2:
                setTexture("mech3");
                break;
            case 3:
                setTexture("mech4");
                break;
            default:
                setTexture(defaultTexture);
        }

    }

    /**
     * add mana to the mech
     *
     * @param mana int value which is the amount of mana to be added to the mech mana pool
     */
    public void addMana(int mana) {
        this.mana += mana;
        if (this.mana >= MAX_MANA) {
            this.mana = MAX_MANA;
        }

    }

    @Override
    public int getHealth() {
        return this.hp;
    }

    @Override
    public void setHealth(int health) {
        this.hp = health;
        if (hp > this.getMaxHealth()) {
            hp = this.getMaxHealth();
        }

        if (hp < 0) {
            hp = 0;
        }
    }

    @Override
    public int getMaxMana() {
        return MAX_MANA;
    }

    @Override
    public int getMaxHealth() {
        return MAX_HEALTH;
    }

    @Override
    public boolean aliveOrNot() {
        return this.hp > 0;
    }

    @Override
    public int getArmour() {

        int equipmentArmour = 0;
        if (GameManager.getManagerFromInstance(InputManager.class).getMech() != null) {
            equipmentArmour = GameManager.getManagerFromInstance(InputManager.class).getMech().getArmour();
        }


        return this.armour + equipmentArmour;
    }

    @Override
    public void setArmour(int armour) {
        this.armour = armour;
    }

    @Override
    public void doDamage(HasHealth target) {
        int healthLeft = target.getHealth() - this.damage + target.getArmour();
        healthLeft = Math.max(healthLeft, 0);
        target.setHealth(healthLeft);
        this.addMana(10);
    }

    @Override
    public int getDamage() {
        int equipmentDamage = 0;
        if (GameManager.getManagerFromInstance(InputManager.class).getMech() != null)
            equipmentDamage = GameManager.getManagerFromInstance(InputManager.class).getMech().getDamage();
        return this.damage + equipmentDamage;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public Map<String, Move> getMoves() {
        return attacks;
    }

    @Override
    public String[] getMoveNames() {
        return attacks.keySet().toArray(new String[attacks.size()]);
    }

    @Override
    public int getMana() {
        return this.mana;
    }

    @Override
    public void setMana(int mana) {
        this.mana = mana;
    }

    @Override
    public void skill(List<HasHealth> playerinTeam, List<HasHealth> teamEnemy) {
        throw new UnsupportedOperationException();
    }
}
