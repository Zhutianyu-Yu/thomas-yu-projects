package deco2800.thomas.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import deco2800.thomas.GameScreen;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.items.ItemUI.BoxUi;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;

import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.moves.Move;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.tasks.Direction;
import deco2800.thomas.tasks.MovementTask;
import deco2800.thomas.tasks.StopMovementTask;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.AbstractWorld;
import deco2800.thomas.worlds.CombatWorld;
import deco2800.thomas.worlds.RoomWorld;
import deco2800.thomas.worlds.WorldTypes;
import deco2800.thomas.worlds.rooms.TraderWorld;
import deco2800.thomas.worlds.staticworlds.GhostWorld;
import deco2800.thomas.worlds.staticworlds.OrbWorld;
import deco2800.thomas.worlds.staticworlds.TutorialWorld;

import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerPeon extends Peon implements TouchDownObserver, HasHealth, Attacker, KeyDownObserver, KeyUpObserver {

    /**
     * some default value for user information
     */
    private static final int DEFAULT_DAMAGE = 70;
    private static final int DEFAULT_RANGE = 4;
    private static final int DEFAULT_HP = 600;
    private static final int DEFAULT_ARMOUR = 30;
    private static final int DEFAULT_MANA = 20;
    private static final int DEFAULT_XP_LIMITED = 50;
    private static final int MAX_MANA = 100;

    private String check = "off";

    private static final Map<Integer, Direction> KEY_TO_DIRECTION;

    static {
        Map<Integer, Direction> map = new HashMap<>();
        map.put(Input.Keys.W, Direction.UP);
        map.put(Input.Keys.S, Direction.DOWN);
        map.put(Input.Keys.A, Direction.LEFT);
        map.put(Input.Keys.D, Direction.RIGHT);
        KEY_TO_DIRECTION = Collections.unmodifiableMap(map);
    }

    protected final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AbstractWorld.class);
    protected PlayerType type = PlayerType.BASE;
    private final HashMap<String, Move> attacks;
    // What image are we showing
    private boolean odd = false;
    // Have we been moved once
    private boolean set = false;
    // The previous tick's position
    private int old;
    private final int range;
    private int hp;
    private int xp;
    private int level;
    private int damage;
    private int armour;
    private int mana;
    private final SquareVector initialPosition;
    private final String defaultTexture = "spacman_green";

    /**
     * class for playerPeon
     * @param row location
     * @param col location
     * @param speed speed
     */
    public PlayerPeon(float row, float col, float speed) {
        super(row, col, speed);
        this.damage = DEFAULT_DAMAGE;
        this.range = DEFAULT_RANGE;
        this.hp = DEFAULT_HP;
        this.xp = 0;
        this.level = 0;
        this.armour = DEFAULT_ARMOUR;
        this.mana = DEFAULT_MANA;
        this.initialPosition = new SquareVector(col, row);

        attacks = new HashMap<>();
        attacks.put("Sword Swing", GameManager.get().getMoveManager().getMove("Sword Swing"));
        attacks.put("Fireball", GameManager.get().getMoveManager().getMove("Fireball"));

        this.setObjectName("playerPeon");
        this.setTexture(defaultTexture);
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListener(this);
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
        GameManager.getManagerFromInstance(InputManager.class).addKeyUpListener(this);
    }

    /**
     * help log
     * @param name name of attack
     */
    @Override
    public void addAttack(String name) {
        if (!attacks.containsKey(name)) {
            attacks.put(name, GameManager.get().getMoveManager().getMove(name));
            LOGGER.debug(String.format("%s move added to Player%d", name, this.getEntityID()));
        } else {
            LOGGER.debug(String.format("%s already in Player%d", name, this.getEntityID()));
        }
    }


    /**
     *
     * @return xp
     */
    public int getXP() {
        return this.xp;
    }


    /**
     *
     * @return hp
     */
    public int getHP() {
        return this.hp;
    }

    /**
     * set hp
     * @param HP
     */
    public void setHP(int HP) {
        this.hp = HP;
    }

    /**
     * return the level of the player
     * @return
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * return the armour of the player
     * @return
     */
    public int getArmour() {
        return this.armour;
    }

    /**
     * set the armour of the player
     * @param armourChange
     */
    public void setArmour(int armourChange) {
        this.armour = armourChange;
    }

    /**
     * get the mana of the player
     * @return
     */
    @Override
    public int getMana() {
        return this.mana;
    }

    /**
     * set the mana of the player
     * @param mana the mana to be set
     */
    @Override
    public void setMana(int mana) {
        this.mana = mana;
    }

    /**
     * get the max mana of the player
     * @return the max mana
     */
    @Override
    public int getMaxMana() {
        return MAX_MANA;
    }

    /**
     * return the move name
     * @return move name
     */
    @Override
    public String[] getMoveNames() {
        return attacks.keySet().toArray(new String[attacks.size()]);
    }

    @Override
    public Map<String, Move> getMoves() {
        return attacks;
    }

    /**
     * get damage of the player
     * @return damage of the player
     */
    @Override
    public int getDamage() {
        return this.damage;
    }

    /**
     * set the damage of the player
     * @param damage new damage value
     */
    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    public SquareVector getInitialPosition() {
        return this.initialPosition;
    }

    /**
     * add mana to the player, when reach max, no more addtion
     * @param mana mana value
     */
    public void addMana(int mana) {
        this.mana += mana;
        if (this.mana >= MAX_MANA) {
            this.mana = MAX_MANA;
        }

    }


    /**
     * add xp to the player, when reach the max value of this level, the player will level up
     * @param XP xp value
     */
    public void addXP(int XP) {
        this.xp += XP;
        if (this.xp >= this.getLevel() * 20 + DEFAULT_XP_LIMITED) {
            this.levelUp();
        }
    }

    /**
     * do skill, will be override in other class
     * @param playerinTeam
     * @param enemy
     */
    public void skill(List<HasHealth> playerinTeam, List<HasHealth> enemy) {
        return;
    }

    /**
     * the player level up will gte more armour, damage, hp and set xp 0
     */
    public void levelUp() {
        this.level++;
        this.armour = this.level * 4 + DEFAULT_ARMOUR;
        this.damage = this.level * 9 + DEFAULT_DAMAGE;
        this.hp = this.level * 70 + DEFAULT_HP;
        this.xp = 0;

    }

    /**
     *
     * @return the hp value
     */
    @Override
    public int getHealth() {
        return this.hp;
    }

    /**
     * set the hp value
     * @param health New HP value of the entity
     */
    @Override
    public void setHealth(int health) {
        this.hp = health;
        if (hp > this.getMaxHealth()) {
            hp = this.getMaxHealth();
        }
        if (hp < 0) {
            hp = 0;
        }
    }

    /**
     *
     * @return the max health of the player
     */
    @Override
    public int getMaxHealth() {
        return this.level * 70 + DEFAULT_HP;
    }

    /**
     * tell the player is dead or not
     * @return
     */
    @Override
    public boolean aliveOrNot() {
        return this.hp > 0;
    }

    /**
     * do damage to other entity
     * @param attackedEntity
     */
    public void doDamage(HasHealth attackedEntity) {
        int healthLeft = attackedEntity.getHealth() - this.damage + attackedEntity.getArmour();
        healthLeft = Math.max(healthLeft, 0);
        attackedEntity.setHealth(healthLeft);
        this.addMana(10);
    }

    /**
     *
     * @return the type of the player
     */
    public PlayerType getType() {
        return this.type;
    }

    /**
     * set the type of the player
     * @param type
     */
    public void setType(PlayerType type) {
        this.type = type;
    }

    /**
     * get the type detail of the player
     * @return
     */
    public String getTypeDetail() {
        return null;
    }

    /**
     * get the skill infor
     * @return
     */
    public String getSkillInfor() {
        return null;
    }


    @Override
    public void onTick(long i) {
        if (getTask() != null && getTask().isAlive()) {
            getTask().onTick(i);

            if (getTask().isComplete()) {
                this.setTexture(defaultTexture);
                setTask(null);
            }
        }

        // Have we moved at least once and are we in a different position
        // compared to the previous tick
        if (set && this.getPosition().hashCode() != old) {
            // Which image should we show
            if (odd) {
                this.setTexture(defaultTexture);
                odd = false;

            } else {
                this.setTexture("spacman_blue");
                odd = true;
            }
        }
        // Store the current position
        old = this.getPosition().hashCode();
        // We have moved a position at least once
        set = true;

    }


    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

        if (button == Input.Buttons.LEFT) {

            // Are we in any of the NPC worlds
            if (GameManager.getWorld() instanceof TutorialWorld) {

                // go to the next image
                ((TutorialWorld) GameManager.getWorld()).nextTute();
            } else if (GameManager.getWorld() instanceof TraderWorld) {
                // go to the next image
                ((TraderWorld) GameManager.getWorld()).nextTrade();
            } else if (GameManager.getWorld() instanceof OrbWorld) {
                // back to the test world
//                ((OrbWorld) GameManager.get().getWorld()).backToTest();
            } else if (GameManager.getWorld() instanceof GhostWorld) {
                return;
            } else if (!(GameManager.getWorld() instanceof CombatWorld)) {

                float[] mouse = WorldUtil.screenToWorldCoordinates(Gdx.input.getX(), Gdx.input.getY());
                float[] clickedPosition = WorldUtil.worldCoordinatesToColRow(mouse[0], mouse[1]);

                System.out.printf("mouse: %.2f %.2f%n", mouse[0], mouse[1]);
                System.out.printf("clickedPosition: %.2f %.2f%n", clickedPosition[0], clickedPosition[1]);
                setTask(new MovementTask(this, new SquareVector(clickedPosition[0], clickedPosition[1])));

                LOGGER.debug(String.format("mouse: %.2f %.2f%n", mouse[0], mouse[1]));
                LOGGER.debug(String.format("clickedPosition: %.2f %.2f%n", clickedPosition[0], clickedPosition[1]));
                setTask(new MovementTask(this, new SquareVector(clickedPosition[0], clickedPosition[1])));
            }

        } else if (button == Input.Buttons.RIGHT) {

            //Do not allow right clicks to occur during combat
            if (GameManager.get().getWorld() instanceof CombatWorld) {
                return;
            }

            List<AbstractEntity> entities = WorldUtil.screenToEntities(Gdx.input.getX(), Gdx.input.getY());
            for (AbstractEntity entity : entities) {
                if (entity.equals(this)) continue;

                if (entity instanceof EnemyPeon && inRange(entity.position) && ((EnemyPeon) entity).getCanDetect()) {
                    LOGGER.info("Player spotted enemy");
                    WorldUtil.createCombatWorld((EnemyPeon) entity, this);
                }

                // Go to the npc world if we are in range
                if (entity instanceof TutorialNPC && inRange(entity.position)) {

                    GameManager.get().changeWorld(WorldTypes.TUTORIAL);
                }

                if (entity instanceof TraderNPC && inRange(entity.position)) {

                    GameManager.get().changeWorld(WorldTypes.TRADE);
                }

                // Player right clicks and gets the orb
                if (entity instanceof OrbEntity && inRange(entity.position)) {
                    GameManager.get().setCurrentOrbEntity((OrbEntity) entity);
                    BossEnemy boss = GameManager.getWorld().getBoss(entity.getRow(), entity.getCol());
                    WorldUtil.createCombatWorld(boss, this);
                }

                // Open the chest
                if (entity instanceof Chest && inRange(entity.position)) {
                    GameManager.getWorld().removeEntity(entity);
                    if (entity.getTexture().equals("chest_close")) {
                        // Code for opening chest UI should be here...
                        double random = Math.floor(Math.random() * 10);
                        if (random < 5) {
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame2().createMiniGame2((Chest) entity);
                        } else {
                            GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getMiniGame().createMiniGame((Chest) entity);
                        }
                    }
                    ((RoomWorld) GameManager.get().getWorld()).addChest(entity.getCol(), entity.getRow(), "chest_open");
                }
            }
        }

    }

    @Override
    public void notifyKeyDown(int keycode) {
        // WASD controls to move the player
        if (KEY_TO_DIRECTION.containsKey(keycode)) {
            if (GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getInventoryDisplayed()) {
                return;
            }
            setTask(new MovementTask(this, KEY_TO_DIRECTION.getOrDefault(keycode, Direction.NONE)));
        } else {
            processKeyDown(keycode);
        }

    }

    private void processKeyDown(int keycode) {
        if (keycode == Input.Keys.SHIFT_LEFT) { // start running
            // If we are pressing left shift start running
            startRun();
        } else if (keycode == Input.Keys.ALT_LEFT) { // start sneaking
            // If we are holding left ALT then walk slowly
            startSneak();

        }

    }


    @Override
    public void notifyKeyUp(int keycode) {

        if (KEY_TO_DIRECTION.containsKey(keycode)) {
            var task = getTask();
            if (GameManager.getManagerFromInstance(ScreenManager.class).getCurrentScreen().getInventoryDisplayed()) {
                return;
            }
            if (task instanceof MovementTask && ((MovementTask) task).getDirection() == KEY_TO_DIRECTION.get(keycode)) {
                // The key released is of the direction we are currently moving.
                setTask(new StopMovementTask(this));
            }
        } else if (keycode == Input.Keys.SHIFT_LEFT) {
            // If we have released shift then end the run
            endRun();
        } else if (keycode == Input.Keys.ALT_LEFT) {

            // If we go of left alt then stop crawling
            endSneak();

        }
    }

    private boolean inRange(SquareVector vector) {
        float row = vector.getRow();
        float column = vector.getCol();

        int verticalDistance = (int) Math.abs(row - getRow());
        int horizontalDistance = (int) Math.abs(column - getCol());

        return verticalDistance <= range && horizontalDistance <= range;
    }


    public void stopMovement() {
        this.setTask(new StopMovementTask(this));
    }


}
