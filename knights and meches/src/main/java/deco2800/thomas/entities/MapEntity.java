package deco2800.thomas.entities;

import deco2800.thomas.Tickable;
import deco2800.thomas.worlds.Tile;

public class MapEntity extends StaticEntity implements Tickable {
    public MapEntity(Tile t, String texture) {
        super(t, 1, texture, t.isObstructed());
        this.setTexture(texture);
    }
}
