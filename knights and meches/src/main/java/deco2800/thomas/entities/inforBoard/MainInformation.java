package deco2800.thomas.entities.inforBoard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.entities.HasHealth;
import deco2800.thomas.entities.Mech;
import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.entities.PlayerType;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.observers.KeyDownObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for the player information board
 * When pressing B, it will appear on the screen
 */
public class MainInformation extends Group implements KeyDownObserver {

    /**
     * the font help display the text
     */
    private final BitmapFont font = new BitmapFont();
    /**
     * store all the meches
     */
    private final ArrayList<Mech> allMechs = new ArrayList<>();
    /**
     * store all the players
     */
    private final ArrayList<PlayerPeon> allPlayers = new ArrayList<>();
    /**
     * The images to display
     */
    private final List<Image> image;
    /**
     * The images to display
     */
    private final List<ImageButton> imageButton;
    /**
     * The image of HPto display
     */
    private Image imageHP;
    /**
     * This player's information will display
     */
    private PlayerPeon playerPeon;
    /**
     * This mech's information will display
     */
    private Mech mech;
    /**
     * check the board is shown or not
     */
    private boolean lastClick;
    /**
     * show the mech or player
     */
    private int mechOrPlayer;

    /**
     * object of maininformation board
     */
    public MainInformation() {
        this.lastClick = false;
        getAllPlayer();
        playerPeon = GameManager.getParty().getMainPlayer();// display the first player
        mech = GameManager.getInGameMech();// display the first player
        this.image = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            this.image.add(new Image());
        }
        this.imageButton = new ArrayList<>();

        //add the key down control to the input Manager
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     * get all players in the world and put them in a new list and return
     *
     * @return all players in this word
     */
    public void getAllPlayer() {
        allPlayers.addAll(GameManager.get().getPlayersInGame());
    }

    /**
     * draw the large image in the information board
     *
     * @param filePath
     */
    private void drawPic(String filePath) {
        image.add(1, new Image(new Texture(Gdx.files.internal(filePath))));
        image.get(1).setPosition((float) (Gdx.graphics.getWidth() * 0.26), (float) (Gdx.graphics.getHeight() * 0.34));
        image.get(1).setSize((float) (Gdx.graphics.getWidth() * 0.18), (float) (Gdx.graphics.getHeight() * 0.3));
        this.addActor(image.get(1));
    }

    /**
     * draw the thumbnails image of the player or mech
     *
     * @param filePath
     * @param who      the type of the image
     * @param player   draw player
     * @param newMech  draw mech
     * @param x        x location
     * @param y        y location
     */
    private void drawThumPic(String filePath, String who, PlayerPeon player,
                             Mech newMech, double x, double y, int which) {
        this.imageButton.add(which - 2,
                new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal(filePath))),
                        new TextureRegionDrawable(new Texture(Gdx.files.internal(filePath))),
                        new TextureRegionDrawable(new Texture(Gdx.files.internal(filePath)))));
        this.imageButton.get(which - 2).setPosition((float) (Gdx.graphics.getWidth() * x),
                (float) (Gdx.graphics.getHeight() * y));
        this.imageButton.get(which - 2).setSize((float) (Gdx.graphics.getWidth() * 0.07),
                (float) (Gdx.graphics.getHeight() * 0.09));
        //click the ImagButton can change the player and show different information
        this.imageButton.get(which - 2).addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                image.get(1).remove();
                imageHP.remove();
                if (who.equals("player")) {
                    playerPeon = player;
                    drawHPbar(playerPeon);
                } else if (who.equals("mech")) {
                    mech = newMech;
                    drawHPbar(mech);
                }
                drawPic(filePath);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                return true;
            }
        });
        this.addActor(this.imageButton.get(which - 2));
    }

    /**
     * create the initial information board, it will show the information of a selected player and its
     * picture. user can view different players' information by click the little image button
     */
    public void createTempBoard(String mechOrPlayer) {
        if (!this.lastClick) {
            //background

            this.image.add(0, new Image(new Texture(Gdx.files.internal("playerboard/info_4_blocks.png"))));
            this.image.get(0).setPosition((float) (Gdx.graphics.getWidth() * 0.2),
                    (float) (Gdx.graphics.getHeight() * 0.2));
            this.image.get(0).setSize((float) (Gdx.graphics.getWidth() * 0.6),
                    (float) (Gdx.graphics.getHeight() * 0.6));
            this.image.get(0).setColor(Color.valueOf("1c5894E6"));
            this.addActor(this.image.get(0));
            if (mechOrPlayer.equals("player")) {
                playerPeon = GameManager.getParty().getMainPlayer();
                //large pic of the player
                if (playerPeon.getType().equals(PlayerType.KNIGHT)) {
                    String path = (playerPeon.getTypeDetail().equals("typeA"))
                            ? "PlayerCharacterMech/Knights N-01.png"
                            : "player/knight/knight_idle.png";
                    drawPic(path);
                } else {
                    String path = (playerPeon.getTypeDetail().equals("typeA"))
                            ? "PlayerCharacterMech/Witch N 2.png"
                            : "player/wizard/wizard_idle.png";
                    drawPic(path);
                }
                drawHPbar(playerPeon);

                for (int i = 0; i < allPlayers.size(); i++) {
                    double x = (i == 0 || i == 2) ? 0.62 : 0.69;
                    double y = (i == 0 || i == 1) ? 0.58 : 0.47;
                    // based on PlayerPeon.gettypeDetail() to choose the image
                    if (allPlayers.get(i).getType() == PlayerType.WIZARD) {
                        String path = (allPlayers.get(i).getTypeDetail().equals("typeA"))
                                ? "PlayerCharacterMech/Witch N 2.png"
                                : "player/wizard/wizard_idle.png";
                        drawThumPic(path, "player", allPlayers.get(i), null, x, y, 2 + i);

                    } else {
                        String path = (allPlayers.get(i).getTypeDetail().equals("typeA")) ? "PlayerCharacterMech/Knights N-01.png" : "player/knight/knight_idle.png";
                        drawThumPic(path, "player", allPlayers.get(i), null, x, y, 2 + i);

                    }
                }
            } else {
                drawPic("PlayerCharacterMech/mech_sprite.png");
                drawHPbar(mech);
                drawThumPic("PlayerCharacterMech/mech_sprite.png", "mech",
                        null, allMechs.get(0), 0.55, 0.23, 2);

            }
            this.lastClick = true;

        } else {
            for (Image im : this.image) {
                this.removeActor(im);
            }
            for (ImageButton imb : this.imageButton) {
                this.removeActor(imb);
            }
            this.removeActor(this.imageHP);
            this.lastClick = false;

        }


    }


    /**
     * draw the text with the title and the content
     *
     * @param batch    batch
     * @param title    text title
     * @param conetent text content
     * @param hight    y location
     * @param titleX   title x location
     * @param contentx content x location
     */
    private void drawtext(Batch batch, String title, String conetent, double hight, double titleX, double contentx) {
        /**
         * the text object display the text
         */
        GlyphLayout playerTextshow;
        if (!title.equals("TYPE")) {
            playerTextshow = new GlyphLayout(font, title);
            this.font.setColor(Color.GOLD);
            font.draw(batch, playerTextshow, (float) (Gdx.graphics.getWidth() * titleX),
                    (float) (Gdx.graphics.getHeight() * hight));
        }
        playerTextshow = new GlyphLayout(font, conetent);
        this.font.setColor(Color.GOLD);
        font.draw(batch, playerTextshow, (float) (Gdx.graphics.getWidth() * contentx),
                (float) (Gdx.graphics.getHeight() * hight));

    }

    /**
     * use to draw the text on the screen
     *
     * @param batch
     * @param parentAlpha
     */


    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (this.lastClick) {
            super.draw(batch, parentAlpha);
            if (mechOrPlayer == 1) {
                // text for player's type
                drawtext(batch, "TYPE", playerPeon.getType().toString(), 0.67, 0.24, 0.35);
                // text for player's level
                drawtext(batch, "Level", "" + playerPeon.getLevel(), 0.65, 0.5, 0.55);

                // text for player's XP
                drawtext(batch, "XP", playerPeon.getXP() + "/" + (playerPeon.getLevel() * 20 + 50), 0.6, 0.5, 0.55);

                // text for player's HP
                drawtext(batch, "HP", playerPeon.getHP() + "/" + (playerPeon.getLevel() * 70 + 600), 0.33, 0.29, 0.32);

                // text for player's mana
                drawtext(batch, "MANA", playerPeon.getMana() + "", 0.55, 0.5, 0.55);

                // text for player's Damage
                drawtext(batch, "Damage", playerPeon.getDamage() + "", 0.5, 0.5, 0.55);

                // text for player's Armour
                drawtext(batch, "Armour", playerPeon.getArmour() + "", 0.45, 0.5, 0.55);

                // text for player's skill
                drawtext(batch, "SKILL", playerPeon.getSkillInfor(), 0.4, 0.5, 0.55);

            } else {
                drawtext(batch, "TYPE", "mech", 0.77, 0.5, 0.33);

                // text for mech's HP
                drawtext(batch, "HP", mech.getHealth() + "/" + mech.getMaxHealth(), 0.71, 0.24, 0.33);

                // text for player's mana
                drawtext(batch, "MANA", mech.getMana() + "", 0.65, 0.24, 0.33);

                // text for player's Damage
                drawtext(batch, "Damage", mech.getDamage() + "", 0.59, 0.24, 0.33);

                // text for player's Armour
                drawtext(batch, "Armour", mech.getArmour() + "", 0.53, 0.24, 0.33);

            }
        }
    }

    /**
     * draw the health bar of the player
     *
     * @param entity the entity who will show the health bar
     */
    private void drawHPbar(HasHealth entity) {
        this.removeActor(imageHP);
        //health bar for player
        if (entity.getHealth() == 0) {
            imageHP = new Image(new Texture(Gdx.files.internal("HealthBar/Health_Bar00.png")));
        } else {
            double hpvalue = (10 * entity.getHealth()) / entity.getMaxHealth();
            int hpper = (hpvalue == 0) ? 10 : (int) (hpvalue * 10);
            imageHP = new Image(new Texture(Gdx.files.internal("HealthBar/Health_Bar" + hpper + ".png")));
        }

        double hight = (entity.getClass().getSimpleName().equals("Mech")) ? 0.67 : 0.25;
        imageHP.setPosition((float) (Gdx.graphics.getWidth() * 0.31), (float) (Gdx.graphics.getHeight() * hight));
        imageHP.setSize((float) (Gdx.graphics.getWidth() * 0.2), (float) (Gdx.graphics.getHeight() * 0.1));
        addActor(this.imageHP);
    }


    /**
     * Pressing B on keyboard can show the information board
     *
     * @param keycode the key being pressed
     */
    @Override
    public void notifyKeyDown(int keycode) {
        if (keycode == Input.Keys.B) {
            mechOrPlayer = 1;
            this.createTempBoard("player");

        }
    }

    public boolean getDisplayed() {
        return this.lastClick;
    }

}
