package deco2800.thomas.entities;

import deco2800.thomas.Tickable;
import deco2800.thomas.worlds.Tile;

public class Wall extends StaticEntity implements Tickable {
    public Wall(Tile t, String texture) {
        super(t, RenderConstants.WALL, texture, true);
        this.setTexture(texture);
    }

    @Override
    public void onTick(long i) {

    }
}
