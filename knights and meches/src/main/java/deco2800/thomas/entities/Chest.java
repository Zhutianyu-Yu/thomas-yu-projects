package deco2800.thomas.entities;

import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Equipment.WoodenSword;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Wood;
import deco2800.thomas.worlds.Tile;

import java.util.ArrayList;
import java.util.Random;

public class Chest extends StaticEntity {


    HealthPotion healthPotion =new HealthPotion("small");
    ManaPotion manaPotion = new ManaPotion("small");
    WoodenArmour woodenArmour = new WoodenArmour();
    WoodenSword woodenSword = new WoodenSword();
    Wood wood = new Wood();
    Gold gold =new Gold();
    Diamond diamond =new Diamond();
    private ArrayList<AbstractItem> selectItem;
    private ArrayList<AbstractItem> item;

    public Chest(Tile t, String texture) {
        super(t, 8, texture, false);
        this.setTexture(texture);
        selectItem = new ArrayList<>();
        item = new ArrayList<>();
        item.add(healthPotion);
        item.add(manaPotion);
        item.add(woodenSword);
        item.add(wood);
        item.add(gold);
        item.add(diamond);
        for (int i = 0; i <= 4; ++i) {
            Random rand = new Random();
            AbstractItem items = item.get(rand.nextInt(item.size()));
            selectItem.add(items);
        }
    }

    public ArrayList<AbstractItem> getItems(){
        return selectItem;
    }

}
