package deco2800.thomas.entities;
import deco2800.thomas.worlds.Tile;

public class Generator extends StaticEntity implements HasHealth {
    private static final int MAX_HEALTH = 100;
    private int health = 100;
    private static final String ENTITY_ID_STRING = "rock";
    
    public Generator() {
        this.setObjectName(ENTITY_ID_STRING);
    }

    public Generator(Tile tile, boolean obstructed) {
        super(tile, RenderConstants.ROCK_RENDER, "rock", obstructed);
        this.setObjectName(ENTITY_ID_STRING);
    }

    @Override
    public void onTick(long i) {

    }

    @Override
    public int getMaxHealth() {
        return MAX_HEALTH;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public boolean aliveOrNot() {
        return this.health > 0;
    }

    @Override
    public int getArmour() {
        return 0;
    }

    @Override
    public void setArmour(int armour) {

    }
}
