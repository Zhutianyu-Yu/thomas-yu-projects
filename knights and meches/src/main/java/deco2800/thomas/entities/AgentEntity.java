package deco2800.thomas.entities;

import com.google.gson.annotations.Expose;

import deco2800.thomas.util.SquareVector;

public abstract class AgentEntity extends AbstractEntity{
	@Expose
	protected float speed;
	protected float baseSpeed;

	static final float RUNRATE = 4f;
	static final float SNEAKRATE = 0.4f;


	public AgentEntity(float row, float col, int renderOrder, float speed) {
		super(col, row, renderOrder);
		this.speed = speed;
		this.baseSpeed = speed;
	}

	public AgentEntity() {
		super();
	}

	public void moveTowards(SquareVector destination) {
		position.moveToward(destination, speed);
	}

	public void startRun() {
		this.speed = RUNRATE * baseSpeed;
	}

	public SquareVector getPosition() {
		return this.position;
	}

	public float getCol() {
		return this.position.getCol();
	}

	public float getRow() {
		return this.position.getRow();
	}

	public void endRun() {
		this.speed = baseSpeed;
	}

	public void startSneak() {
		this.speed = SNEAKRATE * baseSpeed;
	}

	public void endSneak() {
		this.speed = baseSpeed;
	}
	
	public float getSpeed() {
		return speed;
	}

	public void setSpeed( float speed) {
		this.speed = speed;
	}

}
