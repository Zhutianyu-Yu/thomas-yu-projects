package deco2800.thomas.entities;

/**
 * Type of PlayerPeon
 */
public enum PlayerType
{
    /**
     * Normal player type
     */
    BASE,
    /**
     * Knight player
     */
    KNIGHT,
    /**
     * Wizard player
     */
    WIZARD,
    /**
     * Mech player
     */
    MECH
}
