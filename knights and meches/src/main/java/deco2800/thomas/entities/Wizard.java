package deco2800.thomas.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wizard extends PlayerPeon implements HasHealth {


    private static final String wizardTypeA = "typeA";
    private final ArrayList<String> walkingTextures = new ArrayList<>();
    private final String wizardType;
    private String defaultTexture = "Wizard1.1";
    // What walking image we are showing
    private int walkingImgNum = 0;
    // Have we been moved once
    private boolean set = false;
    // The previous tick's position
    private int old;
    //
    private final String[] types = new String[]{wizardTypeA, "typeB"};

    /**
     * the class for the Wizard, there are two (maybe more in future) Wizard, which defined
     * by the argument wizardtype.
     * <p>
     * the key different between those two wizards is that they have different skill.
     * <p>
     * Wizard typeA can make all players in the teram get 100 HP, also the mana used will decreased with
     * the higher level.
     * <p>
     * Wizard typeB can make all enemy lost 100 HP, the mana used will be 80 which is high.
     *
     * @param row        row location of the Wizard
     * @param col        column location of the Wizard
     * @param speed      movement speed of the Wizard
     * @param wizardtype type of the Wizard
     */
    public Wizard(float row, float col, float speed, String wizardtype) {
        super(row, col, speed);
        this.type = PlayerType.WIZARD;

        if (wizardtype.equals("typeA") || wizardtype.equals("typeB"))
            this.wizardType = wizardtype;
        else
            this.wizardType = types[wizardtype.hashCode() % 2];
        this.setTexture(defaultTexture);
        this.speed = 0.1f;
    }


    /**
     * Wizard typeA can make all players in the teram get 100 HP, also the mana used will decreased with
     * the higher level.
     * <p>
     * Wizard typeB can make all enemy lost 100 HP, the mana used will be 80 which is high.
     *
     * @param playerinTeam team members
     * @param teamEnemy    enemy team
     */

    @Override
    public void skill(List<HasHealth> playerinTeam, List<HasHealth> teamEnemy) {
        int manaNeed = 0;
        if (this.wizardType.equals(wizardTypeA)) {
            if (this.getLevel() <= 3) {
                manaNeed = 70;
            } else if (this.getLevel() > 8) {
                manaNeed = 45;
            } else {
                manaNeed = 60;
            }
        } else {
            manaNeed = 80;
        }

        if (manaNeed <= this.getMana()) {
            if (this.wizardType.equals(wizardTypeA)) {
                for (HasHealth player : playerinTeam) {
                    player.setHealth(player.getHealth() + 100);
                }
            } else {
                for (HasHealth playerEnemy : teamEnemy) {
                    playerEnemy.setHealth(playerEnemy.getHealth() - 100);
                }
            }
            this.addMana(0 - manaNeed);
        }
    }

    @Override
    public String getTypeDetail() {
        return this.wizardType;
    }

    @Override
    public String getSkillInfor() {
        if (this.wizardType.equals(wizardTypeA)) {
            return "Wizard can make all players \nin the team get 100 HP, also \nthe mana used will be decreased \nwith the higher level.";
        }
        return "Wizard can make all \nenemy lost 100 HP";
    }

    @Override
    public void onTick(long i) {
        if (getTask() != null && getTask().isAlive()) {
            getTask().onTick(i);

            if (getTask().isComplete()) {
                this.setTexture(defaultTexture);
                setTask(null);
            }
        }
        if (set && this.getPosition().hashCode() != old && !walkingTextures.isEmpty()) {
            switch (walkingImgNum) {
                case 0:
                    this.setTexture(walkingTextures.get(0));
                    walkingImgNum++;
                    break;
                case 1:
                    this.setTexture(walkingTextures.get(1));
                    walkingImgNum++;
                    break;
                case 2:
                    this.setTexture(walkingTextures.get(2));
                    walkingImgNum = 0;
                    break;
                default:
                    this.setTexture(defaultTexture);
                    break;
            }
        }
        // Store the current position
        old = this.getPosition().hashCode();
        // We have moved a position at least once
        set = true;

    }

    /**
     * set the default texture of the Wizard
     *
     * @param texture the texture that will be the Wizard's default texture
     */
    public void setDefaultTexture(String texture) {
        this.defaultTexture = texture;
    }

    /**
     * set the walking animations for the Wizard
     *
     * @param textureNames the texture names as stoed in TextureManager to be utilised in game
     */
    public void setWalkingAnimations(String... textureNames) {
        this.walkingTextures.addAll(Arrays.asList(textureNames));
    }
}
