package deco2800.thomas.entities;


import deco2800.thomas.worlds.Tile;


/**
 * A class for the Tutorial NPC
 */
public class TutorialNPC extends Peon {

    private boolean found = false;
    private long prevTick = 0;
    private long currentTick = 0;
    private int doorNo = 0;
    private boolean down = false;
    private static final String ENTITY_ID_STRING = "tutorialNpcID";

    public TutorialNPC(Tile position, String texture) {

        super(position.getRow(), position.getCol(), 0);
        this.setTexture(texture);
        this.setRenderOrder(RenderConstants.PEON_RENDER - 1);
    }


    @Override
    public void onTick(long i) {

        if (this.getTexture().substring(0, 4).equals("door")) {

            if (this.found) {
                this.down = false;
                if (currentTick - prevTick > 30 && this.doorNo != 4) {
                    this.doorNo = (this.doorNo + 1) % 5;
                    this.setTexture("door" + this.doorNo);
                    prevTick = i;
                }
            } else if (this.doorNo == 4) {
                this.down = true;
                this.doorNo--;
            } else if (this.down && currentTick - prevTick > 30) {
                this.setTexture("door" + this.doorNo);
                this.doorNo--;
                if (this.doorNo < 0) {
                    this.down = false;
                }
                prevTick = i;
            } else {
                this.doorNo = 0;
            }
        }
        currentTick++;
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {

            return false;

        }

        if (!(other instanceof TutorialNPC)) {

            return false;

        }

        TutorialNPC otherTutorialNPC = (TutorialNPC) other;

        return this.getCol() == otherTutorialNPC.getCol() && this.getRow() ==
                otherTutorialNPC.getRow() && this.getHeight() == otherTutorialNPC.getHeight();
    }


    /**
     * Gets the hashCode of the tree.
     *
     * @return the hashCode of the tree
     */
    @Override
    public int hashCode() {
        final float prime = 31;
        float result = 1;
        result = (result + super.getCol()) * prime;
        result = (result + super.getRow()) * prime;
        result = (result + super.getHeight()) * prime;
        return (int) result;
    }
}
