package deco2800.thomas.entities;

import com.badlogic.gdx.Input;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.worlds.staticworlds.GhostWorld;
import deco2800.thomas.worlds.Tile;

public class DeathGhost extends Peon implements TouchDownObserver {

    private static final String ENTITY_ID_STRING = "deathGhostID";


    public DeathGhost() {

        super();
        this.setTexture("ghost1");
    }

    public DeathGhost(Tile tile, int renderOrder , String texture , boolean obstructed) {
        super(tile.getRow(), tile.getCol(), 0);
        this.setObjectName(ENTITY_ID_STRING);
        this.setTexture(texture);
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListener(this);
    }


    /**
     * Notifies the observers of the mouse button being pushed down
     *
     * @param screenX the x position the mouse was pressed at
     * @param screenY the y position the mouse was pressed at
     * @param pointer
     * @param button  the button which was pressed
     */
    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

        if (button == Input.Buttons.LEFT && GameManager.getWorld()
                instanceof GhostWorld) {

            ((GhostWorld) GameManager.getWorld()).advance();
        }
    }
}
