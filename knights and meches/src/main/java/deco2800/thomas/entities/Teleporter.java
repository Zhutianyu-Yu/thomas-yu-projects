package deco2800.thomas.entities;

import deco2800.thomas.Tickable;
import deco2800.thomas.worlds.Tile;

public class Teleporter extends StaticEntity implements Tickable {

    public Teleporter(Tile t,String texture) {
        super(t, RenderConstants.TELEPORTER_RENDER, texture, false);
        this.setTexture(texture);
    }

    @Override
    public void onTick(long i) {

    }
}
