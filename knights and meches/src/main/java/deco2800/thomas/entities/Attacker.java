package deco2800.thomas.entities;

import deco2800.thomas.moves.Move;

import java.util.Map;

public interface Attacker {

    /**
     * show how much damage the entity can give
     *
     * @return how much damage the entity can give
     */
    int getDamage();

    /**
     * update the damage of the entity
     * @param damage new damage value
     */
    void setDamage(int damage);

    /** retrieve the amount of mana this user has
     *
     * @return amount of mana*/
    int getMana();

    /**
     * set the mana of this entity
     *
     * @param mana the mana to be set
     */
    void setMana(int mana);

    /**
     * Retrieve the maximum amount of mana this entity can store
     * @return the max amount of mana
     */
    int getMaxMana();

    /**
     * Returns an array of attacker's move's names
     *
     * @return an array full of attacker's moves as strings
     */
    String[] getMoveNames();

    /**
     * Gets all an entity's moves
     * @return object with move name mapped to move object
     */
    Map<String, Move> getMoves();

    /**
     * Adds an attack in MoveManager to entity
     * @param name name of attack
     */
    void addAttack(String name);
}
