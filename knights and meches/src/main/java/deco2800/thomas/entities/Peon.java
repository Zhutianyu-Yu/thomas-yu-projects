package deco2800.thomas.entities;

import deco2800.thomas.Tickable;
import deco2800.thomas.tasks.AbstractTask;


public class Peon extends AgentEntity implements Tickable {
	private transient AbstractTask task;

	public Peon() {
		super();
		this.setObjectName("Peon");
		this.setHeight(1);
		this.speed = 0.1f;
	}

	/**
	 * Peon constructor
     */
	public Peon(float row, float col, float speed) {
		super(row, col, RenderConstants.PEON_RENDER, speed);
	}

	@Override
	public void onTick(long i) {
		//doNothing
	}

	public void setTask(AbstractTask task) {
		this.task = task;
	}
	
	public AbstractTask getTask() {
		return task;
	}
}
