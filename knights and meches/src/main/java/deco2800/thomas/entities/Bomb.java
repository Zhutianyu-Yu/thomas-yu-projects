package deco2800.thomas.entities;

import deco2800.thomas.worlds.Tile;

public class Bomb extends StaticEntity{

        public Bomb(Tile t) {
            super(t, RenderConstants.ORB_RENDER, "orb_blue", false);
            this.setTexture("orb_blue");
        }
}
