package deco2800.thomas.entities;

import deco2800.thomas.worlds.Tile;

public class TeleportCircle extends StaticEntity{

    public TeleportCircle(Tile t) {
        super(t, 7, "teleport_circle", false);
        this.setTexture("teleport_circle");
    }
}
