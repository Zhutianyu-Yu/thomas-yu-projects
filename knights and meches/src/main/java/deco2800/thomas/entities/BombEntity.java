package deco2800.thomas.entities;


import deco2800.thomas.worlds.Tile;


public class BombEntity extends StaticEntity {

    public BombEntity(Tile t) {
        super(t, 9, "chest_close", false);
        this.setTexture("chest_close");
    }
}
