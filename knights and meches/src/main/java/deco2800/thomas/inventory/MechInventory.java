package deco2800.thomas.inventory;
import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.Mech;
import deco2800.thomas.managers.GameManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MechInventory {
    private final Logger LOG = LoggerFactory.getLogger(MechInventory.class);
    private InventoryAbstract inventory;
    private HashMap<Integer,Boolean> knightStatus = new HashMap();
    private HashMap<Integer,Boolean> wizardStatus = new HashMap();
    private int activeKnight=1;
    private int activeWizard=1;
    private List<Mech> mechList = new ArrayList<>();

    /**
     * find the mech,knight,wizard information
     * @param i the inventory in bag
     */
    public MechInventory(InventoryAbstract i){
        this.inventory=i;
        knightStatus.put(1,false);
        knightStatus.put(2,false);
        knightStatus.put(3,false);
        wizardStatus.put(1,false);
        wizardStatus.put(2,false);
        wizardStatus.put(3,false);


    }

    /**
     * make the knight status available
     * @param i Knight's serial number
     */
    public void changeKnightStatus(int i){
        knightStatus.put(i,!knightStatus.get(i));
    }

    /**
     * get whether the knight is available
     * @param i Knight's serial number
     * @return true for the knight is available, otherwise false
     */
    public Boolean getKnightStatus(int i){
        return knightStatus.get(i);
    }

    /**
     * make the wizard status available
     * @param i wizard's serial number
     */
    public void changeWizardStatus(int i){
        wizardStatus.put(i,!wizardStatus.get(i));
        LOG.info("wizard is {}",i);
    }

    /**
     * get whether the knight is available
     * @param i wizard's serial number
     * @return true for the knight is available, otherwise false
     */
    public Boolean getWizardStatus(int i){
        return wizardStatus.get(i);
    }


    /**
     * make the knight active
     * @param i Knight's serial number
     */
    public void activeKnight(int i){
        activeKnight=i;
    }

    /**
     * return the serial number which knight is chosen to use
     * @return return the serial number which knight is chosen to use
     */
    public int getActiveKnight(){
        return activeKnight;
    }

    /**
     * make the wizard active
     * @param i wizard's serial number
     */
    public void activeWizard(int i){
        activeWizard=i;
    }

    /**
     * return the serial number which wizard is chosen to use
     * @return return the serial number which wizard is chosen to use
     */
    public int getActiveWizard(){
        return activeWizard;
    }

    /**
     * return which mech is been used
     * @return return which mech is been used
     */
    public Mech getMech(){
        Mech mech=null;

        List<AbstractEntity> entity = GameManager.get().getWorld().getEntities();
        for (AbstractEntity element : entity) {
            if (element.getClass().getSimpleName().equals("Mech"))   {
                mechList.add((Mech) element);
            }
        }

        if (activeKnight==1&&activeWizard==1){
            mech=mechList.get(0);
        }else if (activeKnight==1&&activeWizard==2){
            mech=mechList.get(1);
        }else if (activeKnight==2&&activeWizard==1){
            mech=mechList.get(2);
        }else if (activeKnight==2&&activeWizard==2){
            mech=mechList.get(3);
        }
        return mech;
    }

    public int getDamage(){
        int damage=0;
        if (inventory.getWeapon()!=null){
            damage += inventory.getWeapon().getDamage();
        }
        damage+=inventory.getNumOfOrbs()*30;
        return damage;
    }

    public int getArmour(){
        int armour = 0;
        if (inventory.getArmour()!=null){
            armour += inventory.getWeapon().getArmour();
        }
        armour+=inventory.getNumOfOrbs()*30;
        return armour;
    }





}
