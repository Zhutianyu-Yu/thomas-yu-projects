package deco2800.thomas.inventory;

import deco2800.thomas.crafting.ItemStack;
import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.entities.PlayerType;
import deco2800.thomas.inventory.ui.InventoryMenu;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Consumable.ResurrectionScroll;
import deco2800.thomas.items.ConsumableItem;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Material.Wood;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.trading.Trading;
import deco2800.thomas.managers.QuestManager;
import deco2800.thomas.quest.QuestAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class InventoryAbstract {

    /**
     * A 2D array of items which stores the inventory
     */
    private AbstractItem[][] inventoryArray;

    /**
     * A 2D array of quantities that stores the amount of each item in the
     * inventory
     */
    private int[][] quantityArray;

    /**
     * The amount of columns that the inventory has
     */
    private int sizeX;

    /**
     * The amount of rows the inventory has
     */
    private int sizeY;

    /**
     * The weapon that the player currently has equipped
     */
    private AbstractItem weapon;

    /**
     * The orb that the player currently has equipped
     */
    private AbstractItem orb;

    /**
     * The armour that the player currently has equipped
     */
    private AbstractItem armour;

    private String[] orbOrder = new String[5];

    private Trading trading = new Trading(this);

    private MechInventory mechInventory;
    
    private InventoryMenu uiMenu;

    private final Logger LOG = LoggerFactory.getLogger(InventoryMenu.class);

    //Constructor ==============================================================

    /**
     * a default constructor which create the inventory system with a specific size
     */
    public InventoryAbstract(int x, int y){
        inventoryArray = new AbstractItem[x][y];
        quantityArray = new int[x][y];
        sizeX = x;
        sizeY = y;
        for (int i=0;i<x;i++){
            for (int j=0;j<y;j++){
                quantityArray[i][j] = 0;
            }
        }
        this.mechInventory = new MechInventory(this);
        mechInventory.changeKnightStatus(1);
        mechInventory.changeKnightStatus(2);
        mechInventory.changeWizardStatus(1);
        mechInventory.changeWizardStatus(2);

    }

    //Get Methods ===============================================================

    /**
     * a method which return the quantity of inventory in bag
     *
     * @param x the column of the inventory position
     * @param y the row of the inventory position
     * @return the quantity of inventory
     */
    public int getInventoryQuantity(int x, int y){
        return quantityArray[x][y];
    }

    /**
     * give the column size of bag
     *
     * @return the column size of 2d list
     */
    public int getColumnSize(){
        return sizeX;
    }

    /**
     * give the row size of bag
     *
     * @return the row size of bag
     */
    public int getRowSize(){
        return sizeY;
    }

    /**
     * Convert a 2D inventory list to a single list
     * @return a list of inventory
     */
    public List<AbstractItem> get1DInventoryList(){
        ArrayList<AbstractItem> list = new ArrayList<>();
        for (int i = 0; i< sizeX; i++){
            for (int j = 0; j< sizeY; j++){
                list.add(inventoryArray[i][j]);
            }
        }
        return list;
    }

    /**
     * Convert a 2D quantity list to a single list
     * @return a list of quantity
     */
    public List<Integer> get1DQuantityList(){
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i< sizeX; i++){
            for (int j = 0; j< sizeY; j++){
                list.add(quantityArray[i][j]);
            }
        }
        return list;
    }

    /**
     *  get the 2d list which store the inventory
     *
     * @return 2d list which store the inventory
     */
    public AbstractItem[][] getInventoryArray(){
        return inventoryArray;
    }

    /**
     * get the item by providing row and column in bag
     *
     * @param x the column in the bag
     * @param y the row in the bag
     * @return the item in that position
     */
    public AbstractItem getItem(int x, int y){
        return inventoryArray[x][y];
    }

    /**
     * @return Instance of the weapon that is equipped
     */
    public AbstractItem getWeapon() {
        if (this.weapon != null) {
            return this.weapon;
        }
        return null;
    }

    /**
     * @return the instance of the power source item that is equipped
     */
    public AbstractItem getArmour() {
        return this.armour;
    }

    /**
     * @return the instance of the orb item that is equipped
     */
    public AbstractItem getOrb() {
        return this.orb;
    }

    public String[] getOrbOrder(){
        return orbOrder;
    }

    public Trading getTrading() {
        return this.trading;
    }

    public InventoryMenu getMenu() {
        return this.uiMenu;
    }

    public MechInventory getMechInventory() {
        return this.mechInventory;
    }

    public int getNumOfOrbs() {
        int num=0;
        for (int i = 0; i< get1DInventoryList().size(); i++){
            if (get1DInventoryList().get(i)!=null && get1DInventoryList().get(i).getItemName().equals("Orb")){
                orbOrder[num]= get1DInventoryList().get(i).getOrbColour();
                num++;
            }
        }
        return num;
    }

    //Set Methods ==============================================================

    public void setMenu(InventoryMenu menu) {
        this.uiMenu = menu;
    }

    //Adding to inventory methods ==============================================

    /**
     * add the item player get to the bag.
     * if the inventory has existed, the quantity would increase, otherwise
     * add it to the inventory list.
     * @param x the item player get in game
     */
    public void addInventory(AbstractItem x) {
        int quantityLimit = x.getItemSize();
        boolean exist = false;
        for (int i = 0; i < inventoryArray.length; i++) {
            for (int j = 0; j < inventoryArray[0].length; j++) {
                if (inventoryArray[i][j] != null && inventoryArray[i][j].getItemName().equals(x.getItemName())) {
                    if (x instanceof ConsumableItem) {
                        if (((ConsumableItem) inventoryArray[i][j]).getCapacity().
                                equals(((ConsumableItem) x).getCapacity()) &&
                                quantityArray[i][j]<quantityLimit) {
                            exist = true;
                            quantityArray[i][j]++;
                            this.uiMenu.getItemMenu().updateInventorySpot(i, j);

                            //Let the quest manager know that we picked up an item
                            QuestManager questManager = GameManager.get().getQuestManager();
                            questManager.updateQuestStatus(x, QuestAction.PickUpItem, 1);
                        }
                    } else {
                        if (quantityArray[i][j]<quantityLimit){
                            exist = true;
                            quantityArray[i][j]++;
                            try {
                                this.uiMenu.getItemMenu().updateInventorySpot(i, j);
                            } catch (Exception exception) {
                                LOG.warn("Exception thrown", exception);
                            }

                            QuestManager questManager = GameManager.get().getQuestManager();
                            questManager.updateQuestStatus(x, QuestAction.PickUpItem, 1);
                        }
                    }

                }
            }
        }
        if (!exist){
            int row=0;
            int column=0;
            while (row<inventoryArray[0].length){
                if (inventoryArray[column][row]==null){
                    inventoryArray[column][row]=x;
                    quantityArray[column][row]=1;
                    //Let the quest manager know we have picked up an item
                    QuestManager questManager = GameManager.get().getQuestManager();
                    questManager.updateQuestStatus(x, QuestAction.PickUpItem, 1);
                    break;
                }else {
                    row++;
                    if (row>=inventoryArray[column].length){
                        column++;
                        row=0;
                        if (column==inventoryArray.length){
                            //the bag is full and cannot add anything
                            break;
                        }
                    }
                }
            }
        }
    }

    //Searching the inventory methods ==========================================

    /**
     * Finds given quantity of item in this inventory
     * DOES NOT SEARCH MULTIPLE STACKS YET
     * @param item the item you are looking for
     * @param quantity the amount you are looking for
     * @throws ItemNotInInventoryException if the given item of quantity is not
     * found in the inventory
     * @return an array with the values {y, x} aka {i, j} of the position of
     * the item in the inventory
     */
    public int[] findInInventory(AbstractItem item, int quantity)
            throws ItemNotInInventoryException {
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                if (inventoryArray[i][j] != null &&
                        /*inventoryArray[i][j].equals(item) ITEMS ARE CLASSES
                         NOT INSTANCES?!?!*/
                        inventoryArray[i][j].getClass().equals(item.getClass())
                        && quantityArray[i][j] >= quantity) {
                    //then it exists here
                    int[] result = new int[2];
                    result[0] = i;
                    result[1] = j;
                    return (result);
                }
            }
        }
        throw new ItemNotInInventoryException();
    }

    /**
     * Same as above except parameters are different
     * @param stack a pair of the item and quantity
     */
    public int[] findInInventory(ItemStack stack)
            throws ItemNotInInventoryException {
        return findInInventory(stack.getItem(), stack.getQuantity());
    }

    /**
     * Returns how many of a current health potions are within the inventory
     *
     * @param size either "small", "medium", or "large"
     * @return the number of health potions
     */
    public int healthPotionQuantity(String size) {
        try {
            return findQuantityInInventory(new HealthPotion(size));
        } catch (ItemNotInInventoryException e) {
            return 0;
        }
    }

    /**
     * Returns how many of a current mana potions are within the inventory
     *
     * @param size either "small", "medium", or "large"
     * @return the number of mana potions
     */
    public int manaPotionQuantity(String size) {
        try {
            return findQuantityInInventory(new ManaPotion(size));
        } catch (ItemNotInInventoryException e) {
            return 0;
        }
    }

    /**
     * Finds the amount of item currently in the inventory
     * @param item the item to check the amount of
     * @return the integer of how many of that item are in the inventory
     */
    public int findQuantityInInventory(AbstractItem item)
            throws ItemNotInInventoryException {
        int count = 0;
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                //
                AbstractItem currentItem = inventoryArray[i][j];
                if (item != null && currentItem != null &&
                        currentItem.getClass().equals(item.getClass())) {
                    count += quantityArray[i][j];
                } else if (item == null && currentItem == null) {
                    count += 1;
                }
            }
        }
        if (count == 0) {
            throw new ItemNotInInventoryException();
        }
        return count;
    }

    //Removing from inventory methods ==========================================

    /**
     * a function that player could drop the inventory.
     * if the quantity is bigger than 1, reduce the quantity, otherwise
     * delete this inventory in bag list.
     * @param x the column of the inventory position
     * @param y the row of the inventory position
     * @return true if item is dropped within UI, otherwise false
     */
    public boolean dropInventory(int x, int y){
        QuestManager questManager = GameManager.get().getQuestManager();
        if (quantityArray[x][y] > 1) {
            //Let the quest manager know we have dropped an item
            if (inventoryArray[x][y] != null)
                questManager.updateQuestStatus(inventoryArray[x][y], QuestAction.DropItem, 1);
            quantityArray[x][y]--;
        } else if (quantityArray[x][y] == 1) {
            //Let the quest manager know we have dropped an item
            if (inventoryArray[x][y] != null)
                questManager.updateQuestStatus(inventoryArray[x][y], QuestAction.DropItem, 1);
            inventoryArray[x][y] = null;
            quantityArray[x][y] = 0;
        } else if (quantityArray[x][y] == 0) {
            return false;
        }
        try {
            this.uiMenu.getItemMenu().updateInventorySpot(x, y);
        } catch (Exception ignored) {
            return false;
        }
        return true;
    }

    /**
     * Removes one of the given item from the inventory
     * @param item The item to remove from the inventory
     * @return Returns True if the item was successfully removed, otherwise
     * false if none were removed
     */
    public boolean removeOneFromInventory(AbstractItem item) {
        if (item == null) {
            return false;
        }
        for (int i = this.sizeX - 1; i >= 0; i--) {
            for (int j = this.sizeY - 1; j >= 0; j--) {
                if (item.checkIfSame(inventoryArray[i][j])) {
                    dropInventory(i, j);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Attempts to remove a quantity of the given item from the inventory
     * @param item The item to remove
     * @param quantity the amount of the item to remove
     * @return Returns true if it successfully removed the given quantity of
     * the item, false otherwise
     */
    public boolean removeQuantityFromInventory(AbstractItem item,
                                               int quantity) {
        if (item == null) {
            return false;
        }
        try {
            //check that there is enough of the given item in the inventory
            if (quantity <= this.findQuantityInInventory(item)) {
                for (int num = 1; num <= quantity; num++) {
                    if (!removeOneFromInventory(item)) {
                        return false;
                    }
                }
            }
        } catch (ItemNotInInventoryException e) {
            return false; //0 items exist
        }
        return true;
    }

    /**
     * gets and deletes a mana potion in the inventory
     *
     * @param size either "small", "medium", or "large"
     * @return true if mana potion could be used and applied to the UI, otherwise false
     */
    public boolean useManaPotion(String size) {
        int[] position;
        try {
            position =  findInInventory(new ManaPotion(size), 1);
        } catch (ItemNotInInventoryException e) {
            return false;
        }
        try {
            dropInventory(position[0], position[1]);
        } catch (Exception ignore) {
            return false;
        }
        return true;
    }

    public void useManaPotion(String size, int posX, int posY) {
        if (this.getItem(posX, posY) instanceof ManaPotion) {
            ManaPotion manaPotion = new ManaPotion(size);
            PlayerPeon player = GameManager.get().getParty().getPlayers().get(PlayerType.KNIGHT);
            manaPotion.use(player);
            dropInventory(posX, posY);
        }
    }

    /**
     * gets and deletes a health potion in the inventory
     *
     * @param size either "small", "medium", or "large"
     * @return true if health potion could be used, otherwise false
     */
    public boolean useHealthPotion(String size) {
        int[] position;
        try {
            position = findInInventory(new HealthPotion(size), 1);
        } catch (ItemNotInInventoryException e) {
            return false;
        }
        if( uiMenu != null) {
            dropInventory(position[0], position[1]);
        }
        return true;
    }

    public void useHealthPotion(String size, int posX, int posY) {
        if (this.getItem(posX, posY) instanceof HealthPotion) {
            HealthPotion healthPotion = new HealthPotion(size);
            PlayerPeon player = GameManager.get().getParty().getPlayers().get(PlayerType.KNIGHT);
            healthPotion.use(player);
            dropInventory(posX, posY);
        }
    }

    //Equip Methods ============================================================

    /**
     * Equips a weapon that is currently in the inventory
     * @param weapon Instance of the item that is being equipped
     */
    public void equipWeapon(AbstractItem weapon) {
        if (this.weapon != null) {
            this.addInventory(this.weapon);
        }
        this.weapon = weapon;
    }

    /**
     * Equips an armour item that is currently in the inventory
     * @param armour Instance of the armour item that is equipped
     */
    public void equipArmour(AbstractItem armour) {
        if (this.armour != null) {
            this.addInventory(this.armour);
        }
        this.armour = armour;
    }

    /**
     * Equips an orb that is currently in the inventory
     * @param orb Instance of the orb item that is equipped
     */
    public void equipOrb(AbstractItem orb) {
        if (this.orb != null) {
            this.addInventory(this.orb);
        }
        this.orb = orb;
    }

    //Other Methods ============================================================

    /**
     * Moving the inventory in the bag. If moving an item to an empty space,
     * the origin place would be empty. if moving an item to an not empty
     * space, exchanging order of the two items.
     *
     * @param fromX the column of the inventory position
     * @param fromY the row of the inventory position
     * @param toX the column of destination position
     * @param toY the row of destination position
     * @return true if item is moved within UI, otherwise false
     */
    public boolean moveInventory(int fromX, int fromY, int toX, int toY ){
            if (inventoryArray[toX][toY]==null){
                inventoryArray[toX][toY]=inventoryArray[fromX][fromY];
                quantityArray[toX][toY]=quantityArray[fromX][fromY];
                inventoryArray[fromX][fromY]=null;
                quantityArray[fromX][fromY]=0;
            } else if (inventoryArray[toX][toY]!=null){
                AbstractItem replace = inventoryArray[toX][toY];
                int originalQuantity = quantityArray[toX][toY];
                inventoryArray[toX][toY]=inventoryArray[fromX][fromY];
                quantityArray[toX][toY]=quantityArray[fromX][fromY];
                inventoryArray[fromX][fromY]=replace;
                quantityArray[fromX][fromY]=originalQuantity;
            }
        try {
            this.uiMenu.getItemMenu().updateInventorySpot(fromX, fromY);
            this.uiMenu.getItemMenu().updateInventorySpot(toX, toY);
        } catch (Exception ignored) {
            return false;
        }
        return true;
    }

    /**
     * Creates Basic inventory for player
     *
     * Will be used until picking up items in implemented in the game
     */
    public void createDummyInventory() {
        // Consumables
        AbstractItem smallHealthPotion = new HealthPotion("small");
        AbstractItem mediumHealthPotion = new HealthPotion("medium");
        AbstractItem largeHealthPotion = new HealthPotion("large");
        AbstractItem smallManaPotion = new ManaPotion("small");
        AbstractItem mediumManaPotion = new ManaPotion("medium");
        AbstractItem largeManaPotion = new ManaPotion("large");
        AbstractItem resurrectionScroll = new ResurrectionScroll();

        this.addInventory(smallHealthPotion);
        this.addInventory(mediumHealthPotion);
        this.addInventory(largeHealthPotion);
        this.addInventory(smallManaPotion);
        this.addInventory(mediumManaPotion);
        this.addInventory(largeManaPotion);
        this.addInventory(resurrectionScroll);


        // Materials
        AbstractItem diamond = new Diamond();
        AbstractItem gold = new Gold();
        AbstractItem iron = new Iron();
        AbstractItem wood = new Wood();

        this.addInventory(diamond);
        this.addInventory(diamond);
        this.addInventory(diamond);
        this.addInventory(diamond);
        this.addInventory(diamond);
        this.addInventory(diamond);
        this.addInventory(gold);
        this.addInventory(iron);
        this.addInventory(wood);

    }

}
