package deco2800.thomas.inventory;

import org.javatuples.Pair;

public interface InventoryInterface<T> {

    /**
     * prints the contents of the inventory
     */
    public void printInventory();

    /**
     * @param position The postion of the item
     * @return T the item object
     */
    public T getItem(int position);

    /**
     * @param position The name of the Item
     * @return returns the quantity of that item
     * @throws IllegalArgumentException if the item doesn't exist in the array
     */
    public int getNumber(int position);

    /**
     * @param item This is the item that is going to be added to the inventory
     * @return on success returns true. If the item cannot be added returns false
     */
    public boolean addItem(Pair<T, Integer> item);

    /**
     * @param itemOnePosition position of the first item in the ArrayList
     * @param itemTwoPosition position of the second item in the ArrayList
     */
    public void swapItem(int itemOnePosition, int itemTwoPosition);

    /**
     * @param oldPosisiton original position of the item
     * @param newPosition new position of the item
     * @return on success returns true else returns false
     */
    public boolean moveItem(int oldPosisiton, int newPosition);

    /**
     *
     * @param position the position of the item
     * @return on success true else false
     */
    public boolean dropItem(int position);

}
