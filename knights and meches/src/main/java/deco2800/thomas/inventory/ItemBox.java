package deco2800.thomas.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import deco2800.thomas.items.AbstractItem;

import java.util.regex.Pattern;

public class ItemBox extends Actor {

    /** The image of the item */
    private Image img;
    /** The image of the blank ItemBox */
    private Image base;
    /** The AbstractItem of the ItemBox*/
    private AbstractItem item;
    /** The x position on the screen where the ItemBox should be displayed*/
    private int posX;
    /** The y position on the screen where the ItemBox should be displayed*/
    private int posY;
    /** true if box is outlined, otherwise false */
    private boolean boxOutined;

    /** The size of the ItemBox (both width and height) */
    private int size = 58;

    private Label quantityLabel;

    private Label.LabelStyle comicSansBold14LabelStyle = new Label.LabelStyle();

    /**
     * Creates an instance of ItemBox with the appropriate parameters
     * @param x position on the screen where the ItemBox should be displayed
     * @param y position on the screen where the ItemBox should be displayed
     * @param img the initial image of the ItemBox
     * @param item the AbstractItem that is held in the ItemBox
     */
    public ItemBox(int x, int y, Image img, AbstractItem item, int itemQuantity) {
        this.boxOutined = false;
        this.posX = x;
        this.posY = y;
        this.item = item;
        this.img = img;

        BitmapFont comicSansBold14 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/14_bold.fnt"));
        this.comicSansBold14LabelStyle.font = comicSansBold14;
        this.quantityLabel = new Label(Integer.toString(itemQuantity), this.comicSansBold14LabelStyle);
        this.quantityLabel.setPosition(this.posX + this.quantityLabel.getWidth()/2, this.posY + this.quantityLabel.getHeight()/2);

        this.comicSansBold14LabelStyle.font = comicSansBold14;


        String basicString = "inventory/InventoryBox.png";
        Image returnImage = new Image(new Texture(Gdx.files.internal(basicString)));
        returnImage.setSize(this.size, this.size);
        returnImage.setColor(1,1,10,0.5f);
        this.base = returnImage;

    }

    /**
     * Sets the ItemBox's base image to either a blank ItemBox or outlined ItemBox
     */
    public void setBase() {
        if (this.boxOutined) {
            String basicString = "inventory/outlined_item_box.png";
            Image returnImage = new Image(new Texture(Gdx.files.internal(basicString)));
            returnImage.setSize(this.size, this.size);
            returnImage.setColor(1,1,1,0.5f);
            this.base = returnImage;
        } else {
            String basicString = "inventory/InventoryBox.png";
            Image returnImage = new Image(new Texture(Gdx.files.internal(basicString)));
            returnImage.setSize(this.size, this.size);
            returnImage.setColor(0,0,0,0.5f);
            this.base = returnImage;
        }
    }

    /**
     * Updates the Image so the ItemBox displays the item's approximate image
     */
    public void updateImage() {
        setBase();
        try {
            this.img = initImage();
        } catch (Exception e) {
            this.img = null;
        }
        setImagePos();
    }


    /**
     * Sets the Item of the ItemBox
     *
     * @param item the abstract item for the current ItemBox
     */
    public void setItem(AbstractItem item) {
        this.item = item;
    }

    /**
     * Sets the quantity of the item in the ItemBox
     *
     * @param quantity the number of items in the current ItemBox
     */
    public void setQuantity(int quantity) {
        if (quantity > 0) {
            this.quantityLabel = new Label(Integer.toString(quantity), this.comicSansBold14LabelStyle);
            this.quantityLabel.setPosition(this.posX + 5, this.posY + 2);
        } else {
            this.quantityLabel = null;
        }
    }

    /**
     * Creates an image of the item being displayed in the ItemBox
     *
     * @return A Image that is linked to the appropriate Item
     */
    private Image initImage() {
        try {
            String emptyString = "";
            String[] parts = this.item.getClass().getName().split(Pattern.quote("."));
            Image returnImage = new Image(new Texture(Gdx.files.internal(emptyString.concat(parts[2] + "/" + parts[3] + "/" + this.item.getImgName()))));

            float widthScale = 30 / returnImage.getWidth();
            float heightScale = 30 / returnImage.getHeight();

            returnImage.setWidth(returnImage.getWidth() * widthScale);
            returnImage.setHeight(returnImage.getHeight() * heightScale);
            returnImage.setPosition(this.posX + returnImage.getWidth()/2, this.posY + returnImage.getHeight()/2);
            returnImage.setSize(returnImage.getWidth(), returnImage.getHeight());

            return returnImage;
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    /**
     * Sets the image to the appropriate position
     */
    public void setImagePos() {
        this.setPosition(this.posX, this.posY);
        if (this.img != null) {

            this.img.setPosition(this.posX + this.img.getWidth() / 2, this.posY + this.img.getHeight() / 2);
        }
        this.base.setPosition(this.posX, this.posY);
        this.setBounds(this.base.getX(), this.base.getY(), this.base.getWidth(), this.base.getHeight());
        this.setTouchable(Touchable.enabled);
    }

    /**
     * Outlines the UI box to indicate if the box has been pushed
     */
    public void outlineBox() {
        this.boxOutined = !this.boxOutined;
        this.setBase();
        this.updateImage();

    }

    /**
     * @return the Abstract item that is held in this box
     */
    public AbstractItem getItem() {
        return this.item;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        this.base.draw(batch, parentAlpha);
        if (this.img != null) {
            this.img.draw(batch, parentAlpha);
        }
        if (this.quantityLabel != null) {
            this.quantityLabel.draw(batch, parentAlpha);
        }
    }
}
