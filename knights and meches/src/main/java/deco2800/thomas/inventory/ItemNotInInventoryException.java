package deco2800.thomas.inventory;

/**
 * An exception for when an item is not found when searching in an inventory
 */
public class ItemNotInInventoryException extends Exception {
}
