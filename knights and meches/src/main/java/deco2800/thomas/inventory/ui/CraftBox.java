package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import deco2800.thomas.crafting.Recipe;

/**
 * This class is the visual representation of a single recipe in a crafter
 *
 * Each instance of CraftBox represents one recipe ONLY
 */
public class CraftBox extends Image {

    /**
     * The image to show in the menu when the recipe is currently available to
     * be crafted
     */
    private Drawable availableDrawable;

    /**
     * The image to show in the menu when the recipe cannot be crafted at the
     * current time
     */
    private Drawable unavailableDrawable;

    /**
     * The recipe that this instance is representing in the menu
     */
    private Recipe recipe;

    /**
     * Whether or not the recipe can currently be crafted
     */
    private boolean canCraft;

    /**
     * The size in pixels that the image will have. Note that all the images
     * are squares and thus height = IMAGE_SIZE = width
     */
    private static final int IMAGESIZE = 80;
    //Constructor ==============================================================

    /**
     * Constructor that creates a CraftBox
     *
     * The image shown is inherited from the recipe provided
     * @param xPosition the x position in pixels of the left of the CraftBox,
     *                 relative to the parent's (CraftingMenu) origin
     * @param yPosition the y position in pixels of the bottom of the
     *                  CraftBox relative to the parent's (CraftingMenu) origin
     * @param recipe the recipe that this CraftBox represents
     */
    public CraftBox(float xPosition, float yPosition, Recipe recipe) {
        //Initialise this as a null Image
        super(null, Scaling.fit, Align.center);
        //Connects the CraftBox with its corresponding recipe
        this.recipe = recipe;
        //Create the Drawable object that is the items image
        TextureRegionDrawable recipeDrawable = new TextureRegionDrawable(
                new Texture(Gdx.files.internal(recipe.getImageAddress() )) );
        //Set this drawable as what to display when the recipe can be crafted
        this.availableDrawable = recipeDrawable;
        //Set this drawable to what to display when the recipe can't be crafted
        this.unavailableDrawable = recipeDrawable.tint(Color.DARK_GRAY);
        //Default to the recipe not being craftable
        this.canCraft = false;
        //Update the visual to display the proper Drawable
        this.updateImage();

        //Set the CraftBox's position and dimensions
        this.setScaling(Scaling.fit);
        this.setPosition(xPosition, yPosition);
        this.setSize(IMAGESIZE, IMAGESIZE);
        this.setTouchable(Touchable.enabled);
        this.setBounds(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());

    }

    //Get Methods ==============================================================

    /**
     * Returns this CraftBoxes corresponding recipe
     * @return the Recipe that this instance represents
     */
    public Recipe getRecipe() {
        return this.recipe;
    }

    //Set Methods ==============================================================

    /**
     * sets the boolean value of canCraft to the given boolean
     * @param canCraft true if the recipe can currently be crafted, false
     *                 otherwise
     */
    public void setCanCraft(boolean canCraft) {
        this.canCraft = canCraft;
    }

    //Other Methods ============================================================

    /**
     * Updates the currently shown image to the correct image
     * ie the recipe cannot be crafted anymore so it will change the image
     * shown to the unavailable image
     */
    private void updateImage() {
        if (this.canCraft) {
            this.setDrawable(this.availableDrawable);
        } else {
            this.setDrawable(this.unavailableDrawable);
        }
    }

    /**
     * Draws the CraftBox. ie it draws the image of the recipe
     * @param batch the batch's configured to draw in the parent's coordinate
     *             system
     * @param parentAlpha the parent alpha, to be multiplied with this
     *                    actor's alpha, allowing the parent's alpha to
     *                    affect all children
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        updateImage(); //makes sure the correct image is being shown
        super.draw(batch, parentAlpha);
    }

    //Static Methods ===========================================================

    /**
     * returns the size of all CraftBoxes images size. Note that they are all
     * squares and thus IMAGESIZE = height = width
     * @return the integer representing the height/width of the image in pixels
     */
    public static int getImageSize() {
        return IMAGESIZE;
    }

}
