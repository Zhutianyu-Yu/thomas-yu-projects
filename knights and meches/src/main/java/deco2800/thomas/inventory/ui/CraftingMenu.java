package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.scenes.scene2d.Group;
import deco2800.thomas.crafting.ItemCrafter;

import java.util.ArrayList;

/**
 * Inventory menu subgroup for the crafting menu
 */
class CraftingMenu extends Group {

    /**
     * The crafter that this menu will display
     */
    private ItemCrafter crafter;

    /**
     * The list of craftBoxes that this menu is displaying
     */
    private ArrayList<CraftBox> craftBoxList;

    /**
     * The maximum amount of rows of recipes that can be shown in the
     * crafting menu
     */
    private final int maxGridRows;

    /**
     * The maximum amount of columns of recipes that can be shown in the
     * crafting menu
     */
    private final int maxGridColumns;

    /**
     * The x offset from the side of the box that the grid containing the
     * crafting recipes sits
     */
    private static final int GRID_EXTERNAL_X_PADDING = 20;

    /**
     * The y offset from the side of the box that the grid containing the
     * crafting recipes sits
     */
    private static final int GRID_EXTERNAL_Y_PADDING = 20;

    /**
     * The x gap between the each image of the recipes in the crafting menu
     */
    private static final int IMAGE_X_GAP = 20;

    /**
     * The y gap between the each image of the recipes in the crafting menu
     */
    private static final int IMAGE_Y_GAP = 20;

    /**
     * The height of the crafting menu box
     */
    private static final int MENU_HEIGHT = 325;

    /**
     * The width of the crafting menu box
     */
    private static final int MENU_WIDTH = 780;

    //Constructor ==============================================================


    /**
     * Creates new instance of the crafting menu group
     */
    CraftingMenu(ItemCrafter crafter) {

        this.crafter = crafter;
        this.crafter.startingCrafter();

        this.craftBoxList = new ArrayList<>();

        //Set the maximum size of the CraftBox grid
        this.maxGridColumns =
                (MENU_WIDTH - 2 * GRID_EXTERNAL_X_PADDING + IMAGE_X_GAP) /
                          (CraftBox.getImageSize() + IMAGE_X_GAP);

        System.out.printf("Columns: %d %n", maxGridColumns);
        this.maxGridRows =
                (MENU_HEIGHT - 2 * GRID_EXTERNAL_Y_PADDING + IMAGE_Y_GAP) /
                        (CraftBox.getImageSize() + IMAGE_Y_GAP);
        System.out.printf("Rows: %d %n", maxGridRows);

        //Create the CraftBoxes so that crafting can take place in the inventory
        System.out.printf("CRAFTING LIST:%n");
        for (int i = 0; i < Math.min(maxGridRows * maxGridColumns,
                this.crafter.getNumRecipesHeld()); i++) {
            this.craftBoxList.add(new CraftBox(getRecipeX(i), getRecipeY(i),
                    this.crafter.getRecipeList().get(i)));
            this.addActor(this.craftBoxList.get(i));
            System.out.printf("%s %n", this.craftBoxList.get(i)
                    .getRecipe().getRecipeName());
        }
        this.updateCraftBoxes();
    }

    /**
     * Gets the crafter that this menu represents
     * @return The ItemCrafter that this CraftingMenu displays
     */
    public ItemCrafter getCrafter() {
        return this.crafter;
    }

    /**
     * gets the x position of where the questioned image should be
     * @param recipeIndex the index of the image
     * @return the x value in pixels of where to place this image (in
     * relation to the crafting menu origin
     */
    private int getRecipeX(int recipeIndex) {
        int columnIndex = recipeIndex % maxGridColumns;
        return GRID_EXTERNAL_X_PADDING + columnIndex *
                (CraftBox.getImageSize() + IMAGE_X_GAP);
    }

    /**
     * gets the y position of where the questioned image should be
     * @param recipeIndex the index of the image
     * @return the y value in pixels of where to place this image (in
     * relation to the crafting menu origin
     */
    private int getRecipeY(int recipeIndex) {
        int rowIndex = recipeIndex / maxGridColumns;
        return 325 - GRID_EXTERNAL_Y_PADDING - (rowIndex + 1) * CraftBox.getImageSize() -
                rowIndex * IMAGE_Y_GAP;
    }

    //Other Methods ============================================================

    /**
     * Updates the images shown for each craftBox in this menu
     */
    public void updateCraftBoxes() {
        for (CraftBox craftBox : this.craftBoxList) {
            craftBox.setCanCraft(this.crafter.isCraftable(craftBox.getRecipe()))
            ;
        }
    }


}
