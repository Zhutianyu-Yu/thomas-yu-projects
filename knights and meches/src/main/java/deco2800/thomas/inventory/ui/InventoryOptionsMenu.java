package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Equipment.WoodenSword;

import java.util.regex.Pattern;


/**
 * Creates a options menu where players can see information and perform action on an item
 *
 * Width: 1/4 of InventoryMenu width.
 * Height: Around 3/4 of InventoryMenu height. dependant on size of description. 1/4 without description
 */
public class InventoryOptionsMenu extends Group {

    /**
     * Creates instance of font for item names to be written as
     */
    private BitmapFont chalkBoardFont = new BitmapFont(Gdx.files.internal("inventory/FontFiles/clackboard.fnt"));
    private BitmapFont starWarsFont = new BitmapFont(Gdx.files.internal("inventory/FontFiles/starwar.fnt"));

    private BitmapFont newTimesRomanFontNormal = new BitmapFont(Gdx.files.internal("inventory/FontFiles/NewTimesRoman_20.fnt"));
    private BitmapFont newTimesRomanFontBold = new BitmapFont(Gdx.files.internal("inventory/FontFiles/NewTimesRoman_Bold_20.fnt"));
    private BitmapFont newTimesRomanFontItalic = new BitmapFont(Gdx.files.internal("inventory/FontFiles/NewTimesRoman_Italic_20.fnt"));

    private BitmapFont comicSansReg12 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/12_regular.fnt"));
    private BitmapFont comicSansBold14 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/14_bold.fnt"));
    private BitmapFont comicSansBold24 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/24_bold.fnt"));

    private Label.LabelStyle comicSansReg12LabelStyle = new Label.LabelStyle();
    private Label.LabelStyle comicSansBold14LabelStyle = new Label.LabelStyle();
    private Label.LabelStyle comicSansBold24LabelStyle = new Label.LabelStyle();


    private Label.LabelStyle newTimesRomanNormalLabel = new Label.LabelStyle();
    private Label.LabelStyle newTimesRomanBoldLabel = new Label.LabelStyle();
    private Label.LabelStyle newTimesRomanItalicLabel = new Label.LabelStyle();

    private Label.LabelStyle chalkBoardLabelStyle = new Label.LabelStyle();
    private Label.LabelStyle starWarsLabelStyle = new Label.LabelStyle();

    private AbstractItem item;
    private float x;
    private float y;
    private float width = 200;

    private int xCoord;
    private int yCoord;

    private int itemQuantity;


    private int numberOfButtons;

    /** 0 =  nothing, 1 = drop, 2 = eat, 3 = equip weapon, 4 = equip material, 5 = equip power, 6 = equip armour*/
    private int buttonOneFunction;
    private int buttonTwoFunction;

    private Image buttonOne;
    private Image buttonTwo;

    private Image outline;
    private Image middleLine;
    private Label rightLabel;
    private Label leftLabel;

    private Image outline1;
    private Image base;
    private Table table;

    private static final String COLOR_A = "1b214c";
    private static final String COLOR_B = "0aa7ff";
    private static final String WHITE_BOX_TEMPLATE = "inventory/whiteBoxtemplatesize.jpg";



    public InventoryOptionsMenu(AbstractItem item, float x, float y, int itemQuantity, int xPosition, int yPosition) {

        this.item = item;
        this.itemQuantity = itemQuantity;

        buttonFunctionSetup();

        this.y = y;
        this.x = x + 30;

        this.xCoord = xPosition;
        this.yCoord = yPosition;

        initLabelStyles();
        createInfoDisplay();

        createButtons();
    }

    private void createButtons() {
        if (this.numberOfButtons == 0) {
            return;
        }
        this.y -= 70;
        if (this.numberOfButtons == 1) {
            oneButtonSetup();
        } else if (this.numberOfButtons == 2) {
            twoButtonSetup();
        }
    }

    private void checkBounds(float margins) {

        if (this.x - margins/2 - 10 + outline1.getWidth() > Gdx.graphics.getWidth()) {
            this.x -= this.x - margins/2 - 10 + outline1.getWidth() - Gdx.graphics.getWidth();
        }



    }

    private void buttonFunctionSetup() {
        String[] parts = this.item.getClass().getName().split(Pattern.quote("."));
        switch (parts[3]) {
            case "Consumable":
                this.buttonOneFunction = 1;
                this.buttonTwoFunction = 2;
                break;
            case "Equipment":
                if (this.item instanceof IronArmour || this.item instanceof WoodenArmour) {
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 4;
                }  else if (this.item instanceof IronSword || this.item instanceof WoodenSword) {
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 3;
                }
                break;
            case "Material":
                this.buttonOneFunction = 1;
                this.buttonTwoFunction = 0;
                break;
            case "MechComponent":
                this.buttonOneFunction = 1;
                this.buttonTwoFunction = 5;
                break;
            default:
                this.buttonOneFunction = 0;
                this.buttonTwoFunction = 0;
                break;
        }
        if (this.buttonOneFunction != 0) {
            this.numberOfButtons++;
        }
        if (this.buttonTwoFunction != 0) {
            this.numberOfButtons++;
        }
    }



    private void initLabelStyles() {
        this.newTimesRomanNormalLabel.font = this.newTimesRomanFontNormal;
        this.newTimesRomanBoldLabel.font = this.newTimesRomanFontBold;
        this.newTimesRomanItalicLabel.font = this.newTimesRomanFontItalic;

        this.newTimesRomanBoldLabel.font.setColor(Color.WHITE);

        this.comicSansReg12.getData().setScale(0.4f);


        this.comicSansReg12LabelStyle.font = this.comicSansReg12;
        this.comicSansBold14LabelStyle.font = this.comicSansBold14;
        this.comicSansBold24LabelStyle.font = this.comicSansBold24;
    }


    private void createInfoDisplay() {
        this.y = y + 30;
        if (this.numberOfButtons != 0) {
            this.y = y + 70;
        }
        this.table = new Table();
        this.chalkBoardLabelStyle.font = this.chalkBoardFont;
        this.starWarsLabelStyle.font = this.starWarsFont;
        initLabelStyles();

        Label.LabelStyle labelStyle1 = new Label.LabelStyle();
        labelStyle1.fontColor = Color.BLACK;


        Label itemName = new Label(item.getItemName(), this.comicSansBold24LabelStyle);
        itemName.setWrap(false);


        String quantityString = "Quantity: " + this.itemQuantity;
        Label quantityLabel = new Label(quantityString, this.comicSansBold14LabelStyle);
        quantityLabel.setWrap(true);


        String valueString = "Value: " + (this.item.getItemValue() * this.itemQuantity) + "g";
        Label valueLabel = new Label(valueString, this.comicSansBold14LabelStyle);
        valueLabel.setWrap(true);


        String descriptionHeaderString = "Description: ";
        Label descriptionHeaderLabel = new Label(descriptionHeaderString , this.comicSansBold14LabelStyle);
        descriptionHeaderLabel.setWrap(true);


        String descriptionString = this.item.getItemDescription();
        Label descriptionLabel = new Label(descriptionString , this.comicSansReg12LabelStyle);
        descriptionLabel.setWrap(true);




        this.table.add(itemName).prefSize(width, 22f).row();
        this.table.add(quantityLabel).prefSize(width, 22f).row();
        this.table.add(valueLabel).prefSize(width, 22f).row();
        this.table.add(descriptionHeaderLabel).prefSize(width, 22f).row();
        this.table.add(descriptionLabel).width(width).row();

        this.table.pack();

        float margins = 40;
        Image template = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        template.setSize(this.width + margins, this.table.getHeight() + margins);
        template.setColor(Color.valueOf(COLOR_A));

        Image template2 = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        template2.setSize(this.width + margins + 20, this.table.getHeight() + margins + 20);
        template2.setColor(Color.valueOf(COLOR_B));



        this.outline1 = template2;

        checkBounds(margins);

        this.table.setPosition(x, y);
        template.setPosition(this.x - margins/2, this.y - margins/2);
        template2.setPosition(this.x - margins/2 - 10, this.y - margins/2 - 10);

        this.outline1 = template2;
        this.base = template;
    }
    private void oneButtonSetup() {
        float margins = 40;
        float height = 20;
        float xOffset = 0;

        Image back = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        back.setSize(this.width + margins, height + margins);
        back.setColor(Color.valueOf(COLOR_A));

        this.outline = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        this.outline.setSize(this.width + margins + 20, height + margins + 20);
        this.outline.setColor(Color.valueOf(COLOR_B));


        Image leftButton = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        leftButton.setSize(this.width + margins, height + margins);
        leftButton.setColor(0,1,0,0.7f);
        leftButton.setTouchable(Touchable.enabled);
        this.buttonOneFunction = 1;
        this.buttonOne = leftButton;

        Label leftButtonLabel = new Label(getButtonName(this.buttonOneFunction), this.comicSansBold24LabelStyle);

        // setting positions
        back.setPosition(xOffset + this.x - margins/2, this.y - margins/2);
        this.outline.setPosition(xOffset + this.x - margins/2 - 10, this.y - margins/2 - 10);
        leftButton.setPosition(xOffset + this.x - margins/2, this.y - margins/2);

        this.x = xOffset + this.x - margins/2;
        this.y = this.y - margins/2;
        float backWidth = back.getWidth();
        float backHeight = back.getHeight();
        float labelWidth = leftButtonLabel.getWidth();
        float labelHeight = leftButtonLabel.getHeight();


        leftButtonLabel.setPosition(this.x + backWidth/2 - labelWidth/2, this.y + backHeight/2 - labelHeight/2);

        leftButtonLabel.setTouchable(Touchable.disabled);

        // set bounds
        leftButton.setBounds(leftButton.getX(), leftButton.getY(), leftButton.getWidth(), leftButton.getHeight());
        leftButton.setName("left Button");
        this.addActor(leftButton);

        this.leftLabel = leftButtonLabel;
    }

    private void twoButtonSetup() {
        float margins = 40;
        float height = 20;
        float xOffset = 0;
        Image back = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        back.setSize(this.width + margins, height + margins);
        back.setColor(Color.valueOf(COLOR_A));

        this.outline = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        this.outline.setSize(this.width + margins + 20, height + margins + 20);
        this.outline.setColor(Color.valueOf(COLOR_B));

        this.middleLine = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        this.middleLine.setSize(10, height + margins + 20);
        this.middleLine.setColor(Color.valueOf(COLOR_B));


        Image leftButton = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        leftButton.setSize((this.width + margins - 10) / 2, height + margins);
        leftButton.setColor(0,1,0,0.7f);
        leftButton.setTouchable(Touchable.enabled);
        this.buttonOneFunction = 1;
        this.buttonOne = leftButton;

        Image rightButton = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        rightButton.setSize((this.width + margins - 10) / 2, height + margins);
        rightButton.setColor(1,0,0,0.7f);
        rightButton.setTouchable(Touchable.enabled);
        this.buttonTwo = rightButton;

        Image template4 = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        template4.setSize(10, height + margins + 20);
        template4.setColor(Color.valueOf(COLOR_B));

        float baseY = this.y - margins/2;
        float boxHeight = height + margins;
        float internalHeight = 1;


        Image two = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        two.setSize((this.width + margins + 20)/2, 1);
        two.setColor(Color.valueOf(COLOR_B));

        Image three = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        three.setSize((this.width + margins + 20)/2, 1);
        three.setColor(Color.valueOf(COLOR_B));

        Image four = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        four.setSize(1, boxHeight);
        four.setColor(Color.valueOf(COLOR_B));

        Image five = new com.badlogic.gdx.scenes.scene2d.ui.Image(new Texture(Gdx.files.internal(WHITE_BOX_TEMPLATE)));
        five.setSize(1, boxHeight);
        five.setColor(Color.valueOf(COLOR_B));

        Label leftButtonLabel = new Label(getButtonName(this.buttonOneFunction), this.comicSansBold24LabelStyle);
        Label rightButtonLabel = new Label(getButtonName(this.buttonTwoFunction), this.comicSansBold24LabelStyle);

        // setting positions
        back.setPosition(xOffset + this.x - margins/2, this.y - margins/2);

        this.outline.setPosition(xOffset + this.x - margins/2 - 10, this.y - margins/2 - 10);
        this.middleLine.setPosition(xOffset + this.x + this.width/2 - 5, this.y - margins/2 - 10);
        this.middleLine.setPosition(xOffset + this.x + this.width/2 - 5, this.y - margins/2 - 10);
        leftButton.setPosition(xOffset + this.x - margins/2, this.y - margins/2);
        rightButton.setPosition(xOffset + this.x + this.width/2 + 5, this.y - margins/2);
        template4.setPosition(this.x + xOffset - margins - 10, this.y - margins/2 - 10);
        two.setPosition(xOffset + this.x + this.width/2 - 0.5f, baseY + (boxHeight/2) - (internalHeight/2));
        three.setPosition(xOffset + this.x - margins/2, baseY + (boxHeight/2) - (internalHeight/2));
        four.setPosition(xOffset + this.x + (1*this.width/4) - margins/2 + 5, baseY);
        five.setPosition(xOffset + this.x + (3*this.width/4) + margins/2 - 5, baseY);

        leftButtonLabel.setOrigin(leftButtonLabel.getWidth(), leftButtonLabel.getHeight());
        leftButtonLabel.setPosition(four.getX() - leftButtonLabel.getWidth()/2, two.getY() - leftButtonLabel.getHeight()/2);
        leftButtonLabel.setTouchable(Touchable.disabled);

        rightButtonLabel.setOrigin(rightButtonLabel.getWidth(), rightButtonLabel.getHeight());
        rightButtonLabel.setPosition(five.getX() - rightButtonLabel.getWidth()/2, three.getY() - rightButtonLabel.getHeight()/2);
        rightButtonLabel.setTouchable(Touchable.disabled);

        // set bounds
        leftButton.setBounds(leftButton.getX(), leftButton.getY(), leftButton.getWidth(), leftButton.getHeight());
        rightButton.setBounds(rightButton.getX(), rightButton.getY(), rightButton.getWidth(), rightButton.getHeight());

        leftButton.setName("left Button");
        rightButton.setName("right Button");

        this.addActor(leftButton);
        this.addActor(rightButton);

        this.rightLabel = rightButtonLabel;
        this.leftLabel = leftButtonLabel;
    }

    private String getButtonName(int number) {
        switch (number) {
            case 0:
                return "?";
            case 1:
                return "DROP";
            case 2:
                return "USE";
            case 3:
                return "EQUIP";
            case 4:
                return "EQUIP";
            case 5:
                return "Upgrade"; //mech components
            default:
                return "Unknown";
        }
    }

    Actor getButtonOne() {
        return this.buttonOne;
    }

    Actor getButtonTwo() {
        return this.buttonTwo;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (this.outline1 != null) {
            this.outline1.draw(batch, parentAlpha);
        }
        if (this.base != null) {
            this.base.draw(batch, parentAlpha);
        }
        if (this.table != null) {
            this.table.draw(batch, parentAlpha);
        }
        if (this.outline != null) {
            this.outline.draw(batch, parentAlpha);
        }
        super.draw(batch, parentAlpha);
        if (this.rightLabel != null) {
            this.rightLabel.draw(batch, parentAlpha);
        }
        if (this.leftLabel != null) {
            this.leftLabel.draw(batch, parentAlpha);
        }
        if (this.middleLine != null) {
            this.middleLine.draw(batch, parentAlpha);
        }
    }

    int getXCoord() {
        return this.xCoord;
    }

    int getYCoord() {
        return this.yCoord;
    }


    public int getButtonOneFunction() {
        return this.buttonOneFunction;
    }

    public int getButtonTwoFunction() {
        return this.buttonTwoFunction;
    }

    public AbstractItem getItem() {
        return this.item;
    }
}
