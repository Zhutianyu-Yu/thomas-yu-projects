package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

/**
 * This is a test SellButton in inventory to test the implementation of selling
 * This will be changed to the TraderNPC SellButton in Sprint 4
 */
public class SellButton extends Image {

    public SellButton() {
        super(null, Scaling.fit, Align.center);
        TextureRegionDrawable sellButtonDrawable = new TextureRegionDrawable(
                new Texture(Gdx.files.internal("NPC/Button_sell.png")));
        this.setDrawable(sellButtonDrawable);
        this.setScaling(Scaling.fit);
        this.setSize(150, 100);
        this.setPosition(0, 0);
        this.setTouchable(Touchable.enabled);
        this.setBounds(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());

    }

}
