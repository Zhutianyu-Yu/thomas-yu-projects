package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.entities.*;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.MechInventory;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.managers.GameManager;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import deco2800.thomas.managers.SoundManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * InventoryMenu subgroup for the mech menu
 */
class MechMenu extends Group {
    /** width of group */
    private int width;
    /** height of group */
    private int height;
    private InventoryAbstract inventory;
    private MechInventory mechInventory;
    private ImageButton knightButton1;
    private ImageButton knightButton2;
    private ImageButton wizardButton1;
    private ImageButton wizardButton2;
    private Image mechImg;
    private ImageButton equipment1;
    private ImageButton equipment2;
    private ImageButton equipment3;

    /** Holds Bitmap fonts of the text being displayed */
    private BitmapFont font = new BitmapFont(Gdx.files.internal("inventory/FontFiles/pixelFJverdana12pt.fnt"));
    /** Holds Bitmap fonts of the text being displayed */
    private BitmapFont font1 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/pixelFJverdana12pt.fnt"));
    /** Holds Bitmap fonts of the text being displayed */
    private BitmapFont font2 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/pixelFJverdana12pt.fnt"));

    /** Glyphlayout of the knight header */
    private GlyphLayout knightHeader;
    private GlyphLayout wizardHeader;
    private GlyphLayout mechHeader;

    private String knight = "Knight";
    private String wizard = "Wizard";

    private static final String WIZARDPATH = "inventory/mech/wizard_silhouette.png";

    private final Logger LOG = LoggerFactory.getLogger(MechMenu.class);
    /**
     * Creates new instance of MechMenu
     *
     * @param width of group
     * @param height of group
     */
    MechMenu(int width, int height, InventoryAbstract i, MechInventory mechInventory) {
        this.inventory = i;
        this.mechInventory = mechInventory;
        this.width = width;
        this.height = height;
        getAllWizardCharacters();
        getAllKnightCharacters();
        mechUI(this.width, this.height);
        wizardUI(this.width, this.height);
        knightUI(this.width , (this.height * 3) / 4);
        createFonts();


    }

    /**
     * creates mech section of the group
     *
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    private void mechUI(int width, int height) {
        int numberOfSlots = 3;
        int slotYMargin = height - 450;
        int slotXMargin = (width / 6);
        int slotSize = 60;



        setMech(width,height);

        // Inventory section for the Equipment
        Image slot0 = new Image(new Texture(Gdx.files.internal("inventory/inventoryBox.png")));
        slot0.setSize(slotSize, slotSize);
        slot0.setPosition( slotXMargin - slotSize / 2, slotYMargin / 3 - slotSize / 2);
        this.addActor(slot0);

        // Section for the armour
        Image slot2 = new Image(new Texture(Gdx.files.internal("inventory/inventoryBox.png")));

        slot2.setSize(slotSize, slotSize);
        slot2.setPosition((2* width / numberOfSlots) + slotXMargin - 67 / 2, slotYMargin / 3 - slotSize / 2);
        this.addActor(slot2);

        setEquipment3();

    }

    /**
     * creates wizard section of the group
     *
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    private void wizardUI(int width, int height) {
        if (mechInventory.getWizardStatus(1)){
            setWizardButton1(width,height);
        }
        if (mechInventory.getWizardStatus(2)){
            setWizardButton2(width,height);
        }

    }

    /**
     * creates Knight section of the group
     *
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    private void knightUI(int width, int height) {
        if (mechInventory.getKnightStatus(1)){
            setKnightButton1(width,height);
        }
        if (mechInventory.getKnightStatus(2)){
            setKnightButton2(width,height);
        }

    }

    /**
     * set the weapon
     */
    public void setEquipment1(){
        int slotYMargin = height - 450;
        int slotXMargin = (width / 6);
        int slotSize = 75;
        AbstractItem weapon = inventory.getWeapon();
        if (weapon!=null){
            String imagePath =getImage(weapon);
            equipment1 = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal(imagePath)))));
            equipment1.setSize(slotSize, slotSize);
            equipment1.setPosition( slotXMargin - slotSize / 2, slotYMargin / 3 - slotSize / 2);
            this.addActor(equipment1);
        }

    }

    /**
     * set the metal
     */
    public void setEquipment2(){
        int numberOfSlots = 3;
        int slotYMargin = height - 450;
        int slotXMargin = (width / 6);

        int slotSize = 75;
        AbstractItem armour = inventory.getArmour();

        String imagePath = getImage(armour);

        equipment2 = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal(imagePath)))));
        equipment2.setSize(slotSize, slotSize);
        equipment2.setPosition( (2* width / numberOfSlots) + slotXMargin - 67 / 2, slotYMargin / 3 - slotSize / 2);

        this.addActor(equipment2);
    }

    /**
     * set the power
     */
    public void setEquipment3(){
        int numberOfSlots = 3;
        int slotYMargin = height - 450;
        int slotXMargin = (width / 6);
        int slotSize = 77;
        String imagePath = "";

        if (inventory.getNumOfOrbs()==1){
            imagePath="inventory/submenu/1orb.png";
        }else if (inventory.getNumOfOrbs()==2){
            imagePath="inventory/submenu/2orb.png";
        }else if (inventory.getNumOfOrbs()==3){
            imagePath="inventory/submenu/3orb.png";
        }else if (inventory.getNumOfOrbs()==4){
            imagePath="inventory/submenu/4orb.png";
        } else if (inventory.getNumOfOrbs()==5){
            imagePath="inventory/submenu/5orb.png";
        }else {
            imagePath="inventory/submenu/0orb.png";
        }

        equipment3 = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal(imagePath)))));
        equipment3.setSize(slotSize, slotSize);
        equipment3.setPosition((width / numberOfSlots) + slotXMargin - slotSize / 2, slotYMargin / 3 - slotSize / 2+2);
        this.addActor(equipment3);


    }

    /**
     * set the mech
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void setMech(int width, int height){


        int slotYMargin = height - 500;


        int mechSize = 175;




        if (mechInventory.getActiveKnight()==1 && mechInventory.getActiveWizard()==1){
            mechImg = new Image(new Texture("player/mech/base_mech.png"));

        }
        if (mechInventory.getActiveKnight()==1 && mechInventory.getActiveWizard()==2){
            mechImg = new Image(new Texture(Gdx.files
                    .internal("player/mech/blue_mech.png")));
        }

        if (mechInventory.getActiveKnight()==2 && mechInventory.getActiveWizard()==1){
            mechImg = new Image(new Texture(Gdx.files
                    .internal("player/mech/green_mech.png")));
        }
        if (mechInventory.getActiveKnight()==2 && mechInventory.getActiveWizard()==2){
            mechImg = new Image(new Texture(Gdx.files
                    .internal("player/mech/red_mech.png")));
        }

        mechImg.setSize(mechSize, mechSize);
        mechImg.setPosition( (width - mechSize) / 2, slotYMargin - mechSize / 2);
        this.addActor(mechImg);

    }

    /**
     * set the first knight
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void setKnightButton1(int width, int height){

        int slotYMargin = height + (this.height / 4) / 3;
        int slotXMargin = (width / 6+30);
        int slotSize = 80;
        Texture knight1Up = new Texture(Gdx.files.internal(WIZARDPATH));
        if (mechInventory.getActiveKnight()==1){
            knight1Up = new Texture((Gdx.files.internal("PlayerCharacterMech/Knights N-01.png")));
        }
        knightButton1 =
                new ImageButton(new TextureRegionDrawable(new TextureRegion(knight1Up)));
        knightButton1.setSize(slotSize, slotSize);
        knightButton1.setPosition( slotXMargin - slotSize / 2, slotYMargin - slotSize / 2);
        knightButton1.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                resetButton("knight",1,width,height);
                resetButton("knight",2,width,height);

                resetMechImg(width,(height*4)/3);
                LOG.info("knight1");
                GameManager.getManagerFromInstance(SoundManager.class).playSound("changeStatus.wav");
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                mechInventory.activeKnight(1);
                changeMechCharacter(knight, 1);
                return true;
            }
        });
        this.addActor(knightButton1);

    }

    /**
     * set the second knight
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void setKnightButton2(int width, int height){
        int numberOfSlots = 2;
        int slotYMargin = height + (this.height / 4) / 3;
        int slotXMargin = (width / 6+30);
        int slotSize = 80;
        Texture knight2Up = new Texture(Gdx.files.internal(WIZARDPATH));
        if (mechInventory.getActiveKnight()==2){
            knight2Up = new Texture(Gdx.files.internal("player/knight/knight_idle.png"));
        }
        knightButton2 = new ImageButton(new TextureRegionDrawable(new TextureRegion(knight2Up)));;
        knightButton2.setSize(slotSize, slotSize);
        knightButton2.setPosition( (width / numberOfSlots) + slotXMargin - slotSize / 2, slotYMargin - slotSize / 2);
        knightButton2.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                resetButton("knight",1,width,height);
                resetButton("knight",2,width,height);
                resetMechImg(width,(height*4)/3);
                LOG.info("knight2");
                GameManager.getManagerFromInstance(SoundManager.class).playSound("changeStatus.wav");
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                mechInventory.activeKnight(2);
                changeMechCharacter(knight, 2);
                return true;
            }
        });
        this.addActor(knightButton2);
    }


    /**
     * set the fitst wizard
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void setWizardButton1(int width, int height){
        int slotYMargin = height - 275;
        int slotXMargin = (width / 6)+30;
        int slotSize = 80;
        Texture wizard1Up = new Texture(Gdx.files.internal(WIZARDPATH));
        if(mechInventory.getActiveWizard()==1){
            wizard1Up = new Texture(Gdx.files.internal("PlayerCharacterMech/Witch N 1.png"));
        }
        wizardButton1 = new ImageButton(new TextureRegionDrawable(new TextureRegion(wizard1Up)));
        wizardButton1.setSize(slotSize, slotSize);
        wizardButton1.setPosition( slotXMargin - slotSize / 2, slotYMargin - slotSize / 2);
        wizardButton1.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                resetButton("wizard",1,width,height);
                resetButton("wizard",2,width,height);
                resetMechImg(width,height);
                LOG.info("wizard1");
                GameManager.getManagerFromInstance(SoundManager.class).playSound("changeStatus.wav");
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                mechInventory.activeWizard(1);
                changeMechCharacter(wizard, 1);
                return true;
            }
        });
        this.addActor(wizardButton1);
    }

    /**
     * set the second wizard
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void setWizardButton2(int width, int height){
        int numberOfSlots = 2;
        int slotYMargin = height - 275;
        int slotXMargin = (width / 6)+30;
        int slotSize = 80;
        Texture wizard2Up = new Texture(Gdx.files.internal(WIZARDPATH));
        if (mechInventory.getActiveWizard()==2){
            wizard2Up = new Texture(Gdx.files.internal("player/wizard/wizard_idle.png"));
        }
        wizardButton2 = new ImageButton(new TextureRegionDrawable(new TextureRegion(wizard2Up)));
        wizardButton2.setSize(slotSize, slotSize);
        wizardButton2.setPosition( (width / numberOfSlots) + slotXMargin - slotSize / 2, slotYMargin - slotSize / 2);
        wizardButton2.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                resetButton("wizard",1,width,height);
                resetButton("wizard",2,width,height);
                resetMechImg(width,height);
                LOG.info("wizard2");
                GameManager.getManagerFromInstance(SoundManager.class).playSound("changeStatus.wav");
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                mechInventory.activeWizard(2);
                changeMechCharacter(wizard, 2);
                return true;
            }
        });
        this.addActor(wizardButton2);
    }


    /**
     * reset the knight and wizard button
     * @param s "knight" or "wizard"
     * @param i the serial number (which knight or wizard)
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void resetButton(String s, int i,int width,int height){
        if (s.equals("knight")){
            if (i == 1 && mechInventory.getKnightStatus(1)){
                knightButton1.remove();
                setKnightButton1(width,height);
            }else if (i==2 && mechInventory.getKnightStatus(2)){
                knightButton2.remove();
                setKnightButton2(width,height);
            }

        }else if (s.equals("wizard")){
            if (i == 1 && mechInventory.getWizardStatus(1)){
                wizardButton1.remove();
                setWizardButton1(width,height);
            }else if (i==2 && mechInventory.getWizardStatus(2)){
                wizardButton2.remove();
                setWizardButton2(width,height);
            }
        }
    }

    /**
     * reset the mech
     * @param width starting width, where bottom left is x,y
     * @param height starting height, where bottom left is x,y
     */
    public void resetMechImg(int width,int height ){
        mechImg.remove();
        setMech(width,height);
    }

    /**
     * reset the equipment
     * @param i serial number 1.weapon 2. metal 3. power
     */
    public void resetEquipment(int i){
        if (i==1){
            if(inventory.getWeapon()!=null){
                if (equipment1!=null){
                    equipment1.remove();
                }
                setEquipment1();
            }

        }else if (i==2){
            if (inventory.getArmour()!=null){
                if (equipment2!=null){
                    equipment2.remove();
                }
                setEquipment2();
            }
        }else if (i==3){
            equipment3.remove();
            setEquipment3();
        }
    }

    /**
     * get item's image path
     * @param item the target item
     * @return the image path
     */
    private String getImage(AbstractItem item) {
        try {
            String emptyString = "";
            String[] parts = item.getClass().getName().split(Pattern.quote("."));
            return emptyString.concat(parts[2] + "/" + parts[3] + "/" + item.getImgName());
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    /**
     * get all wizard from entity and store Hashmap
     */
    public HashMap<Integer, Wizard> getAllWizardCharacters() {
        List<PlayerPeon> players = GameManager.get().getPlayersInGame();
        HashMap<Integer, Wizard> allWizards = new HashMap<>();
        int playerNum = 1;
        for (PlayerPeon entity : players) {
            //only add the Wizards
            if (entity.getType() == PlayerType.WIZARD) {
                allWizards.put(playerNum, (Wizard) entity);
                ++playerNum;
            }
        }
        return allWizards;
    }

    /**
     * get all knight from entity and store in Hashmap
     */
    public HashMap<Integer, Knight> getAllKnightCharacters() {
        List<PlayerPeon> players = GameManager.get().getPlayersInGame();
        HashMap<Integer, Knight> allKnights = new HashMap<>();
        int playerNum = 1;
        for (PlayerPeon entity : players) {
            //only add the Knights
            if (entity.getType() == PlayerType.KNIGHT) {
                allKnights.put(playerNum, (Knight) entity);
                ++playerNum;
            }
        }

        return allKnights;
    }

    public void changeMechCharacter(String characterType, int playerNumber ) {
        if (GameManager.getInGameMech()!=null){
            LOG.info("Mech !null");
            if (characterType.equals(wizard)) {
                GameManager.getInGameMech().changeMechWizard(getAllWizardCharacters().get(playerNumber));
                GameManager.get().swapPartyMembers(getAllWizardCharacters().get(playerNumber));
                GameManager.getInGameMech().changeMechTexture();

            } else if (characterType.equals(knight)) {
                GameManager.getInGameMech().changeMechKnight(getAllKnightCharacters().get(playerNumber));
                GameManager.get().swapPartyMembers(getAllKnightCharacters().get(playerNumber));
                GameManager.getInGameMech().changeMechTexture();
            }
            LOG.info(String.format("Entity Number of Wizard: %s and Knight: %s",
                    GameManager.getInGameMech().getMechWizard().getEntityID(),
                    GameManager.getInGameMech().getMechKnight().getEntityID()));
        }

    }

    /**
     * Creates the fonts
     */
    private void createFonts() {
        this.knightHeader = new GlyphLayout(font, "Knights");
        this.wizardHeader = new GlyphLayout(font1, "Wizards");
        this.mechHeader = new GlyphLayout(font2, "Mech Construction");
    }

    /**
     * Draws the fonts for the Mech menu screen
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        font.draw(batch, this.knightHeader, 180,800);
        font1.draw(batch, this.wizardHeader,180,630 );
        font2.draw(batch, this.mechHeader,120,510 );
    }
}



