package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import deco2800.thomas.crafting.ItemCrafter;
import deco2800.thomas.crafting.Recipe;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ItemBox;
import deco2800.thomas.inventory.MechInventory;
import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.ConsumableItem;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.KeyUpObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;
import deco2800.thomas.trading.Trading;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

/**
 * Creates group that holds all inventory UI
 */
public class InventoryMenu extends Group implements TouchDownObserver, KeyDownObserver, KeyUpObserver, TouchUpObserver {


    //Item Menus

    /** Item Menu Instance */
    private ItemMenu itemMenu;
    /** Mech Menu Instance */
    private MechMenu mechMenu;
    /** Crafting Menu instance */
    private CraftingMenu crafting;


    /** desired width of the Menu */
    private int templateWidth;
    /** desired height of the Menu */
    private int templateHeight;
    /** judge whether to open the sub mech ui*/
    private boolean mechSub=false;
    /** the fonts for sub menu */
    private static final String FONT_RELATIVE_PATH = "inventory/FontFiles/pixelFJverdana12pt.fnt";
    private BitmapFont mechHead1 = new BitmapFont(Gdx.files.internal(FONT_RELATIVE_PATH));
    private BitmapFont mechHead2 = new BitmapFont(Gdx.files.internal(FONT_RELATIVE_PATH));
    private BitmapFont mechHead3 = new BitmapFont(Gdx.files.internal(FONT_RELATIVE_PATH));
    private BitmapFont mechName1 = new BitmapFont(Gdx.files.internal(FONT_RELATIVE_PATH));
    private BitmapFont mechName2 = new BitmapFont(Gdx.files.internal(FONT_RELATIVE_PATH));
    private BitmapFont mechContribute1 = new BitmapFont();
    private BitmapFont mechContribute2 = new BitmapFont();
    private BitmapFont mechContribute3 = new BitmapFont();
    private GlyphLayout weaponHeader;
    private GlyphLayout orbHeader;
    private GlyphLayout armourHeader;
    private GlyphLayout weaponName;
    private GlyphLayout armourName;
    private GlyphLayout weaponContribute;
    private GlyphLayout orbContribute;
    private GlyphLayout armourContribute;
    /** Tracks weather a button is pushed before touchup is called */
    private boolean isButtonPushed = false;
    /** Stores the int of the first button pushed null if button not currently pushed */
    private Integer firstButtonPushed = null;
    /** Stores the int of the second button pushed null if button not currently pushed */
    private Integer secondButtonPushed = null;
    /** holds the Actor that was hit */
    private Actor target1 = null;
    /** holds the Actor that was hit */
    private Actor target2 = null;
    /** Holds whether the inventory is on the screen or not*/
    private boolean isInventoryDisplayed;

    private boolean isOptionsDisplayed;

    private InventoryOptionsMenu inventoryOptionsMenu;
    /** holds global instance of inventory */
    private InventoryAbstract inventoryAbstract;
    /** holds global instance of mechInventory*/
    private MechInventory mechInventory;



    /** Color to use for borders **/
    private Trading trader;

    Label currencyAmount;

    private final Logger LOG = LoggerFactory.getLogger(InventoryMenu.class);

    /**
     * Creates group for the inventory UI
     *
     * @param inventoryAbstract that is being displayed
     */
    public InventoryMenu(InventoryAbstract inventoryAbstract, MechInventory mechInventory) {
        createMenuTemplateFromScratch();
        this.inventoryAbstract = inventoryAbstract;
        this.mechInventory = mechInventory;
        this.itemMenu = new ItemMenu((Gdx.graphics.getWidth() - 450) / 2 + 10, (Gdx.graphics.getHeight() - 680) / 2 + 10, this.inventoryAbstract);

        //Setup Crafting Menu
        ItemCrafter crafter = new ItemCrafter(this.inventoryAbstract);
        crafter.startingCrafter();
        this.crafting = new CraftingMenu(crafter);
        crafting.setPosition((Gdx.graphics.getWidth() - 450) / 2 + 10, (Gdx.graphics.getHeight() + 10) / 2 + 10);
        //Setup Mech Menu
        mechMenu = new MechMenu((this.templateWidth - 450) / 2 - 10, this.templateHeight - 10, this.inventoryAbstract, mechInventory);
        mechMenu.setPosition((Gdx.graphics.getWidth() - this.templateWidth) / 2 + 10, (Gdx.graphics.getHeight() - this.templateHeight) / 2 + 10);
        //Setup Trading
        this.trader = new Trading(inventoryAbstract);
        displayCurrency("$" + Integer.toString(trader.getMoney()));

        setSubButton();
        this.addActor(mechMenu);
        this.addActor(itemMenu);
        this.addActor(crafting);
        this.isInventoryDisplayed = false;
        this.isOptionsDisplayed = false;

        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenerInventory(this);
        GameManager.getManagerFromInstance(InputManager.class).addTouchUpListenerInventory(this);
        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }

    /**
     * Creates background/template for the UI
     */
    private void createMenuTemplateFromScratch() {
        Image template = new Image(new Texture(Gdx.files.internal("inventory/inventory_bg.png")));
        template.setSize(1256,733);
        template.setPosition((Gdx.graphics.getWidth() - template.getWidth()) /2, (Gdx.graphics.getHeight() - template.getHeight()) / 2);
        this.addActor(template);

        this.templateWidth = (int) template.getWidth();
        this.templateHeight = (int) template.getHeight();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (this.isInventoryDisplayed) {
            super.draw(batch, parentAlpha);
            if (this.isOptionsDisplayed && this.inventoryOptionsMenu != null) {
                this.inventoryOptionsMenu.draw(batch, parentAlpha);
            }

            if (mechSub){
                mechHead1.draw(batch, this.weaponHeader, 290,785);
                mechHead2.draw(batch, this.orbHeader, 560,785);
                mechHead3.draw(batch, this.armourHeader, 890,785);
                mechName1.draw(batch, this.weaponName, 380+(250-(float)mechName1.getRegion().getRegionWidth())/2,660);
                mechName2.draw(batch, this.armourName, 880,660);
                mechContribute1.draw(batch, this.weaponContribute, 285,266);
                mechContribute2.draw(batch, this.armourContribute, 930,266);
                mechContribute3.draw(batch,this.orbContribute,600,271);
            }

        } else {
            this.isOptionsDisplayed = false;
            this.inventoryOptionsMenu = null;
        }
    }

    /**
     * This method will be called when the inventory changes in any way.
     * It makes sure what is shown is correct without having to be called
     * every tick in draw()
     */
    public void updateMenu() {
        itemMenu.updateInventory();
        this.crafting.updateCraftBoxes();
        this.itemMenu.updateInventory();
    }

    @Override
    public void notifyKeyDown(int keycode) {
        if (keycode == Input.Keys.I) {
            mechMenu.resetEquipment(3);
            drawInventory();
            updateMenu();
        }
    }

    @Override
    public void notifyKeyUp(int keycode) {
        //Needed to implement interface but no used as most input is handled with touch
    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
        Vector2 tmp = getStage().screenToStageCoordinates(new Vector2((float) screenX, (float) screenY));
        Actor hitActor = getStage().hit(tmp.x, tmp.y, false);
        if (this.isInventoryDisplayed) {
            try {
                hitActor.getZIndex();
            } catch (Exception e) {
                return;
            }
        } else {
            return;
        }
        if (!(handleButtonOne(hitActor) || handleButtonTwo(hitActor))) {
            resetOptionsMenu();
        }
        if (hitActor instanceof ItemBox) {
            if (button == Input.Buttons.RIGHT) {
                setOptionMenu(hitActor, tmp.x, tmp.y);
            } else if (button == Input.Buttons.LEFT && checkTarget(hitActor)) {
                if (isButtonPushed) {
                    ((ItemBox) hitActor).outlineBox();
                    target2 = hitActor;
                    isButtonPushed = false;
                    secondButtonPushed = button;
                } else {
                    ((ItemBox) hitActor).outlineBox();
                    target1 = hitActor;
                    isButtonPushed = true;
                    firstButtonPushed = button;
                }
            }
        } else if (hitActor instanceof CraftBox) {
            ItemCrafter crafter = this.crafting.getCrafter();
            Recipe recipeSelected = ((CraftBox) hitActor).getRecipe();
            if (crafter.isCraftable(recipeSelected)) {
                crafter.craft(recipeSelected);
                updateMenu();
                GameManager.getManagerFromInstance(SoundManager.class).playSound("crafting.mp3");
            }
        }
    }

    /**
     * Method to process the selling mode of the item menu
     * @param hitActor - The actor that was hit
     */
    public void sellMode(Actor hitActor, ItemMenu menu, Trading trader) {
        if (checkItemTarget(hitActor, menu) &&
                ((ItemBox)hitActor).getItem() != null) {
                /** Holds the actor being sold */
                int y = getCoordinates(hitActor.getZIndex()).getValue1();
                int x = getCoordinates(hitActor.getZIndex()).getValue0();
                LOG.info("x: " + x);
                LOG.info("y: " + y);
                if (!(this.inventoryAbstract.getItem(x, y).getItemName().equals("Orb"))) {
                    trader.sell(x,y);
                } else {
                    LOG.info("You cannot sell an orb");
                }
        }
    }

    @Override
    public void notifyTouchUp(int screenX, int screenY, int pointer, int button) {
        if (isInventoryDisplayed) {
            Vector2 tmp = getStage().screenToStageCoordinates(new Vector2((float)screenX, (float)screenY));
            Actor hitActor = getStage().hit(tmp.x, tmp.y, false);
            handleMoveItem(hitActor, button);
        }
        updateMenu();
    }

    /**
     * displays whether the inventory can be displayed or not
     */
    private void drawInventory() {
        this.isInventoryDisplayed = !this.isInventoryDisplayed;
        if (this.isInventoryDisplayed) {
            this.resetOptionsMenu();
            this.resetParameters();
            itemMenu.updateInventory();
        }
    }

    /**
     * Called when processing input and checks it the target is a UIBox instance
     *
     * @param target target that's checked
     * @return true is target is an instance of BoxUI
     */
    private boolean checkTarget(Actor target) {
        if (checkItemTarget(target, this.itemMenu)) {
            return true;
        }

        return this.crafting.getChildren().contains(target, true);
    }

    /**
     * Called when processing input and checks it the target is a UIBox instance
     *
     * @param target target that's checked
     * @param menu the item menu
     * @return true is target is an instance of BoxUI
     */
    private boolean checkItemTarget(Actor target, ItemMenu menu) {
        for (int i = 0; i < 36; i++) {
            if (menu.getChild(i).equals(target)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param hitActor actor that was hit
     * @param button button that was hit
     */
    private void handleMoveItem(Actor hitActor, int button) {
        if (checkButtons() == 2) {
            if (checkTarget(hitActor)) {
                if (button == 0) {
                    moveItem(hitActor);
                }
            } else {
                firstButtonPushed = null;
                secondButtonPushed = null;
                isButtonPushed = false;
                resetImage(target1);
                target1 = null;
                resetImage(target2);
                target2 = null;
            }
        } else if (checkButtons() == 1){
            if (!checkTarget(hitActor)) {
                try {
                    firstButtonPushed = null;
                    secondButtonPushed = null;
                    isButtonPushed = false;
                    resetImage(target1);
                    target1 = null;
                    resetImage(target2);
                    target2 = null;
                } catch (Exception ignored) {
                    LOG.warn("Exception Thrown", ignored);
                }
            }
        } else {
            try {
                firstButtonPushed = null;
                secondButtonPushed = null;
                isButtonPushed = false;
                resetImage(target1);
                target1 = null;
                resetImage(target2);
                target2 = null;
                isButtonPushed = false;
            } catch (Exception ignored) {
                LOG.warn("Exception Thrown", ignored);
            }
        }
    }

    /**
     * Called when two inventory boxes are pushed after each other and swaps the contents of the inventory boxes
     *
     * @param hitActor The actor that was hit by the touch input
     */
    private void moveItem(Actor hitActor) {
        if (isButtonPushed || target1.equals(target2)) {
            if (!hitActor.equals(target1)) {
                try {
                    resetImage(target1);
                    target1 = null;
                    isButtonPushed = false;
                } catch (Exception ignored) {
                    LOG.warn("Exception Thrown", ignored);
                }
            }
        } else {
            if (hitActor.equals(target2) ) {
                inventoryAbstract.moveInventory(getCoordinates(target1.getZIndex()).getValue0(), getCoordinates(target1.getZIndex()).getValue1(),
                        getCoordinates(target2.getZIndex()).getValue0(), getCoordinates(target2.getZIndex()).getValue1());
            }
            resetImage(target1);
            resetImage(target2);
            target1 = null;
            target2 = null;
            firstButtonPushed = null;
            secondButtonPushed = null;
            this.isButtonPushed = false;
        }
    }

    /**
     * Checks number of buttons pushed
     * @return 1 if one button is pushed, 2 if both are pushed and 0 if actor was pushed twice
     */
    private int checkButtons() {
        int returnNumber = 0;
        if (firstButtonPushed != null) {
            returnNumber++;
        }
        if (secondButtonPushed != null) {
            returnNumber++;

        }
        if (returnNumber == 2 && (this.target1 == this.target2)) {
            return 0;
        }
        return returnNumber;
    }

    /**
     * Called when acting on an actor in the ItemMenu
     *
     * @param i index of the actor
     * @return Pair of the y coordinate and x coordinate
     */
    private Pair<Integer, Integer> getCoordinates(int i) {
        int x = i % 9;
        int y = i / 9;
        return new Pair<>(y, x);
    }

    /**
     * Called to return Actor's color from grey back to original
     *
     * @param actor that color is being reset
     */
    private void resetImage(Actor actor) {
        ((ItemBox)actor).outlineBox();
    }

    /**
     * Checks if inventory is being displayed
     *
     * @return true is it is being displayed, false otherwise
     */
    public boolean isInventoryDisplayed() {
        return this.isInventoryDisplayed;
    }


    /**
     * Creates and sets a new instance of InventoryOptionsMenu
     *
     * @param hitActor actor that was hit
     * @param x position on screen that the options menu will be drawn if menu will fit
     * @param y position on screen that the options menu will be drawn if menu will fit
     */
    private void setOptionMenu(Actor hitActor, float x, float y) {
        int xPos = getCoordinates(hitActor.getZIndex()).getValue0();
        int yPos = getCoordinates(hitActor.getZIndex()).getValue1();
        if ((hitActor instanceof ItemBox) && (((ItemBox)hitActor).getItem() != null)) {
            this.inventoryOptionsMenu = new InventoryOptionsMenu(((ItemBox) hitActor).getItem(), x, y, this.inventoryAbstract.getInventoryQuantity(xPos, yPos), xPos, yPos);
            this.isOptionsDisplayed = true;
            this.addActor(this.inventoryOptionsMenu);
            resetParameters();

        }
    }

    /**
     * Drops item from inventory if input applies to dropping item
     *
     * @param hitActor actor thats was hit
     * @return true if action applies to drop button, otherwise false
     */
    private boolean handleButtonOne(Actor hitActor) {
        if (this.inventoryOptionsMenu == null) {
            return false;
        }
        if (hitActor.equals(this.inventoryOptionsMenu.getButtonOne())) {
            if (this.inventoryOptionsMenu.getButtonOneFunction() == 1) {
                handleDropButton();
            } else if (this.inventoryOptionsMenu.getButtonOneFunction() == 2) {
                handleConsumeButton(hitActor);
            } else if (this.inventoryOptionsMenu.getButtonOneFunction() == 3 || this.inventoryOptionsMenu.getButtonOneFunction() == 4 || this.inventoryOptionsMenu.getButtonOneFunction() ==5) {
                int i = this.inventoryOptionsMenu.getButtonOneFunction();
                handleEquipButton(i);
            } else if (this.inventoryOptionsMenu.getButtonOneFunction() == 6) {
                handleUpgradeButton();
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     *
     * @param hitActor actor that was hit
     * @return true if action applies to button two, otherwise false
     */
    private boolean handleButtonTwo(Actor hitActor) {
        if (this.inventoryOptionsMenu == null) {
            return false;
        }
        if (hitActor.equals(this.inventoryOptionsMenu.getButtonTwo())) {
            if (this.inventoryOptionsMenu.getButtonTwoFunction() == 2) {
                handleConsumeButton(hitActor);
            } else if (this.inventoryOptionsMenu.getButtonTwoFunction() >= 3) {
                int i = this.inventoryOptionsMenu.getButtonTwoFunction();
                handleEquipButton(i);
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }


    /**
     * Drops item from inventory
     */
    private void handleDropButton() {
        this.inventoryAbstract.dropInventory(this.inventoryOptionsMenu.getXCoord(), this.inventoryOptionsMenu.getYCoord());
        GameManager.getManagerFromInstance(SoundManager.class).playSound("drop_item.wav");
        resetOptionsMenu();
        resetParameters();
    }

    /**
     * Upgrades item.
     */
    private void handleUpgradeButton() {
        this.inventoryOptionsMenu.getItem().upgrade();
        resetOptionsMenu();
        resetParameters();
    }

    /**
     * Consumes item from inventory
     * @param hitActor actor that was hit
     */
    private void handleConsumeButton(Actor hitActor) {
        try {
            int x = ((InventoryOptionsMenu) hitActor.getParent()).getXCoord();
            int y = ((InventoryOptionsMenu) hitActor.getParent()).getYCoord();
            ConsumableItem item = ((ConsumableItem) ((InventoryOptionsMenu) hitActor.getParent()).getItem());
            if (item instanceof ManaPotion) {
                this.inventoryAbstract.useManaPotion(item.getCapacity(), x, y);
            } else if (item instanceof HealthPotion) {
                this.inventoryAbstract.useHealthPotion(item.getCapacity(), x, y);
            }
            GameManager.getManagerFromInstance(SoundManager.class).playSound("use_item.wav");
        } catch (Exception ignored) {
            LOG.warn("Exception Thrown", ignored);
        }
        resetOptionsMenu();
        resetParameters();
    }

    /**
     * Equips item to the Mech
     * @param i Type of item that is being equipped
     */
    private void handleEquipButton(int i) {
        switch (i) {
            case 3:
                this.inventoryAbstract.equipWeapon(this.inventoryOptionsMenu.getItem());
                this.inventoryAbstract.dropInventory(this.inventoryOptionsMenu.getXCoord(), this.inventoryOptionsMenu.getYCoord());
                this.mechMenu.resetEquipment(1);
                GameManager.getManagerFromInstance(SoundManager.class).playSound("equip_item.wav");
                break;
            case 4:
                this.inventoryAbstract.equipArmour(this.inventoryOptionsMenu.getItem());
                this.inventoryAbstract.dropInventory(this.inventoryOptionsMenu.getXCoord(), this.inventoryOptionsMenu.getYCoord());
                this.mechMenu.resetEquipment(2);
                GameManager.getManagerFromInstance(SoundManager.class).playSound("equip_item.wav");
                break;
            case 5:
                this.inventoryAbstract.equipOrb(this.inventoryOptionsMenu.getItem());
                this.inventoryAbstract.dropInventory(this.inventoryOptionsMenu.getXCoord(), this.inventoryOptionsMenu.getYCoord());
                this.mechMenu.resetEquipment(3);
                GameManager.getManagerFromInstance(SoundManager.class).playSound("equip_item.wav");
                break;
            default:
                //error
                break;
        }
        resetOptionsMenu();
        resetParameters();
    }

    /**
     * resets the parameters for swapping items
     */
    private void resetParameters() {
        isButtonPushed = false;
        firstButtonPushed = null;
        secondButtonPushed = null;
        if (this.target1 != null) {
            resetImage(target1);
        }
        if (this.target2 != null) {
            resetImage(target2);
        }
        target1 = null;
        target2 = null;
    }

    /**
     * get the instance of the inventory
     * @return the inventory
     */
    public InventoryAbstract getInventory() {
        return this.inventoryAbstract;
    }

    /**
     * Resets menu to the state before the options menu existed
     */
    private void resetOptionsMenu() {
        this.removeActor(this.inventoryOptionsMenu);
        this.inventoryOptionsMenu = null;
    }

    /**
     * @return instance of mech
     */
    public MechInventory getMechInventory() {
        return this.mechInventory;
    }



    /**
     * a "?" button which is used to open sub mech ui
     */
    public void setSubButton(){
        ImageButton subMenuButton=new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("inventory/button.png")))));

        subMenuButton.setSize(30, 30);
        subMenuButton.setPosition(205, 162);

        subMenuButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                createFont();
                createSubMenu();
                GameManager.getManagerFromInstance(SoundManager.class).playSound("openSubMech.mp3");
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        this.addActor(subMenuButton);
    }

    /**
     * a submenu for mech menu
     */
    public void createSubMenu(){
        mechSub=true;
        Image template = new Image(new Texture(Gdx.files.internal("inventory/Inventory_bg_mech.png")));
        template.setSize(900,650);
        template.setPosition((Gdx.graphics.getWidth() - template.getWidth()) /2, (Gdx.graphics.getHeight() - template.getHeight()) / 2);
        this.addActor(template);
        //weapon
        Image weapon;
        String path;
        if (inventoryAbstract.getWeapon()!=null){
            path = getImage(inventoryAbstract.getWeapon());
        }else{
            path="items/unknow_sword.png";
        }
        weapon=new Image(new Texture(Gdx.files.internal(path)));
        weapon.setSize(200,200);
        weapon.setPosition( (Gdx.graphics.getWidth() - template.getWidth())/2+30 ,(Gdx.graphics.getHeight() - template.getHeight())/2+230);
        this.addActor(weapon);

        //armour
        Image armour;
        String path2;
        if (inventoryAbstract.getArmour()!=null){
            path2 = getImage(inventoryAbstract.getArmour());
        }else{
            path2="items/unknow_armor.png";
        }
        armour=new Image(new Texture(Gdx.files.internal(path2)));
        armour.setSize(200,200);
        armour.setPosition( (Gdx.graphics.getWidth() - template.getWidth())/2+680 ,(Gdx.graphics.getHeight() - template.getHeight())/2+230);
        this.addActor(armour);

        //orb
        Image orbCircle;
        String path3="";
        if (inventoryAbstract.getNumOfOrbs()==0){
            path3 = "inventory/submenu/0orb.png";
        }else if (inventoryAbstract.getNumOfOrbs()==1){
            path3 = "inventory/submenu/1orb.png";
        }else if (inventoryAbstract.getNumOfOrbs()==2){
            path3 = "inventory/submenu/2orb.png";
        }else if (inventoryAbstract.getNumOfOrbs()==3){
            path3 = "inventory/submenu/3orb.png";
        }else if (inventoryAbstract.getNumOfOrbs()==4){
            path3 = "inventory/submenu/4orb.png";
        }else if (inventoryAbstract.getNumOfOrbs()==5){
            path3 = "inventory/submenu/5orb.png";
        }
        orbCircle=new Image(new Texture(Gdx.files.internal(path3)));
        orbCircle.setSize(200,200);
        orbCircle.setPosition( (Gdx.graphics.getWidth() - template.getWidth())/2+360 ,(Gdx.graphics.getHeight() - template.getHeight())/2+250);
        this.addActor(orbCircle);

        //orb0
        Image orb0;
        if (inventoryAbstract.getOrbOrder()[0]!=null){
            String color = inventoryAbstract.getOrbOrder()[0];
            orb0 = new Image(new Texture(Gdx.files
                    .internal("items/Orb/Orb_"+color+".png")));

        }else {
            orb0=new Image(new Texture(Gdx.files
                    .internal("inventory/button.png")));
        }
        orb0.setSize(50,50);
        orb0.setPosition(630,620);
        this.addActor(orb0);

        //orb1
        Image orb1;
        if (inventoryAbstract.getOrbOrder()[1]!=null){
            String color = inventoryAbstract.getOrbOrder()[1];
            orb1 = new Image(new Texture(Gdx.files
                    .internal("items/Orb/Orb_"+color+".png")));

        }else {
            orb1=new Image(new Texture(Gdx.files
                    .internal("inventory/button.png")));
        }
        orb1.setSize(50,50);
        orb1.setPosition(745,530);
        this.addActor(orb1);

        //orb2
        Image orb2;
        if (inventoryAbstract.getOrbOrder()[2]!=null){
            String color = inventoryAbstract.getOrbOrder()[2];
            orb2 = new Image(new Texture(Gdx.files
                    .internal("items/Orb/Orb_"+color+".png")));

        }else {
            orb2=new Image(new Texture(Gdx.files
                    .internal("inventory/button.png")));
        }
        orb2.setSize(50,50);
        orb2.setPosition(710,400);
        this.addActor(orb2);

        //orb3
        Image orb3;
        if (inventoryAbstract.getOrbOrder()[3]!=null){
            String color = inventoryAbstract.getOrbOrder()[3];
            orb3 = new Image(new Texture(Gdx.files
                    .internal("items/Orb/Orb_"+color+".png")));

        }else {
            orb3=new Image(new Texture(Gdx.files
                    .internal("inventory/button.png")));
        }
        orb3.setSize(50,50);
        orb3.setPosition(550,400);
        this.addActor(orb3);

        //orb4
        Image orb4;
        if (inventoryAbstract.getOrbOrder()[4]!=null){
            String color = inventoryAbstract.getOrbOrder()[4];
            orb4 = new Image(new Texture(Gdx.files
                    .internal("items/Orb/Orb_"+color+".png")));

        }else {
            orb4=new Image(new Texture(Gdx.files
                    .internal("inventory/button.png")));
        }
        orb4.setSize(50,50);
        orb4.setPosition(515,530);
        this.addActor(orb4);


        ImageButton closeButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("items/Close_Button.png")))));
        closeButton.setSize(20, 20);
        closeButton.setPosition(((Gdx.graphics.getWidth()+template.getWidth()) / 2), (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - template.getHeight()) /2));
        closeButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                mechSub=false;
                removeActor(template);
                removeActor(closeButton);
                removeActor(weapon);
                removeActor(armour);
                removeActor(orbCircle);
                removeActor(orb0);
                removeActor(orb1);
                removeActor(orb2);
                removeActor(orb3);
                removeActor(orb4);
                GameManager.getManagerFromInstance(SoundManager.class).playSound("openSubMech.mp3");
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        this.addActor(closeButton);
    }

    /**
     * create font for sub mech ui
     */
    public void createFont(){
        mechHead1.getData().setScale((float) 1.5);
        mechHead2.getData().setScale((float) 1.5);
        mechHead3.getData().setScale((float) 1.5);

        mechHead1.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        mechHead2.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        mechHead3.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        this.weaponHeader = new GlyphLayout(mechHead1, "Weapon");
        this.orbHeader = new GlyphLayout(mechHead2, "Orb Power");
        this.armourHeader = new GlyphLayout(mechHead3, "Armour");

        //weapon name
        if (inventoryAbstract.getWeapon()!=null){
            this.weaponName = new GlyphLayout(mechName1,inventoryAbstract.getWeapon().getItemName());
        }else {
            this.weaponName = new GlyphLayout(mechName1,"Weapon Empty");
        }

        //armour name
        if (inventoryAbstract.getArmour()!=null){
            this.armourName = new GlyphLayout(mechName2,inventoryAbstract.getArmour().getItemName());
        }else {
            this.armourName = new GlyphLayout(mechName2,"Armour Empty");
        }

        //weapon attribute
        if (inventoryAbstract.getWeapon()!=null){
            this.weaponContribute = new GlyphLayout(mechContribute1,"Damage: +"+ inventoryAbstract.getWeapon().getDamage());
        }else {
            this.weaponContribute = new GlyphLayout(mechContribute1,"Damage: +0");
        }

        //armour attribute
        if (inventoryAbstract.getArmour()!=null){
            this.armourContribute = new GlyphLayout(mechContribute2,"Protection: +"+inventoryAbstract.getArmour().getArmour());
        }else {
            this.armourContribute = new GlyphLayout(mechContribute2,"Protection: +0");
        }

        //orb attribute
        this.orbContribute = new GlyphLayout(mechContribute3," Damage:  +"+30*inventoryAbstract.getNumOfOrbs() +"\n" +"Protection: +"+30*inventoryAbstract.getNumOfOrbs());

    }

    /**
     * the the images path string
     * @param a the abstract Item
     * @return the path string
     */
    public String getImage(AbstractItem a) {
        try {
            String emptyString = "";
            String[] parts = a.getClass().getName().split(Pattern.quote("."));
            return emptyString.concat(parts[2] + "/" + parts[3] + "/" + a.getImgName());
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    /**
     *
     * @return instance of item menu
     */
    public ItemMenu getItemMenu() {
        return this.itemMenu;
    }

    public void displayCurrency(String string) {
        if (currencyAmount != null) {
            this.removeActor(this.currencyAmount);
        }
        BitmapFont comicSansBold24 = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/24_bold.fnt"));
        Label.LabelStyle comicSansLabelStyle = new Label.LabelStyle();
        comicSansLabelStyle.font = comicSansBold24;
        this.currencyAmount = new Label(string, comicSansLabelStyle);
        currencyAmount.setPosition(Gdx.graphics.getWidth() - 100, Gdx.graphics.getHeight() - 470);
        this.addActor(currencyAmount);
    }
}
