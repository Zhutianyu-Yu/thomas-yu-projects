package deco2800.thomas.inventory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ItemBox;
import deco2800.thomas.items.AbstractItem;

import java.util.regex.Pattern;

/**
 * Inventory Menu subgroup for the item menu
 */
public class ItemMenu extends Group {

    /** Inventory instance */
    private InventoryAbstract inventoryAbstract;

    /**
     *
     * @param x starting width set, where bottom left is x,y
     * @param y starting height set, where bottom left is x,y
     * @param inventoryAbstract that is being displayed
     */
    public ItemMenu(int x, int y, InventoryAbstract inventoryAbstract) {
        this.inventoryAbstract = inventoryAbstract;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                int width = 780;
                int size = 58;
                int internalMargin = (6 * size) / 5;
                int xMargin = (width - (internalMargin * 9) + (size / 5)) / 2;
                int height = 335;
                int yMargin = (height - (internalMargin * 4) + (size / 5)) / 2;
                ItemBox[][] itemBoxes = new ItemBox[4][9];
                try {
                    itemBoxes[i][j] = new ItemBox(x + (j * internalMargin) + xMargin, y + ((3 - i) * internalMargin) + yMargin, getImage(i, j), getItem(i, j), this.inventoryAbstract.getInventoryQuantity(i, j));
                    itemBoxes[i][j].setImagePos();
                } catch (Exception e) {
                    itemBoxes[i][j] = new ItemBox(x + (j * internalMargin) + xMargin, y + ((3 - i) * internalMargin) + yMargin, null, null, this.inventoryAbstract.getInventoryQuantity(i, j));
                    itemBoxes[i][j].setImagePos();
                }
                this.addActor(itemBoxes[i][j]);
            }
        }
    }


    public ItemMenu(InventoryAbstract inventory) {
        this.inventoryAbstract = inventory;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {

                int internalMargin = 58;

                int x = Gdx.graphics.getWidth() - (6 * internalMargin);
                int y = Gdx.graphics.getHeight() - (6 * internalMargin);

                ItemBox[][] itemBoxes = new ItemBox[4][9];
                int[] location = converter(i, j);

                int xPosi = x + (location[1] * internalMargin);
                int yPosi = y + ((5 - location[0]) * internalMargin);
                try {
                    itemBoxes[i][j] = new ItemBox(xPosi, yPosi,getImage(i, j), getItem(i, j), this.inventoryAbstract.getInventoryQuantity(i, j));
                    itemBoxes[i][j].setImagePos();
                } catch (Exception e) {
                    itemBoxes[i][j] = new ItemBox(xPosi, yPosi, null, null, this.inventoryAbstract.getInventoryQuantity(i, j));
                    itemBoxes[i][j].setImagePos();
                }
                this.addActor(itemBoxes[i][j]);
            }
        }
    }

    // Converts the 4 x 9 grid into a 6 x 6 grid
    private int[] converter(int i, int j) {

        if (i == 0 && j > 5) {
            j -= 6;
            i++;
        } else if (i != 0) {
            return nonFirstRow(i, j);
        }
        return new int[]{i, j};
    }

    private int[] nonFirstRow(int i, int j) {
        if (i == 1) {
            if (j > 2) {
                i++;
                j -= 3;
            } else {
                j += 3;
            }
        } else if (i == 2) {
            i++;
            if (j > 5) {
                i++;
                j -= 6;
            }
        } else if (i == 3) {
            i++;
            if (j > 2) {
                i++;
                j -= 3;
            } else {
                j += 3;
            }
        }
        return new int[]{i, j};
    }


    /**
     * Gets the image of the AbstractItem
     *
     * @param i x position of the item in the inventoryAbstract instance
     * @param j y position of the item in the inventoryAbstract instance
     * @return an Image of the AbstractItem
     */
    private Image getImage(int i, int j) {
        try {
            String emptyString = "";
            String[] parts = this.inventoryAbstract.getInventoryArray()[i][j].getClass().getName().split(Pattern.quote("."));
            String imagePath = emptyString.concat(parts[2] + "/" + parts[3] + "/" + this.inventoryAbstract.getInventoryArray()[i][j].getImgName());
            return new Image(new Texture(Gdx.files.internal(imagePath)));
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    /**
     * Get item from the inventoryAbstract instance
     *
     * @param i x position of the item
     * @param j y position of the item
     * @return The AbstractItem the is at [i][j]
     */
    private AbstractItem getItem(int i, int j) {
        return this.inventoryAbstract.getInventoryArray()[i][j];
    }


    /**
     * Updates a spot in the inventory array
     *
     * @param i position in the inventory array
     * @param j position in the inventory array
     */
    public void updateInventorySpot(int i, int j) {
        try {
            ((ItemBox)this.getChild(i*9 + j)).setItem(getItem(i, j));
            ((ItemBox)this.getChild(i*9 + j)).setQuantity(this.inventoryAbstract.getInventoryQuantity(i, j));
            ((ItemBox)this.getChild(i*9 + j)).setBase();
        } catch (Exception e) {
            ((ItemBox)this.getChild(i*9 + j)).setItem(null);
            ((ItemBox)this.getChild(i*9 + j)).setQuantity(0);
            ((ItemBox)this.getChild(i*9 + j)).setBase();
        }
        ((ItemBox)this.getChild(i*9 + j)).updateImage();
    }

    /**
     * Updates all ItemBoxes so that they hold/display the correct item
     */
    public void updateInventory() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                try {
                    ((ItemBox)this.getChild(i*9 + j)).setItem(getItem(i, j));
                    ((ItemBox)this.getChild(i*9 + j)).setQuantity(this.inventoryAbstract.getInventoryQuantity(i, j));
                } catch (Exception e) {
                    ((ItemBox)this.getChild(i*9 + j)).setItem(null);
                    ((ItemBox)this.getChild(i*9 + j)).setQuantity(0);
                }
                ((ItemBox)this.getChild(i*9 + j)).updateImage();
            }
        }
    }
}
