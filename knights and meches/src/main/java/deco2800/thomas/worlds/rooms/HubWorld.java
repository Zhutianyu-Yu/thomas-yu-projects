package deco2800.thomas.worlds.rooms;
import deco2800.thomas.entities.*;
import deco2800.thomas.worlds.RoomWorld;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.Tile;
import deco2800.thomas.worlds.WorldTypes;
import java.io.IOException;

public class HubWorld extends RoomWorld {


    final float SPAWN_POS_ROW = 15f;
    final float SPAWN_POS_COL = 18f;

    private TraderNPC trader;

    public HubWorld(){
        super();
        this.name = "City of Decodia";
    }

    /**
     * read file resources/TeleportedRoomMap and generate the world with given map
     */
    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/HubWorld/mapFile","resources/HubWorld/HubWorldMap");
        } catch (IOException e) {
            e.printStackTrace();
        }

        addTeleporter(17,31,WorldTypes.HUB_WORLD_DUNGEON.toString());
        addTeleporter(18,31,WorldTypes.HUB_WORLD_DUNGEON.toString());
        addTeleporter(19,31,WorldTypes.HUB_WORLD_DUNGEON.toString());
        addTeleporter(20,31,WorldTypes.HUB_WORLD_DUNGEON.toString());
        addQuestActivator(19,15, "teleport_circle", "Your first orb");
        setBirthPointPosition(16,19);

        if (GameManager.getParty() != null) {
            GameManager.get().updatePartyPosition(SPAWN_POS_ROW, SPAWN_POS_COL);
        }
        GameManager.get().roomCam(SPAWN_POS_ROW, SPAWN_POS_COL);

        addTeleporter(26,3,WorldTypes.TELEPORTER.toString());

        setBirthPointPosition(18,15);

        this.trader = new TraderNPC(new Tile("", 12, 17), "door0");
        entities.add(this.trader);
        this.trader.setCol(12);
        this.trader.setRow(17);
    }

    public TraderNPC getTrader() {
        return this.trader;
    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }

    @Override
    public void generateBuildings() {

    }

    @Override
    public void generateOrbs() {

    }

    public void initialiseParty() {
        // Create the entities in the game

        Knight knight1 = new Knight(SPAWN_POS_ROW, SPAWN_POS_COL, 0.1f, "typeA");
        //this.grid[3+WORLD_WIDTH][9+WORLD_HEIGHT] = 1;
        knight1.addMana(0);
        knight1.setHealth(100);
        addEntity(knight1);

        //addEntity(new Wizard(9f, 3f, 0.1f, "Rin"));
        Wizard wizard1 = new Wizard(SPAWN_POS_ROW, SPAWN_POS_COL, 0.1f, "typeA");
        //this.grid[4+WORLD_WIDTH][9+WORLD_HEIGHT] = 1;
        wizard1.levelUp();
        wizard1.levelUp();
        wizard1.setHP((int)(wizard1.getHealth() * 0.6));
        addEntity(wizard1);

        Mech mech = new Mech(10f, 3f, 0.1f, wizard1, knight1);
        //this.grid[3+WORLD_WIDTH][10+WORLD_HEIGHT] = 1;

        //stores the value for the game mech
        GameManager.setInGameMech(mech);

        initiateParty(knight1, wizard1, mech);
        this.addEntity(GameManager.getParty().getPlayer(PlayerType.KNIGHT));
        this.addEntity(GameManager.getParty().getPlayer(PlayerType.WIZARD));

        //add other players to game
        Wizard wizard2 = new Wizard(SPAWN_POS_ROW+1, SPAWN_POS_COL+1, 0.1f, "typeB");
        wizard2.setDefaultTexture("Wizard3");
        wizard2.setWalkingAnimations("wizard3walking1", "wizard3walking2", "wizard3walking3");

        Knight knight2 = new Knight(SPAWN_POS_ROW+1, SPAWN_POS_COL+1, 0.1f, "typeB");
        knight2.setDefaultTexture("Knight2");
        knight2.setWalkingAnimations("knight2walking1", "knight2walking2", "knight2walking3");

        GameManager.get().addPlayersToGame(knight1, knight2, wizard1, wizard2);
    }
}
