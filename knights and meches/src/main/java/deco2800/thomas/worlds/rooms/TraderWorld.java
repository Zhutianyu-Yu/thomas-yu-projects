package deco2800.thomas.worlds.rooms;
import deco2800.thomas.traderUI.TraderInterface;
import deco2800.thomas.worlds.RoomWorld;
import deco2800.thomas.worlds.Tile;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import deco2800.thomas.entities.*;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.observers.TouchDownObserver;

/**
 * A class for the Trader Dialogue
 */
public class TraderWorld extends RoomWorld implements TouchDownObserver {

    // Keep count of the number of times we are cycling through
    private int trade;
    private static final int WIDTH = 10;
    private static final int HEIGHT = 4;
    private Dialogue chat;
    private boolean notGenerated = true;
    private static final String BLANK = "blank";
    
    public TraderWorld() {

        super();
        trade = 0;
        GameManager.getManagerFromInstance(InputManager.class).addTradeWorld(this);
    }

    public void generateOrbs(){};

    /**
     * Method to cycle through the background images
     */
    public void nextTrade() {

        this.trade++;
        // Is it the first call
        if (this.trade == 1) {

            this.chat.setTexture("trader1");
            GameManager.getManagerFromInstance(InputManager.class).getTraderInterface().setTraderInterfaceDisplayed(true);


        } else if (this.trade == 2) {

            // Are we on the final cycles
            leaveTraderWorld(GameManager.get().getStage());

        }
    }

    @Override
    protected void generateWorld() {

        this.setBackgroundImage("traderRoom");


        for (int q = -WIDTH; q < WIDTH; q++) {
            for (int r = -10; r < HEIGHT; r++) {
                tiles.add(new Tile(BLANK, q, r));
            }
        }
        GameManager.get().zeroCamera();
        TraderNPC trader = new TraderNPC(new Tile(BLANK, 0, -4), "traderMan");
        trader.setCol(0f);
        trader.setRow(-4f);
        addEntity(trader);
        this.notGenerated = false;
        Dialogue chatBox = new Dialogue(new Tile(BLANK, -4, 0), 1, BLANK, true);
        chatBox.setCol(-2f);
        chatBox.setRow(1f);
        chatBox.setTexture("trader0");
        addEntity(chatBox);
        this.chat = chatBox;
        createWorld();
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenerTrader(this);
    }

    public void worldSetup() {
        this.setBackgroundImage("traderRoom");
        GameManager.get().zeroCamera();
        chat.setTexture("trader0");
        createWorld();
    }

    /**
     * Creates text that when clicked exits from screen
     */
    private void createWorld()
    {
        GameManager manager = GameManager.get();
        Stage stage = manager.getStage();
        Skin skin = manager.getSkin();
        if (skin != null)
        {
            stageVisibility(stage);
        }
    }

    public boolean getGenerated() {
        return this.notGenerated;
    }

    @Override
    public void generateBuildings() {
    }


    /**
     * Toggles visibility of staged actors
     * @param stage current game's stage
     */
    private void stageVisibility(Stage stage)
    {
        for (Actor actor : stage.getActors())
        {
            if (actor instanceof TraderInterface) {
                actor.setVisible(true);
            }
        }
    }

    /** Method for cleaning up ghost world and restoring old world how it was*/
    public void leaveTraderWorld(Stage stage) {
        stageVisibility(stage);
        LOGGER.info("Exiting GhostWorld");
        this.setBackgroundImage("black_bg");
        GameManager.get().restoreWorld();
        GameManager.get().resetCam(1.3f);
    }


    @Override
    protected void addManagers()
    {
        GameManager.get().addManager(new ScreenManager());
    }

    /**
     * Notifies the observers of the mouse button being pushed down
     *
     * @param screenX the x position the mouse was pressed at
     * @param screenY the y position the mouse was pressed at
     * @param pointer
     * @param button  the button which was pressed
     */
    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
        nextTrade();
    }
}
