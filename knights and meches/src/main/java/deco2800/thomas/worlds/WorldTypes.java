package deco2800.thomas.worlds;

public enum WorldTypes
{
    LOAD,
    SERVER,
    TEST,
    COMBAT,
    GHOST_WORLD,
    TRADE,
    TUTORIAL,
    HUB_WORLD_DUNGEON {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 15:
                    return "option1_building_1.png";
                case 16:
                    return "option1_building_2.png";
                case 17:
                    return "option1_tile_1.png";
                case 18:
                    return "option1_tile_2.png";
                case 19:
                    return "option1_tile_3.png";
                case 20:
                    return "option1_tile_4.png";
                case 21:
                    return "option2_building-1.png";
                case 22:
                    return "option2_building-2.png";
                case 23:
                    return "option2_tile_1.png";
                case 24:
                    return "option2_tile_2.png";
                case 25:
                    return "option2_tile_3.png";
                case 26:
                    return "option2_tile_4.png";
                case 27:
                    return "wall_1.png";
                case 28:
                    return "wall_2.png";
                default:
                    return "Transparent-tile";
            }
        }
    },
    FOREST_BIOME {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 4:
                    return "floor-tile-1.png";
                case 5:
                    return "floor-tile-2.png";
                case 6:
                    return "leaves-tile.png";
                case 7:
                    return "wall-tile.png";
                case 8:
                    return "bush-tile.png";
                case 9:
                    return "dungeon-tile-1.png";
                case 10:
                    return "dungeon-tile-2.png";
                default:
                    return "Transparent-tile";
            }
        }
    }
    ,
    TUNDRA_DUNGEON {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 1:
                    return "dungeon-tile 1.png";
                case 2:
                    return "dungeon-tile 2.png";
                case 3:
                    return "dungeon-tower-lower.png";
                case 4:
                    return "dungeon-tower-upper.png";
                case 5:
                    return "floor-tile.png";
                case 6:
                    return "floor-tile2.png";
                case 7:
                    return "ice-tile.png";
                case 8:
                    return "icicle-tile.png";
                case 9:
                    return ".wall-tile.png";
                case 10:
                    return "0-1.png";
                case 11:
                    return "0-2.png";
                case 12:
                    return "0-3.png";
                case 13:
                    return "0-4.png";
                case 14:
                    return "2-1.png";
                case 15:
                    return "2-2.png";
                case 16:
                    return "2-3.png";
                case 17:
                    return "2-4.png";
                case 18:
                    return "3-1.png";
                case 19:
                    return "3-2.png";
                case 20:
                    return "3-3.png";
                case 21:
                    return "3-4.png";
                case 22:
                    return "4-1.png";
                case 23:
                    return "4-2.png";
                case 24:
                    return "4-3.png";
                case 25:
                    return "4-4.png";
                case 26:
                    return "5-1.png";
                case 27:
                    return "5-2.png";
                case 28:
                    return "5-3.png";
                case 29:
                    return "5-4.png";
                case 30:
                    return "6-1.png";
                case 31:
                    return "6-2.png";
                case 32:
                    return "6-3.png";
                case 33:
                    return "6-4.png";

                default:
                    return "Transparent-tile";
            }
        }
    }
    ,
    TELEPORTER {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 1:
                    return "blue-tele-1.png";
                case 2:
                    return "blue-tele-2.png";
                case 3:
                    return "blue-tele-3.png";
                case 4:
                    return "blue-tele-4.png";
                case 5:
                    return "desk-1.png";
                case 6:
                    return "desk-2.png";
                case 7:
                    return "desk-3.png";
                case 8:
                    return "desk-4.png";
                case 9:
                    return "desk-5.png";
                case 10:
                    return "desk-6.png";
                case 11:
                    return "door-1.png";
                case 12:
                    return "door-2.png";
                case 13:
                    return "door-3.png";
                case 14:
                    return "door-4.png";
                case 15:
                    return "floor-generic.png";
                case 16:
                    return "green-tele-1.png";
                case 17:
                    return "green-tele-2.png";
                case 18:
                    return "green-tele-3.png";
                case 19:
                    return "green-tele-4.png";
                case 20:
                    return "grey-purple-wire.png";
                case 21:
                    return "grey-purple-wire-2.png";
                case 22:
                    return "grey-wire-corner-NW.png";
                case 23:
                    return "grey-wire-corner-SW.png";
                case 24:
                    return "grey-wire-left.png";
                case 25:
                    return "grey-wire-right.png";
                case 26:
                    return "grey-wire-top.png";
                case 27:
                    return "mainframe-1.png";
                case 28:
                    return "mainframe-2.png";
                case 29:
                    return "mainframe-3.png";
                case 30:
                    return "pipe-up-1.png";
                case 31:
                    return "pipe-up-2.png";
                case 32:
                    return "pipe-up-wire-1.png";
                case 33:
                    return "pipe-up-wire-2.png";
                case 34:
                    return "pipe-wire-1.png";
                case 35:
                    return "pipe-wire-2.png";
                case 36:
                    return "purple-tele-1.png";
                case 37:
                    return "purple-tele-2.png";
                case 38:
                    return "purple-tele-3.png";
                case 39:
                    return "purple-tele-4.png";
                case 40:
                    return "purple-wire-middle.png";
                case 41:
                    return "purple-wire-up.png";
                case 42:
                    return "red-tele-1.png";
                case 43:
                    return "red-tele-2.png";
                case 44:
                    return "red-tele-3.png";
                case 45:
                    return "red-tele-4.png";
                case 46:
                    return "wall-generic.png";
                case 47:
                    return "wire-angle-1.png";
                case 48:
                    return "wire-angle-2.png";
                case 49:
                    return "wire-angle-3.png";
                case 50:
                    return "wire-angle-4.png";
                default:
                    return "Transparent-tile";
            }
        }
    },
    VOLCANO_BIOME {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 38:
                    return "volcano_lava_large";
                case 1:
                    return "volcano_dungeon_normal";
                case 4:
                    return "volcano_dungeon_Rockwall";
                case 15:
                    return "bridge_vertical";
                case 14:
                    return "bridge_horizontal";
                case 19:
                    return "health_pool_bLeft";
                case 20:
                    return "health_pool_bMid";
                case 21:
                    return "health_pool_bRight";
                case 22:
                    return "health_pool_mLeft";
                case 23:
                    return "health_pool_mMid";
                case 24:
                    return "health_pool_mRight";
                case 25:
                    return "health_pool_tLeft";
                case 26:
                    return "health_pool_tMid";
                case 27:
                    return "health_pool_tRight";
                case 28:
                    return "mana_pool_bLeft";
                case 29:
                    return "mana_pool_bMid";
                case 30:
                    return "mana_pool_bRight";
                case 31:
                    return "mana_pool_mLeft";
                case 32:
                    return "mana_pool_mMid";
                case 33:
                    return "mana_pool_mRight";
                case 34:
                    return "mana_pool_tLeft";
                case 35:
                    return "mana_pool_tMid";
                case 36:
                    return "mana_pool_tRight";
                default:
                    return "Transparent-tile";
            }
        }
    },
    HUB_WORLD {
        @Override
        public String TilesMapping(int i){
            switch (i){
                case 40:
                    return "crossroad-NE.png";
                case 41:
                    return "crossroad-NW.png";
                case 42:
                    return "crossroad-SE.png";
                case 43:
                    return "crossroad-SW.png";
                case 44:
                    return "curb-NE.png";
                case 45:
                    return "curb-NW.png";
                case 46:
                    return "curb-SE.png";
                case 47:
                    return "curb-side.png";
                case 48:
                    return "curb-SW.png";
                case 49:
                    return "curb-up.png";
                case 50:
                    return "gate-1-1.png";
                case 51:
                    return "gate-1-2.png";
                case 52:
                    return "gate-1-3.png";
                case 53:
                    return "gate-1-4.png";
                case 54:
                    return "gate-1-5.png";
                case 55:
                    return "gate-1-6.png";
                case 56:
                    return "gate-2-1.png";
                case 57:
                    return "gate-2-2.png";
                case 58:
                    return "gate-2-3.png";
                case 59:
                    return "gate-2-4.png";
                case 60:
                    return "gate-2-5.png";
                case 61:
                    return "gate-2-6.png";
                case 62:
                    return "gate-3-1.png";
                case 63:
                    return "gate-3-2.png";
                case 64:
                    return "gate-3-3.png";
                case 65:
                    return "gate-3-4.png";
                case 66:
                    return "gate-3-5.png";
                case 67:
                    return "gate-3-6.png";
                case 68:
                    return "hubworld-base.png";
                case 69:
                    return "rear-wall.png";
                case 70:
                    return "road-bottom.png";
                case 71:
                    return "road-left.png";
                case 72:
                    return "road-right.png";
                case 73:
                    return "road-top.png";
                case 74:
                    return "roof-generic-left.png";
                case 75:
                    return "roof-generic-right.png";
                case 76:
                    return "roof-generic-triangle-bottom.png";
                case 77:
                    return "roof-generic-triangle-top.png";
                case 78:
                    return "window-generic.png";
                case 79:
                    return "Gus assets/back1-roof1.png";
                case 80:
                    return "Gus assets/back1-roof2.png";
                case 81:
                    return "Gus assets/back1-roof3.png";
                case 82:
                    return "Gus assets/back1-topleft.png";
                case 83:
                    return "Gus assets/back1-topmid.png";
                case 84:
                    return "Gus assets/back1-topright.png";
                case 85:
                    return "Gus assets/back1-wallleft.png";
                case 86:
                    return "Gus assets/back1-wallmid.png";
                case 87:
                    return "Gus assets/back1-wallright.png";
                case 88:
                    return "Gus assets/back1-windowleft.png";
                case 89:
                    return "Gus assets/back1-windowmid.png";
                case 90:
                    return "Gus assets/back1-windowright.png";
                case 91:
                    return "Gus assets/back2-lightleft.png";
                case 92:
                    return "Gus assets/back2-lightmid.png";
                case 93:
                    return "Gus assets/back2-roof1.png";
                case 94:
                    return "Gus assets/back2-roof2.png";
                case 95:
                    return "Gus assets/back2-roof3.png";
                case 96:
                    return "Gus assets/back2-roof4.png";
                case 97:
                    return "Gus assets/back2-roof5.png";
                case 98:
                    return "Gus assets/back2-roof6.png";
                case 99:
                    return "Gus assets/back2-wall-left.png";
                case 100:
                    return "Gus assets/back2-wallmid.png";
                case 101:
                    return "Gus assets/back2-wallright.png";
                case 102:
                    return "Gus assets/back3-roof1.png";
                case 103:
                    return "Gus assets/back3-roof2.png";
                case 104:
                    return "Gus assets/back3-roof3.png";
                case 105:
                    return "Gus assets/back3-windowleft.png";
                case 106:
                    return "Gus assets/back3-windowmid.png";
                case 107:
                    return "Gus assets/back3-windowright.png";
                case 108:
                    return "Gus assets/back4-roof1.png";
                case 109:
                    return "Gus assets/back4-roof2.png";
                case 110:
                    return "Gus assets/back4-roof3.png";
                case 111:
                    return "Gus assets/back4-windowleft.png";
                case 112:
                    return "Gus assets/back4-windowmid.png";
                case 113:
                    return "Gus assets/back4-windowright.png";
                case 114:
                    return "Gus assets/crafts-doorleft.png";
                case 115:
                    return "Gus assets/crafts-doorright.png";
                case 116:
                    return "Gus assets/crafts-roof1.png";
                case 117:
                    return "Gus assets/crafts-roof2.png";
                case 118:
                    return "Gus assets/crafts-roof3.png";
                case 119:
                    return "Gus assets/crafts-sign1.png";
                case 120:
                    return "Gus assets/crafts-sign2.png";
                case 121:
                    return "Gus assets/crafts-sign3.png";
                case 122:
                    return "Gus assets/crafts-sign4.png";
                case 123:
                    return "Gus assets/crafts-wallleft.png";
                case 124:
                    return "Gus assets/crafts-wallright.png";
                case 125:
                    return "Gus assets/crafts-windowleft.png";
                case 126:
                    return "Gus assets/crafts-windowmid.png";
                case 127:
                    return "Gus assets/crafts-windowright.png";
                case 128:
                    return "Gus assets/front1-door.png";
                case 129:
                    return "Gus assets/front1-long-bottomleft.png";
                case 130:
                    return "Gus assets/front1-long-bottommid.png";
                case 131:
                    return "Gus assets/front1-long-bottomright.png";
                case 132:
                    return "Gus assets/front1-long-topleft.png";
                case 133:
                    return "Gus assets/front1-long-topmid.png";
                case 134:
                    return "Gus assets/front1-long-topright.png";
                case 135:
                    return "Gus assets/front1-roof1.png";
                case 136:
                    return "Gus assets/front1-roof2.png";
                case 137:
                    return "Gus assets/front1-roof3.png";
                case 138:
                    return "Gus assets/front1-wall-left.png";
                case 139:
                    return "Gus assets/front1-wall-mid.png";
                case 140:
                    return "Gus assets/front1-wall-right.png";
                case 141:
                    return "Gus assets/front1-window-left.png";
                case 142:
                    return "Gus assets/front1-window-mid.png";
                case 143:
                    return "Gus assets/front1-window-right.png";
                case 144:
                    return "Gus assets/front2-door.png";
                case 145:
                    return "Gus assets/front2-roof1.png";
                case 146:
                    return "Gus assets/front2-roof2.png";
                case 147:
                    return "Gus assets/front2-roof3.png";
                case 148:
                    return "Gus assets/front2-wall-left.png";
                case 149:
                    return "Gus assets/front2-wall-mid.png";
                case 150:
                    return "Gus assets/front2-wall-right.png";
                case 151:
                    return "Gus assets/front2-window-left.png";
                case 152:
                    return "Gus assets/front2-window-mid.png";
                case 153:
                    return "Gus assets/front2-window-right.png";
                case 154:
                    return "krish-assets/blue-building-1.png";
                case 155:
                    return "krish-assets/blue-building-1--01.png";
                case 156:
                    return "krish-assets/blue-building-1--02.png";
                case 157:
                    return "krish-assets/blue-building-1--03.png";
                case 158:
                    return "krish-assets/blue-building-1--04.png";
                case 159:
                    return "krish-assets/blue-building-1--05.png";
                case 160:
                    return "krish-assets/blue-building-1--06.png";
                case 161:
                    return "krish-assets/blue-building-1--07.png";
                case 162:
                    return "krish-assets/blue-building-1--08.png";
                case 163:
                    return "krish-assets/blue-building-1--09.png";
                case 164:
                    return "krish-assets/blue-building-1--10.png";
                case 165:
                    return "krish-assets/blue-building-1--11.png";
                case 166:
                    return "krish-assets/blue-building-1--12.png";
                case 167:
                    return "krish-assets/blue-building-1--13.png";
                case 168:
                    return "krish-assets/blue-building-1--14.png";
                case 169:
                    return "krish-assets/blue-building-1--15.png";
                case 170:
                    return "krish-assets/blue-building-1--16.png";
                case 171:
                    return "krish-assets/blue-building-1--17.png";
                case 172:
                    return "krish-assets/blue-building-1--18.png";
                case 173:
                    return "krish-assets/green-building-1.png";
                case 174:
                    return "krish-assets/green-building-1---01.png";
                case 175:
                    return "krish-assets/green-building-1---02.png";
                case 176:
                    return "krish-assets/green-building-1---03.png";
                case 177:
                    return "krish-assets/green-building-1---04.png";
                case 178:
                    return "krish-assets/green-building-1---05.png";
                case 179:
                    return "krish-assets/green-building-1---06.png";
                case 180:
                    return "krish-assets/green-building-1---07.png";
                case 181:
                    return "krish-assets/green-building-1---08.png";
                case 182:
                    return "krish-assets/green-building-1---09.png";
                case 183:
                    return "krish-assets/green-building-2.png";
                case 184:
                    return "krish-assets/green-building-2---01.png";
                case 185:
                    return "krish-assets/green-building-2---02.png";
                case 186:
                    return "krish-assets/green-building-2---03.png";
                case 187:
                    return "krish-assets/green-building-2---04.png";
                case 188:
                    return "krish-assets/green-building-2---05.png";
                case 189:
                    return "krish-assets/green-building-2---06.png";
                case 190:
                    return "krish-assets/green-building-2---07.png";
                case 191:
                    return "krish-assets/green-building-2---08.png";
                case 192:
                    return "krish-assets/green-building-2---09.png";
                case 193:
                    return "krish-assets/green-building-2---10.png";
                case 194:
                    return "krish-assets/green-building-2---11.png";
                case 195:
                    return "krish-assets/green-building-2---12.png";
                case 196:
                    return "krish-assets/green-building-2---13.png";
                case 197:
                    return "krish-assets/green-building-2---14.png";
                case 198:
                    return "krish-assets/green-building-2---15.png";
                case 199:
                    return "krish-assets/green-building-2---16.png";
                case 200:
                    return "krish-assets/green-building-2---17.png";
                case 201:
                    return "krish-assets/green-building-2---18.png";
                case 202:
                    return "krish-assets/pink-building-1.jpg";
                case 203:
                    return "krish-assets/pink-building-1---01.png";
                case 204:
                    return "krish-assets/pink-building-1---02.png";
                case 205:
                    return "krish-assets/pink-building-1---03.png";
                case 206:
                    return "krish-assets/pink-building-1---04.png";
                case 207:
                    return "krish-assets/pink-building-1---05.png";
                case 208:
                    return "krish-assets/pink-building-1---06.png";
                case 209:
                    return "krish-assets/pink-building-1---07.png";
                case 210:
                    return "krish-assets/pink-building-1---08.png";
                case 211:
                    return "krish-assets/pink-building-1---09.png";
                case 212:
                    return "krish-assets/pink-building-1---10.png";
                case 213:
                    return "krish-assets/pink-building-1---11.png";
                case 214:
                    return "krish-assets/pink-building-1---12.png";
                case 215:
                    return "krish-assets/pink-building-1---13.png";
                case 216:
                    return "krish-assets/pink-building-1---14.png";
                case 217:
                    return "krish-assets/pink-building-1---15.png";
                case 218:
                    return "krish-assets/pink-building-1---16.png";
                case 219:
                    return "krish-assets/pink-building-1---17.png";
                case 220:
                    return "krish-assets/pink-building-1---18.png";
                case 221:
                    return "krish-assets/pink-building-2.png";
                case 224:
                    return "krish-assets/pink-building-2---03.png";
                case 225:
                    return "krish-assets/pink-building-2---04.png";
                case 226:
                    return "krish-assets/pink-building-2---05.png";
                case 227:
                    return "krish-assets/pink-building-2---06.png";
                case 228:
                    return "krish-assets/pink-building-2---07.png";
                case 229:
                    return "krish-assets/pink-building-2---08.png";
                case 230:
                    return "krish-assets/pixel_city_scape-upscaled.png";
                case 231:
                    return "krish-assets/red-building-1.png";
                case 232:
                    return "krish-assets/red-building-1--01.png";
                case 233:
                    return "krish-assets/red-building-1--02.png";
                case 234:
                    return "krish-assets/red-building-1--03.png";
                case 235:
                    return "krish-assets/red-building-1--04.png";
                case 236:
                    return "krish-assets/red-building-1--05.png";
                case 237:
                    return "krish-assets/red-building-1--06.png";
                case 238:
                    return "krish-assets/red-building-2.png";
                case 239:
                    return "krish-assets/red-building-2--01.png";
                case 240:
                    return "krish-assets/red-building-2--02.png";
                case 241:
                    return "krish-assets/red-building-2--03.png";
                case 242:
                    return "krish-assets/red-building-2--04.png";
                case 243:
                    return "krish-assets/red-building-2--05.png";
                case 244:
                    return "krish-assets/red-building-2--06.png";
                case 245:
                    return "krish-assets/red-building-2--07.png";
                case 246:
                    return "krish-assets/red-building-2--08.png";
                case 247:
                    return "krish-assets/red-building-2--09.png";
                case 248:
                    return "krish-assets/red-building-2--10.png";
                case 249:
                    return "Mark assets/blue-roof-angled-bottom-left.png";
                case 250:
                    return "Mark assets/blue-roof-angled-bottom-mid.png";
                case 251:
                    return "Mark assets/blue-roof-angled-bottom-right.png";
                case 252:
                    return "Mark assets/blue-roof-angled-left-top.png";
                case 253:
                    return "Mark assets/blue-roof-angled-top-mid.png";
                case 254:
                    return "Mark assets/blue-roof-angled-top-right.png";
                case 255:
                    return "Mark assets/blue-roof-left.png";
                case 256:
                    return "Mark assets/blue-roof-middle.png";
                case 257:
                    return "Mark assets/blue-roof-right.png";
                case 258:
                    return "Mark assets/blue-window-larg-left.png";
                case 259:
                    return "Mark assets/blue-window-larg-middle.png";
                case 260:
                    return "Mark assets/blue-window-larg-right.png";
                case 261:
                    return "Mark assets/blue-window-medium.png";
                case 262:
                    return "Mark assets/blue-window-small.png";
                case 263:
                    return "Mark assets/circle-base-left.png";
                case 264:
                    return "Mark assets/circle-base-middle.png";
                case 265:
                    return "Mark assets/circle-base-right.png";
                case 266:
                    return "Mark assets/circle-roof-left.png";
                case 267:
                    return "Mark assets/circle-roof-middle.png";
                case 268:
                    return "Mark assets/circle-roof-rightpng.png";
                case 269:
                    return "Mark assets/circle-window-left.png";
                case 270:
                    return "Mark assets/circle-window-middle.png";
                case 271:
                    return "Mark assets/circle-window-right.png";
                case 272:
                    return "Mark assets/diamond-bottom-left.png";
                case 273:
                    return "Mark assets/diamond-bottom-right.png";
                case 274:
                    return "Mark assets/diamond-top-left.png";
                case 275:
                    return "Mark assets/diamond-top-right.png";
                case 276:
                    return "Mark assets/door-bottom-left.png";
                case 277:
                    return "Mark assets/door-bottom-right.png";
                case 278:
                    return "Mark assets/door-top-left.png";
                case 279:
                    return "Mark assets/door-top-right.png";
                case 280:
                    return "Mark assets/EM.png";
                case 281:
                    return "Mark assets/grey-door.png";
                case 282:
                    return "Mark assets/grey-roof-left.png";
                case 283:
                    return "Mark assets/grey-roof-middle.png";
                case 284:
                    return "Mark assets/grey-roof-right.png";
                case 285:
                    return "Mark assets/grey-wall.png";
                case 286:
                    return "Mark assets/grey-window.png";
                case 287:
                    return "Mark assets/insignia-bottom-left.png";
                case 288:
                    return "Mark assets/insignia-bottom-right.png";
                case 289:
                    return "Mark assets/insignia-top-left.png";
                case 290:
                    return "Mark assets/insignia-top-right.png";
                case 291:
                    return "Mark assets/IT.png";
                case 292:
                    return "Mark assets/ministry-wall.png";
                case 293:
                    return "Mark assets/ministry-window.png";
                case 294:
                    return "Mark assets/ministry-window-NE.png";
                case 295:
                    return "Mark assets/ministry-window-NW.png";
                case 296:
                    return "Mark assets/ministry-window-SE.png";
                case 297:
                    return "Mark assets/ministry-window-SW.png";
                case 298:
                    return "Mark assets/MS.png";
                case 299:
                    return "Mark assets/purple window-1.png";
                case 300:
                    return "Mark assets/purple-door.png";
                case 301:
                    return "Mark assets/purple-door-left.png";
                case 302:
                    return "Mark assets/purple-door-right.png";
                case 303:
                    return "Mark assets/purple-roof-left.png";
                case 304:
                    return "Mark assets/purple-roof-middle.png";
                case 305:
                    return "Mark assets/purple-roof-right.png";
                case 306:
                    return "Mark assets/purple-window-2.png";
                case 307:
                    return "Mark assets/purple-window-3.png";
                case 308:
                    return "Mark assets/red-roof-angled-bottom.png";
                case 309:
                    return "Mark assets/red-roof-angled-top.png";
                case 310:
                    return "Mark assets/red-roof-left.png";
                case 311:
                    return "Mark assets/red-roof-right.png";
                case 312:
                    return "Mark assets/red-window.png";
                case 313:
                    return "Mark assets/red-window-left.png";
                case 314:
                    return "Mark assets/red-window-right.png";
                case 315:
                    return "Mark assets/roof.png";
                case 316:
                    return "Mark assets/roof-bottom.png";
                case 317:
                    return "Mark assets/roof-bottom-left.png";
                case 318:
                    return "Mark assets/roof-bottom-right.png";
                case 319:
                    return "Mark assets/roof-left.png";
                case 320:
                    return "Mark assets/roof-right.png";
                case 321:
                    return "Mark assets/roof-top.png";
                case 322:
                    return "Mark assets/roof-top-left.png";
                case 323:
                    return "Mark assets/roof-top-right.png";
                case 324:
                    return "Mark assets/small-door-left.png";
                case 325:
                    return "Mark assets/small-door-right.png";
                case 326:
                    return "Mark assets/small-roof.png";
                case 327:
                    return "Mark assets/small-roof-bottom-left.png";
                case 328:
                    return "Mark assets/small-roof-bottom-right.png";
                case 329:
                    return "Mark assets/small-roof-top-left.png";
                case 330:
                    return "Mark assets/small-roof-top-right.png";
                case 331:
                    return "Mark assets/blue-window-angled.png";
                case 333:
                    return "Mark assets/purple-wall.png";
                case 334:
                    return "Mark assets/purple-dark-wall.png";
                case 335:
                    return "krish-assets/green-building-2---19.png";
                case 336:
                    return "krish-assets/pink-building-2---01.png";
                case 337:
                    return "krish-assets/pink-building-2---02.png";
                default:
                    return "Transparent-tile";
            }
        }
    };

    public String TilesMapping(int i){
        return "";
    }
}


