package deco2800.thomas.worlds;

public class Wall extends Tile {
    public Wall(String texture, int row, int col) {
        super(texture, row, col);
        this.setTexture(texture);
        this.setObstructed(true);
    }
}
