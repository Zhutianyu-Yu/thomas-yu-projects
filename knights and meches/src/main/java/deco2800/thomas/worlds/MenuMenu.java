package deco2800.thomas.worlds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import deco2800.thomas.GameScreen;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.observers.TouchUpObserver;
import org.lwjgl.Sys;

public class MenuMenu extends Group implements TouchDownObserver, KeyDownObserver {


    private Label inventoryLabel;

    private Label itemLabel;

    private Label characterLabel;

    private Label pauseLabel;

    private Label questLabel;

    private boolean used = false;


    private Label.LabelStyle labelStyle = new Label.LabelStyle();


    private static final String whiteBox = "inventory/box.jpg";

    private Integer lastKeyPushed;

    public MenuMenu() {



        createButtons();

        GameManager.getManagerFromInstance(InputManager.class).setMenuMenu(this);

        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenersMenuButtons(this);


    }

    private void createButtons() {

        this.labelStyle.font = new BitmapFont();

        int x = 0;
        int y = 0;
        int height = 50;
        int width = 164;

        Image background = new Image(new Texture(Gdx.files.internal("inventory/MenuMenu_bg_Screen.png")));
        background.setHeight(height*6);
        background.setWidth(width);
        this.addActor(background);

        Image inventoryMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        inventoryMenuButton.setPosition(x, y);
        inventoryMenuButton.setSize(width,height);
        inventoryMenuButton.setColor(0,0,0,0);
        inventoryLabel = new Label("Inventory", this.labelStyle);
        inventoryLabel.setPosition(x + width/2f - inventoryLabel.getPrefWidth()/2 , y + height/2f - inventoryLabel.getPrefHeight()/2 );
        inventoryLabel.setTouchable(Touchable.disabled);
        inventoryMenuButton.setName("inventoryMenuButton");
        this.addActor(inventoryMenuButton);

        y += height;

        Image itemMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        itemMenuButton.setPosition(x, y);
        itemMenuButton.setSize(width,height);
        itemMenuButton.setColor(0,0,0,0);
        itemLabel = new Label("Items", this.labelStyle);
        itemLabel.setPosition(x, y);
        itemLabel.setPosition(x + width/2f - itemLabel.getPrefWidth()/2 , y + height/2f - itemLabel.getPrefHeight()/2 );
        itemMenuButton.setName("itemMenuButton");
        this.addActor(itemMenuButton);

        y += height;

        Image characterMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        characterMenuButton.setPosition(x, y);
        characterMenuButton.setSize(width,height);
        characterMenuButton.setColor(0,0,0,0);
        characterLabel = new Label("Characters", this.labelStyle);
        characterLabel.setPosition(x + width/2f - characterLabel.getPrefWidth()/2 , y + height/2f - characterLabel.getPrefHeight()/2 );
        characterMenuButton.setName("characterMenuButton");
        this.addActor(characterMenuButton);

        y += height;

        Image pauseMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        pauseMenuButton.setPosition(x, y);
        pauseMenuButton.setSize(width,height);
        pauseMenuButton.setColor(0,0,0,0);
        pauseLabel = new Label("Pause", this.labelStyle);
        pauseLabel.setPosition(x + width/2f - pauseLabel.getPrefWidth()/2 , y + height/2f - pauseLabel.getPrefHeight()/2 );

        pauseMenuButton.setName("pauseMenuButton");
        this.addActor(pauseMenuButton);

        y += height;

        Image questMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        questMenuButton.setPosition(x, y);
        questMenuButton.setSize(width,height);
        questMenuButton.setColor(0,0,0,0);
        questLabel = new Label("Quests", this.labelStyle);
        questLabel.setPosition(x, y);
        questLabel.setPosition(x + width/2f - questLabel.getPrefWidth()/2 , y + height/2f - questLabel.getPrefHeight()/2 );

        questMenuButton.setName("questMenuButton");
        this.addActor(questMenuButton);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        inventoryLabel.draw(batch, parentAlpha);
        characterLabel.draw(batch, parentAlpha);
        itemLabel.draw(batch, parentAlpha);
        pauseLabel.draw(batch, parentAlpha);
        questLabel.draw(batch, parentAlpha);

    }

    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
        Vector2 tmp = getStage().screenToStageCoordinates(new Vector2((float) screenX, (float) screenY));
        Actor hitActor = getStage().hit(tmp.x, tmp.y, false);
        if (hitActor != null && hitActor.getName() != null) {
            int currentlyDisplayed = 0;
            if (getNumberDisplayedCurrently() == 1) {
                currentlyDisplayed = getCurrentlyDisplaying();
            } else {
                turnOffAllDisplays();
            }
            System.out.println("number displayed: " + getNumberDisplayedCurrently());
            System.out.println("current being displayed: " + getCurrentlyDisplaying());
            System.out.println("currentButton: " + hitActor.getName());
            switch (hitActor.getName()) {
                case "inventoryMenuButton":
                    if (currentlyDisplayed != 3) {
                        turnOffAllDisplays();
                    }
                    GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.I);
                    break;
                case "itemMenuButton":
                    if (currentlyDisplayed != 4) {
                        turnOffAllDisplays();
                    }
                    GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.O);
                    break;
                case "characterMenuButton":
                    if (currentlyDisplayed != 5) {
                        turnOffAllDisplays();
                    }
                    GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.B);
                    break;
                case "pauseMenuButton":
                    if (currentlyDisplayed != 2) {
                        turnOffAllDisplays();
                    }
                    GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.P);
                    break;
                case "questMenuButton":
                    if (currentlyDisplayed != 1) {
                        turnOffAllDisplays();
                    }
                    GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.Q);
                    break;
            }
        }

    }


    public void setUsed() {
        this.used = false;
    }

    public boolean getUsed() {
        return this.used;
    }


    @Override
    public void notifyKeyDown(int keycode) {

    }

    private void turnOffAllDisplays() {
        if (GameManager.getManagerFromInstance(InputManager.class).getQuestInterface().getQuestInterfaceDisplayed()) {
            GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.Q);
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getPauseInterface().getPauseInterfaceDisplayed()) {
            GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.P);
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getInventory().getMenu().isInventoryDisplayed()) {
            GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.I);
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getItemIllustrationInterface().getDisplayed()) {
            GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.O);
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getCharacterInformation().getDisplayed()) {
            GameManager.getManagerFromInstance(InputManager.class).keyDown(Input.Keys.B);
        }
    }

    private int getNumberDisplayedCurrently() {
        int output = 0;
        if (GameManager.getManagerFromInstance(InputManager.class).getQuestInterface().getQuestInterfaceDisplayed()) {
            output++;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getPauseInterface().getPauseInterfaceDisplayed()) {
            output++;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getInventory().getMenu().isInventoryDisplayed()) {
            output++;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getItemIllustrationInterface().getDisplayed()) {
            output++;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getCharacterInformation().getDisplayed()) {
            output++;
        }
        return output;
    }

    private int getCurrentlyDisplaying() {
        if (GameManager.getManagerFromInstance(InputManager.class).getQuestInterface().getQuestInterfaceDisplayed()) {
            return 1;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getPauseInterface().getPauseInterfaceDisplayed()) {
            return 2;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getInventory().getMenu().isInventoryDisplayed()) {
            return 3;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getItemIllustrationInterface().getDisplayed()) {
            return 4;
        }
        if (GameManager.getManagerFromInstance(InputManager.class).getCharacterInformation().getDisplayed()) {
            return 5;
        }
        return 0;
    }
}
