package deco2800.thomas.worlds.staticworlds;
import deco2800.thomas.worlds.StaticWorld;
import deco2800.thomas.worlds.Tile;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import deco2800.thomas.entities.DeathGhost;
import deco2800.thomas.entities.Dialogue;
import deco2800.thomas.managers.*;

/**
 * Class for the Death Ghost NPC World
 */
public class GhostWorld extends StaticWorld {

    private static final int WIDTH = 10;
    private static final int HEIGHT = 10;
    private boolean notGenerated = true;
    private DeathGhost ghost;
    private boolean showing = false;
    private long currentTick = 0;
    private long prevTick = 0;
    private int image = 0;
    private boolean initial = true;
    private int talk = 0;
    private static final String BLANK = "blank";


    public GhostWorld() {

        super();
    }

    @Override
    protected void addManagers() {
        AbstractManager manager = new ScreenManager();
        GameManager.get().addManager(manager);
    }


    @Override
    protected void generateWorld() {

        for (int q = -WIDTH; q < WIDTH; q++) {
            for (int r = -HEIGHT; r < HEIGHT; r++) {
                tiles.add(new Tile(BLANK, q, r));
            }
        }

        DeathGhost ghostDead = new DeathGhost(new Tile(BLANK, 0, 0), 1, BLANK, true);
        ghostDead.setCol(3f);
        ghostDead.setRow(0f);
        addEntity(ghostDead);
        this.ghost = ghostDead;
        this.notGenerated = false;
        createWorld();
        setupWorld();
    }

    public void setupWorld() {
        this.setBackgroundImage("ghostBG");
        this.notGenerated = false;
        createWorld();
        this.talk = 0;
        this.image = 0;
    }

    /**
     * Creates text that when clicked exits from screen
     */
    private void createWorld()
    {
        GameManager managerGame = GameManager.get();
        Stage stage = managerGame.getStage();
        Skin skin = managerGame.getSkin();
        if (skin != null)
        {
            stageVisibility(stage, false);
        }

        this.showing = true;
    }


    /**
     * Toggles visibility of staged actors
     * @param stage current game's stage
     * @param visibility true if actors are to be visible, else false
     */
    private void stageVisibility(Stage stage, boolean visibility)
    {
        for (Actor actor : stage.getActors())
        {
            actor.setVisible(visibility);
        }
    }


    /** Method for cleaning up ghost world and restoring old world how it was*/
    private void leaveGhostWorld(Stage stage) {
        stageVisibility(stage, true);
        LOGGER.info("Exiting GhostWorld");
        this.setBackgroundImage("black_bg");
        GameManager.get().restoreWorld();
        GameManager.get().resetCam(1.3f);
    }

    public void advance() {

        this.talk++;
        leaveGhostWorld(GameManager.get().getStage());
        this.showing = false;
    }

    public boolean getGenerated() {
        return this.notGenerated;
    }

    public boolean showing() {
        return this.showing;
    }


    @Override
    public void onTick(long i) {

        currentTick++;
        this.ghost.onTick(i);
        if (prevTick == 0 || currentTick - 30 > prevTick && initial) {
            this.image = (this.image + 1) % 5;
            if (this.image == 0) {
                initial = false;
                return;
            }
            this.ghost.setTexture("ghost" + this.image);
            prevTick = currentTick;
        } else if (currentTick - 10 > prevTick && !initial) {
            if (this.talk == 0) {
                this.talk++;
                Dialogue chat = new Dialogue(new Tile(BLANK, 1, 4), 1, BLANK, true);
                chat.setCol(4f);
                chat.setRow(4f);
                addEntity(chat);
                chat.setTexture("ghost-talk-" + this.talk);
            }
            if (this.image > 1) {
                this.image = 0;
            }
            this.image = this.image == 1 ? 0 : 1;
            this.ghost.setTexture("ghost-" + this.image);
            prevTick = currentTick;
        }
    }
}
