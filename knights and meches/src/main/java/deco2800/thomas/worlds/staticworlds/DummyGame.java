package deco2800.thomas.worlds.staticworlds;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class DummyGame extends ApplicationAdapter {


    private Table table;
    private Image inventoryMenuButton;
    private Label inventoryLabel;

    private Image itemMenuButton;
    private Label itemLabel;

    private Image characterMenuButton;
    private Label characterLabel;

    private Image pauseMenuButton;
    private Label pauseLabel;

    private Image questMenuButton;
    private Label questLabel;



    private Image inventoryMenu;
    private Image itemMenu;
    private Image characterMenu;
    private Image pauseMenu;
    private Image questMenu;


    private Label.LabelStyle labelStyle = new Label.LabelStyle();
    //private BitmapFont font = new BitmapFont(Gdx.files.internal("inventory/FontFiles/ComicSans/14_bold.fnt"));
    private BitmapFont font;


    private static final String whiteBox = "inventory/box.jpg";
    private Stage stage;

    public void createButtons(Stage stage) {
        int x = 300;
        int y = 500;
        int height = 30;
        int width = 120;
        inventoryMenuButton = new Image(new Texture(Gdx.files.internal(whiteBox)));
        inventoryMenuButton.setPosition(x, y);
        inventoryMenuButton.setSize(width,height); //Change to const
        inventoryMenuButton.setColor(Color.GREEN);
        inventoryLabel = new Label("Inventory", this.labelStyle);
        inventoryLabel.setPosition(x, y);
        stage.addActor(inventoryMenuButton);
        stage.addActor(inventoryLabel);

        y -= height;

        itemMenuButton =  new Image(new Texture(Gdx.files.internal(whiteBox)));
        itemMenuButton.setPosition(x, y);
        itemMenuButton.setSize(width,height); //Change to const
        itemMenuButton.setColor(Color.BLUE);
        itemLabel = new Label("Items", this.labelStyle);
        itemLabel.setPosition(x, y);
        stage.addActor(itemMenuButton);
        stage.addActor(itemLabel);

        y -= height;

        characterMenuButton =  new Image(new Texture(Gdx.files.internal(whiteBox)));
        characterMenuButton.setPosition(x, y);
        characterMenuButton.setSize(width,height); //Change to const
        characterMenuButton.setColor(Color.PURPLE);
        characterLabel = new Label("Characters", this.labelStyle);
        characterLabel.setPosition(x, y);
        stage.addActor(characterMenuButton);
        stage.addActor(characterLabel);

        y -= height;

        pauseMenuButton =  new Image(new Texture(Gdx.files.internal(whiteBox)));
        pauseMenuButton.setPosition(x, y);
        pauseMenuButton.setSize(width,height); //Change to const
        pauseMenuButton.setColor(Color.BLUE);
        pauseLabel = new Label("Pause", this.labelStyle);
        pauseLabel.setPosition(x, y);
        stage.addActor(pauseMenuButton);
        stage.addActor(pauseLabel);

        y -= height;

        questMenuButton =  new Image(new Texture(Gdx.files.internal(whiteBox)));
        questMenuButton.setPosition(x, y);
        questMenuButton.setSize(width,height); //Change to const
        questMenuButton.setColor(Color.BROWN);
        questLabel = new Label("Quests", this.labelStyle);
        questLabel.setPosition(x, y);
        stage.addActor(questMenuButton);
        stage.addActor(questLabel);
    }

    @Override
    public void create() {


        System.out.println("width: " + Gdx.graphics.getWidth());
        stage = new Stage(new ScreenViewport());
        this.font = new BitmapFont();
        this.labelStyle.font = this.font;

        createButtons(stage);

    }

    @Override
    public void render() {

        Gdx.gl.glClearColor(1, 0, 0, 1); //rgba format
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }


    @Override
    public void dispose() {
        stage.dispose();
    }



}
