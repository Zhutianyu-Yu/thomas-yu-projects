package deco2800.thomas.worlds.staticworlds;

import deco2800.thomas.worlds.*;
import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class TeleportedRoom extends StaticWorld {
    private final Logger logger = LoggerFactory.getLogger(TeleportedRoom.class);
    public TeleportedRoom(){
        super();
        setBirthPointPosition(5,6);
        this.name = "Dungeon Hub";
    }

    /**
     * read file resources/TeleportedRoomMap and generate the world with given map
     */
    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/teleporter-room-assets/mapFile","resources/teleporter-room-assets/teleporterRoom");
        } catch (IOException e) {
            e.printStackTrace();
        }
        addTeleporter(5,7,WorldTypes.HUB_WORLD.toString());
        addTeleporter(6,7,WorldTypes.HUB_WORLD.toString());
        addTeleporter(1,1,WorldTypes.TUNDRA_DUNGEON.toString());
        addTeleporter(2,1,WorldTypes.TUNDRA_DUNGEON.toString());
        addTeleporter(1,0,WorldTypes.TUNDRA_DUNGEON.toString());
        addTeleporter(2,0,WorldTypes.TUNDRA_DUNGEON.toString());
        addTeleporter(5,1,WorldTypes.VOLCANO_BIOME.toString());
        addTeleporter(6,1,WorldTypes.VOLCANO_BIOME.toString());
        addTeleporter(5,0,WorldTypes.VOLCANO_BIOME.toString());
        addTeleporter(6,0,WorldTypes.VOLCANO_BIOME.toString());
        addTeleporter(9,1,WorldTypes.FOREST_BIOME.toString());
        addTeleporter(10,1,WorldTypes.FOREST_BIOME.toString());
        addTeleporter(9,0,WorldTypes.FOREST_BIOME.toString());
        addTeleporter(10,0,WorldTypes.FOREST_BIOME.toString());
        GameManager.get().roomCam();
    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }
}
