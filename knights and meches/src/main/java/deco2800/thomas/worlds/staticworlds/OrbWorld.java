package deco2800.thomas.worlds.staticworlds;
import deco2800.thomas.worlds.StaticWorld;
import deco2800.thomas.entities.*;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;

/**
 * A class for Getting Orbs
 */
public class OrbWorld extends StaticWorld {

    private OrbEntity orb;
    private String background;

    public OrbWorld(OrbEntity orb) {
        super();
        this.orb = orb;
        if(orb.getTexture().equals("orb_violet")) {
            this.background = "bg_orb_violet";
        } else if (orb.getTexture().equals("orb_red")){
            this.background = "bg_orb_red";
        } else if (orb.getTexture().equals("orb_blue")){
            this.background = "bg_orb_blue";
        } else if (orb.getTexture().equals("orb_yellow")){
            this.background = "bg_orb_yellow";
        } else if (orb.getTexture().equals("orb_pink")){
            this.background = "bg_orb_pink";
        }
    }

    /**
     * Method to setup the Orb World
     */
    public void setup() {
        generateWorld();
    }

//    public void backToTest(){
//        step++;
//        if(step==1) {
//            WorldUtil.backToPrevious();
//        }
//    }

    @Override
    protected void generateWorld() {
        this.setBackgroundImage("black_bg");
    }


    @Override
    protected void addManagers()
    {
        GameManager.get().addManager(new ScreenManager());
    }

    public OrbEntity getOrb() {
        return this.orb;
    }

    public String getBackground() {
        return this.background;
    }

}
