package deco2800.thomas.worlds.staticworlds;

import deco2800.thomas.worlds.StaticWorld;
import deco2800.thomas.worlds.Tile;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import deco2800.thomas.entities.*;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.observers.TouchDownObserver;

/**
 * A class for the Tutorials
 */
public class TutorialWorld extends StaticWorld implements TouchDownObserver {

    // Keep count of the number of times we are cycling through
    private int tute;
    private static final int WIDTH = 10;
    private static final int HEIGHT = 4;

    public TutorialWorld() {

        super();
        this.tute = 0;
        GameManager.getManagerFromInstance(InputManager.class).addTuteWorld(this);
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenerTutorial(this);

    }

    /**
     * Method to cycle through the images in the Tutorial World
     */
    public void nextTute() {

        this.tute++;

        if (this.tute == 13) {
            leaveTutorialWorld(GameManager.get().getStage());
        } else {
            this.setBackgroundImage("tute" + this.tute);
        }


    }

    @Override
    protected void generateWorld() {

        for (int q = -WIDTH; q < WIDTH; q++) {
            for (int r = -10; r < HEIGHT; r++) {
                tiles.add(new Tile("blank", q, r));
            }
        }

        setupWorld();

    }

    public void setupWorld() {
        this.setBackgroundImage("tute1");
        GameManager.get().zeroCamera();
        createWorld();
    }

    /**
     * Creates text that when clicked exits from screen
     */
    private void createWorld()
    {
        GameManager manager = GameManager.get();
        Stage stage = manager.getStage();
        Skin skin = manager.getSkin();
        if (skin != null)
        {
            stageVisibility(stage, false);
        }


    }

    /**
     * Toggles visibility of staged actors
     * @param stage current game's stage
     * @param visibility true if actors are to be visible, else false
     */
    private void stageVisibility(Stage stage, boolean visibility)
    {
        for (Actor actor : stage.getActors())
        {
            actor.setVisible(visibility);
        }
    }

    /** Method for cleaning up ghost world and restoring old world how it was*/
    private void leaveTutorialWorld(Stage stage) {
        stageVisibility(stage, true);
        LOGGER.info("Exiting GhostWorld");
        this.setBackgroundImage("black_bg");
        GameManager.get().restoreWorld();
        GameManager.get().resetCam(1.3f);
        this.tute = 0;
    }

    @Override
    protected void addManagers()
    {
        GameManager.get().addManager(new ScreenManager());
    }

    /**
     * Notifies the observers of the mouse button being pushed down
     *
     * @param screenX the x position the mouse was pressed at
     * @param screenY the y position the mouse was pressed at
     * @param pointer
     * @param button  the button which was pressed
     */
    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {
        nextTute();
    }
}
