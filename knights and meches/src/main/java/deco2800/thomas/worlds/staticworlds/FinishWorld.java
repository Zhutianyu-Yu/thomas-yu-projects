package deco2800.thomas.worlds.staticworlds;

import deco2800.thomas.worlds.StaticWorld;
import deco2800.thomas.worlds.Tile;
import deco2800.thomas.worlds.LevelWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import deco2800.thomas.managers.AbstractManager;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.WorldTypes;

public class FinishWorld extends StaticWorld implements TouchDownObserver {

    private boolean showing = false;



    public FinishWorld() {
        super();
        GameManager.getManagerFromInstance(InputManager.class).addFinishWorld(this);
    }

    public void setup() {
        generateWorld();
    }


    @Override
    protected void generateWorld() {
        this.setBackgroundImage("gameOver");
        for (int q = -7; q < 6; q++) {
            for (int r = -5; r < 4; r++) {
                tiles.add(new Tile("blank", q, r));
            }
        }
        createWorld();
    }



    /**
     * Creates text that when clicked exits from screen
     */
    private void createWorld()
    {
        GameManager manager = GameManager.get();
        Stage stage = manager.getStage();
        Skin skin = manager.getSkin();
        if (skin != null)
        {
            stageVisibility(stage, false);
        }

        this.showing = true;
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenerFinish(this);
    }


    /**
     * Toggles visibility of staged actors
     * @param stage current game's stage
     * @param visibility true if actors are to be visible, else false
     */
    private void stageVisibility(Stage stage, boolean visibility)
    {
        for (Actor actor : stage.getActors())
        {
            actor.setVisible(visibility);
        }
    }

    /** Method for cleaning up ghost world and restoring old world how it was*/
    private void leaveFinishWorld(Stage stage) {
        stageVisibility(stage, true);
        LOGGER.info("Exiting GameOverWorld");
        this.setBackgroundImage("black_bg");
        ((LevelWorld)GameManager.get().getPreviousWorld()).generateOrbs();
        GameManager.get().restoreWorld();
        GameManager.get().resetCam(1.3f);
        GameManager.getManagerFromInstance(InputManager.class).getFinishedWorld().setShowing(false);
    }


    private void startAgain(Stage stage) {
        for (Actor actor : stage.getActors()) {
            actor.remove();
        }
        LOGGER.info("Starting a new game");
        this.setBackgroundImage("black_bg");
        GameManager.getManagerFromInstance(InputManager.class).removeListeners();
        GameManager.get().changeWorld(WorldTypes.HUB_WORLD);
        GameManager.get().resetCam(1.3f);
        GameManager.getManagerFromInstance(InputManager.class).getFinishedWorld().setShowing(false);
    }

    public void setShowing(boolean showing) {
        this.showing = showing;
    }

    public boolean showing() {
        return this.showing;
    }

    @Override
    protected void addManagers() {
        AbstractManager manager = new ScreenManager();
        GameManager.get().addManager(manager);
    }


    /**
     * Notifies the observers of the mouse button being pushed down
     *
     * @param screenX the x position the mouse was pressed at
     * @param screenY the y position the mouse was pressed at
     * @param pointer
     * @param button  the button which was pressed
     */
    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

        float[] mouse = WorldUtil.screenToWorldCoordinates(Gdx.input.getX(), Gdx.input.getY());
        float[] clickedPosition = WorldUtil.worldCoordinatesToColRow(mouse[0], mouse[1]);

        if (-6 < clickedPosition[1] && clickedPosition[1] < -3) {
            if (2 < clickedPosition[0] && clickedPosition[0] < 5) {
                leaveFinishWorld(GameManager.get().getStage());
            } else if (-8 < clickedPosition[0] && clickedPosition[0] < -1) {
                startAgain(GameManager.get().getStage());
            }
        }
    }
}
