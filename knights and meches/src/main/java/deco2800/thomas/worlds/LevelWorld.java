package deco2800.thomas.worlds;

import deco2800.thomas.util.WorldUtil;

/** 
 * A class for all of our worlds related to levels to inherit from. 
 */
public abstract class LevelWorld extends RoomWorld {

    private boolean notGenerated = true;
    
    protected abstract void generateEnemies();


    @Override
    public void onTick(long i) {
        super.onTick(i);
        WorldUtil.detectEnemyCombat(this);
    }

    public void setNotGenerated(boolean notGenerated) {
        this.notGenerated = notGenerated;
    }

    public boolean getNotGenerated() {
        return notGenerated;
    }
}
