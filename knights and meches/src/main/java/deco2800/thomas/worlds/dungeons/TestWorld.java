package deco2800.thomas.worlds.dungeons;

import deco2800.thomas.entities.combat.*;
import deco2800.thomas.worlds.LevelWorld;
import deco2800.thomas.worlds.Tile;
import deco2800.thomas.worlds.WorldTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import deco2800.thomas.entities.*;

import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.WorldUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import deco2800.thomas.managers.GameManager;

@SuppressWarnings("unused")
public class TestWorld extends LevelWorld {
	private final Logger logger = LoggerFactory.getLogger(TestWorld.class);
	/*
	 * radius for tiles 1 - 7 2 - 19 3 - 37 4 - 61 5 - 91 10 - 331 25 - 1951 50 -
	 * 7,651 100 - 30,301 150 - 67,951 200 - 120601
	 * 
	 * N = 1 + 6 * summation[0 -> N]
	 */
	
	private final int WORLD_WIDTH = 25; // Height and width vars for the map size; constrains tile gen
	private final int WORLD_HEIGHT = 25; // Note the map will double these numbers (bounds are +/- these limits)
	private int[][] grid;

	public static String[] ORBS = {"orb_violet","orb_blue","orb_pink","orb_red","orb_yellow"};



	private TraderNPC trader;

	public TestWorld() {
		super();
	}

	@Override
	protected void addManagers()
	{
		GameManager.get().addManager(new ScreenManager());
	}

	//5 tile building
	private StaticEntity createBuilding1(float col, float row) {
		StaticEntity building;
		List<Part> parts = new ArrayList<Part>();	
		
		parts.add(new Part(new SquareVector(1, -1f), "spacman_ded", true));
		parts.add(new Part(new SquareVector(-1, -1f), "spacman_ded", true));
		parts.add(new Part(new SquareVector(-1, 1f), "spacman_ded", true));
		parts.add(new Part(new SquareVector(1, 1f), "spacman_ded", true));
		parts.add(new Part(new SquareVector(0, 0), "spacman_ded", true));

		return  new StaticEntity(col, row, 1, parts);
	}
	
	//building with a fence
	private StaticEntity createBuilding2(float col, float row) {
		List<Part> parts = new ArrayList<Part>();
		/*parts.add(new Part(new SquareVector(0, 0), "buildingA", true));

		// left
		parts.add(new Part(new SquareVector(-2, 0), "fenceN-S", true));
		parts.add(new Part(new SquareVector(-2, 1), "fenceN-S", true));

		// Bottom
		parts.add(new Part(new SquareVector(-1, -1) , "fenceE-W", true));
		parts.add(new Part(new SquareVector(0, -1) , "fenceE-W", true));
		parts.add(new Part(new SquareVector(1, -1) , "fenceE-W", true));

		// Top
		parts.add(new Part(new SquareVector(-1, 2) , "fenceE-W", true));
		parts.add(new Part(new SquareVector(0, 2) , "fenceE-W", true));
		parts.add(new Part(new SquareVector(1, 2) , "fenceE-W", true));

		// bottom right corner
		parts.add(new Part(new SquareVector(2,-1), "fenceN-W", true));

		// bottom left
		parts.add(new Part(new SquareVector(-2,-1), "fenceN-E", true));

		// top left
		parts.add(new Part(new SquareVector(-2,2), "fenceS-E", true));

		// top right
		parts.add(new Part(new SquareVector(2,2), "fenceS-W", true));*/

		for (Part part : parts) {
			this.grid[Math.round(part.getPosition().getCol())+WORLD_WIDTH]
					[Math.round(part.getPosition().getRow())+WORLD_HEIGHT] = 1;
		}

		StaticEntity building =  new StaticEntity(col, row,1, parts);
		this.grid[Math.round(col)+WORLD_WIDTH][Math.round(row)+WORLD_HEIGHT] = 1;
		entities.add(building);
		return building;
		
	}

	private void addTree(float col, float row) {
		Map<SquareVector, String> textures = new HashMap<SquareVector, String>();
		Tile t = GameManager.getWorld().getTile(col, row);
		Tree tree = new  Tree(t, true);
		entities.add(tree);
	}

	//this get ran on first game tick so the world tiles exist.
	@Override
	public void generateBuildings() {
		entities.add(createBuilding2(-5, 0f));
		Random random = new Random();
		int tileCount = GameManager.getWorld().getTileMap().size();
		// Generate some rocks to mine later
		for (int i = 0; i < 100;  i++) {
			Tile t = GameManager.getWorld().getTile(random.nextInt(tileCount));
			if (t != null && this.grid[Math.round(t.getCol()) + WORLD_WIDTH]
					[Math.round(t.getRow()) + WORLD_HEIGHT] == 0) {
				entities.add(new Generator(t,true));
				this.grid[Math.round(t.getCol()) + WORLD_WIDTH][Math.round(t.getRow()) + WORLD_HEIGHT] = 1;
			}
		}
		// Add some trees
		for (int i = 0; i < 50;  i++) {
			Tile t = GameManager.getWorld().getTile(random.nextInt(tileCount));
			if (t != null && this.grid[Math.round(t.getCol()) + WORLD_WIDTH]
					[Math.round(t.getRow()) + WORLD_HEIGHT] == 0) {
				entities.add(new Tree(t,true));
				this.grid[Math.round(t.getCol()) + WORLD_WIDTH][Math.round(t.getRow()) + WORLD_HEIGHT] = 1;
			}
		}

		boolean empty = true;
		int increase = 0;
		while (empty) {
			if (this.grid[3+increase+WORLD_WIDTH][14+WORLD_HEIGHT] == 0) {
				empty = false;
			} else {
				increase++;
			}
		}

		empty = true;
		increase = 0;
		while (empty) {
			if (this.grid[-5+increase+WORLD_WIDTH][8+WORLD_HEIGHT] == 0) {
				empty = false;
			} else {
				increase++;
			}
		}

		for(int i=0; i<=7; i++){
			Tile t = null;
			while (t == null) {
				t = GameManager.getWorld().getTile(random.nextInt(tileCount));
			}
			entities.add(new Chest(t,"chest_close"));
		}

		entities.add(createBuilding2(-5, 0f));
		Tile t = new Tile();
		this.trader = new TraderNPC(t, "door0");
		entities.add(this.trader);
		this.trader.setCol(-5+increase);
		this.trader.setRow(8);
		this.grid[Math.round(t.getCol())+WORLD_WIDTH][Math.round(t.getRow())+WORLD_HEIGHT] = 1;

	}

	public static String[] getOrbs() {
		return ORBS;
	}

	@Override
	protected void generateWorld() {
		this.grid = new int[2 * WORLD_WIDTH][2 * WORLD_HEIGHT];
		Random random = new Random();
		for (int q = -WORLD_WIDTH; q < WORLD_WIDTH; q++) {
			for (int r = -WORLD_HEIGHT; r < WORLD_HEIGHT; r++) {
				int elevation = random.nextInt(3);
				String type = "grass_";
				type += elevation;
				tiles.add(new Tile(type, q, r));
				this.grid[q + WORLD_WIDTH][r + WORLD_HEIGHT] = 0;
			}
		}

	}

	@Override
	public void onTick(long i) {
		super.onTick(i);
		for (AbstractEntity e : this.getEntities()) {
			if (GameManager.get().getManager(InputManager.class).getQuestInterface().
					getQuestInterfaceDisplayed() || GameManager.get().getManager
					(InputManager.class).getPauseInterface().getPauseInterfaceDisplayed()) {
				continue;
			}
			e.onTick(0);
		}
		if (this.getNotGenerated()) {

			this.grid[WORLD_WIDTH][WORLD_HEIGHT] = 1;
			this.grid[8+WORLD_WIDTH][7+WORLD_HEIGHT] = 1;
			this.grid[4+WORLD_WIDTH][12+WORLD_HEIGHT] = 1;
			this.grid[-10+WORLD_WIDTH][-6+WORLD_HEIGHT] = 1;
			this.grid[-20+WORLD_WIDTH][-20+WORLD_HEIGHT] = 1;
			this.grid[3+WORLD_WIDTH][3+WORLD_HEIGHT] = 1;
			this.grid[WORLD_WIDTH][3+WORLD_HEIGHT] = 1;
			generateEnemies();

			// Add a chest to the map
			addChest(7,7,"chest_close");
			this.grid[7+WORLD_WIDTH][7+WORLD_HEIGHT] = 1;


			generateOrbs();

			generateBuildings();
			addTeleporter(3,9,WorldTypes.HUB_WORLD.toString());
			this.setNotGenerated(false);
		}

		if (WorldUtil.getOrbsCollected() == ORBS.length) {
			//TODO WorldUtil.createFinishWorld();
		}
	}

	public TraderNPC getTrader() {
		return this.trader;
	}

	@Override
	public void generateEnemies() {
		addEntity(new EnemyPeon(0, 0, 0.03f, 5, 5));
		addEntity(new SoldierEnemy(7, 8, 0.03f, 5, 5));
		addEntity(new SoldierEnemy(12, 4, 0.03f, 5, 5));
		addEntity(new SoldierEnemy(-6, -10, 0.03f, 5, 5));
		addEntity(new RobotEnemy(-20, -20, 0.03f, 5, 5));
		addEntity(new RobotEnemy(-20, -20, 0.03f, 5, 5));
		addEntity(new RobotEnemy(-20, -20, 0.03f, 5, 5));
		addEntity(new CoreEnemy(-20, -20,0.03f,5,5));
		addEntity(new CPUEnemy(-20, -20,0.03f,5,5));
		addEntity(new JellyfishEnemy(-20, -20,0.03f,5,5));
		addEntity(new SeekerEnemy(-20, -20,0.03f,5,5));
	}

	@Override
	public void generateOrbs() {
		addOrb(5, 5, "orb_violet");
		addOrb(-5, 5, "orb_blue");
		addOrb(-5, -5, "orb_pink");
		addOrb(0, 0, "orb_red");
		addOrb(5, -5, "orb_yellow");
	}

	private void addOrb(int column, int row, String texture)
	{
		addOrbEntity(column, row, texture);
		addEntity(new BossEnemy(row, column, 0, 0, 0));
		this.grid[column + WORLD_WIDTH][row + WORLD_HEIGHT] = 1;
	}


}
/*
 * print out Neighbours for (Tile tile : tiles) { System.out.println();
 * System.out.println(tile); for (Entry<Integer, Tile> firend :
 * tile.getNeighbours().entrySet()) { switch (firend.getKey()) { case
 * Tile.north: System.out.println("north " +(firend.getValue())); break; case
 * Tile.north_east: System.out.println("north_east " + (firend.getValue()));
 * break; case Tile.north_west: System.out.println("north_west " +
 * (firend.getValue())); break; case Tile.south: System.out.println("south " +
 * (firend.getValue())); break; case Tile.south_east:
 * System.out.println("south_east " +(firend.getValue())); break; case
 * Tile.south_west: System.out.println("south_west " + (firend.getValue()));
 * break; } } }
 * 
 */
