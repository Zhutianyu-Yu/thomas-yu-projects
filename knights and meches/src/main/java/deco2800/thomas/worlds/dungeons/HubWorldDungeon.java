package deco2800.thomas.worlds.dungeons;

import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.entities.combat.CoreEnemy;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.LevelWorld;
import deco2800.thomas.worlds.WorldTypes;

import java.io.IOException;

public class HubWorldDungeon extends LevelWorld {

    final float SPAWN_POS_ROW = 4f;
    final float SPAWN_POS_COL = 20f;

    public HubWorldDungeon() {
        super();
        this.name = "Decodia's Underrealm";
    }


    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/HubWorldDungeon/mapFile","resources/HubWorldDungeon/HubWorldDungeonMap");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        addTeleporter(20, 0, WorldTypes.HUB_WORLD.toString());
        addTeleporter(21, 0, WorldTypes.HUB_WORLD.toString());


        setBirthPointPosition(20,4);
        GameManager.get().roomCam();

        if (GameManager.getParty() != null) {
            GameManager.get().updatePartyPosition(SPAWN_POS_ROW, SPAWN_POS_COL);
        }
        GameManager.get().roomCam(SPAWN_POS_ROW, SPAWN_POS_COL);
        generateEnemies();

    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }

    @Override
    protected void generateEnemies() {
        addEntity(new CoreEnemy(9, 20, 0.5f, 2, 5));

    }

    @Override
    public void generateBuildings() {

    }

    @Override
    public void generateOrbs() {
        addOrb(20 ,25,"Orb_Violet");
    }

    private void addOrb(int column, int row, String texture)
    {
        addOrbEntity(column, row, texture);
        addEntity(new BossEnemy(row, column, 0, 0, 0));
    }
}
