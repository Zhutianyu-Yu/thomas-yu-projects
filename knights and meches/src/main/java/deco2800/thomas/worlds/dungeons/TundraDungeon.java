package deco2800.thomas.worlds.dungeons;

import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.entities.combat.SeekerEnemy;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.LevelWorld;
import deco2800.thomas.worlds.WorldTypes;

import java.io.IOException;

public class TundraDungeon extends LevelWorld {

    final float SPAWN_POS_ROW = 3f;
    final float SPAWN_POS_COL = 48f;

    public TundraDungeon() {
        super();
        this.name = "Tundra Cavern";
    }

    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/biomes.tundra/mapFile","resources/biomes.tundra/TundraDungeonMap");
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBirthPointPosition(48,3);
        addTeleporter(47,0, WorldTypes.TELEPORTER.toString());
        addTeleporter(48,0, WorldTypes.TELEPORTER.toString());
        addTeleporter(49,0, WorldTypes.TELEPORTER.toString());


        if (GameManager.getParty() != null) {
            GameManager.get().updatePartyPosition(SPAWN_POS_ROW, SPAWN_POS_COL);
        }
        GameManager.get().roomCam(SPAWN_POS_ROW, SPAWN_POS_COL);
        generateEnemies();
    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }

    @Override
    protected void generateEnemies() {
        addEntity(new SeekerEnemy(6, 46,0.03f,5,3));
    }

    @Override
    public void generateBuildings() {
    }

    @Override
    public void generateOrbs() {
        addOrb(48 ,22,"Orb_Blue");
    }

    private void addOrb(int column, int row, String texture)
    {
        addOrbEntity(column, row, texture);
        addEntity(new BossEnemy(row, column, 0, 0, 0));
    }
}
