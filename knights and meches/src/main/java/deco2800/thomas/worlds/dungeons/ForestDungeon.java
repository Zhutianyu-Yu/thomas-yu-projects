package deco2800.thomas.worlds.dungeons;

import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;
import deco2800.thomas.worlds.LevelWorld;
import deco2800.thomas.worlds.WorldTypes;

import java.io.IOException;

public class ForestDungeon extends LevelWorld {

    final float SPAWN_POS_ROW = 11f;
    final float SPAWN_POS_COL = 34f;


    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/biome.forest/mapFile","resources/biome.forest/ForestWorld");
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBirthPointPosition(34,11);
        addTeleporter(33,9, WorldTypes.TELEPORTER.toString());
        addTeleporter(34,9, WorldTypes.TELEPORTER.toString());
        addTeleporter(35,9, WorldTypes.TELEPORTER.toString());
        GameManager.get().roomCam();

        if (GameManager.getParty() != null) {
            GameManager.get().updatePartyPosition(SPAWN_POS_ROW, SPAWN_POS_COL);
        }
        GameManager.get().roomCam(SPAWN_POS_ROW, SPAWN_POS_COL);
    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }

    @Override
    protected void generateEnemies() {

    }

    @Override
    public void generateBuildings() {

    }

    @Override
    public void generateOrbs() {
        addOrb(42 ,48,"Orb_Yellow");
    }

    private void addOrb(int column, int row, String texture)
    {
        addOrbEntity(column, row, texture);
        addEntity(new BossEnemy(row, column, 0, 0, 0));
    }
}
