package deco2800.thomas.worlds.dungeons;
import deco2800.thomas.entities.combat.*;
import deco2800.thomas.worlds.LevelWorld;
import deco2800.thomas.worlds.WorldTypes;

import deco2800.thomas.entities.AbstractEntity;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.util.WorldUtil;

import java.io.IOException;

public class VolcanoBiome extends LevelWorld{

    public VolcanoBiome() {
        super();
        this.name = "Heart of the Volcano";
    }


    @Override
    protected void generateWorld() {
        try {
            loadingTiles("resources/biomes/volcano/dungeon/mapFile","resources/biomes/volcano/dungeon/VolcanoWorld");
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBirthPointPosition(57,3);
        addTeleporter(54,0,WorldTypes.TELEPORTER.toString());
        addTeleporter(55,0,WorldTypes.TELEPORTER.toString());
        addTeleporter(56,0,WorldTypes.TELEPORTER.toString());
        addTeleporter(57,0,WorldTypes.TELEPORTER.toString());
        addTeleporter(58,0,WorldTypes.TELEPORTER.toString());
        addTeleporter(59,0,WorldTypes.TELEPORTER.toString());
        generateEnemies();
    }

    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            e.onTick(0);
        }
        WorldUtil.detectTeleporter(this);
        if (this.getNotGenerated())
        {
            generateOrbs();
            this.setNotGenerated(false);
        }
    }

    @Override
    protected void addManagers() {
        GameManager.get().addManager(new ScreenManager());
    }

    @Override
    protected void generateEnemies() {
        addEntity(new CoreEnemy(54, 31, 0.06f, 3, 3));
        addEntity(new JellyfishEnemy(49, 15, 0.15f, 2, 2));
        addEntity(new CPUEnemy(60, 32, 0.1f, 3, 3));
        addEntity(new EnemyPeon(15, 33, 0.03f, 5, 5));
        addEntity(new SoldierEnemy(29, 3, 0.05f, 4, 8));
        addEntity(new RobotEnemy(56, 25, 0.03f, 5, 5));
        addEntity(new SeekerEnemy(52, 33, 0.03f, 7, 5));
    }

    @Override
    public void generateBuildings() {
    }


    @Override
    public void generateOrbs() {
        addOrb(32 ,39, "Orb_Red");
        addChest(25,16, "chest_close");
    }

    private void addOrb(int column, int row, String texture)
    {
        addOrbEntity(column, row, texture);
        addEntity(new BossEnemy(row, column, 0, 0, 0));
    }
}
