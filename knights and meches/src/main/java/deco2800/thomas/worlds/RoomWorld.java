package deco2800.thomas.worlds;

import deco2800.thomas.entities.*;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.WorldUtil;

import java.util.List;



/** 
 * A class for all of our worlds related to levels to inherit from. 
 */
public abstract class RoomWorld extends StaticWorld {

    boolean notGenerated = true;
    private static final int DEFAULT_ENEMY_RANGE = 1;

    protected void generateWorld() {
        // do nothing
    }

    @Override
    protected void addManagers() {
    }

    @Override
    public void onTick(long i) {
        super.onTick(i);
        WorldUtil.detectTeleporter(this);
        WorldUtil.detectTraderNPC(this);
        WorldUtil.detectQuestActivator(this);
    }

    public boolean getGenerated() {
		return this.notGenerated;
	}

    public abstract void generateBuildings();

    public abstract void generateOrbs();


     /**
     * Creates the orb at the passed in position,
     * add it to words
     * @param col column
     * @param row row
     */
    public void addOrbEntity(float col, float row, String texture) {
        Tile t = GameManager.getWorld().getTile(col, row);
        OrbEntity orb = new OrbEntity(t,texture);
        addEntity(orb);
    }


    public Chest addChest(float col, float row, String texture) {
        Tile t = GameManager.getWorld().getTile(col, row);
        Chest chest = new Chest(t,texture);
        addEntity(chest);
        return chest;
    }

    public void addBomb(SquareVector position) {
        Tile t = GameManager.get().getWorld().getTile(position);
        BombEntity bomb = new BombEntity(t);
        addEntity(bomb);
    }
    public void addBomb(float col, float row) {
        Tile t = GameManager.get().getWorld().getTile(col,row);
        BombEntity bomb = new BombEntity(t);
        addEntity(bomb);
    }

        /**
     * Searches every enemy entity in the world to see if any are in range of
     * the player
     * @param entityType The type of entity to find
     * @return the EnemyPeon object if they are in range, null if no enemies
     * are in range of the player
     */
    public AbstractEntity isEntityInRange(AbstractEntity entityType) {
        AbstractEntity player = getPlayerPeon();

        //Ensure a player exists, and class is of an entity
        if (player == null || !AbstractEntity.class.isAssignableFrom(entityType.getClass()) ) {
            return null;
        }

        List<AbstractEntity> allEntities = getEntities();

        //Search through every entity present in the world
        for (AbstractEntity entity : allEntities) {
            if (entity.getClass() == entityType.getClass()) {

                //Get the distance between enemy and player
                int verticalDistance = (int) Math.abs(entity.getPosition().getRow()
                        -  player.getPosition().getRow());
                int horizontalDistance = (int) Math.abs(entity.getPosition().getCol()
                        -  player.getPosition().getCol());

                //If enemy is close enough, return the enemy
                if (verticalDistance <= DEFAULT_ENEMY_RANGE &&
                        horizontalDistance <= DEFAULT_ENEMY_RANGE) {
                    return entity;
                }
            }
        }

        return null; //No enemies were found in range
    }
}
