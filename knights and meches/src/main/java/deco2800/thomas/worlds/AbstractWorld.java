package deco2800.thomas.worlds;

import deco2800.thomas.GameScreen;
import deco2800.thomas.entities.*;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.ScreenManager;
import deco2800.thomas.managers.TextureManager;
import deco2800.thomas.util.SquareVector;
import deco2800.thomas.util.Party;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.*;

import static deco2800.thomas.managers.GameManager.*;

/**
 * AbstractWorld is the Game AbstractWorld
 *
 * It provides storage for the WorldEntities and other universal world level items.
 */
public abstract class AbstractWorld {
    protected final org.slf4j.Logger LOGGER =  LoggerFactory.getLogger(AbstractWorld.class);

    protected List<AbstractEntity> entities = new CopyOnWriteArrayList<>();
    protected List<Teleporter> teleporters = new CopyOnWriteArrayList<>();
    protected List<QuestActivator> questActivators = new CopyOnWriteArrayList<>();
    protected Mech gameMech;

    private static final int DEFAULT_ENEMY_RANGE = 1;

    protected CopyOnWriteArrayList<Tile> tiles;
    protected CopyOnWriteArrayList<Wall> walls = new CopyOnWriteArrayList<>();

    protected List<AbstractEntity> entitiesToDelete = new CopyOnWriteArrayList<>();
    protected List<Tile> tilesToDelete = new CopyOnWriteArrayList<>();

    private final int[] birthPointPosition = new int[2];

    /**
     * Represents the world name at the top of the screen
     */
    protected String name = null;

    protected AbstractWorld() {
    	tiles = new CopyOnWriteArrayList<>();

    	addManagers();
    	generateWorld();
    	generateNeighbours();
    	generateTileIndices();

    	if (GameManager.getParty() != null) {
            this.addEntity(GameManager.getParty().getPlayer(PlayerType.KNIGHT));
            this.addEntity(GameManager.getParty().getPlayer(PlayerType.WIZARD));
        }

    }

    /**
     * Name which will appear at the top of the screen to represent this world
     *
     * @return the name as a string
     */
    public String getName() {

        if (name == null) {
            return this.getClass().getSimpleName();
        }

        return name;
    }

    /**
     * Sets the name of this world to given parameter
     *
     * @param name the name of this world
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set the position of the BirthPoint
     * @param col the col of the BirthPoint
     * @param row the row of the BirthPoint
     */
    public void setBirthPointPosition(int col,int row){
        birthPointPosition[0] = col;
        birthPointPosition[1] = row;
    }

    /**
     * get the BirthPoint position
     * (If this is not set in the corresponding world, the default is 0,0)
     * @return the BirthPoint position
     */
    public int[] getBirthPointPosition(){
        return birthPointPosition;
    }


    public void loadingTiles(String mapPath, String obstructedPath) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(mapPath))) {
            String str;
            String[] a;
            //read the first line(the size of the map)
            str = bufferedReader.readLine();
            a = str.split("x");
            int col = Integer.parseInt(a[0]);
            int row = Integer.parseInt(a[1]);
            //get the world type
            str = bufferedReader.readLine();
            WorldTypes worldTypes = WorldTypes.valueOf(str);
            LOGGER.info("get world type");
            while ((str = bufferedReader.readLine()) != null) {
                row--;
                a = str.split(",");
                for (int i=0;i<col;i++){
                    int tileNum = Integer.parseInt(a[i]);
                    String tileType = worldTypes.TilesMapping(tileNum);
                    Tile tile;
                    if (tileType.contains("wall")) {
                        tile = new Wall(tileType,i,row);
                        walls.add((Wall) tile);
                    } else {
                        tile = new Tile(tileType, i, row);
                    }
                    tiles.add(tile);
                }
            }

        } catch (IOException e) {

            throw new IOException();
        }
            int mode = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(obstructedPath))) {
            String str;
            String[] a;
            while ((str = bufferedReader.readLine()) != null) {
                if (str.startsWith("Block")) {
                    mode = 0;
                    continue;
                } else if (str.startsWith("Tile")) {
                    mode = 1;
                    continue;
                }
                a = str.split(" ");
                if (mode == 0) {
                    int col1 = Integer.parseInt(a[0]);
                    int col2 = Integer.parseInt(a[2]);
                    int row2 = Integer.parseInt(a[3]);
                    for (; col1 <= col2; col1++) {
                        for (int row1 = Integer.parseInt(a[1]); row1 <= row2; row1++) {
                            Tile t = getTile(col1, row1);
                            t.setObstructed(true);
                        }
                    }

                } else {
                    int col = Integer.parseInt(a[0]);
                    int row = Integer.parseInt(a[1]);
                    Tile t = getTile(col, row);
                    t.setObstructed(true);
                }
            }

        } catch (IOException e) {

            throw new IOException();
        }
    }
    protected abstract void generateWorld();

    protected void initiateParty(Knight knight1, Wizard wizard1, Mech mech) {
        Party gameParty = new Party(knight1, wizard1, mech);
        GameManager.setUpParty(gameParty);
    }


    /**
     * Creates the teleporter at the passed in position,
     * add it to entities and teleporters
     * @param col column
     * @param row row
     */
    public void addTeleporter(float col, float row,String texture) {
        Teleporter teleporter = new Teleporter(new Tile("blank", col, row),texture);
        addEntity(teleporter);
        addTeleporter(teleporter);
    }

    /**
     * Creates the teleporter at the passed tile
     * @param tile The tile at the position teleporter will be create.
     * @param texture The texture of the teleporter.
     */
    public void addTeleporter(Tile tile, String texture) {
        Teleporter teleporter = new Teleporter(tile, texture);
        addEntity(teleporter);
        addTeleporter(teleporter);
    }

    /**
     * Creates the quest activator at the specified position, and adds it to entities and teleporters
     * @param col column
     * @param row row
     */
    public void addQuestActivator(float col, float row, String texture, String questName) {
        QuestActivator questActivator = new QuestActivator(new Tile(texture , col, row),texture, questName);
        addEntity(questActivator);
        questActivators.add(questActivator);
    }
   

    protected abstract void addManagers();

    /** Generates neighbours for tiles on a world; assigns the sides needed. */
    public void generateNeighbours() {
    // Multiply coords by 2 to remove floats.
    	Map<Integer, Map<Integer, Tile>> tileMap = new HashMap<>();
		Map<Integer, Tile> columnMap;
		for(Tile tile : tiles) {
			columnMap = tileMap.getOrDefault((int)tile.getCol(), new HashMap<>());
			columnMap.put((int) (tile.getRow()), tile);
			tileMap.put((int) (tile.getCol()), columnMap);
		}

		for(Tile tile : tiles) {
			int col = (int) (tile.getCol());
			int row = (int) (tile.getRow());
			LOGGER.debug("tile coords were: " + col + " " + row);

			// West
			if(tileMap.containsKey(col - 1)) {
			    tile.addNeighbour(Tile.WEST, tileMap.get(col - 1).get(row));
			}

			// Central
			if(tileMap.containsKey(col)) {
				// North
				if (tileMap.get(col).containsKey(row + 1)) {
					tile.addNeighbour(Tile.NORTH, tileMap.get(col).get(row + 1));
				}

				// South
				if (tileMap.get(col).containsKey(row - 1)) {
					tile.addNeighbour(Tile.SOUTH,tileMap.get(col).get(row - 1));
				}
			}

			// East
			if(tileMap.containsKey(col + 1)) {
				// East
                tile.addNeighbour(Tile.EAST,tileMap.get(col + 1).get(row));

			}
		}
    }

    private void generateTileIndices() {
    	for(Tile tile : tiles) {
    		tile.calculateIndex();
    	}
    }

    /**
     * Returns a list of entities in this world.
     * @return All Entities in the world
     */
    public List<AbstractEntity> getEntities() {
        return new CopyOnWriteArrayList<>(this.entities);
    }

    /**
     * Returns a list of players in this world.
     * @return All Players in the world
     */
    public Collection<PlayerPeon> getPlayers() {
        return GameManager.getParty().getPlayersList();
    }


    /**
     * Returns the 1st player peon present in this world
     *
     * @return the player peon object, null if no player exists
     */
    public AbstractEntity getPlayerPeon() {
        List<AbstractEntity> allEntities = getEntities();

        //Search through every entity to find a player
        for (AbstractEntity entity : allEntities) {
            if (entity instanceof PlayerPeon) {
                return entity;
            }
        }

        return null; //No player was found
    }

    /**
     * Searches every enemy entity in the world to see if any are in range of
     * the player
     *
     * @return the EnemyPeon object if they are in range, null if no enemies
     * are in range of the player
     */
    public AbstractEntity isEnemyInRange() {

        AbstractEntity player = getPlayerPeon();

        //Ensure a player exists
        if (player == null) {
            return null;
        }

        List<AbstractEntity> allEntities = getEntities();

        //Search through every entity present in the world
        for (AbstractEntity entity : allEntities) {

            //Only need to check enemy entities
            if (entity instanceof EnemyPeon && ((EnemyPeon)entity).getCanDetect()) {

                //Get the distance between enemy and player
                int verticalDistance = (int) Math.abs(entity.getPosition().getRow()
                        -  player.getPosition().getRow());
                int horizontalDistance = (int) Math.abs(entity.getPosition().getCol()
                        -  player.getPosition().getCol());

                //If enemy is close enough, return the enemy
                if (verticalDistance <= DEFAULT_ENEMY_RANGE &&
                        horizontalDistance <= DEFAULT_ENEMY_RANGE) {
                    return entity;
                }
            }
        }

        return null; //No enemies were found in range
    }

    private final static float epsilon = 0.001f;
    public BossEnemy getBoss(float row, float column)
    {
        List<AbstractEntity> allEntities = getEntities();
        for (AbstractEntity entity : allEntities)
        {
            boolean sameRow = Math.abs(entity.getRow() - row) < epsilon;
            boolean sameColumn = Math.abs(entity.getCol() - column) < epsilon;
            boolean isBoss = entity instanceof BossEnemy;

            if (sameRow && sameColumn && isBoss)
            {
                return (BossEnemy)entity;
            }
        }

        return null;
    }

    
    /**
     * Checks to see if there is a teleporter at the location of the character
     * returns the enum value of the world to be teleported if there is,
     * returns null if there is not
     * @return  the enum value of the world to be teleported or null
     */
    public WorldTypes isTeleporterInRange() {
        AbstractEntity player = getPlayerPeon();
        //Ensure a player exists
        if (player == null) {
            return null;
        }
        for (Teleporter teleporter : teleporters) {

            double colDistance = teleporter.getCol() - player.getCol();
            double rowDistance = teleporter.getRow() - player.getRow();
            if (    colDistance < 1.0&&
                    colDistance > -1.0&&
                    rowDistance <1.0&&
                    rowDistance > -1.0
                )  {
                try{
                    return WorldTypes.valueOf(teleporter.getTexture());
                }catch (IllegalArgumentException e){
                    //
                }catch (NullPointerException e){
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * Checks whether or not the player is near the tile activating the quest
     * @return  the enum value of the world to be teleported or null
     */
    public String isQuestActivatorInRange() {
        AbstractEntity player = getPlayerPeon();
        //Ensure a player exists
        if (player == null) {
            return null;
        }
        for (QuestActivator activator : questActivators) {
            double xDist = Math.abs(activator.getRow() - player.getRow());
            double yDist = Math.abs(activator.getCol() - player.getCol());
            if(xDist < 1 && yDist < 1){
                return activator.getQuestName();
            }
        }
        return null;
    }

    /**
     *  Returns a list of entities in this world, ordered by their render level .
     *  @return all entities in the world
     */
    public List<AbstractEntity> getSortedEntities(){
		List<AbstractEntity> e = new CopyOnWriteArrayList<>(this.entities);
    	Collections.sort(e);
		return e;
    }


    /**
     *  Returns a list of entities in this world, ordered by their render level.
     *  @return all entities in the world
     */
    public List<AgentEntity> getSortedAgentEntities(){
        List<AgentEntity> e = this.entities
            .stream()
            .filter(p -> p instanceof AgentEntity)
            .map(p -> (AgentEntity) p)
            .collect(Collectors.toList());

    	Collections.sort(e);
		return e;
    }

    /**
     * Adds an entity to the world.
     * @param entity the entity to add
     */
    public void addEntity(AbstractEntity entity) {
        entities.add(entity);
    }

    /**
     * Adds an entity to the world.
     * @param teleporter the entity to add
     */
    public void addTeleporter(Teleporter teleporter) {
        teleporters.add(teleporter);
    }


    public List<Wall> getWalls() {
        return walls;
    }

    /**
     * Removes an entity from the world.
     * @param entity the entity to remove
     */
    public void removeEntity(AbstractEntity entity) {
        entities.remove(entity);
    }

	public void setEntities(List<AbstractEntity> entities) {
		this.entities = entities;
	}

    public List<Tile> getTileMap() {
        return tiles;
    }

    public void setBackgroundImage(String name)
    {
        ScreenManager manager = getManagerFromInstance(ScreenManager.class);
        if (manager == null)
        {
            LOGGER.info("ScreenManager not found when changing background");
            return;
        }

        GameScreen screen = manager.getCurrentScreen();
        if (screen != null)
        {
            LOGGER.info("Changing background to " + name);
            screen.setBackgroundImage(getManagerFromInstance(TextureManager.class).getTexture(name));
        }
    }

    public Tile getTile(float col, float row) {
    	return getTile(new SquareVector(col,row));
    }

    public Tile getTile(SquareVector position) {
        for (Tile t : tiles) {
        	if (t.getCoordinates().equals(position)) {
        		return t;
			}
		}
		return null;
    }

    public Tile getTile(int index) {
        for (Tile t : tiles) {
        	if (t.getTileID() == index) {
        		return t;
			}
		}
		return null;
    }

    public void setTileMap(CopyOnWriteArrayList<Tile> tileMap) {
        this.tiles = tileMap;
    }

    public void updateTile(Tile tile) {
        for (Tile t : this.tiles) {
            if (t.getTileID() == tile.getTileID()) {
                if (!t.equals(tile)) {
                    this.tiles.remove(t);
                    this.tiles.add(tile);
                }
                return;
            }
        }
        this.tiles.add(tile);
    }

    public void updateEntity(AbstractEntity entity) {
        for (AbstractEntity e : this.entities) {
            if (e.getEntityID() == entity.getEntityID()) {
                this.entities.remove(e);
                this.entities.add(entity);
                return;
            }
        }
        this.entities.add(entity);

        // Since MultiEntities need to be attached to the tiles they live on, setup that connection.
        if (entity instanceof StaticEntity) {
            ((StaticEntity) entity).setup();
        }
    }

    public void onTick(long i) {
        for (AbstractEntity e : entitiesToDelete) {
            entities.remove(e);
        }

        for (Tile t : tilesToDelete) {
            tiles.remove(t);
        }
    }

    public void deleteTile(int tileid) {
        Tile tile = GameManager.get().getWorld().getTile(tileid);
        if (tile != null) {
            tile.dispose();
        }
    }

    public void deleteEntity(int entityID) {
        for (AbstractEntity e : this.getEntities()) {
            if (e.getEntityID() == entityID) {
                e.dispose();
            }
        }
    }

    public void queueEntitiesForDelete(List<AbstractEntity> entities) {
        entitiesToDelete.addAll(entities);
    }

    public void queueTilesForDelete(List<Tile> tiles) {
        tilesToDelete.addAll(tiles);
    }

    
}
