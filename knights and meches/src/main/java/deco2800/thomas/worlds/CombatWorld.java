package deco2800.thomas.worlds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import deco2800.thomas.Tickable;
import deco2800.thomas.entities.*;
import deco2800.thomas.entities.combat.BossEnemy;
import deco2800.thomas.entities.combat.EnemyPeon;
import deco2800.thomas.entities.combat.Lilypad;
import deco2800.thomas.entities.combat.Particle;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.managers.*;
import deco2800.thomas.moves.Move;
import deco2800.thomas.moves.MoveManager;
import deco2800.thomas.moves.MoveType;
import deco2800.thomas.util.*;
import deco2800.thomas.worlds.dungeons.TundraDungeon;
import deco2800.thomas.worlds.dungeons.VolcanoBiome;

import java.util.Map;
import java.util.Random;


/**
 * Class that represents Combat screen
 */
public class CombatWorld extends StaticWorld implements Tickable
{
    private enum CombatButton
    {
        FIGHT,
        PLAYER,
        ITEMS,
        RUN
    }
    /**
     * Field representing the Enemy in combat
     */
    private EnemyPeon enemy;
    private float enemyPreviousCol;
    private float enemyPreviousRow;


    private HealthBar enemyBar;
    private HealthBar playerBar;
    private ManaBar enemyManaBar;
    private ManaBar playerManaBar;

    private Party party;
    private PlayerPeon player;
    private float centreX;
    private float centreY;

    private ButtonView mainButtonView;
    private ButtonView currentButtonView;
    private ButtonView fightButtonView;
    private ButtonView itemButtonView;
    private ButtonView enemyButtonView;
    private ButtonView manaButtonView;
    private ButtonView playerButtonView;
    private ButtonView playerPanel;
    private ButtonView enemyPanel;

    private long enemyStartingTick = 0;
    private boolean enemyCombatPhase = false;
    private long manaStartingTick = 0;
    private boolean manaGonePhase = false;
    private long tick = 0;

    //Buttons for UI
    private static final String NORMAL_BUTTON = "grey_button";
    private static final String MAGIC_BUTTON = "magic_button";
    private static final String PHYSICAL_BUTTON = "red_button";
    private static final String FONT = "default-font";

    private static final String PLAYER_INFO = "Player HP:%d Mana:%d";
    private static final String ENEMY_INFO = "Enemy HP:%d Mana:%d";

    private String oldTexture = null;
    private Pixmap oldPmap = null;
    private int alpha = 0;
    private boolean isBossBattle = false;

    private static final String POTION_TYPE = "small";

    /**
     * Constructor for Combat screen
     * @param enemy enemy in battle
     * @param party current player's party
     */
    public CombatWorld(EnemyPeon enemy, Party party)
    {
        super();
        this.enemy = enemy;
        this.party = party;
        for (PlayerPeon currentPlayer : party.getPlayers().values())
        {
            this.removeEntity(currentPlayer);
        }

        this.player = party.getPlayers().get(PlayerType.KNIGHT);

        centreX = player.getRow();
        centreY = player.getCol();

        enemyPreviousCol = enemy.getCol();
        enemyPreviousRow = enemy.getRow();
        enemy.setRow(centreX - 1 + 0.5f);
        enemy.setCol(centreY + 2);
        player.setRow(centreX - 1 + 0.5f);
        player.setCol(centreY - 2.5f);

        this.addEntity(enemy);
        this.addEntity(player);
        LOGGER.info("CombatWorld created");
        LOGGER.info(String.format(PLAYER_INFO, player.getHealth(), player.getMana()));
        LOGGER.info(String.format(ENEMY_INFO, enemy.getHealth(), enemy.getMana()));

        isBossBattle = enemy instanceof BossEnemy;
    }

    /**
     * sets up the screen
     */
    public void setup()
    {
        generateWorld();
        createMoveScreen();
    }

    /**
     * Re-generates tiles from source world based on player and enemy positions
     */
    @Override
    protected void generateWorld()
    {
        if (enemy == null || player == null) return;

        enemy.setCombatTexture();

        AbstractWorld previousWorld = GameManager.get().getPreviousWorld();

        String lilypadTexture = "lilypad_city";
        String background = "city_background";

        if (previousWorld instanceof TundraDungeon) {
            lilypadTexture = "lilypad_ice";
            background = "ice_background";
        } else if (previousWorld instanceof VolcanoBiome) {
            background = "lava_background";
        }

        Tile playerTile = new Tile("blank", player.getCol(), player.getRow() - 0.5f);
        Tile enemyTile = new Tile("blank",enemy.getCol(),enemy.getRow() - 0.5f);
        tiles.add(playerTile);
        tiles.add(enemyTile);
        entities.add(new Lilypad(playerTile, lilypadTexture));
        entities.add(new Lilypad(enemyTile, lilypadTexture));

        this.setBackgroundImage(background);
    }

    /**
     * Creates text that can be clicked to do actions
     */
    private void createMoveScreen()
    {
        GameManager manager = GameManager.get();
        Stage stage = manager.getStage();
        Skin skin = manager.getSkin();
        if (skin != null)
        {
            stageVisibility(stage, false);
            createMainButtonView(skin, stage, manager);
            createFightButtonView(skin, stage, manager);
            createEnemyButtonView(skin, stage, manager);
            createItemButtonView(skin, stage, manager);
            createManaButtonView(skin, stage, manager);
            createPlayerButtonView(skin, stage, manager);
            createPanels(skin, stage, manager);
            createHealthBars();
            createManaBars();
        }
    }

    /**
     * Creates entity health bars
     */
    private void createHealthBars()
    {
        final float margin = 50;

        playerBar = new HealthBar(player.getHealth(), player.getMaxHealth());

        float height = Gdx.graphics.getHeight();
        float width = Gdx.graphics.getWidth();

        float playerY = -margin + height/2;

        GameManager.get().getStage().addActor(playerBar);
        playerBar.setPosition(margin, playerY);

        enemyBar = new HealthBar(enemy.getHealth(), enemy.getMaxHealth());
        GameManager.get().getStage().addActor(enemyBar);

        float enemyX = -margin + width - enemyBar.getWidth();
        float enemyY = -margin + height/2;

        enemyBar.setPosition(enemyX, enemyY);
    }

    /**
     * Creates entity mana bars
     */
    private void createManaBars()
    {
        final float margin = 50;

        playerManaBar = new ManaBar(player.getMana(), player.getMaxMana());
        GameManager.get().getStage().addActor(playerManaBar);

        Camera camera = GameManager.get().getCamera();
        float playerX = margin;
        float playerY = -margin*2 + camera.viewportHeight/2 + enemyBar.getHeight();

        playerManaBar.setPosition(playerX, playerY);

        enemyManaBar = new ManaBar(enemy.getMana(), enemy.getMaxMana());
        GameManager.get().getStage().addActor(enemyManaBar);

        float enemyX = -margin + camera.viewportWidth - enemyManaBar.getWidth();
        float enemyY = -margin*2 + camera.viewportHeight/2 + enemyBar.getHeight();

        enemyManaBar.setPosition(enemyX, enemyY);

    }

    /**
     * Creates the bordering panels which surround enemy and player status bars
     *
     * @param skin current game's skin
     * @param stage current game's stage
     * @param manager current game's manager
     */
    private void createPanels(Skin skin, Stage stage, GameManager manager) {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);
        Camera camera = GameManager.get().getCamera();

        int height = (int) (-125 + camera.viewportHeight/2);

        playerPanel = new ButtonView(stage, labelTexture, 15, height);

        Texture texture = labelTexture.getTexture("basic_panel");
        TextureData data = texture.getTextureData();
        data.prepare();
        Pixmap pmap = data.consumePixmap();

        texture = labelTexture.getTexture("hpmp_symbols");
        data = texture.getTextureData();
        data.prepare();
        Pixmap pmapSymbol = data.consumePixmap();

        int symbolHeight = -43 + pmap.getHeight()/2;

        Pixmap playerPmap = new Pixmap(pmap.getWidth(), pmap.getHeight(), pmap.getFormat());
        playerPmap.drawPixmap(pmap,0,0);
        Pixmap enemyPmap = new Pixmap(pmap.getWidth(), pmap.getHeight(), pmap.getFormat());
        enemyPmap.drawPixmap(pmap,0,0);

        playerPmap.drawPixmap(pmapSymbol, pmap.getWidth() - 50, symbolHeight);
        enemyPmap.drawPixmap(pmapSymbol, 25, symbolHeight);

        texture = new Texture(playerPmap);
        labelTexture.replaceTexture("player_panel", texture);
        texture = new Texture(enemyPmap);
        labelTexture.replaceTexture("enemy_panel", texture);

        playerPanel.addLabel(this.player.getType().toString(),"player_panel", font,
                200, 125, Align.top, Color.BLACK);
        playerPanel.makeVisible(true);

        enemyPanel = new ButtonView(stage, labelTexture,
                (int)(-215 + camera.viewportWidth), height);

        enemyPanel.addLabel(this.enemy.getType().toString(),"enemy_panel", font,
                200, 125, Align.top, Color.BLACK);
        enemyPanel.makeVisible(true);
    }

    /**
     * Toggles visibility of staged actors
     * @param stage current game's stage
     * @param visibility true if actors are to be visible, else false
     */
    private void stageVisibility(Stage stage, boolean visibility)
    {
        for (Actor actor : stage.getActors())
        {
            actor.setVisible(visibility);
        }
    }

    /**
     * Creates the moves which this player owns as buttons
     *
     * @param skin skin present in manager
     * @param stage stage present in manager
     * @param manager the manager
     */
    private void createFightButtonView(Skin skin, Stage stage, GameManager manager)
    {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.fightButtonView = new ButtonView(stage, labelTexture);

        for (Move move : player.getMoves().values())
        {
            String informationFormat = "%s\nDamage:%d\nCost:%s";
            String textLabel = String.format(informationFormat,
                    move.getName(),
                    move.getAmount(),
                    move.getManaCost());

            fightButtonView.addLabel(textLabel, chooseButtonType(move.getType()), font, new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    combatPhase(move.getName());
                }
            });
        }

        fightButtonView.addLabel("Return", NORMAL_BUTTON, font, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setCurrentButtonView(mainButtonView);
            }
        });

        this.fightButtonView.makeVisible(false);
    }

    /**
     * Create particles which travel upwards to signify a heal
     *
     * @param r RGB value of red
     * @param g RGB value of green
     * @param b RGB value of blue
     */
    private void createPotionParticles(float r, float g, float b) {
        Random rand = new Random();

        float var = 0.2f;

        float playerY = player.getRow() + 0.25f;
        float playerX = player.getCol() + 0.25f;

        entities.add(new Particle( playerY - 0.2f + rand.nextFloat() * var,
                 playerX + rand.nextFloat() * var, 1, r, g, b, this));
        entities.add(new Particle(playerY + 0.2f + rand.nextFloat() * var,
                playerX + rand.nextFloat() * var, 1, r, g, b, this));
        entities.add(new Particle(playerY + rand.nextFloat() * var,
                playerX - 0.2f + rand.nextFloat() * var, 1, r, g, b, this));
        entities.add(new Particle(playerY + rand.nextFloat() * var,
                playerX + 0.2f + rand.nextFloat() * var, 1, r, g, b, this));
    }

    /**
     * Creates an explosion effect around a given entity
     *
     * @param entity entity to create explosion around
     */
    private void createExplosionParticles(AbstractEntity entity) {
        Random rand = new Random();

        for (int i = 0; i < 15; i++) {

            //Pick a random direction for the particle to travel in
            int vertical = 0;
            int horizontal = 0;

            if (rand.nextFloat() <= 0.66f) {
                if (rand.nextFloat() <= 0.5f) {
                    vertical = 1;
                } else {
                    vertical = -1;
                }
            }

            if (rand.nextFloat() <= 0.66f) {
                if (rand.nextFloat() <= 0.5f) {
                    horizontal = 1;
                } else {
                    horizontal = -1;
                }
            }

            //Add the particle in a randomish position in the centre of the entity
            entities.add(new Particle(entity.getRow() + 0.5f + rand.nextFloat()/5,
                    entity.getCol() + 0.25f + rand.nextFloat()/5, 1, 1,
                    0.2f*rand.nextFloat(), 0.2f*rand.nextFloat(), this, vertical, horizontal));
        }
    }

    /**
     *
     * Creates buttons to use health and mana potions
     *
     * @param skin skin from the manager
     * @param stage stage from the manager
     * @param manager the manager
     */
    private void createItemButtonView(Skin skin, Stage stage, GameManager manager)
    {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.itemButtonView = new ButtonView(stage, labelTexture);

        InventoryAbstract inventory = GameManager.getManagerFromInstance(InputManager.class).getInventory();
        itemButtonView.addLabel(String.format("Use HP potion %n Quantity: %s", inventory.healthPotionQuantity(POTION_TYPE)),
                NORMAL_BUTTON, font, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (inventory.useHealthPotion(POTION_TYPE)) {
                    player.setHealth(player.getHealth() + 200);
                    updateBars();
                    itemButtonView.updateLabel(0,
                            String.format("Use HP potion %n Quantity: %s", inventory.healthPotionQuantity(POTION_TYPE)));

                    createPotionParticles(0,1,0);
                }
            }
        });

        itemButtonView.addLabel(String.format("Use MP potion %n Quantity: %s", inventory.manaPotionQuantity(POTION_TYPE)),
                NORMAL_BUTTON, font, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (inventory.useManaPotion(POTION_TYPE)) {
                    player.addMana(30);
                    updateBars();
                    itemButtonView.updateLabel(1,
                            String.format("Use MP potion %n Quantity: %s", inventory.manaPotionQuantity(POTION_TYPE)));

                    createPotionParticles(0,0.5f,1);
                }
            }
        });

        itemButtonView.addLabel("Return", NORMAL_BUTTON, font, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setCurrentButtonView(mainButtonView);
            }
        });

        this.itemButtonView.makeVisible(false);
    }

    /**
     * Decides what button skin is appropriate for a move
     * @param type the type of the move
     * @return a string which represents a button texture
     */
    private String chooseButtonType(MoveType type) {
        if (type == MoveType.MAGICAL) {
            return MAGIC_BUTTON;
        } else if (type == MoveType.PHYSICAL) {
            return PHYSICAL_BUTTON;
        }

        return "black_bg";
    }

    /**
     * Makes the given entity fade away
     *
     * @param entity entity to fade away
     * @param alpha represents how faded the enemy is
     */
    private void deathAnimation(AbstractEntity entity, int alpha) {
        Pixmap pmap;
        TextureManager manager = GameManager.getManagerFromInstance(TextureManager.class);
        if (oldPmap == null) {
             String texture = entity.getTexture();
             oldTexture = texture;
             TextureData data = manager.getTexture(texture).getTextureData();
             data.prepare();
             pmap = data.consumePixmap();
             oldPmap = pmap;
        } else {
            pmap = oldPmap;
        }

        Pixmap newPmap = new Pixmap(pmap.getWidth(), pmap.getHeight(),Pixmap.Format.RGBA8888);
        newPmap.setBlending(Pixmap.Blending.None);

        for (int i = 0; i < pmap.getWidth(); i++) {
            for (int j = 0; j < pmap.getHeight(); j++) {
                int colour = pmap.getPixel(i,j);
                colour = colour & (0xFFFFFFFF - alpha);
                newPmap.drawPixel(i,j,colour);
            }
        }

        Texture newTexture = new Texture(newPmap);
        manager.replaceTexture("death",newTexture);
        entity.setTexture("death");
    }

    /**
     * Carries out a player's move in the combat world
     * @param name name of the move
     */
    private void combatPhase(String name) {

        MoveManager moveManager = GameManager.get().getMoveManager();
        if (!moveManager.calcDamage(moveManager.getMove(name), player, enemy)) {
            LOGGER.info("Not enough mana!");
            manaGonePhase = true;
            return;
        }

        playMoveSound(moveManager.getMove(name).getType());

        LOGGER.info("PLAYER PHASE: Player uses " + name);
        LOGGER.info(String.format(PLAYER_INFO, player.getHealth(), player.getMana()));
        LOGGER.info(String.format(ENEMY_INFO, enemy.getHealth(), enemy.getMana()));
        updateBars();

        createExplosionParticles(enemy);

        if (enemy.aliveOrNot()) {
            enemyCombatPhase = true;
        }
    }

    /**
     * Play a sound effect for a specific move
     * @param type - The type of move played
     */
    private void playMoveSound(MoveType type) {
        SoundManager manager = GameManager.getManagerFromInstance(SoundManager.class);
        if (type == MoveType.MAGICAL) {
            manager.playSound("magic2.mp3");
        } else {
            manager.playSound("physical.mp3");
        }
    }

    /**
     * Carries out an enemy's move in the combat world
     */
    private void enemyPhase() {
        String move = enemy.chooseMove();

        MoveManager moveManager = GameManager.get().getMoveManager();
        if (!moveManager.calcDamage(moveManager.getMove(move),enemy,player)) {
            LOGGER.info("If this appears, this is a BUG: "  +
                    "Either enemy has no mana or enemy move doesn't exist");
        }

        playMoveSound(moveManager.getMove(move).getType());

        LOGGER.info("ENEMY PHASE: Enemy uses " + move);
        LOGGER.info(String.format(PLAYER_INFO, player.getHealth(), player.getMana()));
        LOGGER.info(String.format(ENEMY_INFO, enemy.getHealth(), enemy.getMana()));
        enemyButtonView.updateLabel(0, String.format("Enemy uses: %n%s",move));
        updateBars();

        createExplosionParticles(player);

    }

    /**
     * Stages Fight button
     * @param skin game's current skin
     * @param stage game's stage
     * @param manager GameManager instance to access textures
     */
    private void createMainButtonView(Skin skin, Stage stage, GameManager manager)
    {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.mainButtonView = new ButtonView(stage, labelTexture);
        for (CombatButton button : CombatButton.values())
        {
            if (button == CombatButton.RUN) {
                String name = isBossBattle ? "CAN'T RUN" : button.name();
                mainButtonView.addLabel(name, NORMAL_BUTTON, font, new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if (!isBossBattle)
                            leaveCombatWorld(stage, CombatLeaveType.RUNNING);
                    }
                });
            }
            else if (button == CombatButton.FIGHT)
            {
                mainButtonView.addLabel(button.name(), NORMAL_BUTTON, font, new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setCurrentButtonView(fightButtonView);
                    }
                });
            }
            else if (button == CombatButton.PLAYER)
            {
                mainButtonView.addLabel(button.name(), NORMAL_BUTTON, font, new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setCurrentButtonView(playerButtonView);
                    }
                });
            }
            else
            {
                mainButtonView.addLabel(button.name(), NORMAL_BUTTON, font, new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setCurrentButtonView(itemButtonView);
                    }
                });
            }
        }

        currentButtonView = mainButtonView;
    }

    /**
     * Create buttons which cannot be clicked but show an enemy is attacking
     *
     * @param skin the skin from the manager
     * @param stage the stage from the manager
     * @param manager the manager
     */
    private void createEnemyButtonView(Skin skin, Stage stage, GameManager manager) {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.enemyButtonView = new ButtonView(stage, labelTexture);
        enemyButtonView.addLabel("Enemy Combat Phase", NORMAL_BUTTON, font);

        this.enemyButtonView.makeVisible(false);
    }

    /**
     * Creates text which cant be clicked to show player is out of mana
     *
     * @param skin the skin from the manager
     * @param stage the stage from the manager
     * @param manager the manager
     */
    private void createManaButtonView(Skin skin, Stage stage, GameManager manager) {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.manaButtonView = new ButtonView(stage, labelTexture);
        manaButtonView.addLabel("Not enough mana!!", NORMAL_BUTTON, font);

        this.manaButtonView.makeVisible(false);
    }

    /**
     * Creates a player selection button view
     * @param skin the skin from the manager
     * @param stage the stage from the manager
     * @param manager the manager
     */
    private void createPlayerButtonView(Skin skin, Stage stage, GameManager manager)
    {
        TextureManager labelTexture = manager.getManager(TextureManager.class);
        BitmapFont font = skin.getFont(FONT);

        this.playerButtonView = new ButtonView(stage, labelTexture);
        for (Map.Entry<PlayerType, PlayerPeon> currentPlayer : party.getPlayers().entrySet())
        {
            playerButtonView.addLabel(currentPlayer.getKey().toString(), NORMAL_BUTTON, font, new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    changePlayer(currentPlayer.getValue());
                }
            });
        }

        playerButtonView.addLabel("BACK", NORMAL_BUTTON, font, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setCurrentButtonView(mainButtonView);
            }
        });

        this.playerButtonView.makeVisible(false);
    }

    private void changePlayer(PlayerPeon newPlayer)
    {
        for (PlayerPeon currentPlayer : party.getPlayers().values())
        {
            this.removeEntity(currentPlayer);
        }

        this.player = newPlayer;
        this.playerManaBar.updateValue(player.getMana(), player.getMaxMana());
        this.playerBar.updateValue(player.getHealth(), player.getMaxHealth());
        this.addEntity(player);
        player.setRow(centreX - 1 + 0.5f);
        player.setCol(centreY - 2.5f);

        fightButtonView.removeLabels();
        createFightButtonView(GameManager.get().getSkin(), GameManager.get().getStage(), GameManager.get());

        playerPanel.updateLabel(0, this.player.getType().toString());
    }

    /**
     * Adds new screen manager for world
     */
    @Override
    protected void addManagers()
    {
        GameManager.get().addManager(new ScreenManager());
    }

    /**
     * Sets which buttons will be visible on the screen
     * @param buttonView the buttons to be made visible
     */
    private void setCurrentButtonView(ButtonView buttonView)
    {
        currentButtonView.makeVisible(false);
        currentButtonView = buttonView;
        buttonView.makeVisible(true);
    }

    /** Method for restoring actors to how they were before the combat world was
     * created.*/
    private void cleanup(Stage stage) {
        for (PlayerPeon playerPeon : party.getPlayers().values()) {
            playerPeon.setRow(centreX);
            playerPeon.setCol(centreY);
            playerPeon.setTask(null);
        }

        mainButtonView.removeLabels();
        fightButtonView.removeLabels();
        enemyButtonView.removeLabels();
        itemButtonView.removeLabels();
        playerButtonView.removeLabels();
        manaButtonView.removeLabels();
        playerPanel.removeLabels();
        enemyPanel.removeLabels();
        playerBar.remove();
        playerManaBar.remove();
        enemyBar.remove();
        enemyManaBar.remove();
        stageVisibility(stage, true);
        this.setBackgroundImage("black_bg");
    }

    /** Method for cleaning up combat world and restoring old world how it was*/
    private void leaveCombatWorld(Stage stage, CombatLeaveType leaveType) {
        cleanup(stage);
        LOGGER.info("Exiting CombatWorld");
        GameManager.get().restoreWorld(enemy, leaveType, enemyPreviousCol, enemyPreviousRow);
    }

    /** Method for updating the status of player and enemy health and mana bars*/
    private void updateBars() {
        playerBar.updateValue(player.getHealth());
        enemyBar.updateValue(enemy.getHealth());
        playerManaBar.updateValue(player.getMana());
        enemyManaBar.updateValue(enemy.getMana());
    }

    private void handleButtonAnimations() {
        if (enemyCombatPhase && enemyStartingTick == 0) {
            enemyStartingTick = tick;
            setCurrentButtonView(enemyButtonView);
        }

        if (enemyCombatPhase && enemyStartingTick != 0 && tick >= enemyStartingTick + 30) {
            enemyPhase();
            enemyCombatPhase = false;
        }

        if (!enemyCombatPhase && enemyStartingTick != 0 && tick >= enemyStartingTick + 60) {
            setCurrentButtonView(fightButtonView);
            enemyButtonView.updateLabel(0, "Enemy Combat Phase");
            enemyStartingTick = 0;
        }

        if (manaGonePhase) {
            manaStartingTick = tick;
            setCurrentButtonView(manaButtonView);
            manaGonePhase = false;
        }

        if (manaStartingTick != 0 && tick >= manaStartingTick + 60) {
            setCurrentButtonView(fightButtonView);
            manaStartingTick = 0;
        }
    }

    private void handleDeaths() {
        if (!enemy.aliveOrNot()) {

            currentButtonView.makeVisible(false);

            if (tick % 5 == 0) {
                alpha += 26;
                deathAnimation(enemy, alpha);
            }

            if (alpha >= 255) {
                enemy.setTexture(oldTexture);
                leaveCombatWorld(GameManager.get().getStage(), CombatLeaveType.VICTORY);
            }
        }

        if (!player.aliveOrNot()) {

            currentButtonView.makeVisible(false);

            if (tick % 5 == 0) {
                alpha += 26;
                deathAnimation(player, alpha);
            }

            if (alpha >= 255) {
                player.setTexture(oldTexture);
                player.setHealth(20);
                leaveCombatWorld(GameManager.get().getStage(), CombatLeaveType.DEATH);
                GameManager.get().changeWorld(WorldTypes.GHOST_WORLD);
                GameManager.get().ghostCamera();
            }
        }
    }

    @Override
    public void onTick(long i) {
        super.onTick(i);
        for (AbstractEntity e : this.getEntities()) {
            if (e instanceof Particle) {
                e.onTick(0);
            }
        }

        tick++;

        handleButtonAnimations();
        handleDeaths();
    }

    /**
     * Represents the way in which a player leaves the combat world
     */
    public enum CombatLeaveType
    {
        /*Player has won battle*/
        VICTORY,
        /*Player has died in battle*/
        DEATH,
        /*Player has fled from battle*/
        RUNNING
    }

}
