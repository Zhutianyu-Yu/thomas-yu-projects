package deco2800.thomas.worlds;
import java.io.*;

public class TsxFileLoader {

    public static void main(String[] args) throws IOException{

        try (BufferedWriter textures = new BufferedWriter(new FileWriter("Textures"))) {

            try (BufferedWriter mapping = new BufferedWriter(new FileWriter("mapping"))) {

                try (BufferedWriter obstructed = new BufferedWriter(new FileWriter("Obstructed"))) {

                    try (BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/biomes.tundra/Tundra-tileset.tsx"))) {
                        String str;
                        while ((str=bufferedReader.readLine())!=null){
                            if (str.startsWith(" <tile id=\"")){
                                int mapNum = Integer.parseInt(str.split("\"")[1]) +1;
                                mapping.append(String.format("case %d:",mapNum));
                                mapping.newLine();
                                str=bufferedReader.readLine();
                                String[] a = str.split("\"");
                                mapping.append(String.format("    return \"%s\";",a[5]));
                                mapping.newLine();
                                textures.append(String.format("textureMap.put(\"%s\",new Texture(\"resources/biomes.tundra/%s\"));",a[5],a[5]));
                                textures.newLine();
                            }
                        }
                        int maxRowNum = 61;
                        obstructed.append("Block").append(System.lineSeparator());
        /*
        col 1
        row 1
        (left top)
        col 2
        row 2
        (right bottom)
        col and row are the position in "Tiled"
        make sure col1<col2, row1>row2

         */
                        obstructed.
                                append(String.valueOf(1)).append(" ").
                                append(String.valueOf(maxRowNum - 2)).append(" ").
                                append(String.valueOf(2)).append(" ").
                                append(String.valueOf(maxRowNum - 1)).
                                append(System.lineSeparator());
                        obstructed.append("Tile").append(System.lineSeparator());
                    }
                }
            }
        }
    }
}
