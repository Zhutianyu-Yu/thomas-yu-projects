package deco2800.thomas.worlds;

/** 
 * A class for all of our worlds which are static and have no movement of player to inherit from. 
 */
public abstract class StaticWorld extends AbstractWorld {

    public void setup() {
    }
}
