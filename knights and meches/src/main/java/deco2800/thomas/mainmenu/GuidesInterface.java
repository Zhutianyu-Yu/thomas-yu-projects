package deco2800.thomas.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import deco2800.thomas.managers.GameManager;

public class GuidesInterface extends Group {

    private boolean guiidesInterfaceDisplayed = false;


    private Image background;
    private Image transparent;

    private Stage stage;


    public GuidesInterface(Stage stage) {
        buildTemplate();
        buildButtons();
        this.stage = stage;
    }


    private void buildTemplate() {

        this.background = new Image(new Texture(Gdx.files.internal("Game Information/Gameplay info.png")));
        setImageSize(this.background);
        background.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(background);

        this.transparent = new Image(new Texture(Gdx.files.internal("minimap/transparentButton.png")));
        setImageSize(this.transparent);
    }

    private void buildButtons() {
        ImageButton infoBtn = new ImageButton(transparent.getDrawable());
        setButtonSize(infoBtn);
        infoBtn.setPosition(Gdx.graphics.getWidth() / 2f - this.background.getWidth() / 2f + 70,
                Gdx.graphics.getHeight() / 2f + this.background.getHeight() / 4f - 20);
        infoBtn.setHeight(infoBtn.getHeight() * 0.4f);
        infoBtn.setWidth(infoBtn.getWidth() * 1.7f);
        this.addActor(infoBtn);
        infoBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setDrawable(new Image(new Texture(Gdx.files.internal
                        ("Game Information/Gameplay info.png"))).getDrawable());
            }
        });

        ImageButton keyBtn = new ImageButton(transparent.getDrawable());
        keyBtn.setPosition(Gdx.graphics.getWidth() / 2f - this.background.getWidth() / 2f + 410,
                Gdx.graphics.getHeight() / 2f + this.background.getHeight() / 4f - 20);
        keyBtn.setHeight(keyBtn.getHeight() * 0.7f);
        keyBtn.setWidth(keyBtn.getWidth() * 2.6f);
        this.addActor(keyBtn);
        keyBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setDrawable(new Image(new Texture(Gdx.files.internal
                        ("Game Information/Keyboard Controls Page.png"))).getDrawable());
            }
        });

        ImageButton brdBtn = new ImageButton(transparent.getDrawable());
        brdBtn.setPosition(Gdx.graphics.getWidth() / 2f + this.background.getWidth() / 8f - 10,
                Gdx.graphics.getHeight() / 2f + this.background.getHeight() / 4f - 20);
        brdBtn.setHeight(brdBtn.getHeight() * 0.4f);
        brdBtn.setWidth(brdBtn.getWidth() * 3f);
        this.addActor(brdBtn);
        brdBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setDrawable(new Image(new Texture(Gdx.files.internal
                        ("Game Information/Mouse Controls Page.png"))).getDrawable());
            }
        });

        TextButton exitBtn = new TextButton("EXIT", GameManager.get().getSkin(), "main_menu");
        exitBtn.setPosition(Gdx.graphics.getWidth() / 2f + this.background.getWidth() / 3.3f,
                Gdx.graphics.getHeight() / 2f + this.background.getHeight() / 2.8f);
        this.addActor(exitBtn);
        exitBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                guiidesInterfaceDisplayed = false;
                for (Actor actor : stage.getActors()) {
                    actor.setVisible(!(actor instanceof GuidesInterface));
                }
            }
        });
    }

    private void setImageSize(Image image) {
        image.setHeight(image.getHeight() * 0.65f);
        image.setWidth(image.getWidth() * 0.65f);
    }

    private void setButtonSize(ImageButton imageButton) {
        imageButton.setHeight((float) (imageButton.getHeight() * 1.5));
        imageButton.setWidth((float) (imageButton.getWidth() * 1.5));
    }

    public void setGuiidesInterfaceDisplayed(boolean visible) {
        this.guiidesInterfaceDisplayed = visible;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {

        if (this.guiidesInterfaceDisplayed) {

            super.draw(batch, parentAlpha);
        }
    }

}


