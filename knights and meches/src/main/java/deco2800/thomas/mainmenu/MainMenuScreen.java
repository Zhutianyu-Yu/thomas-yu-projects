package deco2800.thomas.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import deco2800.thomas.GameScreen;
import deco2800.thomas.ThomasGame;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.TextureManager;
import deco2800.thomas.worlds.MenuMenu;

public class MainMenuScreen implements Screen {
    final ThomasGame game;
    private Stage stage;
    private Skin skin;
    private Image transparent;

    /**
     * Constructor of the MainMenuScreen.
     * @param game the game to run
     */
    public MainMenuScreen(final ThomasGame game) {
        this.game = game;

        stage = new Stage(new ExtendViewport(1280, 720), game.getBatch());
        skin = GameManager.get().getSkin();

        Image background = new Image(GameManager.get().getManager(TextureManager.class).getTexture("tmpBackground"));
        background.setFillParent(true);
        stage.addActor(background);

        Texture startButtonTexture = new Texture(Gdx.files.internal("Main Menu Designs/startButtonTransparent.png"));
        TextureRegion startButtonTextureRegion = new TextureRegion(startButtonTexture);
        TextureRegionDrawable startButtonTextureRegionDrawable = new TextureRegionDrawable(startButtonTextureRegion);
        ImageButton startButton = new ImageButton(startButtonTextureRegionDrawable);
        startButton.setPosition(317, 355);
        stage.addActor(startButton);

        Button newGameBtn = new TextButton("SINGLE PLAYER", skin, "main_menu");
        newGameBtn.setPosition(10, 150);
        stage.addActor(newGameBtn);

        this.transparent = new Image(new Texture(Gdx.files.internal("minimap/transparentButton.png")));
        setImageSize(this.transparent);

        ImageButton infoBtn = new ImageButton(transparent.getDrawable());
        setButtonSize(infoBtn);
        infoBtn.setHeight(infoBtn.getHeight() * 0.5f);
        infoBtn.setWidth(infoBtn.getWidth() * 0.5f);
        infoBtn.setPosition((Gdx.graphics.getWidth() - infoBtn.getWidth()) - 80, (Gdx.graphics.getHeight() - infoBtn.getHeight()) - 80);
        infoBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GuidesInterface guidesInterface = new GuidesInterface(stage);
                stage.addActor(guidesInterface);
                guidesInterface.setGuiidesInterfaceDisplayed(true);
            }
        });
        stage.addActor(infoBtn);

        startButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                newGame();
            }
        });

        newGameBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                newGame();
            }
        });
    }

    public void newGame() {
        game.setScreen(new GameScreen(new ThomasGame(), GameScreen.gameType.NEW_GAME));
    }

    private void setImageSize(Image image) {
        image.setHeight((float) (image.getHeight() * 1.5));
        image.setWidth((float) (image.getWidth() * 1.5));
    }

    private void setButtonSize(ImageButton imageButton) {
        imageButton.setHeight((float) (imageButton.getHeight() * 1.5));
        imageButton.setWidth((float) (imageButton.getWidth() * 1.5));
    }
    
   /**
     * Begins things that need to begin when shown.
     */
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Pauses the screen.
     */
    public void pause() {
        //do nothing
    }

    /**
     * Resumes the screen.
     */
    public void resume() {
        //do nothing
    }

    /**
     * Hides the screen.
     */
    public void hide() {
        //do nothing
    }

    /**
     * Resizes the main menu stage to a new width and height.
     * @param width the new width for the menu stage
     * @param height the new width for the menu stage
     */
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    /**
     * Renders the menu.
     * @param delta
     */
    public void render (float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    /**
     * Disposes of the stage that the menu is on.
     */
    public void dispose() {
        stage.dispose();
    }
}