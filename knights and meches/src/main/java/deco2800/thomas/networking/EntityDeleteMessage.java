package deco2800.thomas.networking;

public class EntityDeleteMessage {
    private int entityID;

    public void setEntityID(int entityID) {
        this.entityID = entityID;
    }

    public int getEntityID() {
        return this.entityID;
    }
}
