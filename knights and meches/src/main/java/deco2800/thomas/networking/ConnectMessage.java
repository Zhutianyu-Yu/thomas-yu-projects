package deco2800.thomas.networking;

import deco2800.thomas.entities.AbstractEntity;

public class ConnectMessage {
    private String username;

    public void setUsername(String username) {
        this.username = username;
    }
}
