package deco2800.thomas.networking;

public class TileDeleteMessage {
    private int tileID;

    public void setTileID(int tileID) {
        this.tileID = tileID;
    }

    public int getTileID() {
        return this.tileID;
    }
}