package deco2800.thomas.pause;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import deco2800.thomas.entities.PlayerPeon;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.observers.KeyDownObserver;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.tasks.StopMovementTask;
import deco2800.thomas.worlds.MenuMenu;
import deco2800.thomas.worlds.WorldTypes;

import java.util.Collection;

/**
 * Creates group that displays the pause UI
 */
public class PauseInterface extends Group implements KeyDownObserver {

    private boolean pauseInterfaceDisplayed = false;


    private Image background;
    private Image soundOn;
    private Image soundOff;
    private Image sound;
    private Image transparent;

    private boolean mute = false;

    private ImageButton soundButton;


    public PauseInterface() {
        buildTemplate();
        buildButtons();

        GameManager.getManagerFromInstance(InputManager.class).addKeyDownListener(this);
    }


    private void buildTemplate() {

        GameManager.getManagerFromInstance(SoundManager.class).playSound("open_menu.wav");

        this.background = new Image(new Texture(Gdx.files.internal("minimap/gamePause.png")));
        setImageSize(this.background);

        background.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2);
        this.addActor(background);

        this.soundOn = new Image(new Texture(Gdx.files.internal("minimap/soundOn.png")));
        setImageSize(this.soundOn);
        this.sound = this.soundOn;
        this.sound.setPosition((Gdx.graphics.getWidth() - background.getWidth() + 2),
                (float) (Gdx.graphics.getHeight() - background.getHeight() + 217.5));

        this.soundOff = new Image(new Texture(Gdx.files.internal("minimap/soundOff.png")));
        setImageSize(this.soundOff);

        this.transparent = new Image(new Texture(Gdx.files.internal("minimap/transparentButton.png")));
        setImageSize(this.transparent);
    }


    private void buildButtons() {

        this.soundButton = new ImageButton(sound.getDrawable());
        setButtonSize(this.soundButton);
        this.soundButton.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2 + 90,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2 + 170);
        this.addActor(this.soundButton);

        this.soundButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mute = !mute;
                GameManager.getManagerFromInstance(SoundManager.class).mute();
                GameManager.getManagerFromInstance(SoundManager.class).playSound("button_click.wav");
            }
        });

        ImageButton restartButton = new ImageButton(transparent.getDrawable());
        setButtonSize(restartButton);
        restartButton.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2 + 90,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2 + 10);
        this.addActor(restartButton);
        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(SoundManager.class).playSound("button_click.wav");
                pauseInterfaceDisplayed = false;
            }
        });

        ImageButton guidesButton = new ImageButton(transparent.getDrawable());
        setButtonSize(guidesButton);
        guidesButton.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2 + 400,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2 + 170);
        this.addActor(guidesButton);

        guidesButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(SoundManager.class).playSound("button_click.wav");
                GameManager.get().changeWorld(WorldTypes.TUTORIAL);
                Collection<PlayerPeon> players = GameManager.getParty().getPlayers().values();
                for (PlayerPeon player : players) {
                    player.setCol(player.getInitialPosition().getCol());
                    player.setRow(player.getInitialPosition().getRow());
                    player.setTask(new StopMovementTask(player));
                }
                pauseInterfaceDisplayed = false;
            }
        });

        ImageButton backButton = new ImageButton(transparent.getDrawable());
        setButtonSize(backButton);
        backButton.setPosition((Gdx.graphics.getWidth() - background.getWidth()) / 2 + 400,
                (Gdx.graphics.getHeight() - background.getHeight()) / 2 + 10);
        this.addActor(backButton);

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pauseInterfaceDisplayed = false;
                GameManager.getManagerFromInstance(SoundManager.class).playSound("button_click.wav");
            }
        });
    }


    private void setImageSize(Image image) {
        image.setHeight((float) (image.getHeight() * 1.5));
        image.setWidth((float) (image.getWidth() * 1.5));
    }


    private void setButtonSize(ImageButton imageButton) {
        imageButton.setHeight((float) (imageButton.getHeight() * 1.5));
        imageButton.setWidth((float) (imageButton.getWidth() * 1.5));
    }


    public boolean getPauseInterfaceDisplayed() {
        return this.pauseInterfaceDisplayed;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {

        if (this.pauseInterfaceDisplayed) {

            super.draw(batch, parentAlpha);
            if (mute) {
                this.soundButton.clear();
                this.sound = soundOff;
                buildButtons();
            } else {
                this.soundButton.clear();
                this.sound = soundOn;
                buildButtons();
            }
        }
    }


    @Override
    public void notifyKeyDown(int keycode) {
        if (keycode == Input.Keys.P) {
            if (this.pauseInterfaceDisplayed) {
                this.pauseInterfaceDisplayed = false;
                for (Actor actor : GameManager.get().getStage().getActors()) {
                    actor.setVisible(true);
                }
            } else {
                this.pauseInterfaceDisplayed = true;
                for (Actor actor : GameManager.get().getStage().getActors()) {
                    if (relevantActor(actor)) {
                        continue;
                    }
                    actor.setVisible(false);

                }
            }


        }
    }


    private boolean relevantActor(Actor actor) {
        if (actor instanceof PauseInterface || actor instanceof MenuMenu) {
            return true;
        }
        return false;
    }
}
