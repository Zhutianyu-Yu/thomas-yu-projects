package deco2800.thomas.traderUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.inventory.ItemBox;
import deco2800.thomas.inventory.ui.InventoryMenu;
import deco2800.thomas.inventory.ui.ItemMenu;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.managers.GameManager;
import deco2800.thomas.managers.InputManager;
import deco2800.thomas.managers.SoundManager;
import deco2800.thomas.observers.TouchDownObserver;
import deco2800.thomas.worlds.rooms.TraderWorld;

public class TraderInterface extends Group implements TouchDownObserver {

    private TextButton money;
    private ImageButton ironArmour;
    private ImageButton woodArmour;
    private ImageButton ironSword;
    private ImageButton woodSword;
    private ItemMenu itemMenu;

    private boolean tradeInterfaceDisplayed = false;
    private boolean buyDisplay = false;
    private boolean sellDisplay = false;
    private boolean generated = false;

    private Image table;
    private Image transparent;

    public TraderInterface() {
        this.transparent = new Image(new Texture(Gdx.files.internal("minimap/transparentButton.png")));
        buildTemplate();
        buildButtons();
        buildItemMenu();
        GameManager.getManagerFromInstance(InputManager.class).addTouchDownListenerTradeInterface(this);


    }

    private void buildItemMenu() {
        this.itemMenu = new ItemMenu(GameManager.getManagerFromInstance(InputManager.class).getInventory());
        this.addActor(itemMenu);
        itemMenu.setVisible(false);
    }

    private void buildTemplate() {
        this.table = new Image(new Texture(Gdx.files.internal("npc/shop_table.png")));
        setImageSize(this.table);

        table.setPosition((Gdx.graphics.getWidth()) - this.table.getWidth(),
                (Gdx.graphics.getHeight()) - this.table.getHeight());
        this.addActor(table);
        table.setVisible(buyDisplay);
        Button exit = new ImageButton(new Image(new Texture(Gdx.files.internal("npc/Exit_button.png"))).getDrawable());
        exit.setPosition((Gdx.graphics.getWidth() - exit.getWidth()),
                (exit.getHeight()));
        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((TraderWorld) GameManager.getWorld()).leaveTraderWorld
                        (GameManager.get().getStage());
                tradeInterfaceDisplayed = false;
                for (Actor actor : GameManager.get().getStage().getActors()) {
                    actor.setVisible(!(actor instanceof TraderInterface));
                }
            }
        });
        this.addActor(exit);
        this.generated = true;
    }

    private void buildButtons() {
        Skin skin = GameManager.get().getSkin();
        this.money = new TextButton("MONEY: $", skin, "main_menu");
        money.setPosition((Gdx.graphics.getWidth() - money.getWidth()) / 2,
                (Gdx.graphics.getHeight() - money.getHeight()));
        this.addActor(money);

        Image buy = new Image(new Texture(Gdx.files.internal("npc/Button_buy.png")));
        ImageButton buyButton = new ImageButton(buy.getDrawable());
        setButtonSize(buyButton);
        buyButton.setPosition(this.table.getX(), this.table.getY() - buyButton.getHeight());
        this.addActor(buyButton);
        buyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                buyDisplay = true;
                sellDisplay = false;
            }
        });

        Image sell = new Image(new Texture(Gdx.files.internal("npc/Button_sell.png")));
        ImageButton sellButton = new ImageButton(sell.getDrawable());
        setButtonSize(sellButton);
        sellButton.setPosition(Gdx.graphics.getWidth() - sellButton.getWidth(),
                this.table.getY() - sellButton.getHeight());
        this.addActor(sellButton);
        sellButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                buyDisplay = false;
                sellDisplay = true;
            }
        });

        this.ironArmour = new ImageButton(this.transparent.getDrawable());
        setSmallBuyButtonSize(ironArmour);
        this.ironArmour.setPosition(Gdx.graphics.getWidth() - (this.table.getWidth() * 0.29f), Gdx.graphics.getHeight() - (this.table.getHeight() * 0.18f));
        this.addActor(this.ironArmour);
        this.ironArmour.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(InputManager.class).getInventory().getTrading().buy(new IronArmour());
                playCashSound();
            }
        });
        this.ironArmour.setVisible(false);

        this.woodArmour = new ImageButton(this.transparent.getDrawable());
        setSmallBuyButtonSize(this.woodArmour);
        this.woodArmour.setPosition(this.ironArmour.getX(), this.ironArmour.getY() - (this.table.getHeight() * 0.2f));
        this.addActor(this.woodArmour);
        this.woodArmour.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(InputManager.class).getInventory().getTrading().buy(new IronSword());
                playCashSound();
            }
        });
        this.woodArmour.setVisible(false);

        this.ironSword = new ImageButton(this.transparent.getDrawable());
        setSmallBuyButtonSize(this.ironSword);
        this.ironSword.setPosition(this.ironArmour.getX(), this.woodArmour.getY() - (this.table.getHeight() * 0.23f));
        this.addActor(this.ironSword);
        this.ironSword.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(InputManager.class).getInventory().getTrading().buy(new HealthPotion("large"));
                playCashSound();

            }
        });
        this.ironSword.setVisible(false);

        this.woodSword = new ImageButton(this.transparent.getDrawable());
        setSmallBuyButtonSize(this.woodSword);
        this.woodSword.setPosition(this.ironArmour.getX(), this.ironSword.getY() - (this.table.getHeight() * 0.25f));
        this.addActor(this.woodSword);
        this.woodSword.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameManager.getManagerFromInstance(InputManager.class).getInventory().getTrading().buy(new ManaPotion("large"));
                playCashSound();
            }
        });
        this.woodSword.setVisible(false);
    }

    private void setButtonSize(ImageButton imageButton) {
        imageButton.setHeight((float) (imageButton.getHeight() / 2));
        imageButton.setWidth((float) (imageButton.getWidth() / 2));
    }

    private void setSmallBuyButtonSize(ImageButton imageButton) {
        imageButton.setHeight(this.table.getHeight() * 0.09f);
        imageButton.setWidth(this.table.getWidth() * 0.15f);
    }

    private void setImageSize(Image image) {
        image.setHeight((float) (image.getHeight() / 2));
        image.setWidth((float) (image.getWidth() / 2));
    }

    public boolean getTraderInterfaceDisplayed() {
        return this.tradeInterfaceDisplayed;
    }

    public void setTraderInterfaceDisplayed(boolean displayed) {
        this.tradeInterfaceDisplayed = displayed;
        for (Actor actor : GameManager.get().getStage().getActors()) {
            if (actor instanceof TraderInterface) {
                continue;
            }
            actor.setVisible(false);
        }
    }

    private void checkDisplay() {
        table.setVisible(buyDisplay);
        this.ironArmour.setVisible(buyDisplay);
        this.woodArmour.setVisible(buyDisplay);
        this.ironSword.setVisible(buyDisplay);
        this.woodSword.setVisible(buyDisplay);
        this.itemMenu.setVisible(sellDisplay);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (this.tradeInterfaceDisplayed) {
            if (!generated) {
                buildTemplate();
            } else {
                checkDisplay();
            }
            itemMenu.updateInventory();

            try {
                int count = GameManager.getManagerFromInstance(InputManager.class).getInventory().getTrading().getMoney();
                this.money.setText("MONEY: $" + count);
            } catch (NullPointerException ignored){} // Ignore the null pointer

            super.draw(batch, parentAlpha);
        }
    }

    /**
     * Notifies the observers of the mouse button being pushed down
     *
     * @param screenX the x position the mouse was pressed at
     * @param screenY the y position the mouse was pressed at
     * @param pointer
     * @param button  the button which was pressed
     */
    @Override
    public void notifyTouchDown(int screenX, int screenY, int pointer, int button) {

        Vector2 tmp = getStage().screenToStageCoordinates(new Vector2((float) screenX, (float) screenY));
        Actor hitActor = getStage().hit(tmp.x, tmp.y, false);

        if (sellDisplay && button == Input.Buttons.LEFT && hitActor instanceof ItemBox) {
            InventoryAbstract inventory = GameManager.getManagerFromInstance(InputManager.class).getInventory();
            InventoryMenu menu = inventory.getMenu();
            menu.sellMode(hitActor, this.itemMenu, inventory.getTrading());
            playCashSound();
        }
    }


    private void playCashSound() {
        GameManager.getManagerFromInstance(SoundManager.class).playSound("cash_sound.mp3");
    }
}
