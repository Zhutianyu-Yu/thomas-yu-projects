package deco2800.thomas.entities;

import deco2800.thomas.worlds.Tile;
import org.junit.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TutorialNPCTest {

    TutorialNPC tutor;
    Tile position;

    @Before
    public void setup() {
        this.position = new Tile("blank", 25, 25);
        this.tutor = new TutorialNPC(position, "tuteMan");
    }

    @Test
    public void testConstructor() {
        assertEquals(position.getCol(), tutor.getCol());
        assertEquals(position.getRow(), tutor.getRow());
        assertNotEquals("traderMan", tutor.getTexture());
        assertEquals("tuteMan", tutor.getTexture());
    }

    @Test
    public void testEquals() {
        TutorialNPC tutorNot = new TutorialNPC(new Tile("blank", 8, 8), "traderMan");
        assertNotEquals(tutorNot, tutor);
        TutorialNPC tutorEqual = new TutorialNPC(new Tile("blank", 25, 25), "tuteMan");
        assertEquals(tutorEqual, tutor);
    }

}
