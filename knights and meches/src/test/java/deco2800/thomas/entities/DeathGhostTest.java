package deco2800.thomas.entities;
import deco2800.thomas.worlds.Tile;
import org.junit.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DeathGhostTest {

    private DeathGhost ghost;
    private DeathGhost ghost1;
    private Tile position;

    @Before
    public void setup() {

        ghost1 = new DeathGhost();
        position = new Tile("blank", 2, 3);
        ghost = new DeathGhost(position, 2, "ghost4", true);
    }


    @Test
    public void testConstructor() {

        assertEquals(2f, ghost.getCol());
        assertEquals(3f, ghost.getRow());
        assertEquals("ghost4", ghost.getTexture());
        assertEquals("ghost1", ghost1.getTexture());
    }
}
