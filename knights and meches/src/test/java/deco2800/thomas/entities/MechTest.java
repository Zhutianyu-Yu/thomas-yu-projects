package deco2800.thomas.entities;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MechTest {

    private Mech mech;
    private Knight knight1;
    private Knight knight2;
    private Wizard wizard1;
    private Wizard wizard2;
    private double ratio;
    private int mechtype;

    /**
     * setup mech for testing
     */
    @Before
    public void ConstructEntites() {
        knight1 = new Knight(1, 1, 1, "typeA");
        wizard1 = new Wizard(1, 1, 1, "typeA");
        mech = new Mech(1, 1, 1, wizard1, knight1);
        ratio = mech.getFunctionRatio(wizard1, knight1);
        mechtype = mech.getMechType();
    }

    /**
     * Ensure Mech constructor is working
     */
    @Test
    public void testConstructor() {

        assertThat("", mech.getCol(), is(equalTo(1f)));
        assertThat("", mech.getRow(), is(equalTo(1f)));
        assertThat("", mech.getSpeed(), is(equalTo(1f)));
    }

    /**
     * Ensure mech starting player are initiated correctly
     */
    @Test
    public void testStartingValuesPlayer() {

        assertEquals(knight1, mech.getMechKnight());

        assertEquals(wizard1, mech.getMechWizard());

        assertEquals("mech", mech.getObjectName());
    }

    /**
     * Ensure mech starting armour are initiated correctly
     */
    @Test
    public void testStartingValuesArmour() {
        int armour = knight1.getArmour() + wizard1.getArmour();
        armour = (mechtype == 1 || mechtype == 3) ? (int) (armour * (1 + ratio)) : (int) (armour * (1 - ratio));
        assertEquals(armour, mech.getArmour());
    }

    /**
     * Ensure mech starting mana are initiated correctly
     */
    @Test
    public void testStartingValuesMana() {
        int mana = knight1.getMana() + wizard1.getMana();
        mana = (mechtype == 2 || mechtype == 3) ? (int) (mana * (1 + ratio)) : (int) (mana * (1 - ratio));
        assertEquals(mana, mech.getMana());
    }

    /**
     * Ensure mech starting hp are initiated correctly
     */
    @Test
    public void testStartingValueshp() {
        int hp = knight1.getHealth() + wizard1.getHealth();
        hp = (mechtype == 0 || mechtype == 1) ? (int) (hp * (1 + ratio)) : (int) (hp * (1 - ratio));
        assertEquals(hp, mech.getHealth());
    }

    /**
     * Ensure mech starting damage are initiated correctly
     */
    @Test
    public void testStartingValuesDamage() {
        int damage = knight1.getDamage() + wizard1.getDamage();
        damage = (mechtype == 0 || mechtype == 2) ? (int) (damage * (1 + ratio)) : (int) (damage * (1 - ratio));
        assertEquals(damage, mech.getDamage());
    }

    /**
     * check that changing the wizard and knight in the mech works
     */
    @Test
    public void testChangeCharacers() {
        assertThat("", mech.getMechKnight(), is(equalTo(knight1)));
        mech.changeMechKnight(knight2);
        assertThat("", mech.getMechKnight(), is(equalTo(knight2)));

        assertThat("", mech.getMechWizard(), is(equalTo(wizard1)));
        mech.changeMechWizard(wizard2);
        assertThat("", mech.getMechWizard(), is(equalTo(wizard2)));
    }

    /**
     * Ensure a mech can die
     */
    @Test
    public void testMechDeath() {
        mech.setHealth(1);
        assertTrue(mech.aliveOrNot());
        mech.setHealth(0);
        assertFalse(mech.aliveOrNot());
        mech.setHealth(-1);
        assertFalse(mech.aliveOrNot());
    }

    @Test
    public void testMaxValues() {
        assertEquals(mech.getMaxHealth(), 2000);
        assertEquals(mech.getMaxMana(), 200);
    }

    @Test
    public void setArmourTest() {
        mech.setArmour(2);
        assertEquals(mech.getArmour(), 2);
    }

    @Test
    public void setDamageTest() {
        mech.setDamage(3);
        assertEquals(mech.getDamage(), 3);
    }

    @Test
    public void setManaTest() {
        mech.setMana(6);
        assertEquals(mech.getMana(), 6);
    }
}
