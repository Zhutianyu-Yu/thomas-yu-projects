package deco2800.thomas.entities;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class PlayerTest {
	@Test
	public void testPlayerSet() {
		PlayerPeon p = new PlayerPeon(0,0,1);
	}
	
	@Test
	public void testConstructor() {
		PlayerPeon p = new PlayerPeon(1,1,1);
		assertThat("", p.getCol(), is(equalTo(1f)));
		assertThat("", p.getRow(), is(equalTo(1f)));
		assertThat("", p.getSpeed(), is(equalTo(1f)));
	}


	@Test
	public void testBaseValue() {
		Knight a = new Knight(1,1,1,"typeA");
		assertEquals(600, a.getHealth());
		assertEquals(70, a.getDamage());
		assertEquals(30, a.getArmour());
		assertEquals(0, a.getLevel());
		assertEquals(0, a.getXP());
		assertEquals(20, a.getMana());

	}


	@Test
	public void testLevelBaseValue() {
		Knight a = new Knight(1,1,1,"typeA");
		for (int i = 0; i < 10; i++) {
			assertEquals(70 + 9 * i, a.getDamage());
			assertEquals(600 + 70 * i, a.getHealth());
			assertEquals(30 + 4 * i, a.getArmour());
			a.levelUp();
		}
	}


	@Test
	public void testManaXpLevel() {
		Knight a = new Knight(1,1,1,"typeA");
		a.setMana(0);
		a.addMana(10);
		assertEquals(10, a.getMana());
		a.addMana(100);
		assertEquals(100, a.getMana());
		a.addXP(10);
		assertEquals(10, a.getXP());
		a.addXP(100);
		assertEquals(0, a.getXP());
		assertEquals(1, a.getLevel());
		a.addXP(60);
		assertEquals(1, a.getLevel());
		a.addXP(60);
		assertEquals(2, a.getLevel());
	}

	@Test
	public void testHpDamageArmour() {

		Knight a = new Knight(1,1,1,"typeA");
		int maxHp = a.getLevel() * 70 + 600;
		a.setHealth(0);
		assertFalse(a.aliveOrNot());
		a.setHealth(1000);
		assertEquals(maxHp, a.getHealth());
		a.setDamage(1000);
		assertEquals(1000, a.getDamage());
		a.setArmour(1000);
		assertEquals(1000, a.getArmour());
	}

	@Test
	public void testDoDamage() {
		Knight a = new Knight(1,1,1,"typeA");
		Knight b = new Knight(1,1,1,"typeA");
		a.setMana(0);
		b.setMana(0);
		a.doDamage(b);
		assertEquals(560, b.getHealth());

		assertEquals(10, a.getMana());

	}

	@Test
	public void testSkill() {
		Knight a = new Knight(1,1,1,"typeA");
		Knight b = new Knight(1,1,1,"typeB");
		Knight c = new Knight(1,1,1,"typeA");
		Knight d = new Knight(1,1,1,"typeB");
		a.setMana(0);
		b.setMana(0);
		c.setMana(0);
		d.setMana(0);

		ArrayList<HasHealth> teamOne  = new ArrayList<>();
		teamOne.add(a);
		teamOne.add(b);
		teamOne.add(c);
		teamOne.add(d);

		Wizard wa = new Wizard(2,2,2,"typeA");
		Wizard wb = new Wizard(2,2,2,"typeB");
		Wizard wc = new Wizard(2,2,2,"typeA");
		Wizard wd = new Wizard(2,2,2,"typeA");
		wa.setMana(0);
		wb.setMana(0);
		wc.setMana(0);
		wd.setMana(0);

		ArrayList<HasHealth> teamTwo  = new ArrayList<>();
		teamTwo.add(wa);
		teamTwo.add(wb);
		teamTwo.add(wc);
		teamTwo.add(wd);

		a.addMana(80);
		a.skill(teamOne, teamTwo);
		assertEquals(600, wa.getHealth());
		a.addMana(20);
		a.skill(teamOne, teamTwo);

		for (HasHealth w : teamTwo) {
			assertEquals(490, w.getHealth());
		}
		assertEquals(10, a.getMana());

		b.addMana(60);
		b.skill(teamOne, teamTwo);
		for (HasHealth k : teamOne) {
			assertEquals(45, k.getArmour());
		}
		assertEquals(10, b.getMana());



		wa.addMana(60);
		wa.skill(teamTwo, teamOne);
		assertEquals(490, wa.getHealth());

		wa.levelUp();
		wa.levelUp();
		wa.levelUp();
		wa.levelUp();



		wa.skill(teamTwo, teamOne);

		assertEquals(Math.min(600 + 4 * 70 + 100, wa.getMaxHealth()), wa.getHealth());
		assertEquals(490 + 100, wb.getHealth());
		assertEquals(490 + 100, wc.getHealth());
		assertEquals(490 + 100, wd.getHealth());

		wb.addMana(80);
		wb.skill(teamTwo, teamOne);

		for (HasHealth k : teamOne) {
			assertEquals(500, k.getHealth());
		}



	}




}
