package deco2800.thomas.entities;

import deco2800.thomas.worlds.Tile;
import org.junit.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class TraderNPCTest {

    TraderNPC trader;
    Tile position;
    @Before
    public void setup() {
        position = new Tile("blank", 9, 9);
        trader = new TraderNPC(position, "traderMan");
    }

    @Test
    public void testConstructor() {
        assertEquals(position.getCol(), trader.getCol());
        assertEquals(position.getRow(), trader.getRow());
        assertEquals("traderMan", trader.getTexture());
    }

    @Test
    public void testEquals() {
        TraderNPC traderNot = new TraderNPC(new Tile("blank", 8, 8), "traderMan");
        assertNotEquals(traderNot, trader);
        TraderNPC traderEqual = new TraderNPC(new Tile("blank", 9, 9), "traderMan");
        assertEquals(traderEqual, trader);
    }
}
