package deco2800.thomas.quest;

//import deco2800.thomas.managers.QuestManager;
import org.junit.*;
import org.sqlite.SQLiteException;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static deco2800.thomas.quest.QuestTrackDB.*;

public class QuestDBTest {
    private ArrayList<QuestReward> rewards;
    private ArrayList<PlayerPrerequisite> playerPrerequisites;
    private ArrayList<QuestTask> questTasks;
    private ArrayList<Quest> quests;
    private static final String testDbFileName = "Quest.db";
    private static final String testDbConnectionString = "jdbc:sqlite:" + testDbFileName;

    private void setQuestRelatedObjects() {
        rewards = new ArrayList<>();
        rewards.add(new QuestReward(100, "Gold", 1));
        rewards.add(new QuestReward(0, "Sword", 10));
        rewards.add(new QuestReward(0, null, 0));

        playerPrerequisites = new ArrayList<>();
        playerPrerequisites.add(new PlayerPrerequisite(1, null, 0));
        playerPrerequisites.add(new PlayerPrerequisite(1, "Sword", 2));

        questTasks = new ArrayList<>();
        questTasks.add(new ObjectInteractionQuestTask(
                1,
                "Collect golds",
                "desert",
                20,
                new ArrayList<>(),
                0,
                "Gold",
                "Pick up gold"));

        questTasks.add(new NPCInteractionQuestTask(
                2,
                "Complete Tutorial",
                "desert",
                1,
                List.of(questTasks.get(0)),
                0,
                1,
                "talk to NPC1"));

        questTasks.add(new KillQuestTask(
                3,
                "Kill goblin",
                "desert",
                5,
                List.of(questTasks.get(0), questTasks.get(1)),
                0,
                "fire"));

        quests = new ArrayList<>();
        quests.add(new Quest(
                1,
                "First quest",
                "Complete the tutorial and do some skids",
                true,
                rewards,
                questTasks,
                null,
                null,
                true,
                false,
                false));

        quests.add(new Quest(
                2,
                "Second Quest",
                "Embrace your origins and whatnot",
                true,
                rewards,
                questTasks,
                List.of(quests.get(0).getId()),
                playerPrerequisites,
                false,
                false,
                false));
    }

    private void setDataBase() throws SQLException {
        setQuestRelatedObjects();
        Quest firstQuest = quests.get(0);
        Quest secondQuest = quests.get(1);
        QuestTask objectTask = questTasks.get(0);
        QuestTask npcTask = questTasks.get(1);
        QuestTask killTask = questTasks.get(2);
        int secondQuestId = secondQuest.getId();
        createQuestDataTable();

        questInsert(firstQuest);
        questInsert(secondQuest);

        questTaskInsert(objectTask, secondQuest);
        questTaskInsert(npcTask, secondQuest);
        questTaskInsert(killTask, secondQuest);

        objectInteractionQuestTaskInsert((ObjectInteractionQuestTask) objectTask, secondQuestId);
        npcInteractionQuestTaskInsert((NPCInteractionQuestTask) npcTask, secondQuestId);
        killQuestTaskInsert((KillQuestTask) killTask,secondQuestId);

        for (QuestTask qt: questTasks) {
            for (QuestTask preQuestTask: qt.getTaskPrerequisites()) {
                questTaskPrerequisiteInsert(qt, preQuestTask);
            }
        }

        questPrerequisiteInsert(firstQuest.getId(), secondQuest.getId());

        playerPrerequisiteInsert( playerPrerequisites.get(0), firstQuest);
        playerPrerequisiteInsert(playerPrerequisites.get(1), secondQuest);

        questRewardInsert(rewards.get(0), firstQuest);
        questRewardInsert(rewards.get(1), secondQuest);
        questRewardInsert(rewards.get(2), secondQuest);

    }

    @Before
    public void setUp() throws SQLException {
        setQuestRelatedObjects();
        QuestTrackDB.initDatabase(testDbConnectionString);
        dropAllTable();
    }

    @AfterClass
    public static void dispose() {
        File dbFile = new File(testDbFileName);
        if (dbFile.exists()) {
            if (!dbFile.delete())
                LOGGER.error("Failed to delete test database file");
        }
    }

    @Test(expected = SQLException.class)
    public void insertToNonExistingTable() throws SQLException {
        setQuestRelatedObjects();
        questInsert(quests.get(0));
    }

    @Test(expected = SQLException.class)
    public void deleteFromNonExistingTable() throws SQLException {
        setQuestRelatedObjects();
        questDelete(quests.get(0));
    }

    @Test
    public void getNonExistingQuest() throws SQLException {
        setDataBase();
        Assert.assertNull(getQuest("Non Exist"));
    }

    @Test(expected = SQLiteException.class)
    public void insertNonExistingForeignKey() throws SQLException {
        setQuestRelatedObjects();
        questTaskInsert(questTasks.get(1), quests.get(1));
    }

    @Test
    public void insertAndGetValidQuestTask() throws SQLException {
        setDataBase();
        ArrayList<QuestTask> questTaskList = getQuestTasks(quests.get(1).getId());
        for (QuestTask taskFromDB: questTaskList) {
            for (QuestTask insertedTask: questTasks) {
                if (taskFromDB.getTaskID() == insertedTask.getTaskID()) {
                    Assert.assertEquals(taskFromDB.getTaskDescription(), insertedTask.getTaskDescription());
                    Assert.assertEquals(taskFromDB.getTaskLocation(), insertedTask.getTaskLocation());
                    Assert.assertEquals(taskFromDB.getRequiredQuantity(), insertedTask.getRequiredQuantity());
                }
            }
        }
    }

    @Test
    public void constructQuestFromDataBase() throws SQLException{
        setDataBase();
        Quest questFromDB = getQuest("Second Quest");
        Assert.assertNotNull(questFromDB);
        Assert.assertEquals(questFromDB.getName(), quests.get(1).getName());
        Assert.assertEquals(questFromDB.getDescription(), quests.get(1).getDescription());
    }
    @Test
    public void deleteFromDataBase() throws SQLException {
        setDataBase();
        Quest firstQuest = quests.get(0);
        Quest secondQuest = quests.get(1);

        questTasksDelete(secondQuest);

        questTaskPrerequisiteDelete(1);

        questPrerequisiteDelete(1);

        playerPrerequisiteDelete(1);
        playerPrerequisiteDelete(2);

        questRewardDelete(1);
        questRewardDelete(2);
        questRewardDelete(3);

        questDelete(firstQuest);
        questDelete(secondQuest);

        Assert.assertNull(getQuest(firstQuest.getName()));
        Assert.assertNull(getQuest(secondQuest.getName()));
    }

/*    @Test
    public void questTrackDBLoadStoredQuests() throws SQLException {
        setDataBase();
        QuestManager manager = QuestManager.get();
        Assert.assertTrue(manager.loadQuestsIntoDb());
        Assert.assertTrue(manager.startQuest("Your first orb"));
        List<Quest> quests = manager.getCurrentQuests();
        Assert.assertNotNull(quests);
        Assert.assertNotEquals(0, quests.size());
        Assert.assertEquals(quests.get(0).getName(), "Your first orb");
    }*/
}
