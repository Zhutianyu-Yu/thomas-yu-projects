package deco2800.thomas.quest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test quest implementation
 */
public class QuestTest {
    private ArrayList<QuestReward> rewards;
    private ArrayList<PlayerPrerequisite> playerPrerequisites;
    private ArrayList<QuestTask> questTasks;
    private ArrayList<Quest> quest;
    /**
     * Set up object to create a quest
     */
    @Before
    public void setUp() {
        rewards = new ArrayList<>();
        rewards.add(new QuestReward(100, "001", 1));
        rewards.add(new QuestReward(0, "002", 10));
        rewards.add(new QuestReward(0,null, 0));

        playerPrerequisites = new ArrayList<>();
        playerPrerequisites.add(new PlayerPrerequisite(1,null,0));
        playerPrerequisites.add(new PlayerPrerequisite(1,"001",2));

        questTasks = new ArrayList<>();
        questTasks.add(new NPCInteractionQuestTask(
                1,
                "Complete Tutorial",
                "desert",
                1,
                new ArrayList<>(),
                0,
                1,
                "talk to NPC1"));

        questTasks.add(new KillQuestTask(
                2,
                "Kill goblin",
                "desert",
                5,
                List.of(questTasks.get(0)),
                0,
                "lavaskeleton"));



        questTasks.add(new ObjectInteractionQuestTask(
                3,
                "Collect 5 golds",
                "desert",
                20,
                List.of(questTasks.get(0), questTasks.get(1)),
                0,
                "001",
                "Pick up gold"));

        quest = new ArrayList<>();
        quest.add(new Quest(
                1,
                "First quest",
                "Complete the tutorial and do some skids",
                true,
                rewards,
                questTasks,
                null,
                null,
                true,
                false,
                false));

        quest.add(new Quest(
                2,
                "Second Quest",
                "Embrace your origins and whatnot",
                true,
                rewards,
                questTasks,
                List.of(quest.get(0).getId()),
                playerPrerequisites,
                false,
                false,
                false));
    }

    @Test
    public void checkPlayerPrerequisitesAdded() {
        for (int i = 0; i < playerPrerequisites.size(); i++) {
            if (playerPrerequisites.get(i) != quest.get(1).getPlayerPrerequisites().get(i))
                Assert.fail();
        }
    }

    @Test
    public void getCorrectPlayerPrerequisites1() {
        PlayerPrerequisite test = playerPrerequisites.get(0);
        if (1 != test.getPlayerLevel() || test.getItemTypeId() != null)
            Assert.fail();
    }

    @Test
    public void getCorrectPlayerPrerequisites2() {
        PlayerPrerequisite test = playerPrerequisites.get(1);
        if (test.getPlayerLevel() != 1 || !test.getItemTypeId().equals("001"))
            Assert.fail();
    }

    @Test
    public void checkRewardsAdded() {
        for (int i = 0; i < rewards.size(); i++) {
            if (rewards.get(i) != quest.get(1).getRewards().get(i))
                Assert.fail();
        }
    }

    @Test
    public void getCorrectRewards() {
        QuestReward test = rewards.get(0);
        if (test.getXpReward() != 100 || !test.getItemTypeId().equals("001") ||
                test.getItemQuantity() != 1) {
            Assert.fail();
        }
    }

    /**
     * Check for early activation of quests
     */
    @Test
    public void accessQuestPrematurely() {
        if (!quest.get(0).isComplete() && quest.get(1).isActive())
                Assert.fail();
    }

    /**
     * Check for early activation of tasks
     */
    @Test
    public void accessTaskPrematurely() {
        quest.get(0).setActive(false);
        quest.get(0).markAsComplete();
        quest.get(1).setActive(true);
        QuestTask firstTask = quest.get(1).getQuestTasks().get(0);
        QuestTask secondTask = quest.get(1).getQuestTasks().get(1);
        if (!firstTask.isComplete() && secondTask.isAvailable())
            Assert.fail();
    }

    /**
     * Check incomplete condition for quest
     */
    @Test
    public void checkQuestCondition() {
        Assert.assertFalse(quest.get(1).isComplete());
    }

    /**
     * Check complete condition for tasks
     */
    @Test
    public void completeTask() {
        questTasks.get(0).incrementProgress(1);
        if (!questTasks.get(0).isComplete())
            Assert.fail();
    }

    /**
     * check complete condition for both tasks and quests.
     */
    @Test
    public void completeAllTask() {
        for (QuestTask task:quest.get(1).getQuestTasks()) {
            if (!task.isComplete())
                task.incrementProgress(task.getRequiredQuantity());
        }
        Assert.assertTrue(quest.get(1).isComplete());
    }
}
