package deco2800.thomas;

import deco2800.thomas.entities.*;
import deco2800.thomas.entities.combat.*;
import deco2800.thomas.managers.*;
import deco2800.thomas.moves.Move;
import deco2800.thomas.moves.MoveManager;
import deco2800.thomas.moves.MoveType;
import deco2800.thomas.util.Party;
import deco2800.thomas.worlds.*;
import deco2800.thomas.worlds.dungeons.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Map;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import org.powermock.core.classloader.annotations.PowerMockIgnore;


@RunWith(PowerMockRunner.class)
@PrepareForTest({GameManager.class, DatabaseManager.class, PlayerPeon.class})
@PowerMockIgnore({"jdk.internal.reflect.*"})

/**
 * Tests combat screen
 */
public class CombatTest {
    private CombatWorld world;
    private EnemyPeon enemy;
    private Wizard wizard;
    private Knight knight;
    private PlayerPeon player;
    @Mock
    private GameManager mockGM;


    /**
     * Sets testing environment up
     */
    @Before
    public void Setup() {
        TestWorld testWorld = new TestWorld();

        mockGM = mock(GameManager.class);
        mockStatic(GameManager.class);

        when(GameManager.get()).thenReturn(mockGM);
        when(mockGM.getWorld()).thenReturn(world);
        when(GameManager.get().getPreviousWorld()).thenReturn(testWorld);
        when(GameManager.get().getMoveManager()).thenReturn(new MoveManager());
        // Mocked input manager
        InputManager Im = new InputManager();

        OnScreenMessageManager mockOSMM = mock(OnScreenMessageManager.class);
        when(mockGM.getManager(OnScreenMessageManager.class)).thenReturn(mockOSMM);

        when(GameManager.getManagerFromInstance(InputManager.class)).thenReturn(Im);

        CopyOnWriteArrayList<Tile> tiles = new CopyOnWriteArrayList<Tile>();
        for (int row = 0; row < 5; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                tiles.add(new Tile("grass_1_0", col, row));
            }
        }

        GameManager.get().getMoveManager().clearMoves();

        testWorld.setTileMap(tiles);
        enemy = new EnemyPeon(0, 0, 0, 0, 0);
        player = new PlayerPeon(0, 1, 0);
        player.setType(PlayerType.KNIGHT);

        knight = new Knight(0, 0, 0, "");
        wizard = new Wizard(0, 0, 0, "");

        testWorld.addEntity(player);
        testWorld.addEntity(enemy);

        mockGM.setPreviousWorld(WorldTypes.HUB_WORLD);

        Party party = new Party(player);

        world = new CombatWorld(enemy, party);
        world.setup();
    }

    /**
     * Tests that world tile size is responsive to positions
     */
    @Test
    public void checkMapSize()
    {
        List<Tile> tiles = world.getTileMap();

        //Only two tiles should be present, one under player and one under enemy
        Assert.assertEquals(2, tiles.size(), 0.01);
    }

    /**
     * Tests that moves can be added and retrieved from MoveManager
     */
    @Test
    public void moveManagerGetMoves()
    {
        MoveManager moveManager = GameManager.get().getMoveManager();

        List<Move> moves = moveManager.getMoves();
        Assert.assertEquals(moves.size(), 0);

        moveManager.addMove(new Move("test", 0, 0));
        Assert.assertEquals(moves.size(), 1);

        Assert.assertNotEquals(moveManager.getMove("test"), null);
    }

    /**
     * Tests manager move can be added to player
     */
    @Test
    public void addMoveToPlayer()
    {
        MoveManager moveManager = GameManager.get().getMoveManager();

        moveManager.addMove(new Move("test", 0, 0));
        player.addAttack("test");

        for (Map.Entry<String, Move> entry : player.getMoves().entrySet())
        {
            if (entry.getKey().equals("test")) return;
        }

        Assert.fail();
    }

    /**
     * Tests that player can simply attack enemy
     */
    @Test
    public void simplePlayerAttack()
    {
        final int startHealth = 100;
        final int moveAmount = 5;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 0, startHealth, 0);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount, enemy.getHealth());
    }

    /**
     * Tests player attack with attack damage set
     */
    @Test
    public void attackWithBaseDamage()
    {
        final int startHealth = 100;
        final int moveAmount = 5;
        final int baseAttack = 10;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(baseAttack, 0, startHealth, 0);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount - baseAttack, enemy.getHealth());
    }

    /**
     * Tests that armor is considered when completing a physical attack
     */
    @Test
    public void physicalAttackWithArmor()
    {
        final int startHealth = 100;
        final int moveAmount = 5;
        final int baseArmor = 10;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 0, startHealth, baseArmor);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount + baseArmor, enemy.getHealth());
    }

    /**
     * Tests armor and base-attack damage are considered in physical attack
     */
    @Test
    public void physicalAttackWithArmorAndBase()
    {
        final int startHealth = 100;
        final int moveAmount = 5;
        final int baseArmor = 10;
        final int baseAttack = 15;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(baseAttack, 0, startHealth, baseArmor);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount + baseArmor - baseAttack, enemy.getHealth());
    }

    /**
     * Tests that magical attacks ignore armor
     */
    @Test
    public void magicalAttackWithArmor()
    {
        final int startHealth = 100;
        final int moveAmount = 5;
        final int baseArmor = 10;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", PlayerStatistics.HEALTH, moveAmount, MoveType.MAGICAL, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 0, startHealth, baseArmor);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount, enemy.getHealth());
    }

    /**
     * Tests that mana is decremented after an attack
     */
    @Test
    public void simpleAttackWithMana()
    {
        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", 0, 10);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 20, 0, 0);

        Assert.assertEquals(player.getMana(), 20);
        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(player.getMana(), 10);
    }

    /**
     * Tests that move fails if mana pool is empty
     */
    @Test
    public void emptyManaPool()
    {
        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", 0, 10);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 0, 0, 0);

        Assert.assertEquals(player.getMana(), 0);
        Assert.assertFalse(moveManager.calcDamage(move, player, enemy));
    }

    /**
     * Test that ensures the amount of mana cant exceed the max amount of mana
     */
    @Test
    public void testMaxManaCapPlayer() {
        player.addMana(player.getMaxMana() + 50);
        Assert.assertEquals(player.getMaxMana(), player.getMana());
    }

    /**
     * Test that ensures the amount of health cant exceed the max amount of health
     */
    @Test
    public void testMaxHealthCapPlayer() {
        player.setHealth(player.getMaxHealth() + 50);
        Assert.assertEquals(player.getMaxHealth(), player.getHealth());
    }

    /**
     * Test that ensures the amount of mana cant exceed the max amount of mana
     */
    @Test
    public void testMaxManaCapEnemy() {
        enemy.setMana(enemy.getMaxMana() + 50);
        Assert.assertEquals(enemy.getMaxMana(), enemy.getMana());
    }

    /**
     * Test that ensures the amount of health cant exceed the max amount of health
     */
    @Test
    public void testMaxHealthCapEnemy() {
        enemy.setHealth(enemy.getMaxHealth() + 50);
        Assert.assertEquals(enemy.getMaxHealth(), enemy.getHealth());
    }

    /**
     * Ensure an enemy can die
     */
    @Test
    public void testEnemyDeath() {
        enemy.setHealth(1);
        Assert.assertTrue(enemy.aliveOrNot());
        enemy.setHealth(0);
        Assert.assertFalse(enemy.aliveOrNot());
        enemy.setHealth(-1);
        Assert.assertFalse(enemy.aliveOrNot());
    }

    /**
     * Ensure a player can die
     */
    @Test
    public void testPlayerDeath() {
        player.setHealth(1);
        Assert.assertTrue(player.aliveOrNot());
        player.setHealth(0);
        Assert.assertFalse(player.aliveOrNot());
        player.setHealth(-1);
        Assert.assertFalse(player.aliveOrNot());
    }

    /**
     * Tests simple damage interaction with robot enemy
     */
    @Test
    public void testRobotEnemy()
    {
        testEnemyTypeInteraction(EnemyType.ROBOT);
    }

    /**
     * Tests simple damage interaction with boss enemy
     */
    @Test
    public void testBossEnemy()
    {
        testEnemyTypeInteraction(EnemyType.BOSS);
    }

    /**
     * Tests simple damage interaction with soldier enemy
     */
    @Test
    public void testSoldierEnemy()
    {
        testEnemyTypeInteraction(EnemyType.SOLDIER);
    }

    /**
     * Tests health cannot exceed max health
     */
    @Test
    public void testMechMaxHealth()
    {
        Mech mech = new Mech(0, 0, 0, wizard, knight);
        int max = mech.getMaxHealth();

        mech.setHealth(max + 1);
        Assert.assertEquals(max, mech.getMaxHealth());
    }

    /**
     * Tests health cannot decrease below 0
     */
    @Test
    public void testMechMinHealth()
    {
        Mech mech = new Mech(0, 0, 0, wizard, knight);

        mech.setHealth(-5);
        Assert.assertEquals(0, mech.getHealth());
    }
    /**
     * Base setting of player and enemy statistics
     * @param playerDamage player's default damage
     * @param playerMana player's default mana
     * @param enemyHealth enemy's base health
     * @param enemyArmor enemy's base armor
     */
    private void playerAndEnemySetup(int playerDamage, int playerMana, int enemyHealth, int enemyArmor)
    {
        playerAndEnemySetup(playerDamage, playerMana, enemyHealth, enemyArmor, EnemyType.BASE);
    }

    /**
     * Base setting of player and enemy statistics
     * @param playerDamage player's default damage
     * @param playerMana player's default mana
     * @param enemyHealth enemy's base health
     * @param enemyArmor enemy's base armor
     * @param type player's specific type to be set
     */
    private void playerAndEnemySetup(int playerDamage, int playerMana, int enemyHealth, int enemyArmor, EnemyType type)
    {
        player = new PlayerPeon(0,0,0);
        player.setDamage(playerDamage);
        player.setMana(playerMana);

        if (type == EnemyType.BASE)
        {
            enemy = new EnemyPeon(0,0,0,0,0);
        }
        else if (type == EnemyType.BOSS)
        {
            enemy = new BossEnemy(0, 0, 0, 0, 0);
        }
        else if (type == EnemyType.ROBOT)
        {
            enemy = new RobotEnemy(0, 0, 0, 0, 0);
        }
        else if (type == EnemyType.SOLDIER)
        {
            enemy = new SoldierEnemy(0, 0, 0, 0, 0);
        }
        else if (type == EnemyType.CORE) {
            enemy = new CoreEnemy(0,0,0,0,0);
        }
        else if (type == EnemyType.CPU) {
            enemy = new CPUEnemy(0,0,0,0,0);
        }
        else if (type == EnemyType.SEEKER) {
            enemy = new SeekerEnemy(0,0,0,0,0);
        }
        else if (type == EnemyType.JELLYFISH) {
            enemy = new JellyfishEnemy(0,0,0,0,0);
        }
        else
        {
            Assert.fail();
        }

        enemy.setHealth(enemyHealth);
        enemy.setArmour(enemyArmor);
    }

    @Test
    public void testEnemyDamage() {
        enemy.setDamage(2);
        Assert.assertEquals(2, enemy.getDamage());
    }

    @Test
    public void addEnemyAttack() {
        MoveManager moveManager = GameManager.get().getMoveManager();

        moveManager.addMove(new Move("test", 0, 0));
        enemy.addAttack("test");

        for (String move : enemy.getMoveNames())
        {
            if (move.equals("test")) return;
        }

        Assert.fail();
    }

    private void testEnemyTypeInteraction(EnemyType type) {
        final int startHealth = 100;
        final int moveAmount = 5;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        player.addAttack("test");
        playerAndEnemySetup(0, 0, startHealth, 0, type);

        moveManager.calcDamage(move, player, enemy);

        Assert.assertEquals(startHealth - moveAmount, enemy.getHealth());
    }

    /**
     * Tests simple damage interaction with core enemy
     */
    @Test
    public void testCoreEnemy() {
        testEnemyTypeInteraction(EnemyType.CORE);
    }

    /**
     * Tests simple damage interaction with cpu enemy
     */
    @Test
    public void testCPUEnemy() {
        testEnemyTypeInteraction(EnemyType.CPU);
    }

    /**
     * Tests simple damage interaction with jellyfish enemy
     */
    @Test
    public void testJellyfishEnemy() {
        testEnemyTypeInteraction(EnemyType.JELLYFISH);
    }

    /**
     * Tests simple damage interaction with jellyfish enemy
     */
    @Test
    public void testSeekerEnemy() {
        testEnemyTypeInteraction(EnemyType.SEEKER);
    }


    @Test
    public void worldNames() {
        assertEquals(world.getClass().getSimpleName(),world.getName());
        world.setName("Test");
        assertEquals("Test",world.getName());
    }

    @Test
    public void enemyIdentifier() {
        SoldierEnemy soldier = new SoldierEnemy(0,0,0,0,0);
        assertEquals(EnemyType.SOLDIER, soldier.getType());
        assertEquals(WorldTypes.TEST, soldier.getBiome());
        assertEquals("SOLDIERTEST",soldier.getTypeIdentifier());
    }

    @Test
    public void giveEnemyNoTask() {
        enemy.setTask(null);
        assertNull(enemy.getTask());
    }

    @Test
    public void enemySpawnsWithDetection() {
        SeekerEnemy seeker = new SeekerEnemy(0,0,0,0,0);
        assertTrue(seeker.getCanDetect());
    }

    @Test
    public void testEnemySetMana() {
        enemy.setMana(enemy.getMaxMana() - 5);
        Assert.assertEquals(enemy.getMaxMana() - 5, enemy.getMana());
    }

    @Test
    public void getEnemyMoves() {
        final int moveAmount = 5;

        MoveManager moveManager = GameManager.get().getMoveManager();

        Move move = new Move("test", moveAmount, 0);
        moveManager.addMove(move);

        enemy.addAttack("test");
        Map<String, Move> map = enemy.getMoves();

        assertTrue(map.containsKey("test"));
        enemy.addAttack("test");
        assertTrue(map.containsKey("test"));
    }

    @Test
    public void testChoosingMove() {

        MoveManager moveManager = GameManager.get().getMoveManager();

        for (String move : enemy.getMoves().keySet()) {
            moveManager.addMove(new Move(move, 1, 0));
        }

        EnemyPeon enemyPeon = new EnemyPeon(0,0,0,0,0);

        String move = enemyPeon.chooseMove();
        assertTrue(enemyPeon.getMoves().containsKey(move));
    }

    @Test
    public void notEnemyMove() {
        enemy.setTask(null);
        enemy.onTick(0);
        assertNull(enemy.getTask());
    }

    @Test
    public void notEnemyRoam() {
        EnemyPeon enemyPeon = new EnemyPeon(0,0,1,1,1);
        world.addEntity(enemyPeon);
        enemyPeon.setTask(null);
        enemyPeon.onTick(0);
        assertNull(enemy.getTask());
    }

    @Test
    public void letEnemyRoam() {
        EnemyPeon enemyPeon = new EnemyPeon(0,0,1,1,1);
        TestWorld testWorld = (TestWorld) GameManager.get().getPreviousWorld();
        when(GameManager.getWorld()).thenReturn(testWorld);
        ScreenManager mockScreen = mock(ScreenManager.class);
        when(GameManager.get().getManagerFromInstance(ScreenManager.class)).thenReturn(mockScreen);
        GameScreen mockGameScreen = mock(GameScreen.class);
        when(mockScreen.getCurrentScreen()).thenReturn(mockGameScreen);
        when(mockGameScreen.getCameraDisabled()).thenReturn(false);
        when(GameManager.get().getManager(PathFindingService.class)).thenReturn(mock(PathFindingService.class));
        testWorld.addEntity(enemyPeon);
        enemyPeon.setTask(null);
        enemyPeon.onTick(0);
        assertNull(enemy.getTask());
    }

    @Test
    public void calcDamageFail() {
        MoveManager moveManager = GameManager.get().getMoveManager();
        Move testMove = new Move("test",PlayerStatistics.MANA,1,MoveType.MAGICAL,0);
        moveManager.addMove(testMove);
        assertFalse(moveManager.calcDamage(null,player,enemy));
        assertFalse(moveManager.calcDamage(testMove,null,enemy));
        assertFalse(moveManager.calcDamage(testMove,player,null));
    }

    @Test
    public void unknownStatisticMove() {
        MoveManager moveManager = GameManager.get().getMoveManager();
        Move testMove = new Move("test",PlayerStatistics.MANA,1,MoveType.MAGICAL,0);
        moveManager.addMove(testMove);
        assertTrue(moveManager.calcDamage(testMove,player,enemy));
    }
}
