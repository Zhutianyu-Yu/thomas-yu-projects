package deco2800.thomas.trading;

import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.items.*;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Consumable.ResurrectionScroll;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Equipment.WoodenSword;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Material.Wood;
import deco2800.thomas.items.Orb.Orb;
import deco2800.thomas.items.Other.TeleportScroll;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TradingTest {
    InventoryAbstract bag;

    /** Consumables */
    AbstractItem smallHealthPotion = new HealthPotion("small");
    AbstractItem mediumHealthPotion = new HealthPotion("medium");
    AbstractItem largeHealthPotion = new HealthPotion("large");
    AbstractItem smallManaPotion = new ManaPotion("small");
    AbstractItem mediumManaPotion = new ManaPotion("medium");
    AbstractItem largeManaPotion = new ManaPotion("large");
    AbstractItem resurrectionScroll = new ResurrectionScroll();

    /** Equipment */
    AbstractItem ironArmour = new IronArmour();
    AbstractItem ironSword = new IronSword();
    AbstractItem woodenArmour = new WoodenArmour();
    AbstractItem woodenSword = new WoodenSword();

    /** Materials */
    AbstractItem diamond = new Diamond();
    AbstractItem gold = new Gold();
    AbstractItem iron = new Iron();
    AbstractItem wood = new Wood();

    /** Orb */
    AbstractItem orbViolet = new Orb("Violet");
    AbstractItem orbRed = new Orb("Red");
    AbstractItem orbYellow = new Orb("Yellow");
    AbstractItem orbBlue = new Orb("Blue");
    AbstractItem orbPink = new Orb("Pink");


    /** Other */
    AbstractItem teleportScroll = new TeleportScroll();



    @Before
    public void setInventory(){
        bag = new InventoryAbstract(4,9);
        bag.addInventory(smallHealthPotion);
        bag.addInventory(mediumHealthPotion);
        bag.addInventory(largeHealthPotion);


    }

    @Test
    public void basicSellTest() {
        Assert.assertEquals(100, bag.getTrading().getMoney());
        bag.getTrading().sell(0,0);
        Assert.assertEquals(110, bag.getTrading().getMoney());
        bag.getTrading().sell(0,1);
        Assert.assertEquals(130, bag.getTrading().getMoney());
        bag.getTrading().sell(0,2);
        Assert.assertEquals(160, bag.getTrading().getMoney());
    }


}
