package deco2800.thomas.crafting;

import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Material.Wood;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class RecipeTest {
    private final String ironSwordImage = "Sword_iron.png";
    private final String goldSwordImage = "Sword_gold.png";
    private Recipe ironSword;

    @Before
    public void setUp() throws Exception {
        ironSword = new Recipe("Iron Sword");
        ironSword.setImageAddress(ironSwordImage);
        ironSword.setLevelRequirement(5);
        ironSword.addInput(new Iron(), 4);
        ironSword.addInput(new Wood(), 2);
        ironSword.addOutput(new IronSword(), 1);
    }

    @Test
    public void getInputs() {
        ArrayList<ItemStack> inputs = new ArrayList<>();
        inputs.add(new ItemStack(new Iron(), 4));
        inputs.add(new ItemStack(new Wood(), 2));
        assertEquals(inputs.size(), ironSword.getInputs().size());
        for (int i = 0; i < inputs.size(); i++) {
            AbstractItem actualItem = inputs.get(i).getItem();
            int actualQuantity = inputs.get(i).getQuantity();
            AbstractItem currentItem = ironSword.getInputs().get(i).getItem();
            int currentQuantity = ironSword.getInputs().get(i).getQuantity();
            assertEquals(actualItem, currentItem);
            assertEquals(actualQuantity, currentQuantity);
        }
    }

    @Test
    public void getOutputs() {
        ArrayList<ItemStack> outputs = new ArrayList<>();
        outputs.add(new ItemStack(new IronSword(), 1));
        assertEquals(outputs.size(), ironSword.getOutputs().size());
        for (int i = 0; i < outputs.size(); i++) {
            AbstractItem actualItem = outputs.get(i).getItem();
            int actualQuantity = outputs.get(i).getQuantity();
            AbstractItem currentItem = ironSword.getOutputs().get(i).getItem();
            int currentQuantity = ironSword.getOutputs().get(i).getQuantity();
            assertEquals(actualItem, currentItem);
            assertEquals(actualQuantity, currentQuantity);
        }
    }

    @Test
    public void getAndSetLevelRequirement() {
        assertEquals(5, ironSword.getLevelRequirement());
        ironSword.setLevelRequirement(4);
        assertEquals(4, ironSword.getLevelRequirement());
        ironSword.setLevelRequirement(5);
        assertEquals(5, ironSword.getLevelRequirement());
    }

    @Test
    public void getInventorySpacesNeeded() {
        assertEquals(1, ironSword.getInventorySpacesNeeded());
        ironSword.addOutput(new IronArmour(), 1);
        assertEquals(2, ironSword.getInventorySpacesNeeded());
    }

    @Test
    public void getAndSetImageAddress() {
        assertEquals(ironSwordImage, ironSword.getImageAddress());
        ironSword.setImageAddress(goldSwordImage);
        assertEquals(goldSwordImage, ironSword.getImageAddress());
    }

    @Test
    public void getRecipeName() {
        assertEquals("Iron Sword", ironSword.getRecipeName());
    }

    @Test
    public void testAddInput() {
    }

    @Test
    public void testAddOutput() {
    }

}