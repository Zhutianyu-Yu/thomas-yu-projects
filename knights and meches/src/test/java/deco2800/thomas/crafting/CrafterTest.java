package deco2800.thomas.crafting;

import deco2800.thomas.inventory.InventoryAbstract;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Material.*;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class CrafterTest {

    InventoryAbstract mainInventory;
    ItemCrafter itemCrafter;
    Recipe testRecipe;
    Recipe hpPotion;



    @Before
    public void createInventorys() {
        mainInventory = new InventoryAbstract(5,4);
        for (int i = 1; i < 6; i++) {
            mainInventory.addInventory(new Wood());
            mainInventory.addInventory((new Iron()));
        }
    }

    @Before
    public void createCrafter() {
        itemCrafter = new ItemCrafter(mainInventory);
        testRecipe = new Recipe("Test 1", Recipe.DEFAULT_IMAGE,
                List.of(new ItemStack(new Wood(), 3),
                        new ItemStack(new Iron(), 2)),
                List.of(new ItemStack(new IronSword(), 3),
                        new ItemStack(new WoodenArmour(), 1)));
        itemCrafter.addRecipe(testRecipe);
        hpPotion = new Recipe("Health Potion", Recipe.DEFAULT_IMAGE,
                List.of(new ItemStack(new Wood(), 3),
                        new ItemStack(new Iron(), 1)),
                List.of(new ItemStack(new HealthPotion("small"), 1)));
        itemCrafter.addRecipe(hpPotion);
    }

    @Test
    public void testLists() {
        assertEquals(itemCrafter.getRecipeList(), List.of(testRecipe,
                hpPotion));
    }

    @Test
    public void testNumRecipesHeld() {
        assertEquals(itemCrafter.getNumRecipesHeld(), 2);
    }

    @Test
    public void testCraftability() {
        assertTrue(itemCrafter.isCraftable(testRecipe));
        assertTrue(itemCrafter.isCraftable(hpPotion));
        assertTrue(itemCrafter.craft(hpPotion));
        assertFalse(itemCrafter.isCraftable(hpPotion));
        assertFalse(itemCrafter.isCraftable(testRecipe));
    }

}
