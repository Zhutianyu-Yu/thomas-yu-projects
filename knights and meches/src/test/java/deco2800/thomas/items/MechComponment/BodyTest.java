package deco2800.thomas.items.MechComponment;
import deco2800.thomas.items.MechComponentItem;
import deco2800.thomas.items.MechComponent.Body;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * MechComponent Item Tester
 *
 * @author Zhuke Gao
 * @since <pre>1 Sep 2020</pre>
 * @version 1.0
 */
public class BodyTest {
    MechComponentItem body = new Body();

    @Test
    public void getType() {assertEquals("MechComponentItem", body.getType());
    }

    @Test
    public void getHP() {
        assertEquals(10, body.getHP());
        body.upgrade();
        assertEquals(20, body.getHP());
    }
    @Test
    public void testGetName() {
        assertEquals("Iron Body", body.getItemName());
    }
    @Test
    public void testGetupGradeName(){
        body.upgrade();
        assertEquals("Gold Body", body.getItemName());

    }
    @Test
    public void testGetupGradeid(){
        body.upgrade();
        assertEquals("019", body.getItemIdString());
    }
    @Test
    public void testGetValue() {
        assertEquals(100, body.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("015", body.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals(".", body.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(1, body.getItemSize());
    }


}
