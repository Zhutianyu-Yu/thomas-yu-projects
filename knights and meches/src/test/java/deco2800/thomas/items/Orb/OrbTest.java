package deco2800.thomas.items.Orb;

import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Orb.Orb;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/** 
* Orb Tester. 
* 
* @author Yusen Zhu
* @since <pre>Aug 29, 2020</pre> 
* @version 1.0 
*/
public class OrbTest {

    AbstractItem orb1 = new Orb("Blue");

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetName() {
        assertEquals("Orb", orb1.getItemName());
    }

    @Test
    public void testGetValue() {
        assertEquals(-1, orb1.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("001", orb1.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals("The necessary quest items to win the game.", orb1.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(1, orb1.getItemSize());
    }

    @Test
    public void testGetType() {
        assertEquals("Orb", orb1.getType());
    }

    @Test
    public void testGetImgName() {
        assertEquals("Orb_Blue.png", orb1.getImgName());
    }

    @Test
    public void testColour() {
        assertEquals("Blue", orb1.getOrbColour());
        orb1.setOrbColour("Red");
        assertEquals("Red", orb1.getOrbColour());
    }

    /**
     * Try to test can the Orb class added to AbstractItem class list
     */
    @Test
    public void testClass() {
        ArrayList<AbstractItem> l = new ArrayList<>();
        l.add(orb1);
        assertEquals(orb1, l.get(0));
    }

    @Test
    public void testGetEquipmentType() {
        assertEquals("Orb", orb1.getEquipmentType());
    }

    @Test
    public void testGetDamage() {
        assertEquals(30, orb1.getDamage());
    }

    @Test
    public void testGetArmour() {
        assertEquals(30, orb1.getArmour());
    }

    @Test
    public void testHashCode() throws Exception {
        assertEquals(orb1.getImgName().hashCode() * orb1.getItemIdString().hashCode(), orb1.hashCode());
    }

    @Test
    public void testToString() throws Exception {
        assertEquals(orb1.getImgName() + ":" + orb1.getItemIdString() + ":" + orb1.getItemDescription(), orb1.toString());
    }


} 
