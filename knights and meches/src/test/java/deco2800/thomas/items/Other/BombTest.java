package deco2800.thomas.items.Other;

import deco2800.thomas.items.AbstractItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BombTest {
    AbstractItem bomb = new Bomb();

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetName() {
        assertEquals("Bomb", bomb.getItemName());
    }

    @Test
    public void testGetValue() {
        assertEquals(100, bomb.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("929", bomb.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Pree 'R' to set or detonate a bomb", bomb.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(5, bomb.getItemSize());
    }

    @Test
    public void testGetType() {
        assertEquals("OtherItem", bomb.getType());
    }

    @Test
    public void testGetImgName() {
        assertEquals("Bomb3.png", bomb.getImgName());
    }
}
