package deco2800.thomas.items.Other;

import deco2800.thomas.items.Other.TeleportScroll;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Other Item Tester
 *
 * @author Zhuke Gao
 * @since <pre>1 Sep 2020</pre>
 * @version 1.0
 */
public class OtherTest {
    TeleportScroll otherItem =new TeleportScroll();
    @Test
    public void testGetName() {
        assertEquals("Teleport", otherItem.getItemName());
    }

    @Test
    public void testGetValue() {
        assertEquals(100, otherItem.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("023", otherItem.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Use to teleport", otherItem.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(5, otherItem.getItemSize());
    }

    @Test
    public void testGetType() {
        assertEquals("OtherItem", otherItem.getType());
    }

    @Test
    public void testGetImgName() {
        assertEquals("Teleport_Scroll.png", otherItem.getImgName());
    }


    @Test
    public void testGetEquipmentType() {
        assertEquals("Not Equipment", otherItem.getEquipmentType());
    }


}
