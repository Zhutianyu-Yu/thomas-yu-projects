package deco2800.thomas.items.Consumable;

import deco2800.thomas.entities.PlayerPeon;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 * Consumables Tester
 *
 * @author Yonghao Wu
 * @since <pre>29 Aug 2020</pre>
 * @version 1.0
 */
public class ConsumableTest {
    private HealthPotion A, B, C, X;
    private ManaPotion D, E, F, Y;
    private ResurrectionScroll G, H, Z;
    private PlayerPeon playerPeon;

    @Before
    public void setup() {
        A = new HealthPotion("small");
        B = new HealthPotion("medium");
        C = new HealthPotion("large");
        D = new ManaPotion("small");
        E = new ManaPotion("medium");
        F = new ManaPotion("large");
        G = new ResurrectionScroll();
        H = new ResurrectionScroll();
        X = new HealthPotion("small");
        Y = new ManaPotion("small");
        Z = new ResurrectionScroll();
        playerPeon = new PlayerPeon(1, 1, 1);
    }

    @Test
    public void NameTest() {
        Assert.assertEquals("HealthPotion", A.getItemName());
        Assert.assertEquals("HealthPotion", B.getItemName());
        Assert.assertEquals("HealthPotion", C.getItemName());
        Assert.assertEquals("ManaPotion", D.getItemName());
        Assert.assertEquals("ManaPotion", E.getItemName());
        Assert.assertEquals("ManaPotion", F.getItemName());
        Assert.assertEquals("ResurrectionScroll", G.getItemName());
    }

    @Test
    public void IdTest() {
        Assert.assertEquals("008", A.getItemIdString());
        Assert.assertEquals("008", B.getItemIdString());
        Assert.assertEquals("008", C.getItemIdString());
        Assert.assertEquals("009", D.getItemIdString());
        Assert.assertEquals("009", E.getItemIdString());
        Assert.assertEquals("009", F.getItemIdString());
        Assert.assertEquals("010", G.getItemIdString());
    }

    @Test
    public void ValueTest() {
        Assert.assertEquals(10, A.getItemValue());
        Assert.assertEquals(20, B.getItemValue());
        Assert.assertEquals(30, C.getItemValue());
        Assert.assertEquals(10, D.getItemValue());
        Assert.assertEquals(20, E.getItemValue());
        Assert.assertEquals(30, F.getItemValue());
        Assert.assertEquals(20, G.getItemValue());
    }

    @Test
    public void DescriptionTest() {
        Assert.assertEquals("A Refresher After Battle", A.getItemDescription());
        Assert.assertEquals("A Refresher After Battle", A.getItemDescription());
        Assert.assertEquals("A Refresher After Battle", A.getItemDescription());
        Assert.assertEquals("See The Mana Magic!", D.getItemDescription());
        Assert.assertEquals("See The Mana Magic!", E.getItemDescription());
        Assert.assertEquals("See The Mana Magic!", F.getItemDescription());
        Assert.assertEquals("Scroll to a Different World", G.getItemDescription());
    }

    @Test
    public void SizeTest() {
        Assert.assertEquals(3, A.getItemSize());
        Assert.assertEquals(2, B.getItemSize());
        Assert.assertEquals(1, C.getItemSize());
        Assert.assertEquals(3, D.getItemSize());
        Assert.assertEquals(2, E.getItemSize());
        Assert.assertEquals(1, F.getItemSize());
        Assert.assertEquals(1, G.getItemSize());
    }

    @Test
    public void CapacityTest() {
        Assert.assertEquals("small", A.getCapacity());
        Assert.assertEquals("medium", B.getCapacity());
        Assert.assertEquals("large", C.getCapacity());
        Assert.assertEquals("small", D.getCapacity());
        Assert.assertEquals("medium", E.getCapacity());
        Assert.assertEquals("large", F.getCapacity());
        Assert.assertNull(G.getCapacity());
    }

    @Test
    public void useTest() {
        A.use(playerPeon);
        B.use(playerPeon);
        C.use(playerPeon);
        D.use(playerPeon);
        E.use(playerPeon);
        F.use(playerPeon);
        G.use(playerPeon);
        H.use(playerPeon);
    }

    @Test
    public void HashCodeTest() {
        Assert.assertEquals(A.hashCode(), B.hashCode());
        Assert.assertEquals(D.hashCode(), E.hashCode());
        Assert.assertEquals(G.hashCode(), H.hashCode());
    }

    @Test
    public void ToStringTest() {
        Assert.assertEquals("Small_Health_Potion.png:008:A Refresher After Battle", A.toString());
        Assert.assertEquals("Small_Mana_Potion.png:009:See The Mana Magic!", D.toString());
        Assert.assertEquals("Resurrection_Scroll.png:010:Scroll to a Different World", G.toString());
    }

    @Test
    public void EqualsTest() {
        Assert.assertNotEquals(A, B);
        Assert.assertNotEquals(D, E);
        Assert.assertEquals(A, X);
        Assert.assertEquals(D, Y);
        Assert.assertEquals(G, Z);
    }

    @After
    public void teardown() {
        A = null;
        B = null;
        C = null;
        D = null;
        E = null;
        F = null;
        G = null;
    }
}

