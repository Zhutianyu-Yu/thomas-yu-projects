package deco2800.thomas.items.Material;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 * Materials Tester
 *
 * @author Yonghao Wu
 * @since <pre>3 Sep 2020</pre>
 * @version 1.0
 */
public class MaterialTest {
    private Diamond A, B, C;
    private Gold D, E, F;
    private Iron G, H, I;
    private Wood J, K, L;

    @Before
    public void setup() {
        A = new Diamond();
        B = new Diamond();
        C = new Diamond();
        D = new Gold();
        E = new Gold();
        F = new Gold();
        G = new Iron();
        H = new Iron();
        I = new Iron();
        J = new Wood();
        K = new Wood();
        L = new Wood();
    }

    @Test
    public void NameTest() {
        Assert.assertEquals("Diamond", A.getItemName());
        Assert.assertEquals("Gold", D.getItemName());
        Assert.assertEquals("Iron", G.getItemName());
        Assert.assertEquals("Wood", J.getItemName());
    }

    @Test
    public void IdTest() {
        Assert.assertEquals("014", A.getItemIdString());
        Assert.assertEquals("013", D.getItemIdString());
        Assert.assertEquals("012", G.getItemIdString());
        Assert.assertEquals("011", J.getItemIdString());
    }

    @Test
    public void ValueTest() {
        Assert.assertEquals(70, A.getItemValue());
        Assert.assertEquals(50, D.getItemValue());
        Assert.assertEquals(30, G.getItemValue());
        Assert.assertEquals(10, J.getItemValue());
    }

    @Test
    public void DescriptionTest() {
        Assert.assertEquals("Precious Gem", A.getItemDescription());
        Assert.assertEquals("Valuable Metal", D.getItemDescription());
        Assert.assertEquals("Industrial Good", G.getItemDescription());
        Assert.assertEquals("Nature's Pillar", J.getItemDescription());
    }

    @Test
    public void SizeTest() {
        Assert.assertEquals(3, A.getItemSize());
        Assert.assertEquals(3, B.getItemSize());
        Assert.assertEquals(3, C.getItemSize());
        Assert.assertEquals(3, D.getItemSize());
        Assert.assertEquals(3, E.getItemSize());
        Assert.assertEquals(3, F.getItemSize());
        Assert.assertEquals(3, G.getItemSize());
        Assert.assertEquals(3, H.getItemSize());
        Assert.assertEquals(3, I.getItemSize());
        Assert.assertEquals(3, J.getItemSize());
        Assert.assertEquals(3, K.getItemSize());
        Assert.assertEquals(3, L.getItemSize());
    }

    @Test
    public void AbilityTest() {
        Assert.assertEquals("D", A.getAbility());
        Assert.assertEquals("G", D.getAbility());
        Assert.assertEquals("I", G.getAbility());
        Assert.assertEquals("W", J.getAbility());
    }

    @Test
    public void useTest() {
        A.use();
        B.use();
        C.use();
        D.use();
        E.use();
        F.use();
        G.use();
        H.use();
        I.use();
        J.use();
        K.use();
    }

    @Test
    public void HashCodeTest() {
        Assert.assertEquals(A.hashCode(), B.hashCode());
        Assert.assertEquals(D.hashCode(), E.hashCode());
        Assert.assertEquals(G.hashCode(), H.hashCode());
        Assert.assertEquals(J.hashCode(), K.hashCode());
    }

    @Test
    public void ToStringTest() {
        Assert.assertEquals("Diamond.png:014:Precious Gem", A.toString());
        Assert.assertEquals("Gold.png:013:Valuable Metal", D.toString());
        Assert.assertEquals("Iron.png:012:Industrial Good", G.toString());
        Assert.assertEquals("Wood.png:011:Nature's Pillar", J.toString());
    }

    @After
    public void teardown() {
        A = null;
        B = null;
        C = null;
        D = null;
        E = null;
        F = null;
        G = null;
        H = null;
        I = null;
        J = null;
        K = null;
        L = null;
    }
}

