package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.AbstractItem;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.assertEquals;

/**
 * Equipment Item Tester
 *
 * @author Yusen Zhu
 * @since <pre>1 Sep 2020</pre>
 * @version 1.0
 */
public class WoodenArmourTest {

    AbstractItem armour1 = WoodenArmour.createWoodenArmour();

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetName() {
        assertEquals("Wooden Armour", armour1.getItemName());
    }

    @Test
    public void testGetValue() {
        assertEquals(10, armour1.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("004", armour1.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals("The strong never need armour, but luckily you are not one of them.", armour1.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(1, armour1.getItemSize());
    }

    @Test
    public void testGetType() {
        assertEquals("EquipmentItem", armour1.getType());
    }

    @Test
    public void testGetDamage() {
        assertEquals(0, armour1.getDamage());
    }

    @Test
    public void testGetArmour() {
        assertEquals(3, armour1.getArmour());
    }

    @Test
    public void testGetEquipmentType() {
        assertEquals("Armour", armour1.getEquipmentType());
    }

    @Test
    public void testHashCode(){
        assertEquals(armour1.getImgName().hashCode() * armour1.getItemIdString().hashCode(), armour1.hashCode());
    }

    @Test
    public void testToString(){
        assertEquals(armour1.getImgName() + ":" + armour1.getItemIdString() + ":" + armour1.getItemDescription(), armour1.toString());
    }
}
