package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.EquipmentItem;
import deco2800.thomas.items.Equipment.WoodenSword;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.assertEquals;

/**
 * Equipment Item Tester
 *
 * @author Yusen Zhu
 * @since <pre>1 Sep 2020</pre>
 * @version 1.0
 */
public class WoodenSwordTest {

    WoodenSword sword1 = new WoodenSword();

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetName() {
        assertEquals("Wooden Sword", sword1.getItemName());
    }

    @Test
    public void testGetValue() {
        assertEquals(10, sword1.getItemValue());
    }


    @Test
    public void testGetId() {
        assertEquals("002", sword1.getItemIdString());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Every legendary adventure begins with a wooden sword.", sword1.getItemDescription());
    }

    @Test
    public void testGetSize() {
        assertEquals(3, sword1.getItemSize());
    }

    @Test
    public void testGetType() {
        assertEquals("EquipmentItem", sword1.getType());
    }

    @Test
    public void testGetDamage() {
        assertEquals(3, sword1.getDamage());
    }

    @Test
    public void testGetArmour() {
        assertEquals(0, sword1.getArmour());
    }

    @Test
    public void testGetEquipmentType() {
        assertEquals("Weapon", sword1.getEquipmentType());
    }

    @Test
    public void testHashCode(){
        assertEquals(sword1.getImgName().hashCode() * sword1.getItemIdString().hashCode(), sword1.hashCode());
    }

    @Test
    public void testToString(){
        assertEquals(sword1.getImgName() + ":" + sword1.getItemIdString() + ":" + sword1.getItemDescription(), sword1.toString());
    }
}
