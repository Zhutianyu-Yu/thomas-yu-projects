package deco2800.thomas.items.Equipment;

import deco2800.thomas.items.AbstractItem;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import static org.junit.Assert.assertEquals;

/** 
* IronSword Tester. 
* 
* @author <Authors name> 
* @since <pre>Sep 20, 2020</pre> 
* @version 1.0 
*/ 
public class IronSwordTest {

    AbstractItem i = new IronSword();

@Before
public void before() throws Exception {

} 

@After
public void after() throws Exception {

} 

/** 
* 
* Method: getEquipmentType() 
* 
*/ 
@Test
public void testGetEquipmentType() throws Exception {
    assertEquals("Weapon", i.getEquipmentType());
} 

/** 
* 
* Method: hashCode() 
* 
*/ 
@Test
public void testHashCode() throws Exception {
    assertEquals(i.getImgName().hashCode() * i.getItemIdString().hashCode(), i.hashCode());
} 

/** 
* 
* Method: toString() 
* 
*/ 
@Test
public void testToString() throws Exception {
    assertEquals(i.getImgName() + ":" + i.getItemIdString() + ":" + i.getItemDescription(), i.toString());
}
} 
