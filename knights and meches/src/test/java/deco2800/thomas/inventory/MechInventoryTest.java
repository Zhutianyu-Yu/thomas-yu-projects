package deco2800.thomas.inventory;

import deco2800.thomas.managers.GameManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

public class MechInventoryTest {
    InventoryAbstract inventory;
    MechInventory mech;


    @Before
    public void setInitial(){
        inventory= new InventoryAbstract(4,9);
        inventory.createDummyInventory();
        mech= new MechInventory(inventory);

    }

    @Test
    public void changeKnightStatusTest(){
        mech.changeKnightStatus(1);
        Assert.assertEquals(true,mech.getKnightStatus(1));
        mech.changeKnightStatus(2);
        Assert.assertEquals(true,mech.getKnightStatus(2));
        mech.changeKnightStatus(3);
        Assert.assertEquals(true,mech.getKnightStatus(3));
    }

    @Test
    public void changeWizardStatusTest(){
        mech.changeWizardStatus(1);
        Assert.assertEquals(true,mech.getWizardStatus(1));
        mech.changeWizardStatus(2);
        Assert.assertEquals(true,mech.getWizardStatus(2));
        mech.changeWizardStatus(3);
        Assert.assertEquals(true,mech.getWizardStatus(3));
    }

    @Test
    public void activeKnightTest(){
        mech.activeKnight(1);
        Assert.assertEquals(1,mech.getActiveKnight());
        mech.activeKnight(2);
        Assert.assertEquals(2,mech.getActiveKnight());
        mech.activeKnight(1);
        mech.activeKnight(2);
        Assert.assertEquals(2,mech.getActiveKnight());
    }

    @Test
    public void activeWizardTest(){
        mech.activeWizard(1);
        Assert.assertEquals(1,mech.getActiveWizard());
        mech.activeWizard(2);
        Assert.assertEquals(2,mech.getActiveWizard());
        mech.activeWizard(2);
        mech.activeWizard(1);
        Assert.assertEquals(1,mech.getActiveWizard());
    }


}

