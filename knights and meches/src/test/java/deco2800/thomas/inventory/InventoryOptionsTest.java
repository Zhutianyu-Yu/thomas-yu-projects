package deco2800.thomas.inventory;

import deco2800.thomas.items.AbstractItem;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Consumable.ResurrectionScroll;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Equipment.WoodenSword;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Material.Wood;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Pattern;

public class InventoryOptionsTest {

    InventoryAbstract bag;
    InventoryOptionsMenu option;

    /** Consumables */
    AbstractItem smallHealthPotion = new HealthPotion("small");
    AbstractItem mediumHealthPotion = new HealthPotion("medium");
    AbstractItem largeHealthPotion = new HealthPotion("large");
    AbstractItem smallManaPotion = new ManaPotion("small");
    AbstractItem mediumManaPotion = new ManaPotion("medium");
    AbstractItem largeManaPotion = new ManaPotion("large");
    AbstractItem resurrectionScroll = new ResurrectionScroll();

    /** Equipment */
    AbstractItem ironArmour = new IronArmour();
    AbstractItem ironSword = new IronSword();
    AbstractItem woodenArmour = new WoodenArmour();
    AbstractItem woodenSword = new WoodenSword();

    /** Materials */
    AbstractItem diamond = new Diamond();
    AbstractItem gold = new Gold();
    AbstractItem iron = new Iron();
    AbstractItem wood = new Wood();

    @Before
    public void setOption() {
        bag = new InventoryAbstract(4,9);
    }

    @Test
    public void checkConsumable() {
        InventoryOptionsMenu menu = createInventoryOptionsMenu(this.smallHealthPotion);
        Assert.assertEquals(1, menu.getButtonOneFunction());
        Assert.assertEquals(2, menu.getButtonTwoFunction());
    }

    @Test
    public void checkMaterial() {
        InventoryOptionsMenu menu = createInventoryOptionsMenu(this.diamond);
        Assert.assertEquals(1, menu.getButtonOneFunction());
        Assert.assertEquals(4, menu.getButtonTwoFunction());
    }

    @Test
    public void checkArmour() {
        InventoryOptionsMenu menu = createInventoryOptionsMenu(this.ironArmour);
        Assert.assertEquals(1, menu.getButtonOneFunction());
        Assert.assertEquals(6, menu.getButtonTwoFunction());
    }

    @Test
    public void checkWeapon() {
        InventoryOptionsMenu menu = createInventoryOptionsMenu(this.ironSword);
        Assert.assertEquals(1, menu.getButtonOneFunction());
        Assert.assertEquals(3, menu.getButtonTwoFunction());
    }

    public InventoryOptionsMenu createInventoryOptionsMenu(AbstractItem item) {
        this.bag.addInventory(item);
        int xPos;
        int yPos;
        try {
            xPos = this.bag.findInInventory(item, 1)[0];
            yPos = this.bag.findInInventory(item, 1)[1];
        } catch (Exception e) {
            assert false;
            return null;
        }
        InventoryOptionsMenu menu = new InventoryOptionsMenu(item, 0, 0, this.bag.getInventoryQuantity(xPos, yPos), xPos, yPos);
        return menu;
    }

    class InventoryOptionsMenu {
        private AbstractItem item;
        private float x;
        private float y;
        private float width = 200;

        private int xCoord;
        private int yCoord;

        private int itemQuantity;

        /** 0 =  Error, 1 = drop, 2 = eat, 3 = equip weapon, 4 = equip material, 5 = equip mech component, 6 = equip armour */
        private int buttonOneFunction;
        private int buttonTwoFunction;

        public InventoryOptionsMenu(AbstractItem item, float x, float y,
            int itemQuantity, int xPosition, int yPosition){
            this.item = item;
            this.itemQuantity = itemQuantity;
            this.x = x + 30;
            this.y = y + 30 + 70;
            this.xCoord = xPosition;
            this.yCoord = yPosition;
            buttonFunctionSetup();
            this.y -= 70;
        }

        private void buttonFunctionSetup() {
            String[] parts = this.item.getClass().getName().split(Pattern.quote("."));
            switch (parts[3]) {
                case "Consumable":
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 2;
                    break;
                case "Equipment":
                    if (this.item instanceof  IronArmour || this.item instanceof WoodenArmour) {
                        this.buttonOneFunction = 1;
                        this.buttonTwoFunction = 6;
                    }  else if (this.item instanceof  IronSword || this.item instanceof WoodenSword) {
                        this.buttonOneFunction = 1;
                        this.buttonTwoFunction = 3;
                    }
                    break;
                case "Material":
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 4;
                    break;
                case "mechComponent":
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 5;
                    break;
                case "Orb":
                    this.buttonOneFunction = 1;
                    this.buttonTwoFunction = 0;
                default:
                    this.buttonOneFunction = 0;
                    this.buttonTwoFunction = 0;
                    break;
            }
        }

        public int getButtonOneFunction() {
            return this.buttonOneFunction;
        }

        public int getButtonTwoFunction() {
            return this.buttonTwoFunction;
        }
    }
}
