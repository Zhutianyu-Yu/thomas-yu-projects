package deco2800.thomas.inventory;


import deco2800.thomas.items.*;
import deco2800.thomas.items.Consumable.HealthPotion;
import deco2800.thomas.items.Consumable.ManaPotion;
import deco2800.thomas.items.Consumable.ResurrectionScroll;
import deco2800.thomas.items.Equipment.IronArmour;
import deco2800.thomas.items.Equipment.IronSword;
import deco2800.thomas.items.Equipment.WoodenArmour;
import deco2800.thomas.items.Equipment.WoodenSword;
import deco2800.thomas.items.Material.Diamond;
import deco2800.thomas.items.Material.Gold;
import deco2800.thomas.items.Material.Iron;
import deco2800.thomas.items.Material.Wood;
import deco2800.thomas.items.Orb.Orb;
import deco2800.thomas.items.Other.TeleportScroll;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InventoryTest {
    InventoryAbstract bag;

    /** Consumables */
    AbstractItem smallHealthPotion = new HealthPotion("small");
    AbstractItem mediumHealthPotion = new HealthPotion("medium");
    AbstractItem largeHealthPotion = new HealthPotion("large");
    AbstractItem smallManaPotion = new ManaPotion("small");
    AbstractItem mediumManaPotion = new ManaPotion("medium");
    AbstractItem largeManaPotion = new ManaPotion("large");
    AbstractItem resurrectionScroll = new ResurrectionScroll();

    /** Equipment */
    AbstractItem ironArmour = new IronArmour();
    AbstractItem ironSword = new IronSword();
    AbstractItem woodenArmour = new WoodenArmour();
    AbstractItem woodenSword = new WoodenSword();

    /** Materials */
    AbstractItem diamond = new Diamond();
    AbstractItem gold = new Gold();
    AbstractItem iron = new Iron();
    AbstractItem wood = new Wood();

    /** Orb */
    AbstractItem orbViolet = new Orb("Violet");
    AbstractItem orbRed = new Orb("Red");
    AbstractItem orbYellow = new Orb("Yellow");
    AbstractItem orbBlue = new Orb("Blue");
    AbstractItem orbPink = new Orb("Pink");


    /** Other */
    AbstractItem teleportScroll = new TeleportScroll();

    @Before
    public void setInventory(){
        bag = new InventoryAbstract(4,9);
    }

    @Test
    public void sizeTest(){
        Assert.assertEquals(4,bag.getColumnSize());
        Assert.assertEquals(9,bag.getRowSize());
    }

    @Test
    public void addConsumables(){
        System.out.println(smallHealthPotion.getItemName());
        System.out.println(mediumHealthPotion.getItemName());
        System.out.println(largeHealthPotion.getItemName());

        System.out.println(((ConsumableItem)smallHealthPotion).getCapacity());
        System.out.println(((ConsumableItem)mediumHealthPotion).getCapacity());
        System.out.println(((ConsumableItem)largeHealthPotion).getCapacity());

        bag.addInventory(smallHealthPotion);
        Assert.assertEquals(smallHealthPotion, bag.getInventoryArray()[0][0]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,0));
        Assert.assertTrue(bag.getInventoryArray()[0][0] instanceof ConsumableItem);

        bag.addInventory(mediumHealthPotion);
        Assert.assertEquals(mediumHealthPotion, bag.getInventoryArray()[0][1]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,1));
        Assert.assertTrue(bag.getInventoryArray()[0][1] instanceof ConsumableItem);

        bag.addInventory(largeHealthPotion);
        Assert.assertEquals(largeHealthPotion, bag.getInventoryArray()[0][2]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,2));
        Assert.assertTrue(bag.getInventoryArray()[0][2] instanceof ConsumableItem);

        bag.addInventory(smallManaPotion);
        Assert.assertEquals(smallManaPotion, bag.getInventoryArray()[0][3]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,3));
        Assert.assertTrue(bag.getInventoryArray()[0][3] instanceof ConsumableItem);

        bag.addInventory(mediumManaPotion);
        Assert.assertEquals(mediumManaPotion, bag.getInventoryArray()[0][4]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,4));
        Assert.assertTrue(bag.getInventoryArray()[0][4] instanceof ConsumableItem);

        bag.addInventory(largeManaPotion);
        Assert.assertEquals(largeManaPotion, bag.getInventoryArray()[0][5]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,5));
        Assert.assertTrue(bag.getInventoryArray()[0][5] instanceof ConsumableItem);

        bag.addInventory(resurrectionScroll);
        Assert.assertEquals(resurrectionScroll, bag.getInventoryArray()[0][6]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,6));
        Assert.assertTrue(bag.getInventoryArray()[0][6] instanceof ConsumableItem);
    }


    @Test
    public void addEquipment() {
        bag.addInventory(ironArmour);
        Assert.assertEquals(ironArmour, bag.getInventoryArray()[0][0]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 0));
        Assert.assertTrue(bag.getInventoryArray()[0][0] instanceof EquipmentItem);

        bag.addInventory(ironSword);
        Assert.assertEquals(ironSword, bag.getInventoryArray()[0][1]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 1));
        Assert.assertTrue(bag.getInventoryArray()[0][1] instanceof EquipmentItem);

        bag.addInventory(woodenArmour);
        Assert.assertEquals(woodenArmour, bag.getInventoryArray()[0][2]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 2));
        Assert.assertTrue(bag.getInventoryArray()[0][2] instanceof EquipmentItem);

        bag.addInventory(woodenSword);
        Assert.assertEquals(woodenSword, bag.getInventoryArray()[0][3]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 3));
        Assert.assertTrue(bag.getInventoryArray()[0][3] instanceof EquipmentItem);
    }

    @Test
    public void addMaterials() {
        bag.addInventory(diamond);
        Assert.assertEquals(diamond, bag.getInventoryArray()[0][0]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 0));
        Assert.assertTrue(bag.getInventoryArray()[0][0] instanceof MaterialItem);

        bag.addInventory(gold);
        Assert.assertEquals(gold, bag.getInventoryArray()[0][1]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 1));
        Assert.assertTrue(bag.getInventoryArray()[0][1] instanceof MaterialItem);

        bag.addInventory(iron);
        Assert.assertEquals(iron, bag.getInventoryArray()[0][2]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 2));
        Assert.assertTrue(bag.getInventoryArray()[0][2] instanceof MaterialItem);

        bag.addInventory(wood);
        Assert.assertEquals(wood, bag.getInventoryArray()[0][3]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 3));
        Assert.assertTrue(bag.getInventoryArray()[0][3] instanceof MaterialItem);
    }

    @Test
    public void addOrb() {
        bag.addInventory(orbViolet);
        Assert.assertEquals(orbViolet, bag.getInventoryArray()[0][0]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,0));
        Assert.assertTrue(bag.getInventoryArray()[0][0] instanceof QuestRelatedItem);
        Assert.assertEquals(bag.getInventoryArray()[0][0].getOrbColour(), "Violet");

        bag.addInventory(orbRed);
        Assert.assertEquals(orbRed, bag.getInventoryArray()[0][1]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,1));
        Assert.assertTrue(bag.getInventoryArray()[0][1] instanceof QuestRelatedItem);
        Assert.assertEquals(bag.getInventoryArray()[0][1].getOrbColour(), "Red");

        bag.addInventory(orbYellow);
        Assert.assertEquals(orbYellow, bag.getInventoryArray()[0][2]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,2));
        Assert.assertTrue(bag.getInventoryArray()[0][2] instanceof QuestRelatedItem);
        Assert.assertEquals(bag.getInventoryArray()[0][2].getOrbColour(), "Yellow");

        bag.addInventory(orbBlue);
        Assert.assertEquals(orbBlue, bag.getInventoryArray()[0][3]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,3));
        Assert.assertTrue(bag.getInventoryArray()[0][3] instanceof QuestRelatedItem);
        Assert.assertEquals(bag.getInventoryArray()[0][3].getOrbColour(), "Blue");

        bag.addInventory(orbPink);
        Assert.assertEquals(orbPink, bag.getInventoryArray()[0][4]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0,4));
        Assert.assertTrue(bag.getInventoryArray()[0][4] instanceof QuestRelatedItem);
        Assert.assertEquals(bag.getInventoryArray()[0][4].getOrbColour(), "Pink");
    }

    @Test
    public void addOther() {
        bag.addInventory(teleportScroll);
        Assert.assertEquals(teleportScroll, bag.getInventoryArray()[0][0]);
        Assert.assertEquals(1, bag.getInventoryQuantity(0, 0));
        Assert.assertTrue(bag.getInventoryArray()[0][0] instanceof OtherItem);
    }


}
