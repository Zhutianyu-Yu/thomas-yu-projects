<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Tundra-tileset" tilewidth="320" tileheight="320" tilecount="33" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="320" height="320" source="dungeon-tile 1.png"/>
 </tile>
 <tile id="1">
  <image width="320" height="320" source="dungeon-tile 2.png"/>
 </tile>
 <tile id="2">
  <image width="320" height="320" source="dungeon-tower-lower.png"/>
 </tile>
 <tile id="3">
  <image width="320" height="320" source="dungeon-tower-upper.png"/>
 </tile>
 <tile id="4">
  <image width="320" height="320" source="floor-tile.png"/>
 </tile>
 <tile id="5">
  <image width="320" height="320" source="floor-tile2.png"/>
 </tile>
 <tile id="6">
  <image width="320" height="320" source="ice-tile.png"/>
 </tile>
 <tile id="7">
  <image width="320" height="320" source="icicle-tile.png"/>
 </tile>
 <tile id="8">
  <image width="320" height="320" source="wall-tile.png"/>
 </tile>
 <tile id="9">
  <image width="320" height="320" source="0-1.png"/>
 </tile>
 <tile id="10">
  <image width="320" height="320" source="0-2.png"/>
 </tile>
 <tile id="11">
  <image width="320" height="320" source="0-3.png"/>
 </tile>
 <tile id="12">
  <image width="320" height="320" source="0-4.png"/>
 </tile>
 <tile id="13">
  <image width="320" height="320" source="2-1.png"/>
 </tile>
 <tile id="14">
  <image width="320" height="320" source="2-2.png"/>
 </tile>
 <tile id="15">
  <image width="320" height="320" source="2-3.png"/>
 </tile>
 <tile id="16">
  <image width="320" height="320" source="2-4.png"/>
 </tile>
 <tile id="17">
  <image width="320" height="320" source="3-1.png"/>
 </tile>
 <tile id="18">
  <image width="320" height="320" source="3-2.png"/>
 </tile>
 <tile id="19">
  <image width="320" height="320" source="3-3.png"/>
 </tile>
 <tile id="20">
  <image width="320" height="320" source="3-4.png"/>
 </tile>
 <tile id="21">
  <image width="320" height="320" source="4-1.png"/>
 </tile>
 <tile id="22">
  <image width="320" height="320" source="4-2.png"/>
 </tile>
 <tile id="23">
  <image width="320" height="320" source="4-3.png"/>
 </tile>
 <tile id="24">
  <image width="320" height="320" source="4-4.png"/>
 </tile>
 <tile id="25">
  <image width="320" height="320" source="5-1.png"/>
 </tile>
 <tile id="26">
  <image width="320" height="320" source="5-2.png"/>
 </tile>
 <tile id="27">
  <image width="320" height="320" source="5-3.png"/>
 </tile>
 <tile id="28">
  <image width="320" height="320" source="5-4.png"/>
 </tile>
 <tile id="29">
  <image width="320" height="320" source="6-1.png"/>
 </tile>
 <tile id="30">
  <image width="320" height="320" source="6-2.png"/>
 </tile>
 <tile id="31">
  <image width="320" height="320" source="6-3.png"/>
 </tile>
 <tile id="32">
  <image width="320" height="320" source="6-4.png"/>
 </tile>
</tileset>
