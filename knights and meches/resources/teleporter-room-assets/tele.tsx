<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="tele" tilewidth="320" tileheight="320" tilecount="50" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="320" height="320" source="blue-tele-1.png"/>
 </tile>
 <tile id="1">
  <image width="320" height="320" source="blue-tele-2.png"/>
 </tile>
 <tile id="2">
  <image width="320" height="320" source="blue-tele-3.png"/>
 </tile>
 <tile id="3">
  <image width="320" height="320" source="blue-tele-4.png"/>
 </tile>
 <tile id="4">
  <image width="320" height="320" source="desk-1.png"/>
 </tile>
 <tile id="5">
  <image width="320" height="320" source="desk-2.png"/>
 </tile>
 <tile id="6">
  <image width="320" height="320" source="desk-3.png"/>
 </tile>
 <tile id="7">
  <image width="320" height="320" source="desk-4.png"/>
 </tile>
 <tile id="8">
  <image width="320" height="320" source="desk-5.png"/>
 </tile>
 <tile id="9">
  <image width="320" height="320" source="desk-6.png"/>
 </tile>
 <tile id="10">
  <image width="320" height="320" source="door-1.png"/>
 </tile>
 <tile id="11">
  <image width="320" height="320" source="door-2.png"/>
 </tile>
 <tile id="12">
  <image width="320" height="320" source="door-3.png"/>
 </tile>
 <tile id="13">
  <image width="320" height="320" source="door-4.png"/>
 </tile>
 <tile id="14">
  <image width="320" height="320" source="floor-generic.png"/>
 </tile>
 <tile id="15">
  <image width="320" height="320" source="green-tele-1.png"/>
 </tile>
 <tile id="16">
  <image width="320" height="320" source="green-tele-2.png"/>
 </tile>
 <tile id="17">
  <image width="320" height="320" source="green-tele-3.png"/>
 </tile>
 <tile id="18">
  <image width="320" height="320" source="green-tele-4.png"/>
 </tile>
 <tile id="19">
  <image width="320" height="320" source="grey-purple-wire.png"/>
 </tile>
 <tile id="20">
  <image width="320" height="320" source="grey-purple-wire-2.png"/>
 </tile>
 <tile id="21">
  <image width="320" height="320" source="grey-wire-corner-NW.png"/>
 </tile>
 <tile id="22">
  <image width="320" height="320" source="grey-wire-corner-SW.png"/>
 </tile>
 <tile id="23">
  <image width="320" height="320" source="grey-wire-left.png"/>
 </tile>
 <tile id="24">
  <image width="320" height="320" source="grey-wire-right.png"/>
 </tile>
 <tile id="25">
  <image width="320" height="320" source="grey-wire-top.png"/>
 </tile>
 <tile id="26">
  <image width="320" height="320" source="mainframe-1.png"/>
 </tile>
 <tile id="27">
  <image width="320" height="320" source="mainframe-2.png"/>
 </tile>
 <tile id="28">
  <image width="320" height="320" source="mainframe-3.png"/>
 </tile>
 <tile id="29">
  <image width="320" height="320" source="pipe-up-1.png"/>
 </tile>
 <tile id="30">
  <image width="320" height="320" source="pipe-up-2.png"/>
 </tile>
 <tile id="31">
  <image width="320" height="320" source="pipe-up-wire-1.png"/>
 </tile>
 <tile id="32">
  <image width="320" height="320" source="pipe-up-wire-2.png"/>
 </tile>
 <tile id="33">
  <image width="320" height="320" source="pipe-wire-1.png"/>
 </tile>
 <tile id="34">
  <image width="320" height="320" source="pipe-wire-2.png"/>
 </tile>
 <tile id="35">
  <image width="320" height="320" source="purple-tele-1.png"/>
 </tile>
 <tile id="36">
  <image width="320" height="320" source="purple-tele-2.png"/>
 </tile>
 <tile id="37">
  <image width="320" height="320" source="purple-tele-3.png"/>
 </tile>
 <tile id="38">
  <image width="320" height="320" source="purple-tele-4.png"/>
 </tile>
 <tile id="39">
  <image width="320" height="320" source="purple-wire-middle.png"/>
 </tile>
 <tile id="40">
  <image width="320" height="320" source="purple-wire-up.png"/>
 </tile>
 <tile id="41">
  <image width="320" height="320" source="red-tele-1.png"/>
 </tile>
 <tile id="42">
  <image width="320" height="320" source="red-tele-2.png"/>
 </tile>
 <tile id="43">
  <image width="320" height="320" source="red-tele-3.png"/>
 </tile>
 <tile id="44">
  <image width="320" height="320" source="red-tele-4.png"/>
 </tile>
 <tile id="45">
  <image width="320" height="320" source="wall-generic.png"/>
 </tile>
 <tile id="46">
  <image width="320" height="320" source="wire-angle-1.png"/>
 </tile>
 <tile id="47">
  <image width="320" height="320" source="wire-angle-2.png"/>
 </tile>
 <tile id="48">
  <image width="320" height="320" source="wire-angle-3.png"/>
 </tile>
 <tile id="49">
  <image width="320" height="320" source="wire-angle-4.png"/>
 </tile>
</tileset>
