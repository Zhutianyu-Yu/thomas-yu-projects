<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="hubworld tiles" tilewidth="3840" tileheight="3840" tilecount="295" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="39">
  <image width="320" height="320" source="crossroad-NE.png"/>
 </tile>
 <tile id="40">
  <image width="320" height="320" source="crossroad-NW.png"/>
 </tile>
 <tile id="41">
  <image width="320" height="320" source="crossroad-SE.png"/>
 </tile>
 <tile id="42">
  <image width="320" height="320" source="crossroad-SW.png"/>
 </tile>
 <tile id="43">
  <image width="320" height="320" source="curb-NE.png"/>
 </tile>
 <tile id="44">
  <image width="320" height="320" source="curb-NW.png"/>
 </tile>
 <tile id="45">
  <image width="320" height="320" source="curb-SE.png"/>
 </tile>
 <tile id="46">
  <image width="320" height="320" source="curb-side.png"/>
 </tile>
 <tile id="47">
  <image width="320" height="320" source="curb-SW.png"/>
 </tile>
 <tile id="48">
  <image width="320" height="320" source="curb-up.png"/>
 </tile>
 <tile id="49">
  <image width="320" height="320" source="gate-1-1.png"/>
 </tile>
 <tile id="50">
  <image width="320" height="320" source="gate-1-2.png"/>
 </tile>
 <tile id="51">
  <image width="320" height="320" source="gate-1-3.png"/>
 </tile>
 <tile id="52">
  <image width="320" height="320" source="gate-1-4.png"/>
 </tile>
 <tile id="53">
  <image width="320" height="320" source="gate-1-5.png"/>
 </tile>
 <tile id="54">
  <image width="320" height="320" source="gate-1-6.png"/>
 </tile>
 <tile id="55">
  <image width="320" height="320" source="gate-2-1.png"/>
 </tile>
 <tile id="56">
  <image width="320" height="320" source="gate-2-2.png"/>
 </tile>
 <tile id="57">
  <image width="320" height="320" source="gate-2-3.png"/>
 </tile>
 <tile id="58">
  <image width="320" height="320" source="gate-2-4.png"/>
 </tile>
 <tile id="59">
  <image width="320" height="320" source="gate-2-5.png"/>
 </tile>
 <tile id="60">
  <image width="320" height="320" source="gate-2-6.png"/>
 </tile>
 <tile id="61">
  <image width="320" height="320" source="gate-3-1.png"/>
 </tile>
 <tile id="62">
  <image width="320" height="320" source="gate-3-2.png"/>
 </tile>
 <tile id="63">
  <image width="320" height="320" source="gate-3-3.png"/>
 </tile>
 <tile id="64">
  <image width="320" height="320" source="gate-3-4.png"/>
 </tile>
 <tile id="65">
  <image width="320" height="320" source="gate-3-5.png"/>
 </tile>
 <tile id="66">
  <image width="320" height="320" source="gate-3-6.png"/>
 </tile>
 <tile id="67">
  <image width="1280" height="1280" source="hubworld-base.png"/>
 </tile>
 <tile id="68">
  <image width="320" height="320" source="rear-wall.png"/>
 </tile>
 <tile id="69">
  <image width="320" height="320" source="road-bottom.png"/>
 </tile>
 <tile id="70">
  <image width="320" height="320" source="road-left.png"/>
 </tile>
 <tile id="71">
  <image width="320" height="320" source="road-right.png"/>
 </tile>
 <tile id="72">
  <image width="320" height="320" source="road-top.png"/>
 </tile>
 <tile id="73">
  <image width="320" height="320" source="roof-generic-left.png"/>
 </tile>
 <tile id="74">
  <image width="320" height="320" source="roof-generic-right.png"/>
 </tile>
 <tile id="75">
  <image width="320" height="320" source="roof-generic-triangle-bottom.png"/>
 </tile>
 <tile id="76">
  <image width="320" height="320" source="roof-generic-triangle-top.png"/>
 </tile>
 <tile id="77">
  <image width="320" height="320" source="window-generic.png"/>
 </tile>
 <tile id="78">
  <image width="320" height="320" source="Gus assets/back1-roof1.png"/>
 </tile>
 <tile id="79">
  <image width="320" height="320" source="Gus assets/back1-roof2.png"/>
 </tile>
 <tile id="80">
  <image width="320" height="320" source="Gus assets/back1-roof3.png"/>
 </tile>
 <tile id="81">
  <image width="320" height="320" source="Gus assets/back1-topleft.png"/>
 </tile>
 <tile id="82">
  <image width="320" height="320" source="Gus assets/back1-topmid.png"/>
 </tile>
 <tile id="83">
  <image width="320" height="320" source="Gus assets/back1-topright.png"/>
 </tile>
 <tile id="84">
  <image width="320" height="320" source="Gus assets/back1-wallleft.png"/>
 </tile>
 <tile id="85">
  <image width="320" height="320" source="Gus assets/back1-wallmid.png"/>
 </tile>
 <tile id="86">
  <image width="320" height="320" source="Gus assets/back1-wallright.png"/>
 </tile>
 <tile id="87">
  <image width="320" height="320" source="Gus assets/back1-windowleft.png"/>
 </tile>
 <tile id="88">
  <image width="320" height="320" source="Gus assets/back1-windowmid.png"/>
 </tile>
 <tile id="89">
  <image width="320" height="320" source="Gus assets/back1-windowright.png"/>
 </tile>
 <tile id="90">
  <image width="320" height="320" source="Gus assets/back2-lightleft.png"/>
 </tile>
 <tile id="91">
  <image width="320" height="320" source="Gus assets/back2-lightmid.png"/>
 </tile>
 <tile id="92">
  <image width="320" height="320" source="Gus assets/back2-roof1.png"/>
 </tile>
 <tile id="93">
  <image width="320" height="320" source="Gus assets/back2-roof2.png"/>
 </tile>
 <tile id="94">
  <image width="320" height="320" source="Gus assets/back2-roof3.png"/>
 </tile>
 <tile id="95">
  <image width="320" height="320" source="Gus assets/back2-roof4.png"/>
 </tile>
 <tile id="96">
  <image width="320" height="320" source="Gus assets/back2-roof5.png"/>
 </tile>
 <tile id="97">
  <image width="320" height="320" source="Gus assets/back2-roof6.png"/>
 </tile>
 <tile id="98">
  <image width="320" height="320" source="Gus assets/back2-wall-left.png"/>
 </tile>
 <tile id="99">
  <image width="320" height="320" source="Gus assets/back2-wallmid.png"/>
 </tile>
 <tile id="100">
  <image width="320" height="320" source="Gus assets/back2-wallright.png"/>
 </tile>
 <tile id="101">
  <image width="320" height="320" source="Gus assets/back3-roof1.png"/>
 </tile>
 <tile id="102">
  <image width="320" height="320" source="Gus assets/back3-roof2.png"/>
 </tile>
 <tile id="103">
  <image width="320" height="320" source="Gus assets/back3-roof3.png"/>
 </tile>
 <tile id="104">
  <image width="320" height="320" source="Gus assets/back3-windowleft.png"/>
 </tile>
 <tile id="105">
  <image width="320" height="320" source="Gus assets/back3-windowmid.png"/>
 </tile>
 <tile id="106">
  <image width="320" height="320" source="Gus assets/back3-windowright.png"/>
 </tile>
 <tile id="107">
  <image width="320" height="320" source="Gus assets/back4-roof1.png"/>
 </tile>
 <tile id="108">
  <image width="320" height="320" source="Gus assets/back4-roof2.png"/>
 </tile>
 <tile id="109">
  <image width="320" height="320" source="Gus assets/back4-roof3.png"/>
 </tile>
 <tile id="110">
  <image width="320" height="320" source="Gus assets/back4-windowleft.png"/>
 </tile>
 <tile id="111">
  <image width="320" height="320" source="Gus assets/back4-windowmid.png"/>
 </tile>
 <tile id="112">
  <image width="320" height="320" source="Gus assets/back4-windowright.png"/>
 </tile>
 <tile id="113">
  <image width="320" height="320" source="Gus assets/crafts-doorleft.png"/>
 </tile>
 <tile id="114">
  <image width="320" height="320" source="Gus assets/crafts-doorright.png"/>
 </tile>
 <tile id="115">
  <image width="320" height="320" source="Gus assets/crafts-roof1.png"/>
 </tile>
 <tile id="116">
  <image width="320" height="320" source="Gus assets/crafts-roof2.png"/>
 </tile>
 <tile id="117">
  <image width="320" height="320" source="Gus assets/crafts-roof3.png"/>
 </tile>
 <tile id="118">
  <image width="320" height="320" source="Gus assets/crafts-sign1.png"/>
 </tile>
 <tile id="119">
  <image width="320" height="320" source="Gus assets/crafts-sign2.png"/>
 </tile>
 <tile id="120">
  <image width="320" height="320" source="Gus assets/crafts-sign3.png"/>
 </tile>
 <tile id="121">
  <image width="320" height="320" source="Gus assets/crafts-sign4.png"/>
 </tile>
 <tile id="122">
  <image width="320" height="320" source="Gus assets/crafts-wallleft.png"/>
 </tile>
 <tile id="123">
  <image width="320" height="320" source="Gus assets/crafts-wallright.png"/>
 </tile>
 <tile id="124">
  <image width="320" height="320" source="Gus assets/crafts-windowleft.png"/>
 </tile>
 <tile id="125">
  <image width="320" height="320" source="Gus assets/crafts-windowmid.png"/>
 </tile>
 <tile id="126">
  <image width="320" height="320" source="Gus assets/crafts-windowright.png"/>
 </tile>
 <tile id="127">
  <image width="320" height="320" source="Gus assets/front1-door.png"/>
 </tile>
 <tile id="128">
  <image width="320" height="320" source="Gus assets/front1-long-bottomleft.png"/>
 </tile>
 <tile id="129">
  <image width="320" height="320" source="Gus assets/front1-long-bottommid.png"/>
 </tile>
 <tile id="130">
  <image width="320" height="320" source="Gus assets/front1-long-bottomright.png"/>
 </tile>
 <tile id="131">
  <image width="320" height="320" source="Gus assets/front1-long-topleft.png"/>
 </tile>
 <tile id="132">
  <image width="320" height="320" source="Gus assets/front1-long-topmid.png"/>
 </tile>
 <tile id="133">
  <image width="320" height="320" source="Gus assets/front1-long-topright.png"/>
 </tile>
 <tile id="134">
  <image width="320" height="320" source="Gus assets/front1-roof1.png"/>
 </tile>
 <tile id="135">
  <image width="320" height="320" source="Gus assets/front1-roof2.png"/>
 </tile>
 <tile id="136">
  <image width="320" height="320" source="Gus assets/front1-roof3.png"/>
 </tile>
 <tile id="137">
  <image width="320" height="320" source="Gus assets/front1-wall-left.png"/>
 </tile>
 <tile id="138">
  <image width="320" height="320" source="Gus assets/front1-wall-mid.png"/>
 </tile>
 <tile id="139">
  <image width="320" height="320" source="Gus assets/front1-wall-right.png"/>
 </tile>
 <tile id="140">
  <image width="320" height="320" source="Gus assets/front1-window-left.png"/>
 </tile>
 <tile id="141">
  <image width="320" height="320" source="Gus assets/front1-window-mid.png"/>
 </tile>
 <tile id="142">
  <image width="320" height="320" source="Gus assets/front1-window-right.png"/>
 </tile>
 <tile id="143">
  <image width="320" height="320" source="Gus assets/front2-door.png"/>
 </tile>
 <tile id="144">
  <image width="320" height="320" source="Gus assets/front2-roof1.png"/>
 </tile>
 <tile id="145">
  <image width="320" height="320" source="Gus assets/front2-roof2.png"/>
 </tile>
 <tile id="146">
  <image width="320" height="320" source="Gus assets/front2-roof3.png"/>
 </tile>
 <tile id="147">
  <image width="320" height="320" source="Gus assets/front2-wall-left.png"/>
 </tile>
 <tile id="148">
  <image width="320" height="320" source="Gus assets/front2-wall-mid.png"/>
 </tile>
 <tile id="149">
  <image width="320" height="320" source="Gus assets/front2-wall-right.png"/>
 </tile>
 <tile id="150">
  <image width="320" height="320" source="Gus assets/front2-window-left.png"/>
 </tile>
 <tile id="151">
  <image width="320" height="320" source="Gus assets/front2-window-mid.png"/>
 </tile>
 <tile id="152">
  <image width="320" height="320" source="Gus assets/front2-window-right.png"/>
 </tile>
 <tile id="153">
  <image width="406" height="557" source="krish-assets/blue-building-1.png"/>
 </tile>
 <tile id="154">
  <image width="320" height="320" source="krish-assets/blue-building-1--01.png"/>
 </tile>
 <tile id="155">
  <image width="320" height="320" source="krish-assets/blue-building-1--02.png"/>
 </tile>
 <tile id="156">
  <image width="320" height="320" source="krish-assets/blue-building-1--03.png"/>
 </tile>
 <tile id="157">
  <image width="320" height="320" source="krish-assets/blue-building-1--04.png"/>
 </tile>
 <tile id="158">
  <image width="320" height="320" source="krish-assets/blue-building-1--05.png"/>
 </tile>
 <tile id="159">
  <image width="320" height="320" source="krish-assets/blue-building-1--06.png"/>
 </tile>
 <tile id="160">
  <image width="320" height="320" source="krish-assets/blue-building-1--07.png"/>
 </tile>
 <tile id="161">
  <image width="320" height="320" source="krish-assets/blue-building-1--08.png"/>
 </tile>
 <tile id="162">
  <image width="320" height="320" source="krish-assets/blue-building-1--09.png"/>
 </tile>
 <tile id="163">
  <image width="320" height="320" source="krish-assets/blue-building-1--10.png"/>
 </tile>
 <tile id="164">
  <image width="320" height="320" source="krish-assets/blue-building-1--11.png"/>
 </tile>
 <tile id="165">
  <image width="320" height="320" source="krish-assets/blue-building-1--12.png"/>
 </tile>
 <tile id="166">
  <image width="320" height="320" source="krish-assets/blue-building-1--13.png"/>
 </tile>
 <tile id="167">
  <image width="320" height="320" source="krish-assets/blue-building-1--14.png"/>
 </tile>
 <tile id="168">
  <image width="320" height="320" source="krish-assets/blue-building-1--15.png"/>
 </tile>
 <tile id="169">
  <image width="320" height="320" source="krish-assets/blue-building-1--16.png"/>
 </tile>
 <tile id="170">
  <image width="320" height="320" source="krish-assets/blue-building-1--17.png"/>
 </tile>
 <tile id="171">
  <image width="320" height="320" source="krish-assets/blue-building-1--18.png"/>
 </tile>
 <tile id="172">
  <image width="242" height="320" source="krish-assets/green-building-1.png"/>
 </tile>
 <tile id="173">
  <image width="320" height="320" source="krish-assets/green-building-1---01.png"/>
 </tile>
 <tile id="174">
  <image width="320" height="320" source="krish-assets/green-building-1---02.png"/>
 </tile>
 <tile id="175">
  <image width="320" height="320" source="krish-assets/green-building-1---03.png"/>
 </tile>
 <tile id="176">
  <image width="320" height="320" source="krish-assets/green-building-1---04.png"/>
 </tile>
 <tile id="177">
  <image width="320" height="320" source="krish-assets/green-building-1---05.png"/>
 </tile>
 <tile id="178">
  <image width="320" height="320" source="krish-assets/green-building-1---06.png"/>
 </tile>
 <tile id="179">
  <image width="320" height="320" source="krish-assets/green-building-1---07.png"/>
 </tile>
 <tile id="180">
  <image width="320" height="320" source="krish-assets/green-building-1---08.png"/>
 </tile>
 <tile id="181">
  <image width="320" height="320" source="krish-assets/green-building-1---09.png"/>
 </tile>
 <tile id="182">
  <image width="321" height="398" source="krish-assets/green-building-2.png"/>
 </tile>
 <tile id="183">
  <image width="320" height="320" source="krish-assets/green-building-2---01.png"/>
 </tile>
 <tile id="184">
  <image width="320" height="320" source="krish-assets/green-building-2---02.png"/>
 </tile>
 <tile id="185">
  <image width="320" height="320" source="krish-assets/green-building-2---03.png"/>
 </tile>
 <tile id="186">
  <image width="320" height="320" source="krish-assets/green-building-2---04.png"/>
 </tile>
 <tile id="187">
  <image width="320" height="320" source="krish-assets/green-building-2---05.png"/>
 </tile>
 <tile id="188">
  <image width="320" height="320" source="krish-assets/green-building-2---06.png"/>
 </tile>
 <tile id="189">
  <image width="320" height="320" source="krish-assets/green-building-2---07.png"/>
 </tile>
 <tile id="190">
  <image width="320" height="320" source="krish-assets/green-building-2---08.png"/>
 </tile>
 <tile id="191">
  <image width="320" height="320" source="krish-assets/green-building-2---09.png"/>
 </tile>
 <tile id="192">
  <image width="320" height="320" source="krish-assets/green-building-2---10.png"/>
 </tile>
 <tile id="193">
  <image width="320" height="320" source="krish-assets/green-building-2---11.png"/>
 </tile>
 <tile id="194">
  <image width="320" height="320" source="krish-assets/green-building-2---12.png"/>
 </tile>
 <tile id="195">
  <image width="320" height="320" source="krish-assets/green-building-2---13.png"/>
 </tile>
 <tile id="196">
  <image width="320" height="320" source="krish-assets/green-building-2---14.png"/>
 </tile>
 <tile id="197">
  <image width="320" height="320" source="krish-assets/green-building-2---15.png"/>
 </tile>
 <tile id="198">
  <image width="320" height="320" source="krish-assets/green-building-2---16.png"/>
 </tile>
 <tile id="199">
  <image width="320" height="320" source="krish-assets/green-building-2---17.png"/>
 </tile>
 <tile id="200">
  <image width="320" height="320" source="krish-assets/green-building-2---18.png"/>
 </tile>
 <tile id="201">
  <image width="941" height="2540" source="krish-assets/pink-building-1.jpg"/>
 </tile>
 <tile id="202">
  <image width="320" height="320" source="krish-assets/pink-building-1---01.png"/>
 </tile>
 <tile id="203">
  <image width="320" height="320" source="krish-assets/pink-building-1---02.png"/>
 </tile>
 <tile id="204">
  <image width="320" height="320" source="krish-assets/pink-building-1---03.png"/>
 </tile>
 <tile id="205">
  <image width="320" height="320" source="krish-assets/pink-building-1---04.png"/>
 </tile>
 <tile id="206">
  <image width="320" height="320" source="krish-assets/pink-building-1---05.png"/>
 </tile>
 <tile id="207">
  <image width="320" height="320" source="krish-assets/pink-building-1---06.png"/>
 </tile>
 <tile id="208">
  <image width="320" height="320" source="krish-assets/pink-building-1---07.png"/>
 </tile>
 <tile id="209">
  <image width="320" height="320" source="krish-assets/pink-building-1---08.png"/>
 </tile>
 <tile id="210">
  <image width="320" height="320" source="krish-assets/pink-building-1---09.png"/>
 </tile>
 <tile id="211">
  <image width="320" height="320" source="krish-assets/pink-building-1---10.png"/>
 </tile>
 <tile id="212">
  <image width="320" height="320" source="krish-assets/pink-building-1---11.png"/>
 </tile>
 <tile id="213">
  <image width="320" height="320" source="krish-assets/pink-building-1---12.png"/>
 </tile>
 <tile id="214">
  <image width="320" height="320" source="krish-assets/pink-building-1---13.png"/>
 </tile>
 <tile id="215">
  <image width="320" height="320" source="krish-assets/pink-building-1---14.png"/>
 </tile>
 <tile id="216">
  <image width="320" height="320" source="krish-assets/pink-building-1---15.png"/>
 </tile>
 <tile id="217">
  <image width="320" height="320" source="krish-assets/pink-building-1---16.png"/>
 </tile>
 <tile id="218">
  <image width="320" height="320" source="krish-assets/pink-building-1---17.png"/>
 </tile>
 <tile id="219">
  <image width="320" height="320" source="krish-assets/pink-building-1---18.png"/>
 </tile>
 <tile id="220">
  <image width="167" height="406" source="krish-assets/pink-building-2.png"/>
 </tile>
 <tile id="223">
  <image width="320" height="320" source="krish-assets/pink-building-2---03.png"/>
 </tile>
 <tile id="224">
  <image width="320" height="320" source="krish-assets/pink-building-2---04.png"/>
 </tile>
 <tile id="225">
  <image width="320" height="320" source="krish-assets/pink-building-2---05.png"/>
 </tile>
 <tile id="226">
  <image width="320" height="320" source="krish-assets/pink-building-2---06.png"/>
 </tile>
 <tile id="227">
  <image width="320" height="320" source="krish-assets/pink-building-2---07.png"/>
 </tile>
 <tile id="228">
  <image width="320" height="320" source="krish-assets/pink-building-2---08.png"/>
 </tile>
 <tile id="229">
  <image width="3840" height="3840" source="krish-assets/pixel_city_scape-upscaled.png"/>
 </tile>
 <tile id="230">
  <image width="243" height="400" source="krish-assets/red-building-1.png"/>
 </tile>
 <tile id="231">
  <image width="320" height="320" source="krish-assets/red-building-1--01.png"/>
 </tile>
 <tile id="232">
  <image width="320" height="320" source="krish-assets/red-building-1--02.png"/>
 </tile>
 <tile id="233">
  <image width="320" height="320" source="krish-assets/red-building-1--03.png"/>
 </tile>
 <tile id="234">
  <image width="320" height="320" source="krish-assets/red-building-1--04.png"/>
 </tile>
 <tile id="235">
  <image width="320" height="320" source="krish-assets/red-building-1--05.png"/>
 </tile>
 <tile id="236">
  <image width="320" height="320" source="krish-assets/red-building-1--06.png"/>
 </tile>
 <tile id="237">
  <image width="321" height="410" source="krish-assets/red-building-2.png"/>
 </tile>
 <tile id="238">
  <image width="320" height="320" source="krish-assets/red-building-2--01.png"/>
 </tile>
 <tile id="239">
  <image width="320" height="320" source="krish-assets/red-building-2--02.png"/>
 </tile>
 <tile id="240">
  <image width="320" height="320" source="krish-assets/red-building-2--03.png"/>
 </tile>
 <tile id="241">
  <image width="320" height="320" source="krish-assets/red-building-2--04.png"/>
 </tile>
 <tile id="242">
  <image width="320" height="320" source="krish-assets/red-building-2--05.png"/>
 </tile>
 <tile id="243">
  <image width="320" height="320" source="krish-assets/red-building-2--06.png"/>
 </tile>
 <tile id="244">
  <image width="320" height="320" source="krish-assets/red-building-2--07.png"/>
 </tile>
 <tile id="245">
  <image width="320" height="320" source="krish-assets/red-building-2--08.png"/>
 </tile>
 <tile id="246">
  <image width="320" height="320" source="krish-assets/red-building-2--09.png"/>
 </tile>
 <tile id="247">
  <image width="320" height="320" source="krish-assets/red-building-2--10.png"/>
 </tile>
 <tile id="248">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-bottom-left.png"/>
 </tile>
 <tile id="249">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-bottom-mid.png"/>
 </tile>
 <tile id="250">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-bottom-right.png"/>
 </tile>
 <tile id="251">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-left-top.png"/>
 </tile>
 <tile id="252">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-top-mid.png"/>
 </tile>
 <tile id="253">
  <image width="320" height="320" source="Mark assets/blue-roof-angled-top-right.png"/>
 </tile>
 <tile id="254">
  <image width="320" height="320" source="Mark assets/blue-roof-left.png"/>
 </tile>
 <tile id="255">
  <image width="320" height="320" source="Mark assets/blue-roof-middle.png"/>
 </tile>
 <tile id="256">
  <image width="320" height="320" source="Mark assets/blue-roof-right.png"/>
 </tile>
 <tile id="257">
  <image width="320" height="320" source="Mark assets/blue-window-larg-left.png"/>
 </tile>
 <tile id="258">
  <image width="320" height="320" source="Mark assets/blue-window-larg-middle.png"/>
 </tile>
 <tile id="259">
  <image width="320" height="320" source="Mark assets/blue-window-larg-right.png"/>
 </tile>
 <tile id="260">
  <image width="320" height="320" source="Mark assets/blue-window-medium.png"/>
 </tile>
 <tile id="261">
  <image width="320" height="320" source="Mark assets/blue-window-small.png"/>
 </tile>
 <tile id="262">
  <image width="320" height="320" source="Mark assets/circle-base-left.png"/>
 </tile>
 <tile id="263">
  <image width="320" height="320" source="Mark assets/circle-base-middle.png"/>
 </tile>
 <tile id="264">
  <image width="320" height="320" source="Mark assets/circle-base-right.png"/>
 </tile>
 <tile id="265">
  <image width="320" height="320" source="Mark assets/circle-roof-left.png"/>
 </tile>
 <tile id="266">
  <image width="320" height="320" source="Mark assets/circle-roof-middle.png"/>
 </tile>
 <tile id="267">
  <image width="320" height="320" source="Mark assets/circle-roof-rightpng.png"/>
 </tile>
 <tile id="268">
  <image width="320" height="320" source="Mark assets/circle-window-left.png"/>
 </tile>
 <tile id="269">
  <image width="320" height="320" source="Mark assets/circle-window-middle.png"/>
 </tile>
 <tile id="270">
  <image width="320" height="320" source="Mark assets/circle-window-right.png"/>
 </tile>
 <tile id="271">
  <image width="320" height="320" source="Mark assets/diamond-bottom-left.png"/>
 </tile>
 <tile id="272">
  <image width="320" height="320" source="Mark assets/diamond-bottom-right.png"/>
 </tile>
 <tile id="273">
  <image width="320" height="320" source="Mark assets/diamond-top-left.png"/>
 </tile>
 <tile id="274">
  <image width="320" height="320" source="Mark assets/diamond-top-right.png"/>
 </tile>
 <tile id="275">
  <image width="320" height="320" source="Mark assets/door-bottom-left.png"/>
 </tile>
 <tile id="276">
  <image width="320" height="320" source="Mark assets/door-bottom-right.png"/>
 </tile>
 <tile id="277">
  <image width="320" height="320" source="Mark assets/door-top-left.png"/>
 </tile>
 <tile id="278">
  <image width="320" height="320" source="Mark assets/door-top-right.png"/>
 </tile>
 <tile id="279">
  <image width="320" height="320" source="Mark assets/EM.png"/>
 </tile>
 <tile id="280">
  <image width="320" height="320" source="Mark assets/grey-door.png"/>
 </tile>
 <tile id="281">
  <image width="320" height="320" source="Mark assets/grey-roof-left.png"/>
 </tile>
 <tile id="282">
  <image width="320" height="320" source="Mark assets/grey-roof-middle.png"/>
 </tile>
 <tile id="283">
  <image width="320" height="320" source="Mark assets/grey-roof-right.png"/>
 </tile>
 <tile id="284">
  <image width="320" height="320" source="Mark assets/grey-wall.png"/>
 </tile>
 <tile id="285">
  <image width="320" height="320" source="Mark assets/grey-window.png"/>
 </tile>
 <tile id="286">
  <image width="320" height="320" source="Mark assets/insignia-bottom-left.png"/>
 </tile>
 <tile id="287">
  <image width="320" height="320" source="Mark assets/insignia-bottom-right.png"/>
 </tile>
 <tile id="288">
  <image width="320" height="320" source="Mark assets/insignia-top-left.png"/>
 </tile>
 <tile id="289">
  <image width="320" height="320" source="Mark assets/insignia-top-right.png"/>
 </tile>
 <tile id="290">
  <image width="320" height="320" source="Mark assets/IT.png"/>
 </tile>
 <tile id="291">
  <image width="320" height="320" source="Mark assets/ministry-wall.png"/>
 </tile>
 <tile id="292">
  <image width="320" height="320" source="Mark assets/ministry-window.png"/>
 </tile>
 <tile id="293">
  <image width="320" height="320" source="Mark assets/ministry-window-NE.png"/>
 </tile>
 <tile id="294">
  <image width="320" height="320" source="Mark assets/ministry-window-NW.png"/>
 </tile>
 <tile id="295">
  <image width="320" height="320" source="Mark assets/ministry-window-SE.png"/>
 </tile>
 <tile id="296">
  <image width="320" height="320" source="Mark assets/ministry-window-SW.png"/>
 </tile>
 <tile id="297">
  <image width="320" height="320" source="Mark assets/MS.png"/>
 </tile>
 <tile id="298">
  <image width="320" height="320" source="Mark assets/purple window-1.png"/>
 </tile>
 <tile id="299">
  <image width="320" height="320" source="Mark assets/purple-door.png"/>
 </tile>
 <tile id="300">
  <image width="320" height="320" source="Mark assets/purple-door-left.png"/>
 </tile>
 <tile id="301">
  <image width="320" height="320" source="Mark assets/purple-door-right.png"/>
 </tile>
 <tile id="302">
  <image width="320" height="320" source="Mark assets/purple-roof-left.png"/>
 </tile>
 <tile id="303">
  <image width="320" height="320" source="Mark assets/purple-roof-middle.png"/>
 </tile>
 <tile id="304">
  <image width="320" height="320" source="Mark assets/purple-roof-right.png"/>
 </tile>
 <tile id="305">
  <image width="320" height="320" source="Mark assets/purple-window-2.png"/>
 </tile>
 <tile id="306">
  <image width="320" height="320" source="Mark assets/purple-window-3.png"/>
 </tile>
 <tile id="307">
  <image width="320" height="320" source="Mark assets/red-roof-angled-bottom.png"/>
 </tile>
 <tile id="308">
  <image width="320" height="320" source="Mark assets/red-roof-angled-top.png"/>
 </tile>
 <tile id="309">
  <image width="320" height="320" source="Mark assets/red-roof-left.png"/>
 </tile>
 <tile id="310">
  <image width="320" height="320" source="Mark assets/red-roof-right.png"/>
 </tile>
 <tile id="311">
  <image width="320" height="320" source="Mark assets/red-window.png"/>
 </tile>
 <tile id="312">
  <image width="320" height="320" source="Mark assets/red-window-left.png"/>
 </tile>
 <tile id="313">
  <image width="320" height="320" source="Mark assets/red-window-right.png"/>
 </tile>
 <tile id="314">
  <image width="320" height="320" source="Mark assets/roof.png"/>
 </tile>
 <tile id="315">
  <image width="320" height="320" source="Mark assets/roof-bottom.png"/>
 </tile>
 <tile id="316">
  <image width="320" height="320" source="Mark assets/roof-bottom-left.png"/>
 </tile>
 <tile id="317">
  <image width="320" height="320" source="Mark assets/roof-bottom-right.png"/>
 </tile>
 <tile id="318">
  <image width="320" height="320" source="Mark assets/roof-left.png"/>
 </tile>
 <tile id="319">
  <image width="320" height="320" source="Mark assets/roof-right.png"/>
 </tile>
 <tile id="320">
  <image width="320" height="320" source="Mark assets/roof-top.png"/>
 </tile>
 <tile id="321">
  <image width="320" height="320" source="Mark assets/roof-top-left.png"/>
 </tile>
 <tile id="322">
  <image width="320" height="320" source="Mark assets/roof-top-right.png"/>
 </tile>
 <tile id="323">
  <image width="320" height="320" source="Mark assets/small-door-left.png"/>
 </tile>
 <tile id="324">
  <image width="320" height="320" source="Mark assets/small-door-right.png"/>
 </tile>
 <tile id="325">
  <image width="320" height="320" source="Mark assets/small-roof.png"/>
 </tile>
 <tile id="326">
  <image width="320" height="320" source="Mark assets/small-roof-bottom-left.png"/>
 </tile>
 <tile id="327">
  <image width="320" height="320" source="Mark assets/small-roof-bottom-right.png"/>
 </tile>
 <tile id="328">
  <image width="320" height="320" source="Mark assets/small-roof-top-left.png"/>
 </tile>
 <tile id="329">
  <image width="320" height="320" source="Mark assets/small-roof-top-right.png"/>
 </tile>
 <tile id="330">
  <image width="320" height="320" source="Mark assets/blue-window-angled.png"/>
 </tile>
 <tile id="332">
  <image width="320" height="320" source="Mark assets/purple-wall.png"/>
 </tile>
 <tile id="333">
  <image width="320" height="320" source="Mark assets/purple-dark-wall.png"/>
 </tile>
 <tile id="334">
  <image width="320" height="320" source="krish-assets/green-building-2---19.png"/>
 </tile>
 <tile id="335">
  <image width="320" height="320" source="krish-assets/pink-building-2---01.png"/>
 </tile>
 <tile id="336">
  <image width="320" height="320" source="krish-assets/pink-building-2---02.png"/>
 </tile>
</tileset>
