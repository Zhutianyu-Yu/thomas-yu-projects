<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="volcano dungeon" tilewidth="320" tileheight="640" tilecount="35" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="320" height="320" source="volcano_dungeon_normal.png"/>
 </tile>
 <tile id="2">
  <image width="320" height="320" source="volcano_dungeon_pillar2.png"/>
 </tile>
 <tile id="3">
  <image width="320" height="320" source="volcano_dungeon_Rockwall.png"/>
 </tile>
 <tile id="4">
  <image width="320" height="320" source="volcano_dungeon_pillar1.png"/>
 </tile>
 <tile id="5">
  <image width="320" height="320" source="volcano_lava_large.png"/>
 </tile>
 <tile id="6">
  <image width="320" height="320" source="volcano_dungeon_button_off.png"/>
 </tile>
 <tile id="7">
  <image width="320" height="320" source="volcano_dungeon_button_on.png"/>
 </tile>
 <tile id="8">
  <image width="320" height="320" source="volcano_dungeon_entrance_left.png"/>
 </tile>
 <tile id="9">
  <image width="320" height="320" source="volcano_dungeon_entrance_right.png"/>
 </tile>
 <tile id="13">
  <image width="320" height="320" source="bridge_horizontal.png"/>
 </tile>
 <tile id="14">
  <image width="320" height="320" source="bridge_vertical.png"/>
 </tile>
 <tile id="15">
  <image width="320" height="320" source="volcano_dungeon_button_off_transparent.png"/>
 </tile>
 <tile id="16">
  <image width="320" height="320" source="volcano_dungeon_button_on_transparent.png"/>
 </tile>
 <tile id="17">
  <image width="320" height="640" source="volcano_dungeon_pillar_640x320.png"/>
 </tile>
 <tile id="18">
  <image width="320" height="320" source="health_pool_bLeft.png"/>
 </tile>
 <tile id="19">
  <image width="320" height="320" source="health_pool_bMid.png"/>
 </tile>
 <tile id="20">
  <image width="320" height="320" source="health_pool_bRight.png"/>
 </tile>
 <tile id="21">
  <image width="320" height="320" source="health_pool_mLeft.png"/>
 </tile>
 <tile id="22">
  <image width="320" height="320" source="health_pool_mMid.png"/>
 </tile>
 <tile id="23">
  <image width="320" height="320" source="health_pool_mRight.png"/>
 </tile>
 <tile id="24">
  <image width="320" height="320" source="health_pool_tLeft.png"/>
 </tile>
 <tile id="25">
  <image width="320" height="320" source="health_pool_tMid.png"/>
 </tile>
 <tile id="26">
  <image width="320" height="320" source="health_pool_tRight.png"/>
 </tile>
 <tile id="27">
  <image width="320" height="320" source="mana_pool_bLeft.png"/>
 </tile>
 <tile id="28">
  <image width="320" height="320" source="mana_pool_bMid.png"/>
 </tile>
 <tile id="29">
  <image width="320" height="320" source="mana_pool_bRight.png"/>
 </tile>
 <tile id="30">
  <image width="320" height="320" source="mana_pool_mLeft.png"/>
 </tile>
 <tile id="31">
  <image width="320" height="320" source="mana_pool_mMid.png"/>
 </tile>
 <tile id="32">
  <image width="320" height="320" source="mana_pool_mRight.png"/>
 </tile>
 <tile id="33">
  <image width="320" height="320" source="mana_pool_tLeft.png"/>
 </tile>
 <tile id="34">
  <image width="320" height="320" source="mana_pool_tMid.png"/>
 </tile>
 <tile id="35">
  <image width="320" height="320" source="mana_pool_tRight.png"/>
 </tile>
 <tile id="36">
  <image width="320" height="320" source="wall_rock_light.png"/>
 </tile>
 <tile id="37">
  <image width="320" height="320" source="volcano-console-off.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="320" height="320"/>
  </objectgroup>
 </tile>
 <tile id="38">
  <image width="320" height="320" source="volcano-console-on.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="320" height="320"/>
  </objectgroup>
 </tile>
</tileset>
