<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Hubworld-dungeon-tiles" tilewidth="400" tileheight="640" tilecount="14" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="14">
  <image width="400" height="320" source="option1_building_1.png"/>
 </tile>
 <tile id="15">
  <image width="290" height="426" source="option1_building_2.png"/>
 </tile>
 <tile id="16">
  <image width="320" height="320" source="option1_tile_1.png"/>
 </tile>
 <tile id="17">
  <image width="320" height="320" source="option1_tile_2.png"/>
 </tile>
 <tile id="18">
  <image width="320" height="320" source="option1_tile_3.png"/>
 </tile>
 <tile id="19">
  <image width="320" height="320" source="option1_tile_4.png"/>
 </tile>
 <tile id="20">
  <image width="320" height="640" source="option2_building-1.png"/>
 </tile>
 <tile id="21">
  <image width="320" height="533" source="option2_building-2.png"/>
 </tile>
 <tile id="22">
  <image width="320" height="320" source="option2_tile_1.png"/>
 </tile>
 <tile id="23">
  <image width="320" height="320" source="option2_tile_2.png"/>
 </tile>
 <tile id="24">
  <image width="320" height="320" source="option2_tile_3.png"/>
 </tile>
 <tile id="25">
  <image width="320" height="320" source="option2_tile_4.png"/>
 </tile>
 <tile id="26">
  <image width="320" height="320" source="wall_1.png"/>
 </tile>
 <tile id="27">
  <image width="320" height="320" source="wall_2.png"/>
 </tile>
</tileset>
