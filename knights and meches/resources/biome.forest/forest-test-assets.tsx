<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="forest-test-assets" tilewidth="320" tileheight="320" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="320" height="320" source="blue-tile.png"/>
 </tile>
 <tile id="1">
  <image width="320" height="320" source="red-tile.png"/>
 </tile>
</tileset>
