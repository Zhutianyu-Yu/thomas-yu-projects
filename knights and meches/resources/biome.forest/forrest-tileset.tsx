<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="forrest-tileset" tilewidth="320" tileheight="640" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="320" height="320" source="dungeon-tower-tiles.png"/>
 </tile>
 <tile id="1">
  <image width="320" height="320" source="floor-tile-1.png"/>
 </tile>
 <tile id="2">
  <image width="320" height="320" source="floor-tile-2.png"/>
 </tile>
 <tile id="3">
  <image width="320" height="320" source="leaves-tile.png"/>
 </tile>
 <tile id="4">
  <image width="320" height="320" source="wall-tile.png"/>
 </tile>
 <tile id="5">
  <image width="320" height="320" source="bush-tile.png"/>
 </tile>
 <tile id="6">
  <image width="320" height="320" source="dungeon-tile-1.png"/>
 </tile>
 <tile id="7">
  <image width="320" height="320" source="dungeon-tile-2.png"/>
 </tile>
</tileset>
